package com.tim2.travelservice.integration.schedule;

import com.tim2.travelservice.entity.ScheduleItem;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.PASSENGER_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class GetScheduleByCityLineIdAndScheduleTypeTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnOkAndScheduleWithGivenCityLineId() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.getScheduleByCityLineIdAndScheduleType(testRestTemplate, PASSENGER_HEADER, FIRST_CITY_LINE_ID, SCHEDULE_TYPE);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedScheduleIsReturned(responseEntity);
    }

    private void andExpectedScheduleIsReturned(ResponseEntity responseEntity) throws IOException {
        ScheduleDto scheduleDto = objectMapper.readValue(responseEntity.getBody().toString(), ScheduleDto.class);
        List<ScheduleItem> scheduleItems = dbUtil.findScheduleItemsByScheduleId(scheduleDto.getId());

        assertThat("City line id is correct.", scheduleDto.getCityLine().getId(), is(FIRST_CITY_LINE_ID));
        assertThat("Schedule id is correct.", scheduleDto.getId(), is(SCHEDULE_ID));
        assertThat("Schedule activity is correct.", scheduleDto.getActive(), is(SCHEDULE_ACTIVE));
        assertThat("Schedule type is correct.", scheduleDto.getScheduleType(), is(SCHEDULE_TYPE));
        assertThat("Schedule valid from is correct.", scheduleDto.getValidFrom(), is(SCHEDULE_VALID_FROM));
        assertNull("Schedule valid until is correct.", scheduleDto.getValidUntil());
    }
}
