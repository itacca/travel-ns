package com.tim2.travelservice.integration.account;

import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.AccountDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.GUEST_HTML_HEADER;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class GetCurrentUserTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.getLoggedUser(testRestTemplate, GUEST_HTML_HEADER);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnOkAndLoggedUserInfoWhenValidToken() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.getLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedAccountIsReturned(responseEntity);
    }

    private void andExpectedAccountIsReturned(ResponseEntity responseEntity) throws IOException {
        AccountDto accountDto = objectMapper.readValue(responseEntity.getBody().toString(), AccountDto.class);

        assertThat("Account Id", accountDto.getId(), is(SYSTEM_ADMINISTRATOR_ID));
        assertThat("Account email", accountDto.getEmail(), is(SYSTEM_ADMINISTRATOR_EMAIL));
        assertThat("First name", accountDto.getFirstName(), is(SYSTEM_ADMINISTRATOR_FIRST_NAME));
        assertThat("Last name", accountDto.getLastName(), is(SYSTEM_ADMINISTRATOR_LAST_NAME));
        assertThat("Password", accountDto.getPassword(), isEmptyOrNullString());
    }
}
