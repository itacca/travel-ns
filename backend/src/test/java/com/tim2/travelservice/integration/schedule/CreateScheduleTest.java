package com.tim2.travelservice.integration.schedule;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.ScheduleDataBuilder;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleItem;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.common.api.RestApiErrors.fieldShouldNotBeNull;
import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.util.AssertionErrors.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class CreateScheduleTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE, CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, GUEST_HTML_HEADER, scheduleDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE, CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, VERIFIER_HEADER, scheduleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE, CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, PASSENGER_HEADER, scheduleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenScheduleIdIsNotNull() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE, CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenScheduleTypeIsNull() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(null, null, CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, fieldShouldNotBeNull(RestApiConstants.SCHEDULE_TYPE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIsNull() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(null, SCHEDULE_TYPE_UPDATED, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIdIsNull() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(null, SCHEDULE_TYPE_UPDATED, CityLineDataBuilder.buildCityLineDto(null, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndCreateScheduleWithNoItems() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(null, SCHEDULE_TYPE_UPDATED, CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long scheduleId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
        andNewScheduleWithNoItemsIsCreated(scheduleId);
    }

    private void andNewScheduleWithNoItemsIsCreated(Long id) {
        Schedule schedule = andNewScheduleIsCreated(id);
        List<ScheduleItem> scheduleItems = dbUtil.findScheduleItemsByScheduleId(schedule.getId());
        assertTrue("No schedule items.", scheduleItems.isEmpty());
    }

    private Schedule andNewScheduleIsCreated(Long id) {
        Schedule schedule = dbUtil.findScheduleById(id);

        assertThat("City line id is correct.", schedule.getCityLine().getId(), is(FIRST_CITY_LINE_ID));
        assertThat("Schedule activity is correct.", schedule.getActive(), is(NEW_SCHEDULE_ACTIVE));
        assertThat("Schedule type is correct.", schedule.getScheduleType(), is(SCHEDULE_TYPE_UPDATED));

        return schedule;
    }
}
