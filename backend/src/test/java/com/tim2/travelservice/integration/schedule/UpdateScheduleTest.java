package com.tim2.travelservice.integration.schedule;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.ScheduleDataBuilder;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdateScheduleTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateSchedule(testRestTemplate, GUEST_HTML_HEADER, scheduleDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateSchedule(testRestTemplate, VERIFIER_HEADER, scheduleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateSchedule(testRestTemplate, PASSENGER_HEADER, scheduleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenNoIdIsProvided() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(null, SCHEDULE_TYPE);

        ResponseEntity responseEntity = ApiFunctions.updateSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenScheduleTypeIsNotProvided() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, null);

        ResponseEntity responseEntity = ApiFunctions.updateSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.SCHEDULE_TYPE));
    }

    @Test
    public void shouldReturnOkAndUpdateSchedulePartially() {
        ScheduleDto scheduleDto = ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateSchedule(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andScheduleIsUpdated();
    }

    private void andScheduleIsUpdated() {
        Schedule schedule = dbUtil.findScheduleById(SCHEDULE_ID);

        assertThat("Schedule id is correct.", schedule.getId(), is(SCHEDULE_ID));
        assertThat("Schedule active is correct.", schedule.getActive(), is(SCHEDULE_ACTIVE));
        assertThat("Schedule type is correct.", schedule.getScheduleType(), is(SCHEDULE_TYPE_UPDATED));
        assertThat("Schedule valid from is correct.", schedule.getValidFrom(), is(SCHEDULE_VALID_FROM));
    }
}
