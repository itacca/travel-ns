package com.tim2.travelservice.integration.vehicle;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.VehicleDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.entity.VehicleType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdateVehicleTest {


    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, GUEST_HTML_HEADER, vehicleDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, VERIFIER_HEADER, vehicleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, PASSENGER_HEADER, vehicleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsNull() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenPlateNumberIsEmptyString() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, "", VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.PLATE_NUMBER));
    }

    @Test
    public void shouldReturnBadRequestWhenUpdatingWithExistingPlateNumber() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER_EXISTING, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.VEHICLE, RestApiConstants.PLATE_NUMBER));
    }


    @Test
    public void shouldReturnNotFoundWhenUpdatingNonExistingVehicle() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(555L, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentAndUpdateVehicle() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER_UPDATED, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity responseEntity = ApiFunctions.updateVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        Vehicle vehicle = dbUtil.findVehicleById(VEHICLE_ID);
        assertThat("Plate number is updated.", vehicle.getPlateNumber(), is(VEHICLE_PLATE_NUMBER_UPDATED));
    }
}
