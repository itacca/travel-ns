package com.tim2.travelservice.integration.scheduleitem;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.entity.ScheduleItem;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.tim2.travelservice.data.TestData.SCHEDULE_ITEM_ID;
import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeleteScheduleItemTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.deleteScheduleItemById(testRestTemplate, GUEST_HTML_HEADER, SCHEDULE_ITEM_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.deleteScheduleItemById(testRestTemplate, VERIFIER_HEADER, SCHEDULE_ITEM_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.deleteScheduleItemById(testRestTemplate, PASSENGER_HEADER, SCHEDULE_ITEM_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingScheduleItem() {
        ResponseEntity responseEntity = ApiFunctions.deleteScheduleItemById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.SCHEDULE_ITEM, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentAndDeleteScheduleItem() {
        ResponseEntity responseEntity = ApiFunctions.deleteScheduleItemById(testRestTemplate, SYSTEM_ADMIN_HEADER, SCHEDULE_ITEM_ID);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andScheduleItemIsDeleted(SCHEDULE_ITEM_ID);
    }

    private void andScheduleItemIsDeleted(Long scheduleItemId) {
        Optional<ScheduleItem> scheduleItem = dbUtil.findScheduleItemOptionalById(scheduleItemId);

        assertFalse("Schedule item is deleted.", scheduleItem.isPresent());
    }
}
