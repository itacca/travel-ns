package com.tim2.travelservice.integration.station;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.StationDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.GUEST_JSON_HEADER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindStationByIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnNotFoundWhenGettingNonExistingStation() {
        ResponseEntity responseEntity = ApiFunctions.findStationById(testRestTemplate, GUEST_JSON_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndStation() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.findStationById(testRestTemplate, GUEST_JSON_HEADER, STATION_ID);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedStationIsReturned(responseEntity);
    }

    private void andExpectedStationIsReturned(ResponseEntity responseEntity) throws IOException {
        StationDto stationDto = objectMapper.readValue(responseEntity.getBody().toString(), StationDto.class);

        assertThat("Id is correct.", stationDto.getId(), is(STATION_ID));
        assertThat("Name is correct.", stationDto.getName(), is(STATION_NAME));
        assertThat("Longitude is correct.", stationDto.getLongitude(), is(STATION_LONGITUDE));
        assertThat("Latitude is correct.", stationDto.getLatitude(), is(STATION_LATITUDE));
    }
}
