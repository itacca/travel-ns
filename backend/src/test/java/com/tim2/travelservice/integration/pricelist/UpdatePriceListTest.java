package com.tim2.travelservice.integration.pricelist;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TestData;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdatePriceListTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_OLD_START_DATE, TestData.PRICE_LIST_END_DATE_NEW);

        ResponseEntity responseEntity = ApiFunctions.updatePriceList(testRestTemplate, GUEST_HTML_HEADER, priceListDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_OLD_START_DATE, TestData.PRICE_LIST_END_DATE_NEW);

        ResponseEntity responseEntity = ApiFunctions.updatePriceList(testRestTemplate, PASSENGER_HEADER, priceListDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_OLD_START_DATE, TestData.PRICE_LIST_END_DATE_NEW);

        ResponseEntity responseEntity = ApiFunctions.updatePriceList(testRestTemplate, VERIFIER_HEADER, priceListDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestIfEndDateIsInThePast() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(2L, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_PAST_DATE);

        ResponseEntity responseEntity = ApiFunctions.updatePriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE);
    }

    @Test
    public void shouldReturnBadRequestIfIntervalIsInvalid() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(2L, TestData.PRICE_LIST_END_DATE, TestData.PRICE_LIST_START_DATE);

        ResponseEntity responseEntity = ApiFunctions.updatePriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.INVALID_DATE_INTERVAL);
    }

    @Test
    public void shouldReturnBadRequestIfIdIsNull() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(null, TestData.PRICE_LIST_OLD_START_DATE, TestData.PRICE_LIST_END_DATE_NEW);

        ResponseEntity responseEntity = ApiFunctions.updatePriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentIfPriceListIsSuccessfullyUpdated() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_OLD_START_DATE, TestData.PRICE_LIST_END_DATE_NEW);

        ResponseEntity responseEntity = ApiFunctions.updatePriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andPriceListIsUpdated(priceListDto.getId(), priceListDto.getEndDate());
    }

    private void andPriceListIsUpdated(Long priceListId, DateTime updatedEndDate) {
        PriceList priceList = dbUtil.findOnePriceList(priceListId);

        if (updatedEndDate != null) {
            assertFalse("End date is updated.", priceList.getEndDate().equals(DateTime.parse(TestData.PRICE_LIST_OLD_END_DATE)));
        }
    }
}
