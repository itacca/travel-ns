package com.tim2.travelservice.integration.cityline;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.*;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class CreateCityLineTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, GUEST_HTML_HEADER, cityLineDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, VERIFIER_HEADER, cityLineDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, PASSENGER_HEADER, cityLineDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsNotNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenNameIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, null, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenNameIsEmptyString() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, "", FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenVehiclesAreNotEmptyOrNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.singletonList(new VehicleDto()), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.VEHICLES));
    }

    @Test
    public void shouldReturnBadRequestWhenSchedulesAreNotEmptyOrNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.singletonList(new ScheduleDto()), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.SCHEDULES));
    }

    @Test
    public void shouldReturnBadRequestWhenLineStationsAreNotEmptyOrNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.singletonList(new LineStationDto()));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.LINE_STATIONS));
    }

    @Test
    @Transactional
    public void shouldReturnCreatedAndCityLineId() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.createCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long cityLineId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
        andCityLineIsStoredWithGivenData(cityLineId);
    }

    private void andCityLineIsStoredWithGivenData(Long id) {
        CityLine cityLine = dbUtil.findCityLineById(id);

        assertThat("Id is correct.", cityLine.getId(), is(id));
        assertThat("Name is correct.", cityLine.getName(), is(CITY_LINE_TRAM_NAME1));
        assertThat("Type is correct.", cityLine.getCityLineType(), is(FIRST_CITY_LINE_TRAM_TYPE));
        assertTrue("Vehicles are empty.", CollectionUtils.isEmpty(cityLine.getVehicles()));
        assertTrue("Schedules are empty.", CollectionUtils.isEmpty(cityLine.getSchedules()));
        assertTrue("Line stations are empty.", CollectionUtils.isEmpty(cityLine.getLineStations()));
    }
}
