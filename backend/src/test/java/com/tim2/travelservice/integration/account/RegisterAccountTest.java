package com.tim2.travelservice.integration.account;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.ReturnCode;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Role;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class RegisterAccountTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    public void shouldReturnBadRequestWhenAccountIdIsProvided() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_REGISTRATION_RANDOM_ID, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenEmailIsNotProvided() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, null, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL));
    }

    @Test
    public void shouldReturnBadRequestWhenEmailIsEmptyString() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, "", ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL));
    }

    @Test
    public void shouldReturnBadRequestWhenInvalidEmailFormat() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_INVALID_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL));
    }

    @Test
    public void shouldReturnBadRequestWhenPasswordIsNotProvided() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, null, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD));
    }

    @Test
    public void shouldReturnBadRequestWhenPasswordIsLessThanEightCharacters() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_SHORT_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH));
    }

    @Test
    public void shouldReturnBadRequestWhenFirstNameIsNull() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, null, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenFirstNameIsEmptyString() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, "", ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenLastNameIsNull() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, null, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenLastNameIsEmptyString() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, "", ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenDateOfBirthIsNull() {
        DynamicResponse accountDto = AccountDataBuilder.buildAccountDtoJson(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, null, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DATE_OF_BIRTH));
    }

    @Test
    public void shouldReturnBadRequestWhenDateOfBirthHasInvalidFormat() {
        DynamicResponse accountDto = AccountDataBuilder.buildAccountDtoJson(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_INVALID_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, ReturnCode.INVALID_JSON);
    }

    @Test
    public void shouldReturnBadRequestWhenDateOfBirthIsInTheFuture() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_FUTURE_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST);
    }

    @Test
    public void shouldReturnBadRequestWhenValidatedIsProvided() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, false, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.VALIDATED));
    }

    @Test
    public void shouldReturnBadRequestWhenIdValidatedIsProvided() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, false, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID_VALIDATED));
    }

    @Test
    public void shouldReturnBadRequestWhenRolesIsNotEmptyCollection() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, Arrays.asList(new Role(1L)));

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES));
    }

    @Test
    public void shouldReturnBadRequestWhenAccountWithGivenEmailAlreadyExists() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, SYSTEM_ADMINISTRATOR_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL));
    }

    @Test
    public void shouldReturnCreatedAndAccountIdAfterSuccessfulRegistration() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.registerAccount(testRestTemplate, accountDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long registeredAccountId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
        andAccountIsStoredWithGivenData(registeredAccountId);
    }

    private void andAccountIsStoredWithGivenData(Long registeredAccountId) {
        Account account = dbUtil.findAccountById(registeredAccountId);

        assertThat("Email is correct.", account.getEmail(), is(ACCOUNT_REGISTRATION_EMAIL));
        assertTrue("Password is correct.", bCryptPasswordEncoder.matches(ACCOUNT_REGISTRATION_PASSWORD, account.getPassword()));
        assertThat("First name is correct.", account.getFirstName(), is(ACCOUNT_REGISTRATION_FIRST_NAME));
        assertThat("Last name is correct.", account.getLastName(), is(ACCOUNT_REGISTRATION_LAST_NAME));
        assertThat("Date of birth is correct.", account.getDateOfBirth(), is(DateTime.parse(ACCOUNT_REGISTRATION_DATE_OF_BIRTH)));
        assertTrue("Validated is correct.", account.getValidated());
        assertFalse("idValidated is correct.", account.getIdValidated());
        assertThat("Default role is added.", account.getRoles().get(0).getLabel(), is(DEFAULT_ACCOUNT_ROLE_LABEL));
    }
}
