package com.tim2.travelservice.integration.cityline;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdateCityLineTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, GUEST_HTML_HEADER, cityLineDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, VERIFIER_HEADER, cityLineDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, PASSENGER_HEADER, cityLineDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenVehiclesAreNotNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, Collections.emptyList(), null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.VEHICLES));
    }

    @Test
    public void shouldReturnBadRequestWhenSchedulesAreNotNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, Collections.emptyList(), null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.SCHEDULES));
    }

    @Test
    public void shouldReturnBadRequestWhenLineStationsAreNotNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, Collections.emptyList());

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS));
    }

    @Test
    public void shouldReturnBadRequestWhenNameIsEmptyString() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, "", FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
    }

    @Test
    public void shouldReturnNotFoundWhenUpdatingNonExistingCityLine() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(999L, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentAndUpdateCityLine() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateCityLine(testRestTemplate, SYSTEM_ADMIN_HEADER, cityLineDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andCityLineIsUpdated(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED);
    }

    private void andCityLineIsUpdated(Long id, String updatedName, CityLineType updatedCityLineType) {
        CityLine cityLine = dbUtil.findCityLineById(id);

        assertThat("Name is updated.", cityLine.getName(), is(updatedName));
        assertThat("Type is updated.", cityLine.getCityLineType(), is(updatedCityLineType));
    }
}
