package com.tim2.travelservice.integration.boughtticket;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static com.tim2.travelservice.data.TokenData.GUEST_HTML_HEADER;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindBoughtTicketByIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.findBoughtTicketById(testRestTemplate, GUEST_HTML_HEADER, 1L);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenGettingNotExistingBoughTicket() {
        ResponseEntity responseEntity = ApiFunctions.findBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndBoughtTicket() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.findBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 1L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedTicketIsReturned(responseEntity);
    }

    private void andExpectedTicketIsReturned(ResponseEntity responseEntity) throws IOException {
        BoughtTicketDto boughtTicketDto = objectMapper.readValue(responseEntity.getBody().toString(), BoughtTicketDto.class);

        assertThat("Id", boughtTicketDto.getId(), is(1L));
        assertThat("Active", boughtTicketDto.getActive(), is(true));
        assertThat("Price", boughtTicketDto.getSoldPrice(), is(11.11));
    }
}
