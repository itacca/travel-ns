package com.tim2.travelservice.integration.document;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.DocumentDataBuilder;
import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DocumentDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class VerifyDocumentTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        ResponseEntity responseEntity = ApiFunctions.verifyDocument(testRestTemplate, GUEST_HTML_HEADER, documentDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        ResponseEntity responseEntity = ApiFunctions.verifyDocument(testRestTemplate, PASSENGER_HEADER, documentDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsNull() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(null, true);

        ResponseEntity responseEntity = ApiFunctions.verifyDocument(testRestTemplate, SYSTEM_ADMIN_HEADER, documentDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenVerifiedIsNull() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, null);

        ResponseEntity responseEntity = ApiFunctions.verifyDocument(testRestTemplate, SYSTEM_ADMIN_HEADER, documentDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VERIFIED));
    }

    @Test
    public void shouldReturnNotFoundWhenNonExistingDocument() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(999L, true);

        ResponseEntity responseEntity = ApiFunctions.verifyDocument(testRestTemplate, SYSTEM_ADMIN_HEADER, documentDto);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenVerifyingInactiveTicket() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(1L, true);

        ResponseEntity responseEntity = ApiFunctions.verifyDocument(testRestTemplate, SYSTEM_ADMIN_HEADER, documentDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.INACTIVE_DOCUMENT_VERIFICATION);
    }

    @Test
    public void shouldReturnNoContentAndVerifyDocument() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        ResponseEntity responseEntity = ApiFunctions.verifyDocument(testRestTemplate, SYSTEM_ADMIN_HEADER, documentDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andDocumentIsVerified(documentDto.getId(), documentDto.getVerified());
    }

    private void andDocumentIsVerified(Long documentId, Boolean verified) {
        Document document = dbUtil.findDocumentById(documentId);

        assertThat("Document is verified.", document.getVerified(), is(verified));
    }
}
