package com.tim2.travelservice.integration.boughtticket;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.entity.BoughtTicket;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static com.tim2.travelservice.data.TokenData.GUEST_HTML_HEADER;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class ValidateBoughtTicketByIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, GUEST_HTML_HEADER, 1L);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenValidatingNotExistingBoughTicket() {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndTrueAndSetSingleRideBoughtTicketActivityToFalse() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 5L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedBoughtTicketIsReturned(5L, responseEntity, TicketDuration.SINGLE_RIDE, true, false);
    }

    @Test
    public void shouldReturnOkAndTrueForNonExpiredDailyTicket() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 6L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedBoughtTicketIsReturned(6L, responseEntity, TicketDuration.DAILY, true, true);
    }

    @Test
    public void shouldReturnOkAndFalseForExpiredDailyTicketAndUpdateActivityToFalse() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 7L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedBoughtTicketIsReturned(7L, responseEntity, TicketDuration.DAILY, false, false);
    }

    @Test
    public void shouldReturnOkAndTrueForNonExpiredMonthlyTicket() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 8L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedBoughtTicketIsReturned(8L, responseEntity, TicketDuration.MONTHLY, true, true);
    }

    @Test
    public void shouldReturnOkAndFalseForExpiredMonthlyTicketAndUpdateActivityToFalse() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedBoughtTicketIsReturned(9L, responseEntity, TicketDuration.MONTHLY, false, false);
    }

    @Test
    public void shouldReturnOkAndTrueForNonExpiredAnnualTicket() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 10L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedBoughtTicketIsReturned(10L, responseEntity, TicketDuration.ANNUAL, true, true);
    }

    @Test
    public void shouldReturnOkAndFalseForExpiredAnnualTicketAndUpdateActivityToFalse() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.validateBoughtTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 11L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedBoughtTicketIsReturned(11L, responseEntity, TicketDuration.ANNUAL, false, false);
    }

    private void andExpectedBoughtTicketIsReturned(Long id, ResponseEntity responseEntity, TicketDuration ticketDuration, Boolean returnedActivity, boolean activityInDatabase) throws IOException {
        DynamicResponse dynamicResponse = objectMapper.readValue(responseEntity.getBody().toString(), DynamicResponse.class);
        BoughtTicket boughtTicket = dbUtil.findBoughTicketById(id);

        assertThat("Ticket duration is correct.", boughtTicket.getTicket().getTicketDuration(), is(ticketDuration));
        assertThat("Correct activity is returned.", dynamicResponse.get(RestApiConstants.IS_ACTIVE), is(returnedActivity));
        assertThat("Correct activity is in database.", boughtTicket.getActive(), is(activityInDatabase));
    }
}
