package com.tim2.travelservice.integration.document;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TokenData;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindImageBlobByDocumentIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnNotFoundWhenGettingImageBlobForNonExistingDocument() {
        ResponseEntity responseEntity = ApiFunctions.findImageBlobByDocumentId(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, 999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkWhenGettingImageBlobForExistingDocument() {
        ResponseEntity responseEntity = ApiFunctions.findImageBlobByDocumentId(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, 1L);

        CommonFunctions.thenOkIsReturned(responseEntity);
    }
}
