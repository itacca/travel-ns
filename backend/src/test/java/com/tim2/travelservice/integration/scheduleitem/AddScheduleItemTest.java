package com.tim2.travelservice.integration.scheduleitem;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.ScheduleDataBuilder;
import com.tim2.travelservice.data.builder.ScheduleItemDataBuilder;
import com.tim2.travelservice.entity.ScheduleItem;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleItemDto;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

import static com.tim2.travelservice.common.api.RestApiErrors.fieldShouldNotBeNull;
import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class AddScheduleItemTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, SCHEDULE_ITEM_DEPARTURE_TIME, ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE)));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, GUEST_HTML_HEADER, scheduleItems);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, SCHEDULE_ITEM_DEPARTURE_TIME, ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE)));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, VERIFIER_HEADER, scheduleItems);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, SCHEDULE_ITEM_DEPARTURE_TIME, ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE)));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, PASSENGER_HEADER, scheduleItems);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenScheduleItemIdIsNotNull() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(SCHEDULE_ITEM_ID, SCHEDULE_ITEM_DEPARTURE_TIME, ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE)));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleItems);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenVehicleIsNotNull() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, SCHEDULE_ITEM_DEPARTURE_TIME, ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE), new VehicleDto()));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleItems);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.VEHICLE));
    }

    @Test
    public void shouldReturnBadRequestWhenDepartureTimeIsNull() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, null, ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE)));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleItems);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, fieldShouldNotBeNull(RestApiConstants.DEPARTURE_TIME));
    }

    @Test
    public void shouldReturnBadRequestWhenScheduleIsNull() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, SCHEDULE_ITEM_DEPARTURE_TIME, null));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleItems);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, fieldShouldNotBeNull(RestApiConstants.SCHEDULE));
    }

    @Test
    public void shouldReturnBadRequestWhenScheduleIdIsNull() {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, SCHEDULE_ITEM_DEPARTURE_TIME, ScheduleDataBuilder.buildScheduleDto(null, SCHEDULE_TYPE)));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleItems);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    @Transactional
    public void shouldReturnOkAndCreateNewScheduleItem() throws Exception {
        List<ScheduleItemDto> scheduleItems = Collections.singletonList(ScheduleItemDataBuilder.buildScheduleItemDto(null, SCHEDULE_ITEM_DEPARTURE_TIME, ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, SCHEDULE_TYPE)));

        ResponseEntity responseEntity = ApiFunctions.addScheduleItems(testRestTemplate, SYSTEM_ADMIN_HEADER, scheduleItems);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        List<DynamicResponse> dynamicResponses = objectMapper.readValue(responseEntity.getBody().toString(), new TypeReference<List<DynamicResponse>>() {
        });
        List<Long> scheduleItemIds = CommonFunctions.andEntityIdsAreReturned(dynamicResponses);
        andScheduleItemsAreStoredWithGivenData(scheduleItemIds);
    }

    private void andScheduleItemsAreStoredWithGivenData(List<Long> scheduleItemIds) {
        assertThat("One id is returned.", scheduleItemIds.size(), is(1));

        ScheduleItem scheduleItem = dbUtil.findScheduleItemById(scheduleItemIds.get(0));

        validateReturnedScheduleItem(scheduleItem, SCHEDULE_ITEM_DEPARTURE_TIME, SCHEDULE_ID, SCHEDULE_TYPE);
    }

    private void validateReturnedScheduleItem(ScheduleItem scheduleItem, DateTime departureTime, Long scheduleId, ScheduleType scheduleType) {
        assertThat("Departure time is correct.", scheduleItem.getDepartureTime(), is(departureTime));
        assertThat("Schedule id is correct.", scheduleItem.getSchedule().getId(), is(scheduleId));
        assertThat("Schedule type is correct.", scheduleItem.getSchedule().getScheduleType(), is(scheduleType));
    }
}