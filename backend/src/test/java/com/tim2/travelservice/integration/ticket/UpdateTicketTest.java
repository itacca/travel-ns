package com.tim2.travelservice.integration.ticket;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.TicketDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdateTicketTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, GUEST_HTML_HEADER, ticketDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, VERIFIER_HEADER, ticketDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, PASSENGER_HEADER, ticketDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(null, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, SYSTEM_ADMIN_HEADER, ticketDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenTicketTypeIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_DURATION, null, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, SYSTEM_ADMIN_HEADER, ticketDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE));
    }

    @Test
    public void shouldReturnBadRequestWhenTicketDurationIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, null, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, SYSTEM_ADMIN_HEADER, ticketDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION));
    }

    @Test
    public void shouldReturnNotFoundWhenCityLineIsNull() {
        CityLineDto cityLineDto = null;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, SYSTEM_ADMIN_HEADER, ticketDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIdIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, SYSTEM_ADMIN_HEADER, ticketDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_ID));
    }

    @Test
    public void shouldReturnNoContentAndUpdateTicket() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        ResponseEntity responseEntity = ApiFunctions.updateTicket(testRestTemplate, SYSTEM_ADMIN_HEADER, ticketDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andTicketIsUpdated(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION);
    }

    private void andTicketIsUpdated(Long ticketId, TicketDuration ticketDuration) {
        Ticket ticket = dbUtil.findTicketById(ticketId);

        assertThat("Ticket id.", ticket.getId(), is(ticketId));
        assertThat("Ticket duration is updated.", ticket.getTicketDuration(), is(ticketDuration));
    }
}

