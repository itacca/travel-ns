package com.tim2.travelservice.integration.cityline;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.ReturnCode;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.tim2.travelservice.data.TestData.CITY_LINE_BUS_ID_1;
import static com.tim2.travelservice.data.TestData.NON_REFERENCED_CITY_LINE_ID;
import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeleteCityLineByIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.deleteCityLineById(testRestTemplate, GUEST_HTML_HEADER, CITY_LINE_BUS_ID_1);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.deleteCityLineById(testRestTemplate, VERIFIER_HEADER, CITY_LINE_BUS_ID_1);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.deleteCityLineById(testRestTemplate, PASSENGER_HEADER, CITY_LINE_BUS_ID_1);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingCityLine() {
        ResponseEntity responseEntity = ApiFunctions.deleteCityLineById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenDeletingCityLineReferencedByOtherEntities() {
        ResponseEntity responseEntity = ApiFunctions.deleteCityLineById(testRestTemplate, SYSTEM_ADMIN_HEADER, CITY_LINE_BUS_ID_1);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, ReturnCode.INTEGRITY_VIOLATION_EXCEPTION);
    }

    @Test
    public void shouldReturnNoContentAndDeleteNonReferencedCityLine() {
        ResponseEntity responseEntity = ApiFunctions.deleteCityLineById(testRestTemplate, SYSTEM_ADMIN_HEADER, NON_REFERENCED_CITY_LINE_ID);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andCityLineIsDeleted(NON_REFERENCED_CITY_LINE_ID);
    }

    private void andCityLineIsDeleted(Long cityLineId) {
        Optional<CityLine> stationOptional = dbUtil.findCityLineOptionalById(cityLineId);

        assertFalse("CityLine is deleted", stationOptional.isPresent());
    }
}
