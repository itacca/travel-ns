package com.tim2.travelservice.integration.station;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.LineStationDto;
import com.tim2.travelservice.web.dto.StationDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdateStationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, GUEST_HTML_HEADER, stationDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, VERIFIER_HEADER, stationDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, PASSENGER_HEADER, stationDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsNull() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenNameIsEmptyString() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, "", STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenLineStationsIsNotNull() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, Collections.singletonList(new LineStationDto()));

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS));
    }

    @Test
    public void shouldReturnNotFoundWhenUpdatingNonExistingStation() {
        StationDto stationDto = StationDataBuilder.buildStationDto(9999L, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentAndUpdateStationPartially() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, null, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andStationIsUpdated(STATION_ID, STATION_NAME_UPDATED, null, null);
    }

    @Test
    public void shouldReturnNoContentAndUpdateWholeStation() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        ResponseEntity responseEntity = ApiFunctions.updateStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andStationIsUpdated(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED);
    }

    private void andStationIsUpdated(Long id, String name, Double longitude, Double latitude) {
        Station station = dbUtil.findStationById(id);

        if (name != null) {
            assertThat("Name is updated.", station.getName(), is(name));
        }
        if (longitude != null) {
            assertThat("Longitude is updated.", station.getLongitude(), is(longitude));
        }
        if (latitude != null) {
            assertThat("Latitude is correct.", station.getLatitude(), is(latitude));
        }
    }
}
