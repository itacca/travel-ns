package com.tim2.travelservice.integration.pricelist;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TestData.PRICE_LIST_ID;
import static com.tim2.travelservice.data.TokenData.GUEST_HTML_HEADER;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindPriceListByIdTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.findPriceListById(testRestTemplate, GUEST_HTML_HEADER, PRICE_LIST_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenGettingNotExistingPriceList() {
        ResponseEntity responseEntity = ApiFunctions.findPriceListById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndPriceList() throws Exception {
        ResponseEntity responseEntity = ApiFunctions.findPriceListById(testRestTemplate, SYSTEM_ADMIN_HEADER, 2L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andPriceListIsReturned(responseEntity);
    }

    private void andPriceListIsReturned(ResponseEntity responseEntity) throws Exception {
        PriceListDto priceListDto = objectMapper.readValue(responseEntity.getBody().toString(), PriceListDto.class);
        assertThat("Price list id is correct.", priceListDto.getId(), is(2L));
    }
}
