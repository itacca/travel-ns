package com.tim2.travelservice.integration.linestation;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TokenData;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.LineStationDataBuilder;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.CITY_LINE_BUS_ID_1;
import static com.tim2.travelservice.data.TestData.CITY_LINE_BUS_ID_2;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdateLineStationsTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, TokenData.GUEST_HTML_HEADER, lineStationDtos);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, TokenData.VERIFIER_HEADER, lineStationDtos);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, TokenData.PASSENGER_HEADER, lineStationDtos);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenRelativePositionIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, null, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));


        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, null, StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));


        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIdIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(null), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));


        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenStationIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), null),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION));
    }

    @Test
    public void shouldReturnBadRequestWhenStationIdIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(null)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIdsAreNotTheSame() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID);
    }

    @Test
    public void shouldReturnNotFoundWhenCityLineDoesNotExist() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNotFoundWhenStationDoesNotExist() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(999L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNotFoundWhenLineStationDoesNotExist() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(999L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.LINE_STATION, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentAndUpdateDeleteAndCreateLineStations() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        ResponseEntity responseEntity = ApiFunctions.updateLineStations(testRestTemplate, SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andLineStationsAreCreatedDeletedAndUpdated();
    }

    private void andLineStationsAreCreatedDeletedAndUpdated() {
        List<LineStation> lineStations = dbUtil.findLineStationsByCityLineId(CITY_LINE_BUS_ID_2);
        assertThat("Correct number of line stations is returned.", lineStations.size(), is(4));

        LineStation lineStation1 = lineStations.get(0);
        LineStation lineStation2 = lineStations.get(1);
        LineStation lineStation3 = lineStations.get(2);
        LineStation lineStation4 = lineStations.get(3);

        validateReturnedStation(lineStation1, 2L, 2, CITY_LINE_BUS_ID_2, 2L);
        validateReturnedStation(lineStation2, 3L, 1, CITY_LINE_BUS_ID_2, 3L);
        validateReturnedStation(lineStation3, 4L, 4, CITY_LINE_BUS_ID_2, 4L);
        validateReturnedStation(lineStation4, dbUtil.findLineStationsLastId(), 3, CITY_LINE_BUS_ID_2, 5L);
    }

    private void validateReturnedStation(LineStation lineStation, Long lineStationId, int relativePosition, Long cityLineId, Long stationId) {
        Assert.assertThat("Line station id is correct.", lineStation.getId(), is(lineStationId));
        Assert.assertThat("Relative position is correct.", lineStation.getRelativePosition(), is(relativePosition));
        Assert.assertThat("City line id correct.", lineStation.getCityLine().getId(), is(cityLineId));
        Assert.assertThat("Station id is correct.", lineStation.getStation().getId(), is(stationId));
    }
}
