package com.tim2.travelservice.integration.pricelistitem;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindByPriceListIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldReturnOkAndPriceListItems() {
        ResponseEntity<RestResponsePage<PriceListItemDto>> responseEntity = ApiFunctions.findPriceListItemsByPriceListId(testRestTemplate, SYSTEM_ADMIN_HEADER, "2");

        CommonFunctions.thenOkIsReturned(responseEntity);
        andPriceListItemsAreReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenPriceListIsNotExisting() {
        ResponseEntity responseEntity = ApiFunctions.findPriceListItemsByPriceListIdNoPage(testRestTemplate, SYSTEM_ADMIN_HEADER, "2222");

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID));
    }

    private void andPriceListItemsAreReturned(ResponseEntity<RestResponsePage<PriceListItemDto>> responseEntity) {
        List<PriceListItemDto> priceListItemDtos = responseEntity.getBody().getContent();

        priceListItemDtos.forEach(priceListItemDto -> assertThat("Price list id is correct.", priceListItemDto.getPriceList().getId(), is(2L)));
        priceListItemDtos.forEach(priceListItemDto -> assertThat("Status is correct.", priceListItemDto.getActive(), is(true)));
    }
}
