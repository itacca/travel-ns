package com.tim2.travelservice.integration.pricelistitem;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TestData;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.data.builder.PriceListItemDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.entity.TicketType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.PriceListDto;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import com.tim2.travelservice.web.dto.TicketDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeletePriceListItemTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(1L, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(1L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(1L, TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity responseEntity = ApiFunctions.deletePriceListItem(testRestTemplate, GUEST_HTML_HEADER, priceListItemDto.getId());

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(1L, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(1L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(1L, TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity responseEntity = ApiFunctions.deletePriceListItem(testRestTemplate, VERIFIER_HEADER, priceListItemDto.getId());

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(1L, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(1L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(1L, TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity responseEntity = ApiFunctions.deletePriceListItem(testRestTemplate, PASSENGER_HEADER, priceListItemDto.getId());

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNoContentAndUpdatePriceListItemStatusToFalse() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(1L, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(1L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(1L, TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity responseEntity = ApiFunctions.deletePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto.getId());

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andPriceListItemIsDeleted(priceListItemDto.getId());
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingPriceListItem() {
        ResponseEntity responseEntity = ApiFunctions.deletePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, 999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST_ITEM, RestApiConstants.ID));
    }

    private void andPriceListItemIsDeleted(Long priceListItemId) {
        PriceListItem priceListItem = dbUtil.findOnePriceListItem(priceListItemId);

        assertFalse("Price list item is deleted.", priceListItem.getActive());
    }
}
