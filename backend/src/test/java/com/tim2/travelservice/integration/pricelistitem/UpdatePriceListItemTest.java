package com.tim2.travelservice.integration.pricelistitem;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TestData;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.data.builder.PriceListItemDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.entity.TicketType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdatePriceListItemTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnBadRequestWhenIdIsNotProvided() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, TestData.PRICE_LIST_ITEM_PRICE, ticketDto, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListIdIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceIsInvalid() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, TestData.INVALID_PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER);
    }

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, TestData.INVALID_PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, GUEST_HTML_HEADER, priceListItemDto);
        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, TestData.INVALID_PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, VERIFIER_HEADER, priceListItemDto);
        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, TestData.INVALID_PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, PASSENGER_HEADER, priceListItemDto);
        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnCreatedIfPriceListItemIsSuccessfullyUpdated() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(3L, PRICE_LIST_ITEM_PRICE_NEW, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.updatePriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long addedPriceListItemId = andPriceListItemIsReturned(responseEntity.getBody());
        andPriceListItemIsStoredWithGivenData(addedPriceListItemId);

        andPriceListItemIsStoredWithGivenData(addedPriceListItemId);
    }

    private Long andPriceListItemIsReturned(DynamicResponse body) {
        assertThat(RestApiConstants.ID + " is not null.", body.get(RestApiConstants.ID), notNullValue());

        return ((Integer) body.get(RestApiConstants.ID)).longValue();
    }

    private void andPriceListItemIsStoredWithGivenData(Long addedPriceListItemId) {
        PriceListItem priceListItem = dbUtil.findOnePriceListItem(addedPriceListItemId);

        assertThat("Price is correct.", priceListItem.getPrice(), is(TestData.PRICE_LIST_ITEM_PRICE_NEW));
        assertThat("Status is active", priceListItem.getActive(), is(true));
    }
}
