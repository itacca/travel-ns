package com.tim2.travelservice.integration.pricelist;


import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.data.builder.PriceListItemDataBuilder;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.PriceListDto;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class CreatePriceListTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE_NEW, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, GUEST_HTML_HEADER, priceListDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE_NEW, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, PASSENGER_HEADER, priceListDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE_NEW, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, VERIFIER_HEADER, priceListDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenStartDateIsNull() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDtoWithEndDate(PRICE_LIST_START_DATE, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.START_DATE));
    }

    @Test
    public void shouldReturnBadRequestWhenStartDateIsInThePast() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(null, PRICE_LIST_PAST_DATE, PRICE_LIST_END_DATE_NEW, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE);
    }

    @Test
    public void shouldReturnBadRequestWhenEndDateIsNull() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDtoWithStartDate(PRICE_LIST_START_DATE, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE));
    }

    @Test
    public void shouldReturnBadRequestWhenEndDateIsInThePast() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(null, PRICE_LIST_START_DATE, PRICE_LIST_PAST_DATE, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE);
    }

    @Test
    public void shouldReturnBadRequestWhenDateIntervalIsInvalid() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(null, PRICE_LIST_END_DATE_NEW, PRICE_LIST_START_DATE, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.INVALID_DATE_INTERVAL);
    }

    @Test
    public void shouldReturnBadRequestWhenDateIntervalIsOverlappingWithOthers() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(null, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.INTERVALS_ARE_OVERLAPPING);
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListItemListIsNull() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(null, PRICE_LIST_START_DATE_NEW, PRICE_LIST_END_DATE_NEW);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST_ITEMS));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListItemListIsNotEmpty() {
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE_NEW);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE_NEW, PRICE_LIST_END_DATE_NEW, Collections.singletonList(priceListItemDto));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.collectionShouldBeEmpty(RestApiConstants.PRICE_LIST_ITEMS));
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsProvided() {
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE_NEW);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE_NEW, PRICE_LIST_END_DATE_NEW, Collections.singletonList(priceListItemDto));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnCreatedAndAddPriceList() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE_NEW, PRICE_LIST_END_DATE_NEW, Collections.emptyList());

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long addedPriceListId = andPriceListIsReturned(responseEntity.getBody());
        andPriceListIsStoredWithGivenData(addedPriceListId);
    }

    private Long andPriceListIsReturned(DynamicResponse body) {
        assertThat(RestApiConstants.ID + " is not null.", body.get(RestApiConstants.ID), notNullValue());

        return ((Integer) body.get(RestApiConstants.ID)).longValue();
    }

    private void andPriceListIsStoredWithGivenData(Long addedPriceListId) {
        PriceList priceList = dbUtil.findOnePriceList(addedPriceListId);

        assertThat("Start date is correct.", priceList.getStartDate(), is(DateTime.parse(PRICE_LIST_START_DATE_NEW)));
        assertThat("End date is correct.", priceList.getEndDate(), is(DateTime.parse(PRICE_LIST_END_DATE_NEW)));
    }
}
