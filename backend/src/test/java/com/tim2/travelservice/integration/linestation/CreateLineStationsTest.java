package com.tim2.travelservice.integration.linestation;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TokenData;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.LineStationDataBuilder;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class CreateLineStationsTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.GUEST_HTML_HEADER, lineStationDtos);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.VERIFIER_HEADER, lineStationDtos);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.PASSENGER_HEADER, lineStationDtos);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenIdIsNotNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(1L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenRelativePositionIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, null, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, null, StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIdIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(null), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenStationIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), null),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION));
    }

    @Test
    public void shouldReturnBadRequestWhenStationIdIsNull() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(null)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIdsAreNotTheSame() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(STATION_ID)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID);
    }

    @Test
    public void shouldReturnNotFoundWhenCityLineIsNotExisting() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(99L), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(99L), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNotFoundWhenStationIsNotExisting() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(99L)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnCreatedAndLineStationsIds() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        ResponseEntity responseEntity = ApiFunctions.createLineStations(testRestTemplate, TokenData.SYSTEM_ADMIN_HEADER, lineStationDtos);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        List<DynamicResponse> dynamicResponses = objectMapper.readValue(responseEntity.getBody().toString(), new TypeReference<List<DynamicResponse>>() {
        });
        List<Long> lineStationsIds = CommonFunctions.andEntityIdsAreReturned(dynamicResponses);
        andLineStationsAreStoredWithGivenData(lineStationsIds);
    }

    private void andLineStationsAreStoredWithGivenData(List<Long> lineStationsIds) {
        assertThat("Two ids are returned.", lineStationsIds.size(), is(2));

        LineStation lineStation1 = dbUtil.findLineStationById(lineStationsIds.get(0));
        LineStation lineStation2 = dbUtil.findLineStationById(lineStationsIds.get(1));

        validateReturnedLineStation(lineStation1, 1, CITY_LINE_BUS_ID_1, STATION_ID);
        validateReturnedLineStation(lineStation2, 2, CITY_LINE_BUS_ID_1, STATION_ID_2);
    }

    private void validateReturnedLineStation(LineStation lineStation, Integer relativePosition, Long cityLineId, Long stationId) {
        assertThat("Relative position is correct.", lineStation.getRelativePosition(), is(relativePosition));
        assertThat("City line is correct.", lineStation.getCityLine().getId(), is(cityLineId));
        assertThat("Station is correct.", lineStation.getStation().getId(), is(stationId));
    }
}
