package com.tim2.travelservice.integration.cityline;

import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.data.TokenData.PASSENGER_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindAllCityLinesTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnOkAndListOfCityLines() {
        ResponseEntity<RestResponsePage<CityLineDto>> responseEntity = ApiFunctions.findAllCityLines(testRestTemplate, PASSENGER_HEADER);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedCityLinesAreReturned(responseEntity);
    }

    private void andExpectedCityLinesAreReturned(ResponseEntity<RestResponsePage<CityLineDto>> responseEntity) {
        List<CityLineDto> cityLineDtos = responseEntity.getBody().getContent();

        assertThat("Number of returned city lines is valid.", (long) cityLineDtos.size(), is(dbUtil.findCityLinesCount()));
    }
}
