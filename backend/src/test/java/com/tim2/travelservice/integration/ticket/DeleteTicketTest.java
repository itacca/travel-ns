package com.tim2.travelservice.integration.ticket;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.ReturnCode;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.tim2.travelservice.data.TestData.FIRST_TICKET_ID;
import static com.tim2.travelservice.data.TestData.NON_REFERENCED_TICKET_ID;
import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeleteTicketTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.deleteTicketById(testRestTemplate, GUEST_HTML_HEADER, FIRST_TICKET_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.deleteTicketById(testRestTemplate, VERIFIER_HEADER, FIRST_TICKET_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.deleteTicketById(testRestTemplate, PASSENGER_HEADER, FIRST_TICKET_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingTicket() {
        ResponseEntity responseEntity = ApiFunctions.deleteTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenDeletingTicketReferencedByOtherEntities() {
        ResponseEntity responseEntity = ApiFunctions.deleteTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, FIRST_TICKET_ID);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, ReturnCode.INTEGRITY_VIOLATION_EXCEPTION);
    }

    @Test
    public void shouldReturnNoContentAndDeleteNonReferencedStation() {
        ResponseEntity responseEntity = ApiFunctions.deleteTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, NON_REFERENCED_TICKET_ID);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andTicketIsDeleted(NON_REFERENCED_TICKET_ID);
    }

    private void andTicketIsDeleted(Long ticketId) {
        Optional<Ticket> ticket = dbUtil.findTicketByIdOptional(ticketId);

        assertFalse("Ticket is deleted", ticket.isPresent());
    }

}
