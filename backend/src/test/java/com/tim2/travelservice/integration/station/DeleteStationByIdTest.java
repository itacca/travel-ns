package com.tim2.travelservice.integration.station;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.ReturnCode;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.tim2.travelservice.data.TestData.NON_REFERENCED_STATION_ID;
import static com.tim2.travelservice.data.TestData.STATION_ID;
import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeleteStationByIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.deleteStationById(testRestTemplate, GUEST_HTML_HEADER, STATION_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.deleteStationById(testRestTemplate, VERIFIER_HEADER, STATION_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.deleteStationById(testRestTemplate, PASSENGER_HEADER, STATION_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingStation() {
        ResponseEntity responseEntity = ApiFunctions.deleteStationById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenDeletingStationReferencedByOtherEntities() {
        ResponseEntity responseEntity = ApiFunctions.deleteStationById(testRestTemplate, SYSTEM_ADMIN_HEADER, STATION_ID);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, ReturnCode.INTEGRITY_VIOLATION_EXCEPTION);
    }

    @Test
    public void shouldReturnNoContentAndDeleteNonReferencedStation() {
        ResponseEntity responseEntity = ApiFunctions.deleteStationById(testRestTemplate, SYSTEM_ADMIN_HEADER, NON_REFERENCED_STATION_ID);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andStationIsDeleted(NON_REFERENCED_STATION_ID);
    }

    private void andStationIsDeleted(Long stationId) {
        Optional<Station> stationOptional = dbUtil.findStationOptionalById(stationId);

        assertFalse("Station is deleted", stationOptional.isPresent());
    }
}
