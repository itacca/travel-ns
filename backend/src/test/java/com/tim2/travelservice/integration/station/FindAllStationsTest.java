package com.tim2.travelservice.integration.station;

import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.StationDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindAllStationsTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnOkAndAllStations() {
        ResponseEntity<RestResponsePage<StationDto>> responseEntity = ApiFunctions.findAllStations(testRestTemplate, SYSTEM_ADMIN_HEADER);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andStationsAreReturned(responseEntity);
    }

    private void andStationsAreReturned(ResponseEntity<RestResponsePage<StationDto>> responseEntity) {
        List<StationDto> stations = responseEntity.getBody().getContent();

        assertThat("All stations are returned.", (long) stations.size(), is(dbUtil.findStationsCount()));
    }
}
