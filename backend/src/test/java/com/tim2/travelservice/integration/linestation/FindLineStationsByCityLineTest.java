package com.tim2.travelservice.integration.linestation;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

import static com.tim2.travelservice.data.TestData.CITY_LINE_BUS_ID_2;
import static com.tim2.travelservice.data.TokenData.GUEST_JSON_HEADER;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindLineStationsByCityLineTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnNotFoundWhenCityLineIsNotExisting() {
        ResponseEntity responseEntity = ApiFunctions.findLineStationsByCityLineId(testRestTemplate, GUEST_JSON_HEADER, 999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndLineStationsForGivenCityLineId() throws Exception {
        ResponseEntity responseEntity = ApiFunctions.findLineStationsByCityLineId(testRestTemplate, GUEST_JSON_HEADER, CITY_LINE_BUS_ID_2);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedLineStationsAreReturned(responseEntity);
    }

    private void andExpectedLineStationsAreReturned(ResponseEntity responseEntity) throws IOException {
        List<LineStationDto> lineStationDtos = objectMapper.readValue(responseEntity.getBody().toString(), new TypeReference<List<LineStationDto>>() {
        });

        assertThat("All line stations for given city line are returned.", lineStationDtos.size(), is(4));
    }
}
