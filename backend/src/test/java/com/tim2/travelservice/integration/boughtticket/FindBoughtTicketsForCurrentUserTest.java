package com.tim2.travelservice.integration.boughtticket;

import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.data.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindBoughtTicketsForCurrentUserTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldReturnOkAndBoughtTickets() {
        ResponseEntity<RestResponsePage<BoughtTicketDto>> responseEntity = ApiFunctions.findBoughtTicketsForCurrentUser(testRestTemplate, SYSTEM_ADMIN_HEADER);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andBoughtTicketsAreReturned(responseEntity);
    }

    private void andBoughtTicketsAreReturned(ResponseEntity<RestResponsePage<BoughtTicketDto>> responseEntity) {
        List<BoughtTicketDto> boughtTickets = responseEntity.getBody().getContent();

        boughtTickets.forEach(boughtTicketDto -> assertThat("Principal is ticket owner.", boughtTicketDto.getAccount().getEmail(), is(SYSTEM_ADMINISTRATOR_EMAIL)));
    }
}
