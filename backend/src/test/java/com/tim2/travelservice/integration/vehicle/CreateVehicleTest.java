package com.tim2.travelservice.integration.vehicle;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.VehicleDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.entity.VehicleType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class CreateVehicleTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, GUEST_HTML_HEADER, vehicleDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, VERIFIER_HEADER, vehicleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, PASSENGER_HEADER, vehicleDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenVehicleIdIsNotNull() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(1L, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenPlateNumberIsNull() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, null, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PLATE_NUMBER));
    }

    @Test
    public void shouldReturnBadRequestWhenVehicleTypeIsNull() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, null, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VEHICLE_TYPE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIsNull() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
    }


    @Test
    public void shouldReturnBadRequestWhenCityLineIdIsNull() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineTypeIsNull() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, null));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE));
    }

    @Test
    public void shouldReturnBadRequestWhenPlateNumberIsExisting() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER_EXISTING, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.VEHICLE, RestApiConstants.PLATE_NUMBER));
    }

    @Test
    @Transactional
    public void shouldReturnCreatedAndVehicleId() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addVehicle(testRestTemplate, SYSTEM_ADMIN_HEADER, vehicleDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long vehicleId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
        andVehicleIsStoredWithGivenData(vehicleId);
    }

    private void andVehicleIsStoredWithGivenData(Long vehicleId) {
        Vehicle vehicle = dbUtil.findVehicleById(vehicleId);

        assertThat("Id is correct.", vehicle.getId(), is(vehicleId));
        assertThat("Plate number is correct.", vehicle.getPlateNumber(), is(VEHICLE_PLATE_NUMBER));
        assertThat("Type is correct.", vehicle.getVehicleType(), is(VehicleType.BUS));
        assertThat("City line id is correct.", vehicle.getCityLine().getId(), is(CITY_LINE_BUS_ID_1));
    }
}
