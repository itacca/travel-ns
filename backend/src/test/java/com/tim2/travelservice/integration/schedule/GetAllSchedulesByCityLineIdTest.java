package com.tim2.travelservice.integration.schedule;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TestData;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.PASSENGER_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class GetAllSchedulesByCityLineIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnNotFoundWhenCityLineDoesNotExist() {
        ResponseEntity responseEntity = ApiFunctions.getAllSchedulesByCityLineId(testRestTemplate, PASSENGER_HEADER, NON_EXISTING_CITY_LINE_ID);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndAllSchedulesWithGivenCityLineId() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.getAllSchedulesByCityLineId(testRestTemplate, PASSENGER_HEADER, FIRST_CITY_LINE_ID);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedSchedulesAreReturned(responseEntity);
    }

    private void andExpectedSchedulesAreReturned(ResponseEntity responseEntity) throws IOException {
        List<ScheduleDto> scheduleDtos = objectMapper.readValue(responseEntity.getBody().toString(), new TypeReference<List<ScheduleDto>>() {
        });

        ScheduleDto firstSchedule = scheduleDtos.get(0);
        assertThat("Number of returned schedules is correct.", scheduleDtos.size(), is(NUMBER_OF_SCHEDULES));
        assertThat("First schedule id is correct.", firstSchedule.getId(), is(TestData.SCHEDULE_ID));
        assertThat("First schedule type is correct.", firstSchedule.getScheduleType(), is(TestData.SCHEDULE_TYPE));
        assertThat("First schedule active is correct.", firstSchedule.getActive(), is(TestData.SCHEDULE_ACTIVE));
    }
}
