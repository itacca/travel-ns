package com.tim2.travelservice.integration.account;

import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.AccountDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.data.TestData.ADMINISTRATOR_ROLE_ID;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class FindTravelAdminsTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.findByRoleIdNoPage(testRestTemplate, GUEST_HTML_HEADER, ADMINISTRATOR_ROLE_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.findByRoleIdNoPage(testRestTemplate, PASSENGER_HEADER, ADMINISTRATOR_ROLE_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.findByRoleIdNoPage(testRestTemplate, VERIFIER_HEADER, ADMINISTRATOR_ROLE_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsTravelAdmin() {
        ResponseEntity responseEntity = ApiFunctions.findByRoleIdNoPage(testRestTemplate, TRAVEL_ADMIN_HEADER, ADMINISTRATOR_ROLE_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnOkAndTravelAdmins() {
        ResponseEntity<RestResponsePage<AccountDto>> responseEntity = ApiFunctions.findByRoleId(testRestTemplate, SYSTEM_ADMIN_HEADER, ADMINISTRATOR_ROLE_ID);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andTravelAdminsAreReturned(responseEntity);
    }

    private void andTravelAdminsAreReturned(ResponseEntity<RestResponsePage<AccountDto>> responseEntity) {
        List<AccountDto> accountDtos = responseEntity.getBody().getContent();

        accountDtos.forEach(accountDto -> assertThat("Role is correct.", accountDto.getRoles().get(0).getId(), is(2L)));
    }
}
