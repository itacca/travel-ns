package com.tim2.travelservice.integration.cityline;

import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

import static com.tim2.travelservice.data.TestData.CITY_LINE_VALID_BUS_TYPE;
import static com.tim2.travelservice.data.TokenData.PASSENGER_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindCityLinesByTypeTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    /*
    @Test
    public void shouldReturnBadRequestWhenCityLineTypeIsNotValid() {
        ResponseEntity<RestResponsePage<CityLineDto>> responseEntity = ApiFunctions.getCityLinesByType(testRestTemplate, PASSENGER_HEADER, CITY_LINE_INVALID_TYPE);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.invalidRequestParamFormat(RestApiConstants.TYPE));
    }
    */

    @Test
    public void shouldReturnOkAndListOfCityLinesOfRequestedType() throws IOException {
        ResponseEntity<RestResponsePage<CityLineDto>> responseEntity = ApiFunctions.getCityLinesByType(testRestTemplate, PASSENGER_HEADER, CITY_LINE_VALID_BUS_TYPE);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedCityLinesAreReturned(responseEntity);
    }

    private void andExpectedCityLinesAreReturned(ResponseEntity<RestResponsePage<CityLineDto>> responseEntity) {
        List<CityLineDto> cityLineDtos = responseEntity.getBody().getContent();

        cityLineDtos.forEach(cityLineDto -> assertThat("Returned city line has expected type",
                cityLineDto.getCityLineType().toString(), is(CITY_LINE_VALID_BUS_TYPE)));
    }
}
