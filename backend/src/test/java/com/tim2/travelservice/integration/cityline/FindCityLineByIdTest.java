package com.tim2.travelservice.integration.cityline;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.GUEST_JSON_HEADER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindCityLineByIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnNotFoundWhenGettingNonExistingCityLine() {
        ResponseEntity responseEntity = ApiFunctions.findCityLineById(testRestTemplate, GUEST_JSON_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndCityLine() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.findCityLineById(testRestTemplate, GUEST_JSON_HEADER, CITY_LINE_BUS_ID_1);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedCityLineIsReturned(responseEntity);
    }

    private void andExpectedCityLineIsReturned(ResponseEntity responseEntity) throws IOException {
        CityLineDto cityLineDto = objectMapper.readValue(responseEntity.getBody().toString(), CityLineDto.class);

        assertThat("Id is correct.", cityLineDto.getId(), is(CITY_LINE_BUS_ID_1));
        assertThat("Type is correct.", cityLineDto.getCityLineType(), is(FIRST_CITY_LINE_TYPE));
        assertThat("Name is correct.", cityLineDto.getName(), is(FIRST_CITY_LINE_NAME));
    }
}
