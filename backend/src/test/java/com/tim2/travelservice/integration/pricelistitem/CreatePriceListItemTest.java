package com.tim2.travelservice.integration.pricelistitem;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.TestData;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.data.builder.PriceListItemDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.entity.TicketType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class CreatePriceListItemTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnBadRequestWhenTicketIsNull() {
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, null, PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET));
    }

    @Test
    public void shouldReturnBadRequestWhenTicketDurationIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, cityLineDto), priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION));
    }

    @Test
    public void shouldReturnBadRequestWhenTicketTypeIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE));
    }

    @Test
    public void shouldReturnBadRequestWhenTicketIdIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineIsNull() {
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, null);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineNameIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);
        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenCityLineTypeIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceIsNegative() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.INVALID_PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER);
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListIdIsNull() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListItemIdIsProvided() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME1, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_ID, TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenPriceListItemForGivenTicketAlreadyExists() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.CREATE_PRICE_LIST_ITEM_ERROR);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, VERIFIER_HEADER, priceListItemDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, PASSENGER_HEADER, priceListItemDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, GUEST_HTML_HEADER, priceListItemDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    @Transactional
    public void shouldReturnCreatedWhenPriceListItemIsSuccessfullyAdded() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(TestData.CITY_LINE_TRAM_ID_1, TestData.CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(TestData.PRICE_LIST_ID, TestData.PRICE_LIST_START_DATE, TestData.PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(TestData.PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addPriceListItem(testRestTemplate, SYSTEM_ADMIN_HEADER, priceListItemDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long addedPriceListItemId = andPriceListItemIsReturned(responseEntity.getBody());
        andPriceListItemIsStoredWithGivenData(addedPriceListItemId);
    }

    private Long andPriceListItemIsReturned(DynamicResponse body) {
        assertThat(RestApiConstants.ID + " is not null.", body.get(RestApiConstants.ID), notNullValue());

        return ((Integer) body.get(RestApiConstants.ID)).longValue();
    }

    private void andPriceListItemIsStoredWithGivenData(Long addedPriceListItemId) {
        PriceListItem priceListItem = dbUtil.findOnePriceListItem(addedPriceListItemId);

        assertThat("Price is correct.", priceListItem.getPrice(), is(TestData.PRICE_LIST_ITEM_PRICE));
        assertThat("Ticket type is correct.", priceListItem.getTicket().getTicketType(), is(TicketType.REGULAR));
        assertThat("Ticket duration is correct.", priceListItem.getTicket().getTicketDuration(), is(TicketDuration.SINGLE_RIDE));
        assertThat("City line name is correct.", priceListItem.getTicket().getCityLine().getName(), is(TestData.CITY_LINE_TRAM_NAME_ADD));
        assertThat("City line type is correct.", priceListItem.getTicket().getCityLine().getCityLineType(), is(CityLineType.TRAM));
        assertThat("Price list id is correct.", priceListItem.getPriceList().getId(), is(TestData.PRICE_LIST_ID));
    }
}
