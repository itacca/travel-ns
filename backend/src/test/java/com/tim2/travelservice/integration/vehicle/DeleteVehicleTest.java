package com.tim2.travelservice.integration.vehicle;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.tim2.travelservice.data.TestData.VEHICLE_ID;
import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeleteVehicleTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.deleteVehicleById(testRestTemplate, GUEST_HTML_HEADER, VEHICLE_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.deleteVehicleById(testRestTemplate, VERIFIER_HEADER, VEHICLE_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.deleteVehicleById(testRestTemplate, PASSENGER_HEADER, VEHICLE_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingStation() {
        ResponseEntity responseEntity = ApiFunctions.deleteVehicleById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentAndDeleteNonReferencedStation() {
        ResponseEntity responseEntity = ApiFunctions.deleteVehicleById(testRestTemplate, SYSTEM_ADMIN_HEADER, VEHICLE_ID);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andVehicleIsDeleted(VEHICLE_ID);
    }

    private void andVehicleIsDeleted(Long vehicleId) {
        Optional<Vehicle> vehicleOptional = dbUtil.findVehicleOptionalById(vehicleId);

        assertFalse("Vehicle is deleted", vehicleOptional.isPresent());
    }
}
