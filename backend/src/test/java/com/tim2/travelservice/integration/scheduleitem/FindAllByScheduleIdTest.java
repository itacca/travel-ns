package com.tim2.travelservice.integration.scheduleitem;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.ScheduleItemDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

import static com.tim2.travelservice.data.TestData.NOT_EXISTING_SCHEDULE_ID;
import static com.tim2.travelservice.data.TestData.SCHEDULE_ID;
import static com.tim2.travelservice.data.TokenData.PASSENGER_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindAllByScheduleIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnNotFoundWhenScheduleDoesNotExist() {
        ResponseEntity responseEntity = ApiFunctions.findAllScheduleItemsByScheduleId(testRestTemplate, PASSENGER_HEADER, NOT_EXISTING_SCHEDULE_ID);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.SCHEDULE, RestApiConstants.ID));
    }

    @Test
    @Transactional
    public void shouldReturnOkAndAllScheduleItemsWithGivenScheduleId() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.findAllScheduleItemsByScheduleId(testRestTemplate, PASSENGER_HEADER, SCHEDULE_ID);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedScheduleItemsAreReturned(responseEntity);
    }

    private void andExpectedScheduleItemsAreReturned(ResponseEntity responseEntity) throws IOException {
        List<ScheduleItemDto> scheduleItemDtos = objectMapper.readValue(responseEntity.getBody().toString(), new TypeReference<List<ScheduleItemDto>>() {
        });

        scheduleItemDtos.forEach(scheduleItemDto ->
                assertThat("Returned schedule item has expected schedule id.", scheduleItemDto.getSchedule().getId(), is(SCHEDULE_ID)));
    }
}
