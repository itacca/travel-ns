package com.tim2.travelservice.integration.account;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeleteTravelAdminTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.deleteTravelAdminById(testRestTemplate, GUEST_HTML_HEADER, TRAVEL_ADMIN_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.deleteTravelAdminById(testRestTemplate, VERIFIER_HEADER, TRAVEL_ADMIN_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.deleteTravelAdminById(testRestTemplate, PASSENGER_HEADER, TRAVEL_ADMIN_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsTravelAdmin() {
        ResponseEntity responseEntity = ApiFunctions.deleteTravelAdminById(testRestTemplate, TRAVEL_ADMIN_HEADER, TRAVEL_ADMIN_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingTravelAdmin() {
        ResponseEntity responseEntity = ApiFunctions.deleteTravelAdminById(testRestTemplate, SYSTEM_ADMIN_HEADER, 555L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingUseRThatIsNotTravelAdmin() {
        ResponseEntity responseEntity = ApiFunctions.deleteTravelAdminById(testRestTemplate, SYSTEM_ADMIN_HEADER, SYSTEM_ADMINISTRATOR_ID);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.USER_IS_NOT_TRAVEL_ADMIN);
    }

    @Test
    public void shouldReturnNoContentAndDeleteTravelAdmin() {
        ResponseEntity responseEntity = ApiFunctions.deleteTravelAdminById(testRestTemplate, SYSTEM_ADMIN_HEADER, TRAVEL_ADMIN_ID_DELETE);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andAdminIsDeleted(TRAVEL_ADMIN_ID_DELETE);
    }

    private void andAdminIsDeleted(Long id) {
        Optional<Account> accountOptional = dbUtil.findAccountOptionalById(id);

        assertFalse("Admin is deleted", accountOptional.isPresent());
    }
}
