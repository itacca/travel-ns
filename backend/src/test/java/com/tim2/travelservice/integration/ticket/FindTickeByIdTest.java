package com.tim2.travelservice.integration.ticket;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.TicketDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.GUEST_HTML_HEADER;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindTickeByIdTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.findTicketById(testRestTemplate, GUEST_HTML_HEADER, 1L);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenGettingNotExistingTicket() {
        ResponseEntity responseEntity = ApiFunctions.findTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 9999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnOkAndRequestedTicket() throws IOException {
        ResponseEntity responseEntity = ApiFunctions.findTicketById(testRestTemplate, SYSTEM_ADMIN_HEADER, 1L);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andExpectedTicketIsReturned(responseEntity);
    }

    private void andExpectedTicketIsReturned(ResponseEntity responseEntity) throws IOException {
        TicketDto ticketDto = objectMapper.readValue(responseEntity.getBody().toString(), TicketDto.class);

        assertThat("Id", ticketDto.getId(), is(1L));
        assertThat("Ticket duration", ticketDto.getTicketDuration(), is(FIRST_TICKET_DURATION));
        assertThat("Ticket type", ticketDto.getTicketType(), is(FIRST_TICKET_TYPE));
        assertThat("Ticket duration", ticketDto.getCityLine().getId(), is(FIRST_CITY_LINE_ID));
    }

}
