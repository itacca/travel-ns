package com.tim2.travelservice.integration.station;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.LineStationDto;
import com.tim2.travelservice.web.dto.StationDto;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class CreateStationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, GUEST_HTML_HEADER, stationDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, VERIFIER_HEADER, stationDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, PASSENGER_HEADER, stationDto);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenStationIdIsNotNull() {
        StationDto stationDto = StationDataBuilder.buildStationDto(1L, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
    }

    @Test
    public void shouldReturnBadRequestWhenNameIsNull() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, null, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenLongitudeIsNull() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, null, ADD_STATION_LATITUDE, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LONGITUDE));
    }

    @Test
    public void shouldReturnBadRequestWhenLatitudeIsNull() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, null, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LATITUDE));
    }

    @Test
    public void shouldReturnBadRequestWhenLineStationsAreNotNull() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, Collections.singletonList(new LineStationDto()));

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS));
    }

    @Test
    @Transactional
    public void shouldReturnCreatedAndStationId() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.addStation(testRestTemplate, SYSTEM_ADMIN_HEADER, stationDto);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long stationId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
        andAccountIsStoredWithGivenData(stationId);
    }

    private void andAccountIsStoredWithGivenData(Long stationId) {
        Station station = dbUtil.findStationById(stationId);

        assertThat("Id is correct.", station.getId(), is(stationId));
        assertThat("Name is correct.", station.getName(), is(ADD_STATION_NAME));
        assertThat("Longitude is correct.", station.getLongitude(), is(ADD_STATION_LONGITUDE));
        assertThat("Latitude is correct.", station.getLatitude(), is(ADD_STATION_LATITUDE));
        assertTrue("Line stations is correct.", CollectionUtils.isEmpty(station.getLineStations()));
    }
}
