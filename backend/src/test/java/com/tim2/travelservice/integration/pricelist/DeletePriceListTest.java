package com.tim2.travelservice.integration.pricelist;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TestData.PRICE_LIST_ID;
import static com.tim2.travelservice.data.TokenData.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class DeletePriceListTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        ResponseEntity responseEntity = ApiFunctions.deletePriceList(testRestTemplate, GUEST_HTML_HEADER, PRICE_LIST_ID);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsPassenger() {
        ResponseEntity responseEntity = ApiFunctions.deletePriceList(testRestTemplate, PASSENGER_HEADER, PRICE_LIST_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnForbiddenWhenUserIsVerifier() {
        ResponseEntity responseEntity = ApiFunctions.deletePriceList(testRestTemplate, VERIFIER_HEADER, PRICE_LIST_ID);

        CommonFunctions.thenForbiddenIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnNotFoundWhenDeletingNonExistingPriceList() {
        ResponseEntity responseEntity = ApiFunctions.deletePriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, 999L);

        CommonFunctions.thenNotFoundIsReturned(responseEntity, RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID));
    }

    @Test
    public void shouldReturnNoContentWhenPriceListIsDeleted() {
        ResponseEntity responseEntity = ApiFunctions.deletePriceList(testRestTemplate, SYSTEM_ADMIN_HEADER, PRICE_LIST_ID);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
    }
}
