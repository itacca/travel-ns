package com.tim2.travelservice.integration.account;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.ReturnCode;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.GUEST_HTML_HEADER;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UpdateCurrentUserTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void shouldReturnLoginPageWhenUserIsGuest() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, GUEST_HTML_HEADER, accountDto);

        CommonFunctions.thenLoginPageIsReturned(responseEntity);
    }

    @Test
    public void shouldReturnBadRequestWhenPasswordIsLessThanEightCharacters() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_SHORT_PASSWORD_UPDATE, null, null);

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH));
    }

    @Test
    public void shouldReturnBadRequestWhenFirstNameIsEmptyString() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, "", null);

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenLastNameIsEmptyString() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, null, "");

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME));
    }

    @Test
    public void shouldReturnBadRequestWhenDateOfBirthHasInvalidFormat() {
        DynamicResponse accountDto = AccountDataBuilder.buildAccountDtoJson(null, null, null, ACCOUNT_INVALID_DATE_OF_BIRTH_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, ReturnCode.INVALID_JSON);
    }

    @Test
    public void shouldReturnBadRequestWhenDateOfBirthIsInTheFuture() {
        DynamicResponse accountDto = AccountDataBuilder.buildAccountDtoJson(null, null, null, ACCOUNT_REGISTRATION_FUTURE_DATE_OF_BIRTH);

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER, accountDto);

        CommonFunctions.thenBadRequestIsReturned(responseEntity, RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST);
    }

    @Test
    public void shouldReturnNoContentAndUpdateWholeAccount() {
        DynamicResponse accountDto = AccountDataBuilder.buildAccountDtoJson(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, ACCOUNT_DATE_OF_BIRTH_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER, accountDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andAccountIsUpdated(SYSTEM_ADMINISTRATOR_EMAIL, ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, ACCOUNT_DATE_OF_BIRTH_UPDATED);
    }

    @Test
    public void shouldReturnNoContentAndUpdateAccountPartially() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, null, ACCOUNT_LAST_NAME_UPDATED);

        ResponseEntity responseEntity = ApiFunctions.updateLoggedUser(testRestTemplate, SYSTEM_ADMIN_HEADER, accountDto);

        CommonFunctions.thenNoContentIsReturned(responseEntity);
        andAccountIsUpdated(SYSTEM_ADMINISTRATOR_EMAIL, ACCOUNT_PASSWORD_UPDATED, null, ACCOUNT_LAST_NAME_UPDATED, null);
    }

    private void andAccountIsUpdated(String accountEmail, String updatedPassword, String updatedFirstName, String updatedLastName, String updatedDateOfBirth) {
        Account account = dbUtil.findAccountByEmail(accountEmail);

        if (updatedPassword != null) {
            assertFalse("Password is updated.", account.getPassword().equals(DEFAULT_ENCODED_PASSWORD));
        }
        if (updatedFirstName != null) {
            assertThat("First Name is updated.", account.getFirstName(), is(updatedFirstName));
        }
        if (updatedLastName != null) {
            assertThat("Last Name is updated.", account.getLastName(), is(updatedLastName));
        }
        if (updatedDateOfBirth != null) {
            assertThat("Date of birth is updated.", account.getDateOfBirth(), is(DateTime.parse(updatedDateOfBirth)));
        }
    }
}
