package com.tim2.travelservice.integration.document;

import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.entity.DocumentType;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static com.tim2.travelservice.data.TokenData.PASSENGER_MULTIPART_HEADER;
import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_MULTIPART_HEADER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class UploadDocumentTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnCreatedAndDocumentIdAndSetActiveForAllOtherDocumentsFromUserToInactive() {
        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.uploadDocument(testRestTemplate, SYSTEM_ADMIN_MULTIPART_HEADER, ID_PICTURE_FILE_NAME, IMAGE_TYPE, VALID_DOCUMENT_TYPE);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long documentId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
        andPreviousDocumentIsSetToInactive(documentId);
        andDocumentIsStoredWithGivenData(documentId, SYSTEM_ADMINISTRATOR_EMAIL);
    }

    @Test
    public void shouldReturnCreatedAndDocumentId() {
        ResponseEntity<DynamicResponse> responseEntity = ApiFunctions.uploadDocument(testRestTemplate, PASSENGER_MULTIPART_HEADER, ID_PICTURE_FILE_NAME, IMAGE_TYPE, VALID_DOCUMENT_TYPE);

        CommonFunctions.thenCreatedIsReturned(responseEntity);
        Long documentId = CommonFunctions.andEntityIdIsReturned(responseEntity.getBody());
        andDocumentIsStoredWithGivenData(documentId, PASSENGER_EMAIL);
    }

    private void andPreviousDocumentIsSetToInactive(Long documentId) {
        List<Document> activeDocuments = dbUtil.findByAccountAndActive(AccountDataBuilder.buildAccount(SYSTEM_ADMINISTRATOR_ID), true);

        assertThat("There is only one active document after file upload.", activeDocuments.size(), is(1));
    }

    private void andDocumentIsStoredWithGivenData(Long documentId, String email) {
        Document document = dbUtil.findDocumentById(documentId);

        assertThat("Correct image type is saved.", document.getImageType(), is(IMAGE_TYPE));
        assertThat("Correct document type is saved.", document.getDocumentType(), is(DocumentType.SENIOR));
        assertTrue("Correct active is saved.", document.getActive());
        assertFalse("Correct verified is saved.", document.getVerified());
        assertThat("Correct account is saved.", document.getAccount().getEmail(), is(email));
    }
}
