package com.tim2.travelservice.integration.ticket;

import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.utils.ApiFunctions;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.utils.DbUtil;
import com.tim2.travelservice.web.dto.TicketDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.tim2.travelservice.data.TokenData.SYSTEM_ADMIN_HEADER;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/data/mysql/test-data.sql")
public class FindAllTicketsTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private DbUtil dbUtil;

    @Test
    public void shouldReturnOkAndAllTickets() {
        ResponseEntity<RestResponsePage<TicketDto>> responseEntity = ApiFunctions.findAllTickets(testRestTemplate, SYSTEM_ADMIN_HEADER);

        CommonFunctions.thenOkIsReturned(responseEntity);
        andTicketsAreReturned(responseEntity);
    }

    private void andTicketsAreReturned(ResponseEntity<RestResponsePage<TicketDto>> responseEntity) {
        List<TicketDto> ticketDtos = responseEntity.getBody().getContent();

        assertThat("All tickets are returned.", (long) ticketDtos.size(), is(dbUtil.findTicketsCount()));
    }
}
