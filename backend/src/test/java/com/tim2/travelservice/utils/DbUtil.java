package com.tim2.travelservice.utils;

import com.tim2.travelservice.entity.*;
import com.tim2.travelservice.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DbUtil {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CityLineRepository cityLineRepository;

    @Autowired
    private PriceListItemRepository priceListItemRepository;

    @Autowired
    private PriceListRepository priceListRepository;

    @Autowired
    private ScheduleItemRepository scheduleItemRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private BoughtTicketRepository boughtTicketRepository;

    @Autowired
    private LineStationRepository lineStationRepository;

    @Autowired
    private DocumentRepository documentRepository;

    public Account findAccountByEmail(String accountEmail) {
        return accountRepository.findByEmail(accountEmail).get();
    }

    public Optional<Account> findOneOptional(String accountEmail) {
        return accountRepository.findByEmail(accountEmail);
    }

    public Account findAccountById(Long registeredAccountId) {
        return accountRepository.findById(registeredAccountId).get();
    }

    public PriceListItem findOnePriceListItem(Long priceListItemId) {
        return priceListItemRepository.findById(priceListItemId).get();
    }

    public PriceList findOnePriceList(Long priceListId) {
        return priceListRepository.findById(priceListId).get();
    }

    public BoughtTicket findBoughTicketById(Long id) {
        return boughtTicketRepository.findById(id).get();
    }

    public Station findStationById(Long id) {
        return stationRepository.findById(id).get();
    }

    public Vehicle findVehicleById(Long id) {
        return vehicleRepository.findById(id).get();
    }

    public Long findStationsCount() {
        Long stationsCount = stationRepository.count();

        return stationsCount > 20 ? 20 : stationsCount;
    }

    public Long findVehiclesCount() {
        Long vehiclesCount = vehicleRepository.count();

        return vehiclesCount > 20 ? 20 : vehiclesCount;
    }

    public Optional<Station> findStationOptionalById(Long id) {
        return stationRepository.findById(id);
    }

    public Schedule findScheduleById(Long id) {
        return scheduleRepository.findById(id).get();
    }

    public List<ScheduleItem> findScheduleItemsByScheduleId(Long id) {
        return scheduleItemRepository.findByScheduleId(id);
    }

    public ScheduleItem findScheduleItemById(Long id) {
        return scheduleItemRepository.findById(id).get();
    }

    public Optional<ScheduleItem> findScheduleItemOptionalById(Long id) {
        return scheduleItemRepository.findById(id);
    }

    public Optional<Vehicle> findVehicleOptionalById(Long id) {
        return vehicleRepository.findById(id);
    }

    public Ticket findTicketById(Long ticketId) {
        return ticketRepository.findById(ticketId).get();
    }

    public Optional<Ticket> findTicketByIdOptional(Long ticketId) {
        return ticketRepository.findById(ticketId);
    }

    public long findTicketsCount() {
        Long ticketsCount = ticketRepository.count();

        return ticketsCount > 20 ? 20 : ticketsCount;
    }

    public Optional<Account> findAccountOptionalById(Long id) {
        return accountRepository.findById(id);
    }

    public CityLine findCityLineById(Long id) {
        return cityLineRepository.findById(id).get();
    }

    public LineStation findLineStationById(Long id) {
        return lineStationRepository.findById(id).get();
    }

    public List<LineStation> findLineStationsByCityLineId(Long id) {
        return lineStationRepository.findByCityLineId(id);
    }

    public Long findLineStationsLastId() {
        List<LineStation> lineStations = lineStationRepository.findAll();
        return lineStations.get(lineStations.size() - 1).getId();
    }

    public Optional<CityLine> findCityLineOptionalById(Long cityLineId) {
        return cityLineRepository.findById(cityLineId);
    }

    public Document findDocumentById(Long documentId) {
        return documentRepository.findById(documentId).get();
    }

    public List<Document> findByAccountAndActive(Account account, Boolean active) {
        return documentRepository.findByAccountAndActive(account, active);
    }

    public long findCityLinesCount() {
        long cityLinesCount = cityLineRepository.count();

        return cityLinesCount > 20 ? 20 : cityLinesCount;
    }
}
