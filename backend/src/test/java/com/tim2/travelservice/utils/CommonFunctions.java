package com.tim2.travelservice.utils;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.ReturnCode;
import com.tim2.travelservice.exception.ApiError;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static junit.framework.Assert.fail;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class CommonFunctions {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void thenOkIsReturned(ResponseEntity responseEntity) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

    public static void thenCreatedIsReturned(ResponseEntity responseEntity) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.CREATED));
    }

    public static void thenBadRequestIsReturned(ResponseEntity responseEntity) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    public static void thenNoContentIsReturned(ResponseEntity responseEntity) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.NO_CONTENT));
    }

    public static void thenForbiddenIsReturned(ResponseEntity responseEntity) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.FORBIDDEN));
    }

    public static void thenLoginPageIsReturned(ResponseEntity responseEntity) {
        boolean correctStatusCode = responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.FOUND;
        assertTrue("Http status code", correctStatusCode);

        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            assertTrue("Content Type is correct", responseEntity.getHeaders().get("Content-Type").get(0).contains("html"));
        } else {
            assertTrue("Good redirect URI", responseEntity.getHeaders().getLocation().getPath().contains("login"));
        }
    }

    public static void thenNotFoundIsReturned(ResponseEntity responseEntity, String error) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
        validateError(error, responseEntity);
    }

    public static void thenBadRequestIsReturned(ResponseEntity responseEntity, String error) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        validateError(error, responseEntity);
    }

    private static void validateError(String explanationErrorMessage, ResponseEntity responseEntity) {
        if (responseEntity.getBody() instanceof DynamicResponse) {
            processDynamicResponseErrorValidation(explanationErrorMessage, (DynamicResponse) responseEntity.getBody());
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                ApiError apiError = objectMapper.readValue(responseEntity.getBody().toString(), ApiError.class);
                assertThat("Error message", apiError.getData().get(RestApiConstants.EXPLANATION), is(explanationErrorMessage));
            } catch (IOException e) {
                e.printStackTrace();
                fail("Unexpected exception, make test to fail.");
            }
        }
    }

    private static void processDynamicResponseErrorValidation(String error, DynamicResponse dynamicResponse) {
        Map<String, String> data = (Map<String, String>) dynamicResponse.get(RestApiConstants.DATA);

        if (data != null) {
            assertThat("Error message", data.get(RestApiConstants.EXPLANATION), is(error));
        }
    }


    public static void thenBadRequestIsReturned(ResponseEntity responseEntity, ReturnCode returnCode) {
        assertThat("Http status code", responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        validateError(responseEntity, returnCode);
    }

    private static void validateError(ResponseEntity responseEntity, ReturnCode returnCode) {
        if (responseEntity.getBody() instanceof DynamicResponse) {
            processDynamicResponseErrorValidation(returnCode, (DynamicResponse) responseEntity.getBody());
        } else {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                ApiError apiError = objectMapper.readValue(responseEntity.getBody().toString(), ApiError.class);
                assertThat("Return code", apiError.getCode(), is(returnCode.getCode()));
            } catch (IOException e) {
                e.printStackTrace();
                assertThat("Unexpected exception, make test to fail.", true, is(false));
            }
        }

    }

    private static void processDynamicResponseErrorValidation(ReturnCode returnCode, DynamicResponse dynamicResponse) {
        assertThat("Return code", dynamicResponse.get(RestApiConstants.CODE), is(returnCode.getCode()));
    }

    public static Long andEntityIdIsReturned(DynamicResponse body) {
        assertThat(RestApiConstants.ID + " is not null.", body.get(RestApiConstants.ID), notNullValue());

        return ((Integer) body.get(RestApiConstants.ID)).longValue();
    }

    public static <T> List<T> getContentListFromPageObject(MvcResult mvcResult, TypeReference<List<T>> typeReference) throws IOException {
        HashMap<String, Object> body = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<HashMap<String, Object>>() {
        });

        String pageContent = objectMapper.writeValueAsString(body.get("content"));

        return objectMapper.readValue(pageContent, typeReference);
    }

    public static List<Long> andEntityIdsAreReturned(List<DynamicResponse> body) {
        return body.stream().map(CommonFunctions::andEntityIdIsReturned).collect(Collectors.toList());
    }
}
