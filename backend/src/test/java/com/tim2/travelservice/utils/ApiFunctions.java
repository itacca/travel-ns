package com.tim2.travelservice.utils;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.data.model.RestResponsePage;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.web.dto.*;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import java.util.List;

public class ApiFunctions {

    public static ResponseEntity getLoggedUser(TestRestTemplate testRestTemplate, HttpHeaders headers) {
        String url = String.format("%s/me", RestApiEndpoints.ACCOUNT);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity findUserByEmail(TestRestTemplate testRestTemplate, HttpHeaders headers, String email) {
        String url = String.format("%s?%s=%s", RestApiEndpoints.ACCOUNT, RestApiRequestParams.EMAIL, email);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity updateLoggedUser(TestRestTemplate testRestTemplate, HttpHeaders headers, AccountDto accountDto) {
        String url = String.format("%s", RestApiEndpoints.ACCOUNT);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(accountDto, headers), String.class);
    }

    public static ResponseEntity<DynamicResponse> registerAccount(TestRestTemplate testRestTemplate, AccountDto accountDto) {
        String url = String.format("%s", RestApiEndpoints.ACCOUNTS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(accountDto), DynamicResponse.class);
    }

    public static ResponseEntity<DynamicResponse> registerTravelAdmin(TestRestTemplate testRestTemplate, HttpHeaders headers, AccountDto accountDto) {
        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(accountDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity<DynamicResponse> registerAccount(TestRestTemplate testRestTemplate, DynamicResponse accountDto) {
        String url = String.format("%s", RestApiEndpoints.ACCOUNTS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(accountDto), DynamicResponse.class);
    }

    public static ResponseEntity<RestResponsePage<CityLineDto>> getCityLinesByType(TestRestTemplate testRestTemplate, HttpHeaders headers, String cityLineType) {
        String url = String.format("%s?type=%s", RestApiEndpoints.CITY_LINES, cityLineType);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<CityLineDto>>() {
        });
    }

    public static ResponseEntity<DynamicResponse> registerTravelAdmin(TestRestTemplate testRestTemplate, HttpHeaders headers, DynamicResponse accountDto) {
        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(accountDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity<RestResponsePage<AccountDto>> findByRoleId(TestRestTemplate testRestTemplate, HttpHeaders headers, Long roleId) {
        String url = String.format("%s?%s=%d", RestApiEndpoints.ACCOUNTS, RestApiRequestParams.ROLE_ID, roleId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<AccountDto>>() {
        });
    }

    public static ResponseEntity findByRoleIdNoPage(TestRestTemplate testRestTemplate, HttpHeaders headers, Long roleId) {
        String url = String.format("%s?%s=%d", RestApiEndpoints.ACCOUNTS, RestApiRequestParams.ROLE_ID, roleId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity deleteTravelAdminById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, id);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity updateLoggedUser(TestRestTemplate testRestTemplate, HttpHeaders headers, DynamicResponse accountDto) {
        String url = String.format("%s", RestApiEndpoints.ACCOUNT);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(accountDto, headers), String.class);
    }

    public static ResponseEntity<DynamicResponse> updatePriceListItem(TestRestTemplate testRestTemplate, HttpHeaders headers, PriceListItemDto priceListItemDto) {
        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEM);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(priceListItemDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity deletePriceListItem(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/{id}", RestApiEndpoints.PRICE_LIST_ITEM);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class, id);
    }

    public static ResponseEntity<DynamicResponse> addPriceListItem(TestRestTemplate testRestTemplate, HttpHeaders headers, DynamicResponse priceListItemDto) {
        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEMS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(priceListItemDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity<DynamicResponse> addPriceListItem(TestRestTemplate testRestTemplate, HttpHeaders headers, PriceListItemDto priceListItemDto) {
        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEMS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(priceListItemDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity<DynamicResponse> addPriceList(TestRestTemplate testRestTemplate, HttpHeaders headers, PriceListDto priceListDto) {
        String url = String.format("%s", RestApiEndpoints.PRICE_LISTS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(priceListDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity updatePriceList(TestRestTemplate testRestTemplate, HttpHeaders headers, PriceListDto priceListDto) {
        String url = String.format("%s", RestApiEndpoints.PRICE_LIST);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(priceListDto, headers), String.class);
    }

    public static ResponseEntity deletePriceList(TestRestTemplate testRestTemplate, HttpHeaders headers, Long priceListId) {
        String url = String.format("%s/{id}", RestApiEndpoints.PRICE_LIST);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class, priceListId);
    }

    public static ResponseEntity<RestResponsePage<BoughtTicketDto>> findBoughtTicketsForCurrentUser(TestRestTemplate testRestTemplate, HttpHeaders headers) {
        String url = String.format("%s", RestApiEndpoints.BOUGHT_TICKETS);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<BoughtTicketDto>>() {
        });
    }

    public static ResponseEntity<RestResponsePage<PriceListItemDto>> findPriceListItemsByPriceListId(TestRestTemplate testRestTemplate, HttpHeaders headers, String priceListId) {
        String url = String.format("%s?%s=%s", RestApiEndpoints.PRICE_LIST_ITEMS, RestApiRequestParams.PRICE_LIST_ID, priceListId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<PriceListItemDto>>() {
        });
    }

    public static ResponseEntity findPriceListItemsByPriceListIdNoPage(TestRestTemplate testRestTemplate, HttpHeaders headers, String priceListId) {
        String url = String.format("%s?%s=%s", RestApiEndpoints.PRICE_LIST_ITEMS, RestApiRequestParams.PRICE_LIST_ID, priceListId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<RestResponsePage<PriceListItemDto>> findPriceListItemsByPriceListIdAndTicketId(TestRestTemplate testRestTemplate, HttpHeaders headers, String priceListId, String ticketId) {
        String url = String.format("%s?%s=%s&%s=%s", RestApiEndpoints.PRICE_LIST_ITEMS, RestApiRequestParams.PRICE_LIST_ID, priceListId, RestApiRequestParams.TICKET_ID, ticketId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<PriceListItemDto>>() {
        });
    }

    public static ResponseEntity findPriceListItemsByPriceListIdAndTicketIdNoPage(TestRestTemplate testRestTemplate, HttpHeaders headers, String priceListId, String ticketId) {
        String url = String.format("%s?%s=%s&%s=%s", RestApiEndpoints.PRICE_LIST_ITEMS, RestApiRequestParams.PRICE_LIST_ID, priceListId, RestApiRequestParams.TICKET_ID, ticketId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity findPriceListById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long priceListId) {
        String url = String.format("%s/%d", RestApiEndpoints.PRICE_LIST, priceListId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<RestResponsePage<VehicleDto>> findAllVehicles(TestRestTemplate testRestTemplate, HttpHeaders headers) {
        String url = String.format("%s", RestApiEndpoints.VEHICLES);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<VehicleDto>>() {
        });
    }

    public static ResponseEntity deleteVehicleById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/%d", RestApiEndpoints.VEHICLE, id);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<DynamicResponse> addVehicle(TestRestTemplate testRestTemplate, HttpHeaders headers, VehicleDto vehicleDto) {
        String url = String.format("%s", RestApiEndpoints.VEHICLES);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(vehicleDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity updateVehicle(TestRestTemplate testRestTemplate, HttpHeaders headers, VehicleDto vehicleDto) {
        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(vehicleDto, headers), String.class);
    }

    public static ResponseEntity validateBoughtTicketById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/validate/%d", RestApiEndpoints.BOUGHT_TICKET, id);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity findBoughtTicketById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/%d", RestApiEndpoints.BOUGHT_TICKET, id);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<DynamicResponse> addStation(TestRestTemplate testRestTemplate, HttpHeaders headers, StationDto stationDto) {
        String url = String.format("%s", RestApiEndpoints.STATIONS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(stationDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity findStationById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/%d", RestApiEndpoints.STATION, id);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<RestResponsePage<StationDto>> findAllStations(TestRestTemplate testRestTemplate, HttpHeaders headers) {
        String url = String.format("%s", RestApiEndpoints.STATIONS);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<StationDto>>() {
        });
    }

    public static ResponseEntity updateStation(TestRestTemplate testRestTemplate, HttpHeaders headers, StationDto stationDto) {
        String url = String.format("%s", RestApiEndpoints.STATION);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(stationDto, headers), String.class);
    }

    public static ResponseEntity deleteStationById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/%d", RestApiEndpoints.STATION, id);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<RestResponsePage<CityLineDto>> findAllCityLines(TestRestTemplate testRestTemplate, HttpHeaders header) {
        String url = String.format("%s", RestApiEndpoints.CITY_LINES);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(header), new ParameterizedTypeReference<RestResponsePage<CityLineDto>>() {
        });
    }

    public static ResponseEntity findTicketById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/%d", RestApiEndpoints.TICKET, id);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);

    }

    public static ResponseEntity updateTicket(TestRestTemplate testRestTemplate, HttpHeaders headers, TicketDto ticketDto) {
        String url = String.format("%s", RestApiEndpoints.TICKET);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(ticketDto, headers), String.class);
    }

    public static ResponseEntity deleteTicketById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long ticketId) {
        String url = String.format("%s/%d", RestApiEndpoints.TICKET, ticketId);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<RestResponsePage<TicketDto>> findAllTickets(TestRestTemplate testRestTemplate, HttpHeaders headers) {
        String url = String.format("%s", RestApiEndpoints.TICKETS);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<TicketDto>>() {
        });
    }

    public static ResponseEntity addTicket(TestRestTemplate testRestTemplate, HttpHeaders headers, TicketDto ticketDto) {
        String url = String.format("%s", RestApiEndpoints.TICKETS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(ticketDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity<DynamicResponse> createCityLine(TestRestTemplate testRestTemplate, HttpHeaders headers, CityLineDto cityLineDto) {
        String url = String.format("%s", RestApiEndpoints.CITY_LINES);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(cityLineDto, headers), DynamicResponse.class);
    }

    public static ResponseEntity createLineStations(TestRestTemplate testRestTemplate, HttpHeaders headers, List<LineStationDto> lineStationDtos) {
        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(lineStationDtos, headers), String.class);
    }

    public static ResponseEntity updateCityLine(TestRestTemplate testRestTemplate, HttpHeaders headers, CityLineDto cityLineDto) {
        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(cityLineDto, headers), String.class);
    }

    public static ResponseEntity updateLineStations(TestRestTemplate testRestTemplate, HttpHeaders headers, List<LineStationDto> lineStationDtos) {
        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(lineStationDtos, headers), String.class);
    }

    public static ResponseEntity findCityLineById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, id);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity findLineStationsByCityLineId(TestRestTemplate testRestTemplate, HttpHeaders headers, Long cityLineId) {
        String url = String.format("%s?%s=%d", RestApiEndpoints.LINE_STATIONS, RestApiRequestParams.CITY_LINE_ID, cityLineId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity deleteCityLineById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long id) {
        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, id);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<DynamicResponse> createSchedule(TestRestTemplate testRestTemplate, HttpHeaders passengerHeader, ScheduleDto scheduleDto) {
        String url = String.format("%s", RestApiEndpoints.SCHEDULES);

        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(scheduleDto, passengerHeader),
                DynamicResponse.class);
    }

    public static ResponseEntity getAllSchedulesByCityLineId(TestRestTemplate testRestTemplate, HttpHeaders header, Long cityLineId) {
        String url = String.format("%s?%s=%d", RestApiEndpoints.SCHEDULES, RestApiRequestParams.CITY_LINE_ID, cityLineId);

        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(header), String.class);
    }

    public static ResponseEntity getScheduleByCityLineId(TestRestTemplate testRestTemplate, HttpHeaders header, Long cityLineId) {
        String url = String.format("%s?%s=%d", RestApiEndpoints.SCHEDULE, RestApiRequestParams.CITY_LINE_ID, cityLineId);

        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(header), String.class);
    }

    public static ResponseEntity updateSchedule(TestRestTemplate testRestTemplate, HttpHeaders header, ScheduleDto scheduleDto) {
        String url = String.format("%s", RestApiEndpoints.SCHEDULE);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(scheduleDto, header), String.class);
    }

    public static ResponseEntity getScheduleByCityLineIdAndScheduleType(TestRestTemplate testRestTemplate, HttpHeaders header,
                                                                        Long cityLineId, ScheduleType scheduleType) {
        String url = String.format("%s?%s=%d&%s=%s", RestApiEndpoints.SCHEDULE, RestApiRequestParams.CITY_LINE_ID, cityLineId,
                RestApiRequestParams.SCHEDULE_TYPE, scheduleType.toString().toLowerCase());

        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(header), String.class);
    }

    public static ResponseEntity findAllScheduleItemsByScheduleId(TestRestTemplate testRestTemplate, HttpHeaders header,
                                                                  Long scheduleId) {
        String url = String.format("%s?%s=%d", RestApiEndpoints.SCHEDULE_ITEMS, RestApiRequestParams.SCHEDULE_ID, scheduleId);

        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(header), String.class);

    }

    public static ResponseEntity addScheduleItems(TestRestTemplate testRestTemplate, HttpHeaders headers,
                                                  List<ScheduleItemDto> scheduleItemDtos) {
        String url = String.format("%s", RestApiEndpoints.SCHEDULE_ITEMS);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(scheduleItemDtos, headers), String.class);
    }

    public static ResponseEntity deleteScheduleItemById(TestRestTemplate testRestTemplate, HttpHeaders headers, Long scheduleItemId) {
        String url = String.format("%s/%d", RestApiEndpoints.SCHEDULE_ITEM, scheduleItemId);
        return testRestTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<DynamicResponse> uploadDocument(TestRestTemplate testRestTemplate, HttpHeaders headers, String fileName, String imageType, String documentType) {
        LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("file", new ClassPathResource(fileName));

        String url = String.format("%s?%s=%s&%s=%s", RestApiEndpoints.DOCUMENTS, RestApiRequestParams.IMAGE_TYPE, imageType, RestApiRequestParams.DOCUMENT_TYPE, documentType);
        return testRestTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(parameters, headers), DynamicResponse.class);
    }

    public static ResponseEntity findImageBlobByDocumentId(TestRestTemplate testRestTemplate, HttpHeaders headers, Long documentId) {
        String url = String.format("%s/%d", RestApiEndpoints.DOCUMENT, documentId);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public static ResponseEntity<RestResponsePage<Document>> findDocumentsByActiveAndVerified(TestRestTemplate testRestTemplate, HttpHeaders headers, Boolean active, Boolean verified) {
        String url = String.format("%s?%s=%b&%s=%b", RestApiEndpoints.DOCUMENTS, RestApiRequestParams.ACTIVE, active, RestApiRequestParams.VERIFIED, verified);
        return testRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<RestResponsePage<Document>>() {
        });
    }

    public static ResponseEntity verifyDocument(TestRestTemplate testRestTemplate, HttpHeaders headers, DocumentDto documentDto) {
        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        return testRestTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(documentDto, headers), String.class);
    }
}
