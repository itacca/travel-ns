package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.PriceListService;
import com.tim2.travelservice.validator.dto.PriceListDtoValidator;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class PriceListControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private PriceListService priceListServiceMock;

    @MockBean
    private PriceListDtoValidator priceListDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnNoContentWhenPriceListIsSuccessfullyUpdated() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        doNothing().when(priceListDtoValidatorMock).validateUpdateRequest(priceListDto);
        doNothing().when(priceListServiceMock).updateBasicInformation(priceListDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(priceListDtoValidatorMock, times(1)).validateUpdateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).updateBasicInformation(priceListDto);
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void updateShouldReturnForbiddenWhenRoleIsPassenger() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListDtoValidatorMock, times(0)).validateUpdateRequest(any(PriceListDto.class));
        verify(priceListServiceMock, times(0)).updateBasicInformation(any(PriceListDto.class));
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void updateShouldReturnForbiddenWhenRoleIsVerifier() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListDtoValidatorMock, times(0)).validateUpdateRequest(any(PriceListDto.class));
        verify(priceListServiceMock, times(0)).updateBasicInformation(any(PriceListDto.class));
    }

    @Test
    @WithMockUser
    public void updateShouldReturnForbiddenWhenRoleIsGuest() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListDtoValidatorMock, times(0)).validateUpdateRequest(any(PriceListDto.class));
        verify(priceListServiceMock, times(0)).updateBasicInformation(any(PriceListDto.class));
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestIfEndDateIsInThePast() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_PAST_DATE);

        doNothing().when(priceListDtoValidatorMock).validateUpdateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE))
                .when(priceListServiceMock).updateBasicInformation(priceListDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE)));

        verify(priceListDtoValidatorMock, times(1)).validateUpdateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).updateBasicInformation(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestIfDatesAreInvalid() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_END_DATE, PRICE_LIST_PAST_DATE);

        doNothing().when(priceListDtoValidatorMock).validateUpdateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.INVALID_DATE_INTERVAL))
                .when(priceListServiceMock).updateBasicInformation(priceListDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.INVALID_DATE_INTERVAL)));

        verify(priceListDtoValidatorMock, times(1)).validateUpdateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).updateBasicInformation(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestIfIdIsNotProvided() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_END_DATE, PRICE_LIST_PAST_DATE);

        doNothing().when(priceListDtoValidatorMock).validateUpdateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(priceListServiceMock).updateBasicInformation(priceListDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(priceListDtoValidatorMock, times(1)).validateUpdateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).updateBasicInformation(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void deleteByIdShouldReturnNoContentWhenPriceListIsSuccessfullyDeleted() throws Exception {
        doNothing().when(priceListServiceMock).deleteById(3L);

        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(priceListServiceMock, times(1)).deleteById(3L);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void deleteByIdShouldReturnNotFoundWhenPriceListIsNotInDatabase() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)))
                .when(priceListServiceMock).deleteById(3L);

        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID))));

        verify(priceListServiceMock, times(1)).deleteById(3L);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void deleteByIdShouldReturnForbiddenWhenRoleIsVerifier() throws Exception {
        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListServiceMock, times(0)).deleteById(3L);
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void deleteByIdShouldReturnForbiddenWhenRoleIsPassenger() throws Exception {
        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListServiceMock, times(0)).deleteById(3L);
    }

    @Test
    @WithMockUser
    public void deleteByIdShouldReturnLoginPageWhenRoleIsGuest() throws Exception {
        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListServiceMock, times(0)).deleteById(3L);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void deleteByIdShouldReturnBadRequestWhenPriceListDoesNotExistInDatabase() throws Exception {
        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)))
                .when(priceListServiceMock).deleteById(3L);

        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID))));

        verify(priceListServiceMock, times(1)).deleteById(3L);
    }

    @Test
    public void findByIdShouldReturnOkWhenPriceListIsSuccessfullyFound() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        when(priceListServiceMock.findById(PRICE_LIST_ID)).thenReturn(priceListDto);

        String url = String.format("%s/%d", RestApiEndpoints.PRICE_LIST, PRICE_LIST_ID);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(priceListServiceMock, times(1)).findById(PRICE_LIST_ID);
        andExpectedPriceListIsReturned(mvcResult);
    }

    @Test
    public void findByIdShouldReturnNotFoundWhenPriceListIsNotInDatabase() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)))
                .when(priceListServiceMock).findById(PRICE_LIST_ID);

        String url = String.format("%s/%d", RestApiEndpoints.PRICE_LIST, PRICE_LIST_ID);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID))));

        verify(priceListServiceMock, times(1)).findById(PRICE_LIST_ID);
    }

    private void andExpectedPriceListIsReturned(MvcResult mvcResult) throws IOException {
        PriceList priceList = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), PriceList.class);

        assertThat("Price list id", priceList.getId(), is(PRICE_LIST_ID));
        assertThat("Start date", priceList.getStartDate(), is(DateTime.parse(PRICE_LIST_START_DATE)));
        assertThat("End date", priceList.getEndDate(), is(DateTime.parse(PRICE_LIST_END_DATE)));
    }
}