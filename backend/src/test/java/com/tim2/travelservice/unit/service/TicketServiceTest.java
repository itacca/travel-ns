package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.TicketService;
import com.tim2.travelservice.store.TicketStore;
import com.tim2.travelservice.validator.db.TicketDbValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.TicketDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class TicketServiceTest {

    @Autowired
    private TicketService ticketService;

    @MockBean
    private TicketStore ticketStore;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private TicketDbValidator ticketDbValidator;

    @Test
    public void createShouldReturnTicketIdAfterSuccessfulInsert() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(null, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        CityLine cityLine = CityLineDataBuilder.buildCityLine(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        Ticket ticket = TicketDataBuilder.buildTicket(null, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLine);
        Ticket ticketWithId = TicketDataBuilder.buildTicket(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, cityLine);

        when(modelMapperMock.map(ticketDto, Ticket.class)).thenReturn(ticket);
        when(ticketStore.save(ticket)).thenReturn(ticketWithId);

        DynamicResponse dynamicResponse = ticketService.addTicket(ticketDto);

        verify(modelMapperMock, times(1)).map(ticketDto, Ticket.class);
        verify(ticketStore, times(1)).save(ticket);
        assertThat("Ticket id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void findByIdShouldThrowNotFoundWithExplanationExceptionWhenGettingNonExistingTicket() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID)))
                .when(ticketDbValidator).validateFindByIdRequest(1L);

        ticketService.findById(1L);

        verify(ticketDbValidator, times(1)).validateFindByIdRequest(1L);
    }

    @Test
    public void findByIdShouldReturnExistingTicket() {
        Ticket ticket = TicketDataBuilder.buildTicket(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, null);

        when(ticketDbValidator.validateFindByIdRequest(1L)).thenReturn(ticket);

        TicketDto ticketDto = ticketService.findById(1L);

        verify(ticketDbValidator, times(1)).validateFindByIdRequest(1L);
        andExpectedTicketIsReturned(ticketDto);
    }

    @Test
    public void findAllShouldReturnAllTickets() {
        Page<Ticket> ticketPage = initTicketPage();

        when(ticketStore.findAll(PageRequest.of(0, 20))).thenReturn(ticketPage);

        Page<TicketDto> ticketDtoPage = ticketService.findAll(PageRequest.of(0, 20));

        verify(ticketStore, times(1)).findAll(PageRequest.of(0, 20));
        assertThat("All tickets are returned.", ticketDtoPage.getContent().size(), is(ticketPage.getContent().size()));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateShouldThrowNotFoundWithExplanationExceptionWhenUpdatingNonExistingTicket() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID)))
                .when(ticketDbValidator).validateUpdateTicketRequest(ticketDto);

        ticketService.updateTicket(ticketDto);

        verify(ticketDbValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(modelMapperMock, times(0)).map(any(TicketDto.class), eq(Ticket.class));
        verify(ticketStore, times(0)).save(any(Ticket.class));
    }

    @Test
    public void updateShouldUpdateExistingTicket() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);
        Ticket existingTicket = TicketDataBuilder.buildTicket(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, null);
        Ticket updatedTicket = TicketDataBuilder.buildTicket(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, null);

        when(ticketDbValidator.validateUpdateTicketRequest(ticketDto)).thenReturn(existingTicket);
        when(modelMapperMock.map(ticketDto, Ticket.class)).thenReturn(updatedTicket);

        ticketService.updateTicket(ticketDto);

        verify(ticketDbValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(modelMapperMock, times(1)).map(ticketDto, Ticket.class);
        verify(ticketStore, times(1)).save(updatedTicket);
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void deleteShouldThrowNotFoundWithExplanationExceptionWhenDeletingNonExistingTicket() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID)))
                .when(ticketDbValidator).validateDeleteTicketRequest(1L);

        ticketService.deleteTicket(1L);

        verify(ticketDbValidator, times(1)).validateDeleteTicketRequest(1L);
        verify(ticketStore, times(0)).delete(any(Ticket.class));
    }

    @Test
    public void deleteShouldDeleteExistingStation() {
        Ticket existingTicket = TicketDataBuilder.buildTicket(FIRST_TICKET_ID, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, null);

        when(ticketDbValidator.validateDeleteTicketRequest(1L)).thenReturn(existingTicket);

        ticketService.deleteTicket(1L);

        verify(ticketDbValidator, times(1)).validateDeleteTicketRequest(1L);
        verify(ticketStore, times(1)).delete(existingTicket);
    }

    private void andExpectedTicketIsReturned(TicketDto ticketDto) {
        assertThat("Correct id is returned.", ticketDto.getId(), is(1L));
        assertThat("Correct ticket duration is returned.", ticketDto.getTicketDuration(), is(FIRST_TICKET_DURATION));
        assertThat("Correct ticket type is returned.", ticketDto.getTicketType(), is(FIRST_TICKET_TYPE));
    }

    private Page<Ticket> initTicketPage() {
        Ticket ticket1 = TicketDataBuilder.buildTicket(1L, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, null);
        Ticket ticket2 = TicketDataBuilder.buildTicket(2L, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, null);
        Ticket ticket3 = TicketDataBuilder.buildTicket(3L, FIRST_TICKET_DURATION, FIRST_TICKET_TYPE, null);

        return new PageImpl<>(Arrays.asList(ticket1, ticket2, ticket3));
    }


}
