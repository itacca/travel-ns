package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.data.TestData;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.entity.Role;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.AccountService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.validator.dto.AccountDtoValidator;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class AccountsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private AccountService accountServiceMock;

    @MockBean
    private AccountDtoValidator accountDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenAccountIdIsProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(1L, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenEmailIsNotProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, null, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenEmailIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, "", ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenInvalidEmailFormat() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_INVALID_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenPasswordIsNotProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, null, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenPasswordIsLessThanEightCharacters() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_SHORT_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenFirstNameIsNull() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, null, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenFirstNameIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, "", ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenLastNameIsNull() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, null, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenLastNameIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, "", ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenDateOfBirthIsInTheFuture() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_FUTURE_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST)));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenValidatedIsProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, false, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.VALIDATED)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.VALIDATED))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenIdValidatedIsProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, false, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID_VALIDATED)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID_VALIDATED))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenRolesIsNotEmptyCollection() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, Arrays.asList(new Role(1L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES)))
                .when(accountDtoValidatorMock).validateRegistrationRequest(accountDto);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnBadRequestWhenAccountWithGivenEmailAlreadyExists() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(TestData.SYSTEM_ADMINISTRATOR_EMAIL);

        doNothing().when(accountDtoValidatorMock).validateRegistrationRequest(any(AccountDto.class));
        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)))
                .when(accountServiceMock).register(any(AccountDto.class));

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(any(AccountDto.class));
        verify(accountServiceMock, times(1)).register(any(AccountDto.class));
    }

    @Test
    public void registerAccountShouldReturnCreatedWhenUserIsSuccessfullyRegistered() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(TestData.ACCOUNT_REGISTRATION_EMAIL, TestData.ACCOUNT_REGISTRATION_PASSWORD, TestData.ACCOUNT_REGISTRATION_FIRST_NAME, TestData.ACCOUNT_REGISTRATION_LAST_NAME, TestData.ACCOUNT_REGISTRATION_DATE_OF_BIRTH);
        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 5L);

        doNothing().when(accountDtoValidatorMock).validateRegistrationRequest(any(AccountDto.class));
        when(accountServiceMock.register(any(AccountDto.class))).thenReturn(dynamicResponse);

        mockMvc.perform(post(RestApiEndpoints.ACCOUNTS)
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(5)));

        verify(accountDtoValidatorMock, times(1)).validateRegistrationRequest(any(AccountDto.class));
        verify(accountServiceMock, times(1)).register(any(AccountDto.class));
    }

    @Test
    public void registerTravelAdminShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(1L, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))).when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void registerTravelAdminShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(1L, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void registerTravelAdminShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(1L, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenAccountIdIsProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(1L, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenEmailIsNotProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, null, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenEmailIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, "", ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);

        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenInvalidEmailFormat() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_INVALID_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenFirstNameIsNull() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, null, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenFirstNameIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, "", ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenLastNameIsNull() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, null, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenLastNameIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, "", ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenDateOfBirthIsInTheFuture() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_FUTURE_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST)));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenValidatedIsProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, false, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.VALIDATED)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.VALIDATED))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenPasswordIsProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.PASSWORD)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.PASSWORD))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenIdValidatedIsProvided() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, false, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID_VALIDATED)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID_VALIDATED))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenRolesIsNotEmptyCollection() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, Arrays.asList(new Role(1L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES)))
                .when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(accountDto);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(accountDto);
        verify(accountServiceMock, times(0)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnBadRequestWhenAccountWithGivenEmailAlreadyExists() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(TestData.SYSTEM_ADMINISTRATOR_EMAIL, TestData.ACCOUNT_REGISTRATION_FIRST_NAME, TestData.ACCOUNT_REGISTRATION_LAST_NAME, TestData.ACCOUNT_REGISTRATION_DATE_OF_BIRTH);

        doNothing().when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(any(AccountDto.class));
        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)))
                .when(accountServiceMock).registerAdmin(any(AccountDto.class));

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL))));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(any(AccountDto.class));
        verify(accountServiceMock, times(1)).registerAdmin(any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void registerTravelAdminShouldReturnCreatedWhenUserIsSuccessfullyRegistered() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(TestData.ACCOUNT_REGISTRATION_EMAIL, TestData.ACCOUNT_REGISTRATION_FIRST_NAME, TestData.ACCOUNT_REGISTRATION_LAST_NAME, TestData.ACCOUNT_REGISTRATION_DATE_OF_BIRTH);
        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 5L);

        doNothing().when(accountDtoValidatorMock).validateTravelAdminRegistrationRequest(any(AccountDto.class));
        when(accountServiceMock.registerAdmin(any(AccountDto.class))).thenReturn(dynamicResponse);

        String url = String.format("%s/travel-admins", RestApiEndpoints.ACCOUNTS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(5)));

        verify(accountDtoValidatorMock, times(1)).validateTravelAdminRegistrationRequest(any(AccountDto.class));
        verify(accountServiceMock, times(1)).registerAdmin(any(AccountDto.class));
    }

    @Test
    public void findTravelAdminsShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s?%s=%d", RestApiEndpoints.ACCOUNTS, RestApiRequestParams.ROLE_ID, ADMINISTRATOR_ROLE_ID);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound())
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void findTravelAdminsShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        String url = String.format("%s?%s=%d", RestApiEndpoints.ACCOUNTS, RestApiRequestParams.ROLE_ID, ADMINISTRATOR_ROLE_ID);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void findTravelAdminsShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        String url = String.format("%s?%s=%d", RestApiEndpoints.ACCOUNTS, RestApiRequestParams.ROLE_ID, ADMINISTRATOR_ROLE_ID);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void findTravelAdminsShouldReturnForbiddenWhenUserIsTravelAdmin() throws Exception {
        String url = String.format("%s?%s=%d", RestApiEndpoints.ACCOUNTS, RestApiRequestParams.ROLE_ID, ADMINISTRATOR_ROLE_ID);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void findTravelAdminsShouldReturnOkAndTravelAdmins() throws Exception {
        Page<AccountDto> accountDtos = initTestData();

        when(accountServiceMock.findByRoleId(ADMINISTRATOR_ROLE_ID, PageRequest.of(0, 20))).thenReturn(accountDtos);

        String url = String.format("%s?%s=%d", RestApiEndpoints.ACCOUNTS, RestApiRequestParams.ROLE_ID, ADMINISTRATOR_ROLE_ID);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(accountServiceMock, times(1)).findByRoleId(ADMINISTRATOR_ROLE_ID, PageRequest.of(0, 20));
        andExpectedTravelAdminsAreReturned(mvcResult);
    }

    private void andExpectedTravelAdminsAreReturned(MvcResult mvcResult) throws IOException {
        List<AccountDto> accountDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<AccountDto>>() {
        });
        accountDtos.forEach(accountDto -> assertThat("Role is correct.", accountDto.getRoles().get(0).getId(), is(2L)));
    }

    private Page<AccountDto> initTestData() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(Role.builder().id(2L).build());
        AccountDto accountDto1 = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, roles);
        AccountDto accountDto2 = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, roles);
        AccountDto accountDto3 = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, roles);

        return new PageImpl<>(Arrays.asList(accountDto1, accountDto2, accountDto3));
    }
}
