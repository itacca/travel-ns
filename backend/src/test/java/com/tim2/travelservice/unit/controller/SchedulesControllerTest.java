package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.ScheduleDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.ScheduleService;
import com.tim2.travelservice.validator.dto.ScheduleDtoValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class SchedulesControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private ScheduleService scheduleServiceMock;

    @MockBean
    private ScheduleDtoValidator scheduleDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createScheduleShouldReturnBadRequestWhenScheduleIdIsNotNull() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        scheduleDto.setId(SCHEDULE_ID);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(scheduleDtoValidatorMock).validateNewScheduleRequest(scheduleDto);

        String url = String.format("%s", RestApiEndpoints.SCHEDULES);
        mockMvc.perform(post(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        verify(scheduleDtoValidatorMock, times(1)).validateNewScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(0)).create(any(ScheduleDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createScheduleShouldReturnBadRequestWhenScheduleTypeIsNull() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        scheduleDto.setScheduleType(null);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.SCHEDULE_TYPE)))
                .when(scheduleDtoValidatorMock).validateNewScheduleRequest(scheduleDto);

        String url = String.format("%s", RestApiEndpoints.SCHEDULES);
        mockMvc.perform(post(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        verify(scheduleDtoValidatorMock, times(1)).validateNewScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(0)).create(any(ScheduleDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createScheduleShouldReturnBadRequestWhenCityLineIsNull() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        scheduleDto.setCityLine(null);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE)))
                .when(scheduleDtoValidatorMock).validateNewScheduleRequest(scheduleDto);

        String url = String.format("%s", RestApiEndpoints.SCHEDULES);
        mockMvc.perform(post(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        verify(scheduleDtoValidatorMock, times(1)).validateNewScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(0)).create(any(ScheduleDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createScheduleShouldReturnBadRequestWhenCityLineIdIsNull() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        scheduleDto.getCityLine().setId(null);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(scheduleDtoValidatorMock).validateNewScheduleRequest(scheduleDto);

        String url = String.format("%s", RestApiEndpoints.SCHEDULES);
        mockMvc.perform(post(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        verify(scheduleDtoValidatorMock, times(1)).validateNewScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(0)).create(any(ScheduleDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createScheduleShouldReturnCreatedAndAddNewSchedule() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        DynamicResponse dynamicResponse = new DynamicResponse();
        dynamicResponse.put(RestApiConstants.ID, 1L);

        doNothing().when(scheduleDtoValidatorMock).validateNewScheduleRequest(any(ScheduleDto.class));
        when(scheduleServiceMock.create(any(ScheduleDto.class))).thenReturn(dynamicResponse);

        String url = String.format("%s", RestApiEndpoints.SCHEDULES);
        mockMvc.perform(post(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)));

        verify(scheduleDtoValidatorMock, times(1)).validateNewScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(1)).create(any(ScheduleDto.class));
    }

    private ScheduleDto prepareTestData() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME,
                CityLineType.valueOf(CITY_LINE_VALID_BUS_TYPE));
        return ScheduleDataBuilder.buildScheduleDto(null, ScheduleType.WORK_DAY, cityLineDto);
    }
}
