package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.CityLineService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.validator.dto.CityLineDtoValidator;
import com.tim2.travelservice.web.dto.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class CityLinesControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private CityLineService cityLineServiceMock;

    @MockBean
    private CityLineDtoValidator cityLineDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void findByTypeShouldReturnBadRequestWhenInvalidType() throws Exception {
        doThrow(new InvalidRequestDataException(RestApiErrors.invalidRequestParamFormat(RestApiConstants.TYPE)))
                .when(cityLineDtoValidatorMock).validateGetCityLineByTypeRequest(RANDOM_TYPE);
        doThrow(new InvalidRequestDataException(RestApiErrors.invalidFieldFormat(RestApiConstants.TYPE)))
                .when(cityLineDtoValidatorMock).validateGetCityLineByTypeRequest(RANDOM_TYPE);

        String url = String.format("%s?%s=%s", RestApiEndpoints.CITY_LINES, RestApiRequestParams.TYPE, RANDOM_TYPE);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        verify(cityLineDtoValidatorMock, times(1)).validateGetCityLineByTypeRequest(RANDOM_TYPE);
        verify(cityLineServiceMock, times(0)).findByType(PageRequest.of(0, 20), RANDOM_TYPE);
    }

    @Test
    public void findByTypeShouldReturnListOfCityLinesOfGivenType() throws Exception {
        List<CityLineDto> cityLines = prepareTestData();

        doNothing().when(cityLineDtoValidatorMock).validateGetCityLineByTypeRequest(TRAM);
        when(cityLineServiceMock.findByType(PageRequest.of(0, 20), TRAM)).thenReturn(new PageImpl<>(cityLines));
        doNothing().when(cityLineDtoValidatorMock).validateGetCityLineByTypeRequest(TRAM);

        String url = String.format("%s?%s=%s", RestApiEndpoints.CITY_LINES, RestApiRequestParams.TYPE, TRAM);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(cityLineDtoValidatorMock, times(1)).validateGetCityLineByTypeRequest(TRAM);
        verify(cityLineServiceMock, times(1)).findByType(PageRequest.of(0, 20), TRAM);
        andExpectedCityLinesAreReturned(mvcResult);
    }

    @Test
    public void findAllCityLinesShouldReturnListOfCityLines() throws Exception {
        List<CityLineDto> cityLines = prepareTestData();
        when(cityLineServiceMock.findAll(PageRequest.of(0, 20))).thenReturn(new PageImpl<>(cityLines));

        String url = String.format("%s", RestApiEndpoints.CITY_LINES);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(cityLineServiceMock, times(1)).findAll(PageRequest.of(0, 20));
        andExpectedNumberOfCityLinesIsReturned(mvcResult);
    }

    private void andExpectedNumberOfCityLinesIsReturned(MvcResult mvcResult) throws IOException {
        List<CityLineDto> cityLineDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<CityLineDto>>() {
        });

        assertThat("Number of returned city lines is valid.", cityLineDtos.size(), is(NUMBER_OF_CITY_LINES_OF_TRAM_TYPE));
    }

    public void createShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(cityLineDtoValidatorMock, times(0)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void createShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(cityLineDtoValidatorMock, times(0)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void createShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(cityLineDtoValidatorMock, times(0)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenIdIsNotNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(cityLineDtoValidatorMock).validateCreateRequest(cityLineDto);

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(cityLineDtoValidatorMock, times(1)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenNameIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, null, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME)))
                .when(cityLineDtoValidatorMock).validateCreateRequest(cityLineDto);

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME))));

        verify(cityLineDtoValidatorMock, times(1)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenNameIsEmptyString() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, "", FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME)))
                .when(cityLineDtoValidatorMock).validateCreateRequest(cityLineDto);

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME))));

        verify(cityLineDtoValidatorMock, times(1)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenVehiclesAreNotEmptyOrNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.singletonList(new VehicleDto()), Collections.emptyList(), Collections.emptyList());

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.VEHICLES)))
                .when(cityLineDtoValidatorMock).validateCreateRequest(cityLineDto);

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.VEHICLES))));

        verify(cityLineDtoValidatorMock, times(1)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenSchedulesAreNotEmptyOrNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.singletonList(new ScheduleDto()), Collections.emptyList());

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.SCHEDULES)))
                .when(cityLineDtoValidatorMock).validateCreateRequest(cityLineDto);

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.SCHEDULES))));

        verify(cityLineDtoValidatorMock, times(1)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenLineStationsAreNotEmptyOrNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.singletonList(new LineStationDto()));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.LINE_STATIONS)))
                .when(cityLineDtoValidatorMock).validateCreateRequest(cityLineDto);

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.LINE_STATIONS))));

        verify(cityLineDtoValidatorMock, times(1)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).create(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnCreatedAndCityLineId() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 9L);

        doNothing().when(cityLineDtoValidatorMock).validateCreateRequest(cityLineDto);
        when(cityLineServiceMock.create(cityLineDto)).thenReturn(dynamicResponse);

        mockMvc.perform(post(RestApiEndpoints.CITY_LINES)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(cityLineDto))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(9)));

        verify(cityLineDtoValidatorMock, times(1)).validateCreateRequest(cityLineDto);
        verify(cityLineServiceMock, times(1)).create(cityLineDto);
    }

    private List<CityLineDto> prepareTestData() {
        CityLineDto cityLineTram1 = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME1,
                CityLineType.valueOf(CITY_LINE_VALID_TRAM_TYPE));
        CityLineDto cityLineTram2 = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_2, CITY_LINE_TRAM_NAME2,
                CityLineType.valueOf(CITY_LINE_VALID_TRAM_TYPE));
        CityLineDto cityLineTram3 = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_3, CITY_LINE_TRAM_NAME3,
                CityLineType.valueOf(CITY_LINE_VALID_TRAM_TYPE));
        return Arrays.asList(cityLineTram1, cityLineTram2, cityLineTram3);
    }

    private void andExpectedCityLinesAreReturned(MvcResult mvcResult) throws IOException {
        List<CityLineDto> cityLineDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<CityLineDto>>() {
        });

        cityLineDtos.forEach(cityLineDto -> assertThat("Returned city line has expected type",
                cityLineDto.getCityLineType().toString(), is(CITY_LINE_VALID_TRAM_TYPE)));
    }
}
