package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.ScheduleDataBuilder;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.service.ScheduleService;
import com.tim2.travelservice.store.ScheduleStore;
import com.tim2.travelservice.validator.db.ScheduleDbValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class ScheduleServiceTest {

    @Autowired
    private ScheduleService scheduleService;

    @MockBean
    private ScheduleStore scheduleStore;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private ScheduleDbValidator scheduleDbValidator;

    @Test
    public void findByCityLineIdAndScheduleTypeShouldReturnTargetSchedule() {
        Schedule schedule = prepareTestData();
        ScheduleDto scheduleDto = prepareDtoTestData();
        when(scheduleStore.findActiveForGivenCityLineAndType(FIRST_CITY_LINE_ID, ScheduleType.WORK_DAY)).thenReturn(Optional.of(schedule));
        when(modelMapperMock.map(schedule, ScheduleDto.class)).thenReturn(scheduleDto);

        ScheduleDto returnValue = scheduleService.findByCityLineIdAndType(FIRST_CITY_LINE_ID, WORK_DAY);
        verify(scheduleStore, times(1)).findActiveForGivenCityLineAndType(FIRST_CITY_LINE_ID, ScheduleType.WORK_DAY);
        assertThat("City line id", returnValue.getCityLine().getId(), is(FIRST_CITY_LINE_ID));
        assertThat("Schedule id", returnValue.getId(), is(SCHEDULE_ID));
        assertThat("Schedule type", returnValue.getScheduleType(), is(ScheduleType.WORK_DAY));
    }

    @Test
    public void createNewScheduleShouldReturnScheduleIdAfterSuccessfulCreation() {
        Schedule schedule = prepareTestData();
        schedule.setId(null);
        ScheduleDto scheduleDto = prepareDtoTestData();
        scheduleDto.setId(null);
        Schedule returnedSchedule = prepareTestData();

        when(scheduleStore.save(schedule)).thenReturn(returnedSchedule);
        when(modelMapperMock.map(scheduleDto, Schedule.class)).thenReturn(schedule);

        DynamicResponse dynamicResponse = scheduleService.create(scheduleDto);

        verify(modelMapperMock, times(1)).map(scheduleDto, Schedule.class);
        verify(scheduleStore, times(1)).save(ArgumentMatchers.any(Schedule.class));
        assertThat("Schedule id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
    }

    private Schedule prepareTestData() {
        CityLine cityLine = CityLineDataBuilder.buildCityLine(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME,
                CityLineType.valueOf(CITY_LINE_VALID_BUS_TYPE));
        return ScheduleDataBuilder.buildSchedule(SCHEDULE_ID, ScheduleType.WORK_DAY, cityLine);
    }

    private ScheduleDto prepareDtoTestData() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME,
                CityLineType.valueOf(CITY_LINE_VALID_BUS_TYPE));
        return ScheduleDataBuilder.buildScheduleDto(null, ScheduleType.WORK_DAY, cityLineDto);
    }
}
