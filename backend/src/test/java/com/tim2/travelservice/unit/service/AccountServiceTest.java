package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Role;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.AccountService;
import com.tim2.travelservice.store.AccountStore;
import com.tim2.travelservice.validator.db.AccountDbValidator;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @MockBean
    private AccountStore accountStoreMock;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private AccountDbValidator accountDbValidatorMock;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoderMock;

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateByEmailShouldThrowNotFoundWithExplanationWhenUserIsNotExisting() {
        when(accountDbValidatorMock.validateUpdateByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL)).thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)));

        accountService.updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, new AccountDto());

        verify(accountDbValidatorMock, times(1)).validateUpdateByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL);
    }

    @Test
    public void updateByEmailShouldNotUpdateNonUpdatableFieldsIfTheyAreProvided() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_REGISTRATION_EMAIL, true, false);
        Account existingAccount = AccountDataBuilder.buildAccount(SYSTEM_ADMINISTRATOR_EMAIL, DEFAULT_ENCODED_PASSWORD, SYSTEM_ADMINISTRATOR_FIRST_NAME, SYSTEM_ADMINISTRATOR_LAST_NAME, SYSTEM_ADMINISTRATOR_DATE_OF_BIRTH, false, true);
        Account updatedAccount = AccountDataBuilder.buildAccount(null, null, null);

        when(accountDbValidatorMock.validateUpdateByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL)).thenReturn(existingAccount);
        when(modelMapperMock.map(accountDto, Account.class)).thenReturn(updatedAccount);

        accountService.updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, accountDto);

        verify(accountDbValidatorMock, times(1)).validateUpdateByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL);
        verify(modelMapperMock, times(1)).map(accountDto, Account.class);
        verify(bCryptPasswordEncoderMock, times(0)).encode(anyString());
        verify(accountStoreMock, times(1)).save(existingAccount);
        assertFalse("idValidated is not updated.", existingAccount.getIdValidated());
        assertTrue("Validated is not updated.", existingAccount.getValidated());
        assertThat("Email is not updated.", existingAccount.getEmail(), is(SYSTEM_ADMINISTRATOR_EMAIL));
    }

    @Test
    public void updateByEmailShouldUpdateUser() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED);
        Account existingAccount = AccountDataBuilder.buildAccount(SYSTEM_ADMINISTRATOR_EMAIL, DEFAULT_ENCODED_PASSWORD, SYSTEM_ADMINISTRATOR_FIRST_NAME, SYSTEM_ADMINISTRATOR_LAST_NAME, SYSTEM_ADMINISTRATOR_DATE_OF_BIRTH);
        Account updatedAccount = AccountDataBuilder.buildAccount(SYSTEM_ADMINISTRATOR_EMAIL, ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, SYSTEM_ADMINISTRATOR_DATE_OF_BIRTH);

        when(accountDbValidatorMock.validateUpdateByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL)).thenReturn(existingAccount);
        when(modelMapperMock.map(accountDto, Account.class)).thenReturn(updatedAccount);
        when(bCryptPasswordEncoderMock.encode(ACCOUNT_PASSWORD_UPDATED)).thenReturn(UPDATED_ENCODED_PASSWORD);

        accountService.updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, accountDto);

        verify(accountDbValidatorMock, times(1)).validateUpdateByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL);
        verify(modelMapperMock, times(1)).map(accountDto, Account.class);
        verify(bCryptPasswordEncoderMock, times(1)).encode(ACCOUNT_PASSWORD_UPDATED);
        verify(accountStoreMock, times(1)).save(existingAccount);
        assertNotEquals("Password is updated.", existingAccount.getPassword(), DEFAULT_ENCODED_PASSWORD);
        assertThat("First name is updated.", existingAccount.getFirstName(), is(ACCOUNT_FIRST_NAME_UPDATED));
        assertThat("Last name is updated.", existingAccount.getLastName(), is(ACCOUNT_LAST_NAME_UPDATED));
    }

    @Test(expected = InvalidRequestDataException.class)
    public void registerShouldThrowBadRequestWithExplanationExceptionWhenUserWithGivenEmailAlreadyExist() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)))
                .when(accountDbValidatorMock).validateRegistrationRequest(accountDto);

        accountService.register(accountDto);

        verify(accountDbValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(modelMapperMock, times(0)).map(any(AccountDto.class), Account.class);
        verify(bCryptPasswordEncoderMock, times(0)).encode(anyString());
        verify(accountStoreMock, times(0)).create(any(Account.class));
    }

    @Test
    public void registerShouldReturnAccountIdAfterSuccessfulRegistration() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);
        Account account = AccountDataBuilder.buildAccount(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);
        Account accountWithId = AccountDataBuilder.buildAccount(1L, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doNothing().when(accountDbValidatorMock).validateRegistrationRequest(accountDto);
        when(modelMapperMock.map(accountDto, Account.class)).thenReturn(account);
        when(bCryptPasswordEncoderMock.encode(ACCOUNT_REGISTRATION_PASSWORD)).thenReturn(DEFAULT_ENCODED_PASSWORD);
        when(accountStoreMock.create(any(Account.class))).thenReturn(accountWithId);

        DynamicResponse dynamicResponse = accountService.register(accountDto);

        verify(accountDbValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(modelMapperMock, times(1)).map(accountDto, Account.class);
        verify(bCryptPasswordEncoderMock, times(1)).encode(ACCOUNT_REGISTRATION_PASSWORD);
        verify(accountStoreMock, times(1)).create(account);
        assertThat("Account id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
    }

    @Test(expected = InvalidRequestDataException.class)
    public void registerTravelAdminShouldThrowBadRequestWithExplanationExceptionWhenUserWithGivenEmailAlreadyExist() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)))
                .when(accountDbValidatorMock).validateRegistrationRequest(accountDto);

        accountService.registerAdmin(accountDto);

        verify(accountDbValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(modelMapperMock, times(0)).map(any(AccountDto.class), Account.class);
        verify(bCryptPasswordEncoderMock, times(0)).encode(anyString());
        verify(accountStoreMock, times(0)).create(any(Account.class));
    }

    @Test
    public void registerTravelAdminShouldReturnAccountIdAfterSuccessfulRegistration() {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);
        Account accountWithPassword = AccountDataBuilder.buildAccount(null, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        Account accountWithId = AccountDataBuilder.buildAccount(1L, ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);

        doNothing().when(accountDbValidatorMock).validateRegistrationRequest(accountDto);
        when(modelMapperMock.map(accountDto, Account.class)).thenReturn(accountWithPassword);
        when(bCryptPasswordEncoderMock.encode(ACCOUNT_REGISTRATION_PASSWORD)).thenReturn(DEFAULT_ENCODED_PASSWORD);
        when(accountStoreMock.create(any(Account.class))).thenReturn(accountWithId);

        DynamicResponse dynamicResponse = accountService.register(accountDto);

        verify(accountDbValidatorMock, times(1)).validateRegistrationRequest(accountDto);
        verify(modelMapperMock, times(1)).map(accountDto, Account.class);
        verify(bCryptPasswordEncoderMock, times(1)).encode(any(String.class));
        verify(accountStoreMock, times(1)).create(accountWithPassword);
        assertThat("Account id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void findByEmailShouldThrowNotFoundWithExplanationWhenUserIsNotExisting() {
        when(accountDbValidatorMock.validateFindByEmailRequest(ACCOUNT_REGISTRATION_EMAIL))
                .thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)));

        accountService.findByEmail(ACCOUNT_REGISTRATION_EMAIL);

        verify(accountDbValidatorMock, times(1)).validateFindByEmailRequest(ACCOUNT_REGISTRATION_EMAIL);
    }

    @Test
    public void findByPriceListIdShouldReturnExisting() {
        Account existingAccount = AccountDataBuilder.buildAccount(1L, SYSTEM_ADMINISTRATOR_EMAIL, DEFAULT_ENCODED_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(1L, SYSTEM_ADMINISTRATOR_EMAIL, null, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);


        when(accountDbValidatorMock.validateFindByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL)).thenReturn(existingAccount);
        when(modelMapperMock.map(existingAccount, AccountDto.class)).thenReturn(accountDto);

        AccountDto returnedAccountDto = accountService.findByEmail(SYSTEM_ADMINISTRATOR_EMAIL);

        verify(accountDbValidatorMock, times(1)).validateFindByEmailRequest(SYSTEM_ADMINISTRATOR_EMAIL);
        assertThat("Email is correct.", returnedAccountDto.getEmail(), is(SYSTEM_ADMINISTRATOR_EMAIL));
        assertNull("Password is not returned.", returnedAccountDto.getPassword());
    }

    public void findByRoleIdShouldReturnAllTravelAdmins() {
        Page<Account> accounts = initTestData();

        when(accountStoreMock.findByRoleId(2L, PageRequest.of(0, 20))).thenReturn(accounts);

        Page<AccountDto> accountDtos = accountService.findByRoleId(ADMINISTRATOR_ROLE_ID, PageRequest.of(0, 20));

        verify(accountStoreMock, times(1)).findByRoleId(2L, PageRequest.of(0, 20));
        accountDtos.forEach(accountDto -> assertThat("Role is correct.", accountDto.getRoles().get(0).getId(), is(2L)));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void deleteTravelAdminByIdShouldThrowNotFoundWithExplanationExceptionWhenDeletingNonExistingTravelAdmin() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.ID)))
                .when(accountDbValidatorMock).validateDeleteTravelAdminById(1L);

        accountService.deleteTravelAdminById(1L);

        verify(accountDbValidatorMock, times(1)).validateDeleteTravelAdminById(1L);
        verify(accountStoreMock, times(0)).delete(any(Account.class));
    }

    @Test(expected = InvalidRequestDataException.class)
    public void deleteTravelAdminByIdShouldThrowInvalidRequestDataExceptionWhenDeletingUserThatIsNotTravelAdmin() {
        doThrow(new InvalidRequestDataException(RestApiErrors.USER_IS_NOT_TRAVEL_ADMIN))
                .when(accountDbValidatorMock).validateDeleteTravelAdminById(1L);

        accountService.deleteTravelAdminById(1L);

        verify(accountDbValidatorMock, times(1)).validateDeleteTravelAdminById(1L);
        verify(accountStoreMock, times(0)).delete(any(Account.class));
    }

    @Test
    public void deleteTravelAdminByIdShouldDeleteExistingStation() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(Role.builder().id(2L).build());
        Account existingAccount = AccountDataBuilder.buildAccount(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, roles);

        when(accountDbValidatorMock.validateDeleteTravelAdminById(1L)).thenReturn(existingAccount);

        accountService.deleteTravelAdminById(1L);

        verify(accountDbValidatorMock, times(1)).validateDeleteTravelAdminById(1L);
        verify(accountStoreMock, times(1)).delete(existingAccount);
    }


    private Page<Account> initTestData() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(Role.builder().id(2L).build());
        Account account1 = AccountDataBuilder.buildAccount(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, roles);
        Account account2 = AccountDataBuilder.buildAccount(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, roles);
        Account account3 = AccountDataBuilder.buildAccount(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, roles);

        return new PageImpl<>(Arrays.asList(account1, account2, account3));
    }

}
