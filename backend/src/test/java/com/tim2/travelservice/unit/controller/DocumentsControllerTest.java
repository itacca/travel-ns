package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.data.builder.DocumentDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.DocumentService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.validator.dto.DocumentDtoValidator;
import com.tim2.travelservice.web.dto.DocumentDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class DocumentsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private DocumentService documentServiceMock;

    @MockBean
    private DocumentDtoValidator documentDtoValidatorMock;

    private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void uploadDocumentShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "", "application/json", "{\"json\": \"someValue\"}".getBytes());

        mockMvc.perform(multipart(RestApiEndpoints.DOCUMENTS)
                .file(file)
                .with(csrf())
                .param(RestApiRequestParams.DOCUMENT_TYPE, VALID_DOCUMENT_TYPE)
                .param(RestApiRequestParams.IMAGE_TYPE, IMAGE_TYPE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(documentDtoValidatorMock, times(0)).validateUploadRequest(anyString());
        verify(documentServiceMock, times(0)).saveDocument(any(MultipartFile.class), anyString(), eq(INVALID_DOCUMENT_TYPE), anyString());
    }

    @Test
    @WithMockUser
    public void uploadDocumentShouldThrowBadRequestWhenDocumentTypeIsInvalid() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "", "application/json", "{\"json\": \"someValue\"}".getBytes());

        doThrow(new InvalidRequestDataException(RestApiErrors.invalidRequestParamFormat(RestApiRequestParams.DOCUMENT_TYPE)))
                .when(documentDtoValidatorMock).validateUploadRequest(INVALID_DOCUMENT_TYPE);

        mockMvc.perform(multipart(RestApiEndpoints.DOCUMENTS)
                .file(file)
                .with(csrf())
                .param(RestApiRequestParams.DOCUMENT_TYPE, INVALID_DOCUMENT_TYPE)
                .param(RestApiRequestParams.IMAGE_TYPE, IMAGE_TYPE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.invalidRequestParamFormat(RestApiRequestParams.DOCUMENT_TYPE))));

        verify(documentDtoValidatorMock, times(1)).validateUploadRequest(INVALID_DOCUMENT_TYPE);
        verify(documentServiceMock, times(0)).saveDocument(any(MultipartFile.class), anyString(), eq(INVALID_DOCUMENT_TYPE), anyString());
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL)
    public void uploadDocumentShouldReturnCreatedAndDocumentId() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "", "application/json", "{\"json\": \"someValue\"}".getBytes());
        DynamicResponse body = new DynamicResponse(RestApiConstants.ID, 9L);

        doNothing().when(documentDtoValidatorMock).validateUploadRequest(VALID_DOCUMENT_TYPE);
        when(documentServiceMock.saveDocument(file, IMAGE_TYPE, VALID_DOCUMENT_TYPE, SYSTEM_ADMINISTRATOR_EMAIL)).thenReturn(body);

        mockMvc.perform(multipart(RestApiEndpoints.DOCUMENTS)
                .file(file)
                .with(csrf())
                .param(RestApiRequestParams.DOCUMENT_TYPE, VALID_DOCUMENT_TYPE)
                .param(RestApiRequestParams.IMAGE_TYPE, IMAGE_TYPE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(9)));

        verify(documentDtoValidatorMock, times(1)).validateUploadRequest(VALID_DOCUMENT_TYPE);
        verify(documentServiceMock, times(1)).saveDocument(file, IMAGE_TYPE, VALID_DOCUMENT_TYPE, SYSTEM_ADMINISTRATOR_EMAIL);
    }

    @Test
    public void findByActiveAndVerifiedShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        mockMvc.perform(get(RestApiEndpoints.DOCUMENTS)
                .with(csrf())
                .param(RestApiRequestParams.ACTIVE, "true")
                .param(RestApiRequestParams.VERIFIED, "false")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(documentServiceMock, times(0)).findByActiveAndVerified(true, false, PageRequest.of(0, 20));
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void findByActiveAndVerifiedShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        mockMvc.perform(get(RestApiEndpoints.DOCUMENTS)
                .with(csrf())
                .param(RestApiRequestParams.ACTIVE, "true")
                .param(RestApiRequestParams.VERIFIED, "false")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(documentServiceMock, times(0)).findByActiveAndVerified(true, false, PageRequest.of(0, 20));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void findByActiveAndVerifiedShouldReturnOkAndTickets() throws Exception {
        List<DocumentDto> documentDtos = Arrays.asList(
                DocumentDataBuilder.buildDocumentDto(1L, true, false),
                DocumentDataBuilder.buildDocumentDto(1L, true, false));

        when(documentServiceMock.findByActiveAndVerified(true, false, PageRequest.of(0, 20))).thenReturn(new PageImpl<>(documentDtos));

        MvcResult mvcResult = mockMvc.perform(get(RestApiEndpoints.DOCUMENTS)
                .with(csrf())
                .param(RestApiRequestParams.ACTIVE, "true")
                .param(RestApiRequestParams.VERIFIED, "false")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(documentServiceMock, times(1)).findByActiveAndVerified(true, false, PageRequest.of(0, 20));
        andExpectedDocumentsAreReturned(mvcResult);
    }

    private void andExpectedDocumentsAreReturned(MvcResult mvcResult) throws Exception {
        List<DocumentDto> documentDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<DocumentDto>>() {
        });

        assertThat("All documents are returned.", documentDtos.size(), is(2));
    }
}
