package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.CityLineService;
import com.tim2.travelservice.validator.dto.CityLineDtoValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Collections;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class CityLineControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private CityLineService cityLineServiceMock;

    @MockBean
    private CityLineDtoValidator cityLineDtoValidatorMock;

    private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void deleteByIdShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(cityLineServiceMock, times(0)).deleteById(anyLong());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void deleteByIdShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(cityLineServiceMock, times(0)).deleteById(anyLong());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void deleteByIdShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(cityLineServiceMock, times(0)).deleteById(anyLong());
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteByIdShouldReturnNotFoundWhenDeletingNonExistingCityLine() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(cityLineServiceMock).deleteById(999L);

        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, 999L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID))));


        verify(cityLineServiceMock, times(1)).deleteById(999L);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteByIdShouldReturnNoContentWhenDeletingExistingCityLine() throws Exception {
        doNothing().when(cityLineServiceMock).deleteById(1L);

        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(cityLineServiceMock, times(1)).deleteById(1L);
    }

    @Test
    public void findByIdShouldReturnNotFoundWhenGettingNonExistingCityLine() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(cityLineServiceMock).findById(999L);

        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, 999L);
        mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID))));

        verify(cityLineServiceMock, times(1)).findById(999L);
    }

    @Test
    public void findByIdShouldReturnNoContentWhenGettingExistingCityLine() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(1L, CITY_LINE_BUS_NAME1, CityLineType.BUS);

        when(cityLineServiceMock.findById(1L)).thenReturn(cityLineDto);

        String url = String.format("%s/%d", RestApiEndpoints.CITY_LINE, 1L);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(cityLineServiceMock, times(1)).findById(1L);
        andExpectedCityLineIsReturned(mvcResult);
    }

    @Test
    public void updateShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(cityLineDtoValidatorMock, times(0)).validateUpdateRequest(any());
        verify(cityLineServiceMock, times(0)).update(any());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void updateShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(cityLineDtoValidatorMock, times(0)).validateUpdateRequest(any());
        verify(cityLineServiceMock, times(0)).update(any());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void updateShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(cityLineDtoValidatorMock, times(0)).validateUpdateRequest(any());
        verify(cityLineServiceMock, times(0)).update(any());
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenIdIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(cityLineDtoValidatorMock).validateUpdateRequest(cityLineDto);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(cityLineDtoValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).update(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenVehiclesAreNotNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, Collections.emptyList(), null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.VEHICLES)))
                .when(cityLineDtoValidatorMock).validateUpdateRequest(cityLineDto);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.VEHICLES))));

        verify(cityLineDtoValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).update(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenSchedulesAreNotNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, Collections.emptyList(), null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.SCHEDULES)))
                .when(cityLineDtoValidatorMock).validateUpdateRequest(cityLineDto);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.SCHEDULES))));

        verify(cityLineDtoValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).update(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenLineStationsAreNotNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, Collections.emptyList());

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS)))
                .when(cityLineDtoValidatorMock).validateUpdateRequest(cityLineDto);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS))));

        verify(cityLineDtoValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).update(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenNameIsEmptyString() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, "", FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME)))
                .when(cityLineDtoValidatorMock).validateUpdateRequest(cityLineDto);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME))));

        verify(cityLineDtoValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).update(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnNotFoundWhenUpdatingNonExistingCityLine() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(999L, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(cityLineDtoValidatorMock).validateUpdateRequest(cityLineDto);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID))));

        verify(cityLineDtoValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(cityLineServiceMock, times(0)).update(cityLineDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnNoContentAndUpdateCityLine() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        doNothing().when(cityLineDtoValidatorMock).validateUpdateRequest(cityLineDto);

        String url = String.format("%s", RestApiEndpoints.CITY_LINE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(cityLineDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(cityLineDtoValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(cityLineServiceMock, times(1)).update(cityLineDto);
    }

    private void andExpectedCityLineIsReturned(MvcResult mvcResult) throws IOException {
        CityLineDto cityLineDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), CityLineDto.class);

        assertThat("Correct id is returned.", cityLineDto.getId(), is(1L));
        assertThat("Correct name is returned.", cityLineDto.getName(), is(CITY_LINE_BUS_NAME1));
        assertThat("Correct type is returned.", cityLineDto.getCityLineType(), is(CityLineType.BUS));
    }
}
