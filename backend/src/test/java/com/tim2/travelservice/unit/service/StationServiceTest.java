package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.StationService;
import com.tim2.travelservice.store.StationStore;
import com.tim2.travelservice.validator.db.StationDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.StationDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class StationServiceTest {

    @Autowired
    private StationService stationService;

    @MockBean
    private StationStore stationStoreMock;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private StationDbValidator stationDbValidatorMock;

    @Test
    public void createShouldReturnStationIdAfterSuccessfulInsert() {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);
        Station station = StationDataBuilder.buildStation(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);
        Station stationWithId = StationDataBuilder.buildStation(1L, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        when(modelMapperMock.map(stationDto, Station.class)).thenReturn(station);
        when(stationStoreMock.save(station)).thenReturn(stationWithId);

        DynamicResponse dynamicResponse = stationService.create(stationDto);

        verify(modelMapperMock, times(1)).map(stationDto, Station.class);
        verify(stationStoreMock, times(1)).save(station);
        assertThat("Station id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void findByIdShouldThrowNotFoundWithExplanationExceptionWhenGettingNonExistingStation() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(stationDbValidatorMock).validateFindByIdRequest(1L);

        stationService.findById(1L);

        verify(stationDbValidatorMock, times(1)).validateFindByIdRequest(1L);
    }

    @Test
    public void findByIdShouldReturnExistingStation() {
        Station station = StationDataBuilder.buildStation(1L, STATION_NAME, STATION_LONGITUDE, STATION_LATITUDE, null);

        when(stationDbValidatorMock.validateFindByIdRequest(1L)).thenReturn(station);

        StationDto stationDto = stationService.findById(1L);

        verify(stationDbValidatorMock, times(1)).validateFindByIdRequest(1L);
        andExpectedStationIsReturned(stationDto);
    }

    @Test
    public void findAllShouldReturnAllStations() {
        Page<Station> stationPage = initStationPage();

        when(stationStoreMock.findAll(PageRequest.of(0, 20))).thenReturn(stationPage);

        Page<StationDto> stationDtoPage = stationService.findAll(PageRequest.of(0, 20));

        verify(stationStoreMock, times(1)).findAll(PageRequest.of(0, 20));
        assertThat("All stations are returned.", stationDtoPage.getContent().size(), is(stationPage.getContent().size()));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateShouldThrowNotFoundWithExplanationExceptionWhenUpdatingNonExistingStation() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, null, null, null);

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(stationDbValidatorMock).validateUpdateStationRequest(stationDto);

        stationService.update(stationDto);

        verify(stationDbValidatorMock, times(1)).validateUpdateStationRequest(stationDto);
        verify(modelMapperMock, times(0)).map(any(StationDto.class), eq(Station.class));
        verify(stationStoreMock, times(0)).save(any(Station.class));
    }

    @Test
    public void updateShouldUpdateExistingStation() {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);
        Station existingStation = StationDataBuilder.buildStation(1L, STATION_NAME, STATION_LONGITUDE, STATION_LATITUDE, null);
        Station updatedStation = StationDataBuilder.buildStation(1L, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        when(stationDbValidatorMock.validateUpdateStationRequest(stationDto)).thenReturn(existingStation);
        when(modelMapperMock.map(stationDto, Station.class)).thenReturn(updatedStation);

        stationService.update(stationDto);

        verify(stationDbValidatorMock, times(1)).validateUpdateStationRequest(stationDto);
        verify(modelMapperMock, times(1)).map(stationDto, Station.class);
        verify(stationStoreMock, times(1)).save(updatedStation);
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void deleteByIdShouldThrowNotFoundWithExplanationExceptionWhenDeletingNonExistingStation() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(stationDbValidatorMock).validateDeleteByIdRequest(1L);

        stationService.deleteById(1L);

        verify(stationDbValidatorMock, times(1)).validateDeleteByIdRequest(1L);
        verify(stationStoreMock, times(0)).delete(any(Station.class));
    }

    @Test
    public void deleteByIdShouldDeleteExistingStation() {
        Station existingStation = StationDataBuilder.buildStation(1L, STATION_NAME, STATION_LONGITUDE, STATION_LATITUDE, null);

        when(stationDbValidatorMock.validateDeleteByIdRequest(1L)).thenReturn(existingStation);

        stationService.deleteById(1L);

        verify(stationDbValidatorMock, times(1)).validateDeleteByIdRequest(1L);
        verify(stationStoreMock, times(1)).delete(existingStation);
    }

    private void andExpectedStationIsReturned(StationDto stationDto) {
        assertThat("Correct id is returned.", stationDto.getId(), is(1L));
        assertThat("Correct station name is returned.", stationDto.getName(), is(STATION_NAME));
        assertThat("Correct station longitude is returned.", stationDto.getLongitude(), is(STATION_LONGITUDE));
        assertThat("Correct station latitude is returned.", stationDto.getLatitude(), is(STATION_LATITUDE));
    }

    private Page<Station> initStationPage() {
        Station station1 = StationDataBuilder.buildStation(1L, STATION_NAME, STATION_LONGITUDE, STATION_LATITUDE, null);
        Station station2 = StationDataBuilder.buildStation(2L, STATION_NAME, STATION_LONGITUDE, STATION_LATITUDE, null);
        Station station3 = StationDataBuilder.buildStation(3L, STATION_NAME, STATION_LONGITUDE, STATION_LATITUDE, null);

        return new PageImpl<>(Arrays.asList(station1, station2, station3));
    }
}
