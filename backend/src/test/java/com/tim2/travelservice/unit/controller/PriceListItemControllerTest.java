package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.data.builder.PriceListItemDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.*;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.PriceListItemService;
import com.tim2.travelservice.validator.dto.PriceListItemDtoValidator;
import com.tim2.travelservice.web.dto.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class PriceListItemControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private PriceListItemService priceListItemServiceMock;

    @MockBean
    private PriceListItemDtoValidator priceListItemDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnCreatedWhenPriceListItemIsSuccessfullyUpdated() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 6L);

        doNothing().when(priceListItemDtoValidatorMock).validateUpdateRequest(priceListItemDto);
        when(priceListItemServiceMock.update(priceListItemDto)).thenReturn(dynamicResponse);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(6)));

        verify(priceListItemDtoValidatorMock, times(1)).validateUpdateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).update(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenPriceIsInvalid() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, INVALID_PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateUpdateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER))
                .when(priceListItemServiceMock).update(priceListItemDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER)));

        verify(priceListItemDtoValidatorMock, times(1)).validateUpdateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).update(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenPriceIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateUpdateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE)))
                .when(priceListItemServiceMock).update(priceListItemDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE))));

        verify(priceListItemDtoValidatorMock, times(1)).validateUpdateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).update(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenPriceListIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE, ticketDto);

        doNothing().when(priceListItemDtoValidatorMock).validateUpdateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST)))
                .when(priceListItemServiceMock).update(priceListItemDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST))));

        verify(priceListItemDtoValidatorMock, times(1)).validateUpdateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).update(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenPriceListIdIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateUpdateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(priceListItemServiceMock).update(priceListItemDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(priceListItemDtoValidatorMock, times(1)).validateUpdateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).update(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenPriceListItemIdIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateUpdateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(priceListItemServiceMock).update(priceListItemDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(priceListItemDtoValidatorMock, times(1)).validateUpdateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).update(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenPriceListEndDateIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateUpdateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE)))
                .when(priceListItemServiceMock).update(priceListItemDto);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE))));

        verify(priceListItemDtoValidatorMock, times(1)).validateUpdateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).update(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void updateShouldReturnForbiddenWhenRoleIsVerifier() throws Exception {
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(INVALID_PRICE_LIST_ITEM_PRICE);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void updateShouldReturnForbiddenWhenRoleIsPassenger() throws Exception {
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(INVALID_PRICE_LIST_ITEM_PRICE);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser()
    public void updateShouldReturnForbiddenWhenRoleIsGuest() throws Exception {
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(INVALID_PRICE_LIST_ITEM_PRICE);

        mockMvc.perform(put(RestApiEndpoints.PRICE_LIST_ITEM)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void deleteByIdShouldReturnNoContentWhenPriceListItemIsSuccessfullyDeleted() throws Exception {
        CityLine cityLineDto = CityLineDataBuilder.buildCityLine(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME_ADD, CityLineType.TRAM);
        Ticket ticketDto = TicketDataBuilder.buildTicket(8L, TicketType.REGULAR, TicketDuration.SINGLE_RIDE, cityLineDto);
        PriceList priceListDto = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItem priceListItemDto = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE_NEW, ticketDto, false, priceListDto);

        when(priceListItemServiceMock.deleteById(3L)).thenReturn(priceListItemDto);

        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST_ITEM + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(priceListItemServiceMock, times(1)).deleteById(3L);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void deleteByIdShouldReturnForbiddenWhenRoleIsVerifier() throws Exception {
        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST_ITEM + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void deleteByIdShouldReturnForbiddenWhenRoleIsPassenger() throws Exception {
        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST_ITEM + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void deleteByIdShouldReturnBadRequestWhenPriceListItemDoesNotExistInDatabase() throws Exception {
        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST_ITEM, RestApiConstants.ID)))
                .when(priceListItemServiceMock).deleteById(3L);

        mockMvc.perform(delete(RestApiEndpoints.PRICE_LIST_ITEM + "/{id}", 3)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST_ITEM, RestApiConstants.ID))));

        verify(priceListItemServiceMock, times(1)).deleteById(3L);
    }
}