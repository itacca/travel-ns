package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.data.builder.DocumentDataBuilder;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.entity.DocumentType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.AccountService;
import com.tim2.travelservice.service.DocumentService;
import com.tim2.travelservice.store.DocumentStore;
import com.tim2.travelservice.validator.db.DocumentDbValidator;
import com.tim2.travelservice.web.dto.DocumentDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class DocumentServiceTest {

    @Autowired
    private DocumentService documentService;

    @MockBean
    private AccountService accountServiceMock;

    @MockBean
    private DocumentStore documentStoreMock;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private DocumentDbValidator documentDbValidatorMock;

    private final ArgumentCaptor<Document> documentCaptor = ArgumentCaptor.forClass(Document.class);

    @Test
    public void saveDocumentShouldReturnDocumentIdAfterSuccessfulInsert() throws IOException {
        MultipartFile fileMock = mock(MultipartFile.class);
        Account account = AccountDataBuilder.buildAccount(1L, SYSTEM_ADMINISTRATOR_EMAIL, DEFAULT_ENCODED_PASSWORD, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME, ACCOUNT_REGISTRATION_DATE_OF_BIRTH, null, null, null);
        Document savedDocument = DocumentDataBuilder.buildDocument(9L, IMAGE_TYPE, DocumentType.SENIOR);

        when(accountServiceMock.findEntityByEmail(SYSTEM_ADMINISTRATOR_EMAIL)).thenReturn(account);
        when(documentStoreMock.save(any())).thenReturn(savedDocument);

        DynamicResponse dynamicResponse = documentService.saveDocument(fileMock, IMAGE_TYPE, VALID_DOCUMENT_TYPE, SYSTEM_ADMINISTRATOR_EMAIL);

        verify(documentStoreMock).save(documentCaptor.capture());
        Document document = documentCaptor.getValue();
        assertThat("Document is being saved with good account.", document.getAccount(), is(account));
        assertThat("Document is being saved with good image type.", document.getImageType(), is(IMAGE_TYPE));
        assertThat("Document is being saved with good document type.", document.getDocumentType(), is(DocumentType.SENIOR));
        assertThat("Document id is returned.", dynamicResponse.get(RestApiConstants.ID), Matchers.is(9L));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void findImageBlobByDocumentIdShouldThrowNotFoundWithExplanationExceptionDocumentDoesNotExist() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID)))
                .when(documentDbValidatorMock).validateFindImageBlobByDocumentId(999L);

        documentService.findImageBlobByDocumentId(999L);

        verify(documentDbValidatorMock, times(1)).validateFindImageBlobByDocumentId(999L);
    }

    @Test
    public void findImageBlobByDocumentIdShouldReturnOkAndImageBlob() {
        Document existingDocument = DocumentDataBuilder.buildDocument("image-blob".getBytes());

        when(documentDbValidatorMock.validateFindImageBlobByDocumentId(1L)).thenReturn(existingDocument);

        DynamicResponse response = documentService.findImageBlobByDocumentId(1L);

        verify(documentDbValidatorMock, times(1)).validateFindImageBlobByDocumentId(1L);
        assertThat("Correct image blob is returned.", response.get(RestApiConstants.IMAGE_BLOB), is(existingDocument.getImageBlob()));
    }

    @Test
    public void findByActiveAndVerifiedShouldReturnTicketsByActiveAndVerified() {
        List<Document> documents = Arrays.asList(
                DocumentDataBuilder.buildDocument(1L, true, false),
                DocumentDataBuilder.buildDocument(1L, true, false));

        when(documentStoreMock.findByActiveAndVerified(true, false, PageRequest.of(0, 20))).thenReturn(new PageImpl<>(documents));

        Page<DocumentDto> documentPage = documentService.findByActiveAndVerified(true, false, PageRequest.of(0, 20));

        verify(documentStoreMock, times(1)).findByActiveAndVerified(true, false, PageRequest.of(0, 20));
        assertThat(documentPage.getTotalElements(), is((long) documents.size()));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void verifyShouldThrowNotFoundWithExplanationWhenDocumentIsNonExisting() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID)))
                .when(documentDbValidatorMock).validateVerifyRequest(documentDto);

        documentService.verify(documentDto);

        verify(documentDbValidatorMock, times(1)).validateVerifyRequest(documentDto);
    }

    @Test(expected = InvalidRequestDataException.class)
    public void verifyShouldThrowInvalidRequestDataExceptionWhenDocumentIsInactive() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        doThrow(new InvalidRequestDataException(RestApiErrors.INACTIVE_DOCUMENT_VERIFICATION))
                .when(documentDbValidatorMock).validateVerifyRequest(documentDto);

        documentService.verify(documentDto);

        verify(documentDbValidatorMock, times(1)).validateVerifyRequest(documentDto);
    }

    @Test
    public void verifyShouldVerifyTicket() {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);
        Document existingDocument = DocumentDataBuilder.buildDocument(3L, true, false);

        when(documentDbValidatorMock.validateVerifyRequest(documentDto)).thenReturn(existingDocument);

        documentService.verify(documentDto);

        verify(documentDbValidatorMock, times(1)).validateVerifyRequest(documentDto);
        verify(documentStoreMock, times(1)).save(existingDocument);
    }
}
