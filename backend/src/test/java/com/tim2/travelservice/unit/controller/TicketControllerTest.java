package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.TicketService;
import com.tim2.travelservice.validator.dto.TicketDtoValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.TicketDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class TicketControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private TicketService ticketService;

    @MockBean
    private TicketDtoValidator ticketDtoValidator;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser
    public void findByIdShouldReturnNotFoundWhenGettingNonExistingTicket() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID))).when(ticketService).findById(999L);

        String url = String.format("%s/%d", RestApiEndpoints.TICKET, 999L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID))));

        verify(ticketService, times(1)).findById(999L);
    }

    @Test
    @WithMockUser
    public void findByIdShouldReturnOkAndTicket() throws Exception {
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(1L);
        when(ticketService.findById(1L)).thenReturn(ticketDto);

        String url = String.format("%s/%d", RestApiEndpoints.TICKET, 1L);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(ticketService, times(1)).findById(1L);
        andTicketIsReturned(mvcResult, 1L);
    }

    @Test
    public void deleteShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.TICKET, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(ticketService, times(0)).deleteTicket(anyLong());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void deleteShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.TICKET, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(ticketService, times(0)).deleteTicket(anyLong());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void deleteShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.TICKET, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(ticketService, times(0)).deleteTicket(anyLong());
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteShouldReturnNotFoundWhenGettingNonExistingStation() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID)))
                .when(ticketService).deleteTicket(999L);

        String url = String.format("%s/%d", RestApiEndpoints.TICKET, 999L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());

        verify(ticketService, times(1)).deleteTicket(999L);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteShouldReturnNoContentWhenDeletingExistingTicket() throws Exception {
        doNothing().when(ticketService).deleteTicket(NON_REFERENCED_TICKET_ID);

        String url = String.format("%s/%d", RestApiEndpoints.TICKET, NON_REFERENCED_TICKET_ID);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(ticketService, times(1)).deleteTicket(NON_REFERENCED_TICKET_ID);
    }

    @Test
    public void updateShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(ticketDtoValidator, times(0)).validateUpdateTicketRequest(any(TicketDto.class));
        verify(ticketService, times(0)).updateTicket(any(TicketDto.class));
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void updateShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(ticketDtoValidator, times(0)).validateUpdateTicketRequest(any(TicketDto.class));
        verify(ticketService, times(0)).updateTicket(any(TicketDto.class));
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void updateShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(ticketDtoValidator, times(0)).validateUpdateTicketRequest(any(TicketDto.class));
        verify(ticketService, times(0)).updateTicket(any(TicketDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenIdIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(null, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(ticketDtoValidator).validateUpdateTicketRequest(ticketDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(ticketDtoValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(ticketService, times(0)).updateTicket(ticketDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenDurationIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, null, FIRST_TICKET_TYPE, cityLineDto);


        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION)))
                .when(ticketDtoValidator).validateUpdateTicketRequest(ticketDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION))));

        verify(ticketDtoValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(ticketService, times(0)).updateTicket(ticketDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenTypeIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, null, cityLineDto);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE)))
                .when(ticketDtoValidator).validateUpdateTicketRequest(ticketDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE))));

        verify(ticketDtoValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(ticketService, times(0)).updateTicket(ticketDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenCityLineIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE)))
                .when(ticketDtoValidator).validateUpdateTicketRequest(ticketDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE))));

        verify(ticketDtoValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(ticketService, times(0)).updateTicket(ticketDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenCityLineIdIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_ID)))
                .when(ticketDtoValidator).validateUpdateTicketRequest(ticketDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_ID))));

        verify(ticketDtoValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(ticketService, times(0)).updateTicket(ticketDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnNoContentAndUpdateTicket() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME, FIRST_CITY_LINE_TYPE);
        ;
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(FIRST_TICKET_ID, FIRST_TICKET_UPDATED_DURATION, FIRST_TICKET_TYPE, cityLineDto);

        doNothing().when(ticketDtoValidator).validateUpdateTicketRequest(ticketDto);

        String url = String.format("%s", RestApiEndpoints.TICKET);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(ticketDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(ticketDtoValidator, times(1)).validateUpdateTicketRequest(ticketDto);
        verify(ticketService, times(1)).updateTicket(ticketDto);
    }

    private void andTicketIsReturned(MvcResult mvcResult, Long id) throws IOException {
        TicketDto ticketDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), TicketDto.class);

        assertThat("Ticket id", ticketDto.getId(), is(id));
    }
}
