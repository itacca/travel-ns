package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.VehicleDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.VehicleType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.VehicleService;
import com.tim2.travelservice.validator.dto.VehicleDtoValidator;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class VehicleControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private VehicleService vehicleServiceMock;

    @MockBean
    private VehicleDtoValidator vehicleDtoValidatorMock;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void deleteShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.VEHICLE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(vehicleServiceMock, times(0)).delete(anyLong());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void deleteShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.VEHICLE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(vehicleServiceMock, times(0)).delete(anyLong());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void deleteShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.VEHICLE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(vehicleServiceMock, times(0)).delete(anyLong());
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteShouldReturnNotFoundWhenDeletingNonExistingStation() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID)))
                .when(vehicleServiceMock).delete(999L);

        String url = String.format("%s/%d", RestApiEndpoints.VEHICLE, 999L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID))));

        verify(vehicleServiceMock, times(1)).delete(999L);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteShouldReturnNoContentWhenDeletingExistingStation() throws Exception {
        doNothing().when(vehicleServiceMock).delete(1L);

        String url = String.format("%s/%d", RestApiEndpoints.VEHICLE, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(vehicleServiceMock, times(1)).delete(1L);
    }

    @Test
    public void updateShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(vehicleDtoValidatorMock, times(0)).validateUpdateVehicleRequest(any(VehicleDto.class));
        verify(vehicleServiceMock, times(0)).update(any(VehicleDto.class));
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void updateShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(vehicleDtoValidatorMock, times(0)).validateUpdateVehicleRequest(any(VehicleDto.class));
        verify(vehicleServiceMock, times(0)).update(any(VehicleDto.class));
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void updateShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(vehicleDtoValidatorMock, times(0)).validateUpdateVehicleRequest(any(VehicleDto.class));
        verify(vehicleServiceMock, times(0)).update(any(VehicleDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenIdIsNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(vehicleDtoValidatorMock).validateUpdateVehicleRequest(vehicleDto);

        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(vehicleDtoValidatorMock, times(1)).validateUpdateVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).update(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenPlateNumberIsEmptyString() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, "", VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.PLATE_NUMBER)))
                .when(vehicleDtoValidatorMock).validateUpdateVehicleRequest(vehicleDto);

        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.PLATE_NUMBER))));

        verify(vehicleDtoValidatorMock, times(1)).validateUpdateVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).update(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnNotFoundWhenUpdatingNonExistingVehicle() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doNothing().when(vehicleDtoValidatorMock).validateUpdateVehicleRequest(vehicleDto);
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID)))
                .when(vehicleServiceMock).update(vehicleDto);

        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID))));

        verify(vehicleDtoValidatorMock, times(1)).validateUpdateVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(1)).update(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnNoContentAndUpdateVehicle() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doNothing().when(vehicleDtoValidatorMock).validateUpdateVehicleRequest(vehicleDto);

        String url = String.format("%s", RestApiEndpoints.VEHICLE);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(vehicleDtoValidatorMock, times(1)).validateUpdateVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(1)).update(vehicleDto);
    }
}
