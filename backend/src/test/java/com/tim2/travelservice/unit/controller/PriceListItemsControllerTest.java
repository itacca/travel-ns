package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.data.builder.PriceListItemDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.entity.TicketType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.PriceListItemService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.validator.dto.PriceListItemDtoValidator;
import com.tim2.travelservice.web.dto.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class PriceListItemsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private PriceListItemService priceListItemServiceMock;

    @MockBean
    private PriceListItemDtoValidator priceListItemDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnCreatedWhenPriceListItemIsSuccessfullyAdded() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);
        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 6L);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        when(priceListItemServiceMock.create(priceListItemDto)).thenReturn(dynamicResponse);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(6)));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void createShouldReturnForbiddenWhenRoleIsVerifier() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListItemDtoValidatorMock, times(0)).validateCreateRequest(any(PriceListItemDto.class));
        verify(priceListItemServiceMock, times(0)).create(any(PriceListItemDto.class));
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void createShouldReturnForbiddenWhenRoleIsPassenger() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListItemDtoValidatorMock, times(0)).validateCreateRequest(any(PriceListItemDto.class));
        verify(priceListItemServiceMock, times(0)).create(any(PriceListItemDto.class));
    }

    @Test
    @WithMockUser()
    public void createShouldReturnForbiddenWhenRoleIsGuest() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListItemDtoValidatorMock, times(0)).validateCreateRequest(any(PriceListItemDto.class));
        verify(priceListItemServiceMock, times(0)).create(any(PriceListItemDto.class));
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenTicketIsNull() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, null, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenPriceIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenPriceIsNegative() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(INVALID_PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER)));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenPriceListIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, null);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenPriceListIdIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST_ID)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST_ID))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenPriceListEndDateIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenTicketTypeIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenTicketDurationIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenCityLineIsNull() throws Exception {
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, null);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenCityLineNameIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenCityLineTypeIsNull() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenIdIsProvided() throws Exception {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_ID, ticketDto, priceListDto);

        doNothing().when(priceListItemDtoValidatorMock).validateCreateRequest(priceListItemDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(priceListItemServiceMock).create(priceListItemDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LIST_ITEMS)
                .content(objectMapper.writeValueAsString(priceListItemDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(priceListItemDtoValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(priceListItemServiceMock, times(1)).create(priceListItemDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void findByPriceListIdShouldReturnOkAndActivePriceListItems() throws Exception {
        Page<PriceListItemDto> priceListItemDtos = initTestData();

        when(priceListItemServiceMock.findByPriceListId(PRICE_LIST_ID, PageRequest.of(0, 20))).thenReturn(priceListItemDtos);

        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEMS);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("price_list_id", "4")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(priceListItemServiceMock, times(1)).findByPriceListId(PRICE_LIST_ID, PageRequest.of(0, 20));
        andExpectedPriceListItemsAreReturned(mvcResult);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void findByPriceListIdShouldReturnBadRequestWhenPriceListDoesNotExistInDatabase() throws Exception {
        Page<PriceListItemDto> priceListItemDtos = initTestData();

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)))
                .when(priceListItemServiceMock).findByPriceListId(PRICE_LIST_ID, PageRequest.of(0, 20));

        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEMS);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("price_list_id", "4")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID))));

        verify(priceListItemServiceMock, times(1)).findByPriceListId(PRICE_LIST_ID, PageRequest.of(0, 20));
    }

    @Test
    public void findAllByPriceListAndTicketShouldReturnOkAndAllPriceListItems() throws Exception {
        Page<PriceListItemDto> priceListItemDtos = initTestDataForAll();

        when(priceListItemServiceMock.findAllByPriceListAndTicket(PRICE_LIST_ID, 5L, PageRequest.of(0, 20))).thenReturn(priceListItemDtos);

        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEMS);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param(RestApiRequestParams.PRICE_LIST_ID, "4")
                .param(RestApiRequestParams.TICKET_ID, "5")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(priceListItemServiceMock, times(1)).findAllByPriceListAndTicket(PRICE_LIST_ID, 5L, PageRequest.of(0, 20));
        andExpectedPriceListItems(mvcResult);
    }

    @Test
    public void findAllByPriceListAndTicketShouldReturnBadRequestIfPriceListDoesNotExistInDatabase() throws Exception {
        Page<PriceListItemDto> priceListItemDtos = initTestData();

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)))
                .when(priceListItemServiceMock).findAllByPriceListAndTicket(PRICE_LIST_ID, TICKET_ID, PageRequest.of(0, 20));

        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEMS);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param(RestApiRequestParams.PRICE_LIST_ID, "4")
                .param(RestApiRequestParams.TICKET_ID, "5")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID))));

        verify(priceListItemServiceMock, times(1)).findAllByPriceListAndTicket(PRICE_LIST_ID, TICKET_ID, PageRequest.of(0, 20));
    }

    @Test
    public void findAllByPriceListAndTicketShouldReturnBadRequestIfTicketDoesNotExistInDatabase() throws Exception {
        Page<PriceListItemDto> priceListItemDtos = initTestData();

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID)))
                .when(priceListItemServiceMock).findAllByPriceListAndTicket(PRICE_LIST_ID, TICKET_ID, PageRequest.of(0, 20));

        String url = String.format("%s", RestApiEndpoints.PRICE_LIST_ITEMS);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param(RestApiRequestParams.PRICE_LIST_ID, "4")
                .param(RestApiRequestParams.TICKET_ID, "5")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID))));

        verify(priceListItemServiceMock, times(1)).findAllByPriceListAndTicket(PRICE_LIST_ID, TICKET_ID, PageRequest.of(0, 20));
    }

    private void andExpectedPriceListItemsAreReturned(MvcResult mvcResult) throws IOException {
        List<PriceListItemDto> priceListItemDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<PriceListItemDto>>() {
        });
        priceListItemDtos.forEach(priceListItemDto -> assertThat("Price list id is correct.", priceListItemDto.getPriceList().getId(), is(PRICE_LIST_ID)));
        priceListItemDtos.forEach(priceListItemDto -> assertThat("Status is active.", priceListItemDto.getActive(), is(true)));
    }

    private Page<PriceListItemDto> initTestData() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto1 = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        TicketDto ticketDto2 = TicketDataBuilder.buildTicketDto(6L, TicketType.STUDENT, TicketDuration.DAILY, cityLineDto);
        TicketDto ticketDto3 = TicketDataBuilder.buildTicketDto(7L, TicketType.SENIOR, TicketDuration.DAILY, cityLineDto);

        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto1 = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto1, true, priceListDto);
        PriceListItemDto priceListItemDto2 = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto2, true, priceListDto);
        PriceListItemDto priceListItemDto3 = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto3, true, priceListDto);

        return new PageImpl<>(Arrays.asList(priceListItemDto1, priceListItemDto2, priceListItemDto3));
    }

    private void andExpectedPriceListItems(MvcResult mvcResult) throws IOException {
        List<PriceListItemDto> priceListItemDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<PriceListItemDto>>() {
        });

        priceListItemDtos.forEach(priceListItemDto -> assertThat("Price list id is correct.", priceListItemDto.getPriceList().getId(), is(PRICE_LIST_ID)));
        priceListItemDtos.forEach(priceListItemDto -> assertThat("Ticket id is correct.", priceListItemDto.getTicket().getId(), is(5L)));
    }

    private Page<PriceListItemDto> initTestDataForAll() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto1 = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        PriceListItemDto priceListItemDto1 = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto1, true, priceListDto);
        PriceListItemDto priceListItemDto2 = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto1, false, priceListDto);
        PriceListItemDto priceListItemDto3 = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto1, false, priceListDto);

        return new PageImpl<>(Arrays.asList(priceListItemDto1, priceListItemDto2, priceListItemDto3));
    }
}