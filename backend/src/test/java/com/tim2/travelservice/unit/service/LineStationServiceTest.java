package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.LineStationDataBuilder;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.LineStationService;
import com.tim2.travelservice.store.LineStationStore;
import com.tim2.travelservice.validator.db.LineStationDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class LineStationServiceTest {

    @Autowired
    private LineStationService lineStationService;

    @MockBean
    private LineStationStore lineStationStoreMock;

    @Autowired
    private ModelMapper modelMapper;

    @MockBean
    private LineStationDbValidator lineStationDbValidatorMock;

    @Test(expected = NotFoundWithExplanationException.class)
    public void findByCityLineIdShouldThrowNotFoundWithExplanationWhenCityLineDoesNotExist() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(lineStationDbValidatorMock).validateFindByCiteLineIdRequest(999L);

        lineStationService.findByCityLineId(999L);

        verify(lineStationDbValidatorMock, times(1)).validateFindByCiteLineIdRequest(999L);
    }

    @Test
    public void findByCityLineIdShouldReturnLineStations() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(1L, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(5L)));

        doNothing().when(lineStationDbValidatorMock).validateFindByCiteLineIdRequest(CITY_LINE_BUS_ID_1);

        List<LineStationDto> lineStations = lineStationService.findByCityLineId(CITY_LINE_BUS_ID_1);

        verify(lineStationDbValidatorMock, times(1)).validateFindByCiteLineIdRequest(CITY_LINE_BUS_ID_1);
        lineStationDtos.forEach(lineStationDto -> assertThat("Returned line station is for given city line.", lineStationDto.getCityLine().getId(), is(CITY_LINE_BUS_ID_1)));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void creatAllShouldThrowNotFoundWithExplanationWhenCityLineIsNotExisting() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(99L), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(99L), StationDataBuilder.buildStationDto(STATION_ID_2)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(lineStationDbValidatorMock).validateCreateAllRequest(lineStationDtos);

        lineStationService.createAll(lineStationDtos);

        verify(lineStationDbValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void creatAllShouldThrowNotFoundWithExplanationWhenStationIsNotExisting() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(99L)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(lineStationDbValidatorMock).validateCreateAllRequest(lineStationDtos);

        lineStationService.createAll(lineStationDtos);

        verify(lineStationDbValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
    }

    @Test
    public void createAllShouldReturnLineStationIdsAfterSuccessfulCreation() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));
        List<DynamicResponse> dynamicResponses = Arrays.asList(new DynamicResponse(RestApiConstants.ID, 1L), new DynamicResponse(RestApiConstants.ID, 2L));

        List<DynamicResponse> cityLineIds = lineStationService.createAll(lineStationDtos);

        verify(lineStationDbValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        cityLineIds.forEach(response -> assertThat("Id is returned.", response.get(RestApiConstants.ID), isNotNull()));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateAllShouldThrowNotFoundWithExplanationWhenCityLineIsNotExisting() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(5L)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(lineStationDbValidatorMock).validateUpdateAllRequest(lineStationDtos);

        lineStationService.updateAll(lineStationDtos);

        verify(lineStationDbValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateAllShouldThrowNotFoundWithExplanationWhenStationIsNotExisting() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(999L)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(lineStationDbValidatorMock).validateUpdateAllRequest(lineStationDtos);

        lineStationService.updateAll(lineStationDtos);

        verify(lineStationDbValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateAllShouldThrowNotFoundWithExplanationWhenLineStationIsNotExisting() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(999L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.LINE_STATION, RestApiConstants.ID)))
                .when(lineStationDbValidatorMock).validateUpdateAllRequest(lineStationDtos);

        lineStationService.updateAll(lineStationDtos);

        verify(lineStationDbValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
    }

    @Test
    public void updateAllShouldUpdateLineStations() {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));
        List<LineStation> existingLineStations = Arrays.asList(
                LineStationDataBuilder.buildLineStation(1L, 2, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStation(2L)),
                LineStationDataBuilder.buildLineStation(2L, 1, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStation(3L)),
                LineStationDataBuilder.buildLineStation(3L, 4, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStation(4L)),
                LineStationDataBuilder.buildLineStation(4L, 3, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStation(5L)),
                LineStationDataBuilder.buildLineStation(5L, 3, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStation(5L)));

        when(lineStationStoreMock.findByCityLineId(CITY_LINE_BUS_ID_2)).thenReturn(existingLineStations);

        lineStationService.updateAll(lineStationDtos);

        verify(lineStationDbValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationStoreMock, times(2)).saveAll(anyList());
        verify(lineStationStoreMock, times(1)).deleteAll(anyList());
    }
}
