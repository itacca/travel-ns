package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.data.builder.BoughtTicketBuilder;
import com.tim2.travelservice.service.BoughtTicketService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class BoughtTicketsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private BoughtTicketService boughtTicketServiceMock;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void findAllForPrincipalShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s", RestApiEndpoints.BOUGHT_TICKETS);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(boughtTicketServiceMock, times(0)).findByAccountEmail(anyString(), any(PageRequest.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void findAllForPrincipalShouldReturnOkAndBoughtTickets() throws Exception {
        when(boughtTicketServiceMock.findByAccountEmail(SYSTEM_ADMINISTRATOR_EMAIL, PageRequest.of(0, 20))).thenReturn(initBoughtTicketDtoPage());

        String url = String.format("%s", RestApiEndpoints.BOUGHT_TICKETS);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(boughtTicketServiceMock, times(1)).findByAccountEmail(SYSTEM_ADMINISTRATOR_EMAIL, PageRequest.of(0, 20));
        andExpectedBoughtTicketsAreReturned(mvcResult);
    }

    private void andExpectedBoughtTicketsAreReturned(MvcResult mvcResult) throws IOException {
        List<BoughtTicketDto> boughtTickets = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<BoughtTicketDto>>() {
        });

        boughtTickets.forEach(boughtTicketDto -> assertThat("Principal is ticket owner.", boughtTicketDto.getAccount().getEmail(), is(SYSTEM_ADMINISTRATOR_EMAIL)));
    }

    private Page<BoughtTicketDto> initBoughtTicketDtoPage() {
        BoughtTicketDto boughtTicketDto1 = BoughtTicketBuilder.buildBoughtTicketDto(1L, AccountDataBuilder.buildAccountDto(SYSTEM_ADMINISTRATOR_EMAIL));
        BoughtTicketDto boughtTicketDto2 = BoughtTicketBuilder.buildBoughtTicketDto(2L, AccountDataBuilder.buildAccountDto(SYSTEM_ADMINISTRATOR_EMAIL));
        BoughtTicketDto boughtTicketDto3 = BoughtTicketBuilder.buildBoughtTicketDto(3L, AccountDataBuilder.buildAccountDto(SYSTEM_ADMINISTRATOR_EMAIL));

        return new PageImpl<>(Arrays.asList(boughtTicketDto1, boughtTicketDto2, boughtTicketDto3));
    }
}
