package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.LineStationDataBuilder;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.LineStationService;
import com.tim2.travelservice.validator.dto.LineStationDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class LineStationsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private LineStationService lineStationServiceMock;

    @MockBean
    private LineStationDtoValidator lineStationDtoValidatorMock;

    private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void createAllShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(lineStationDtoValidatorMock, times(0)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void createAllShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(lineStationDtoValidatorMock, times(0)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void createAllShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(lineStationDtoValidatorMock, times(0)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnBadRequestWhenIdIsNotNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(1L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnBadRequestWhenRelativePositionIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, null, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnBadRequestWhenCityLineIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, null, StationDataBuilder.buildStationDto(STATION_ID_2)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnBadRequestWhenCityLineIdIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(null), StationDataBuilder.buildStationDto(STATION_ID_2)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnBadRequestWhenStationIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), null),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnBadRequestWhenStationIdIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(null)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnBadRequestWhenCityLineIdsAreNotTheSame() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(STATION_ID)));

        doThrow(new InvalidRequestDataException(RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID)));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnNotFoundWhenCityLineIsNotExisting() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(99L), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(99L), StationDataBuilder.buildStationDto(STATION_ID_2)));

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnNotFoundWhenStationIsNotExisting() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(99L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID))));

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).createAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createAllShouldReturnCreatedAndLineStationsIds() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(null, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID)),
                LineStationDataBuilder.buildLineStationDto(null, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(STATION_ID_2)));
        List<DynamicResponse> dynamicResponses = Arrays.asList(new DynamicResponse(RestApiConstants.ID, 1L), new DynamicResponse(RestApiConstants.ID, 2L));

        doNothing().when(lineStationDtoValidatorMock).validateCreateAllRequest(lineStationDtos);
        when(lineStationServiceMock.createAll(lineStationDtos)).thenReturn(dynamicResponses);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        MvcResult mvcResult = mockMvc.perform(post(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andReturn();

        verify(lineStationDtoValidatorMock, times(1)).validateCreateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(1)).createAll(lineStationDtos);
        andLineStationsIdsAreReturned(mvcResult);
    }

    @Test
    public void updateAllShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(lineStationDtoValidatorMock, times(0)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void updateAllShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(lineStationDtoValidatorMock, times(0)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void updateAllShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(lineStationDtoValidatorMock, times(0)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnBadRequestWhenRelativePositionIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, null, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnBadRequestWhenCityLineIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, null, StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnBadRequestWhenCityLineIdIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(null), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnBadRequestWhenStationIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), null),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnBadRequestWhenStationIdIsNull() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(null)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnBadRequestWhenCityLineIdsAreNotTheSame() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(5L)));

        doThrow(new InvalidRequestDataException(RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID)));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnNotFoundWhenCityLineDoesNotExist() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(999L), StationDataBuilder.buildStationDto(5L)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnNotFoundWhenStationDoesNotExist() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(999L)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnNotFoundWhenLineStationDoesNotExist() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(999L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.LINE_STATION, RestApiConstants.ID)))
                .when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.LINE_STATION, RestApiConstants.ID))));

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(0)).updateAll(lineStationDtos);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateAllShouldReturnNoContentAndUpdateDeleteAndCreateLineStations() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(null, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_2), StationDataBuilder.buildStationDto(5L)));

        doNothing().when(lineStationDtoValidatorMock).validateUpdateAllRequest(lineStationDtos);

        String url = String.format("%s", RestApiEndpoints.LINE_STATIONS);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(lineStationDtos))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(lineStationDtoValidatorMock, times(1)).validateUpdateAllRequest(lineStationDtos);
        verify(lineStationServiceMock, times(1)).updateAll(lineStationDtos);
    }

    @Test
    public void shouldReturnNotFoundWhenCityLineIsNotExisting() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(lineStationServiceMock).findByCityLineId(999L);

        String url = String.format("%s?%s=%d", RestApiEndpoints.LINE_STATIONS, RestApiRequestParams.CITY_LINE_ID, 999L);
        mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID))));

        verify(lineStationServiceMock, times(1)).findByCityLineId(999L);
    }

    @Test
    public void shouldReturnOkAndLineStationsForGivenCityLineId() throws Exception {
        List<LineStationDto> lineStationDtos = Arrays.asList(
                LineStationDataBuilder.buildLineStationDto(2L, 2, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(2L)),
                LineStationDataBuilder.buildLineStationDto(3L, 1, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(3L)),
                LineStationDataBuilder.buildLineStationDto(4L, 4, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(4L)),
                LineStationDataBuilder.buildLineStationDto(1L, 3, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1), StationDataBuilder.buildStationDto(5L)));

        when(lineStationServiceMock.findByCityLineId(CITY_LINE_BUS_ID_1)).thenReturn(lineStationDtos);

        String url = String.format("%s?%s=%d", RestApiEndpoints.LINE_STATIONS, RestApiRequestParams.CITY_LINE_ID, CITY_LINE_BUS_ID_1);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(lineStationServiceMock, times(1)).findByCityLineId(CITY_LINE_BUS_ID_1);
        andLineStationsAreReturned(mvcResult);
    }

    private void andLineStationsAreReturned(MvcResult mvcResult) throws Exception {
        List<LineStationDto> lineStationDtos = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<LineStationDto>>() {
        });

        lineStationDtos.forEach(lineStationDto -> assertThat("Returned line station is for given city line.", lineStationDto.getCityLine().getId(), is(CITY_LINE_BUS_ID_1)));
    }

    private void andLineStationsIdsAreReturned(MvcResult mvcResult) throws Exception {
        List<DynamicResponse> response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<DynamicResponse>>() {
        });

        assertThat("Two ids are returned.", response.size(), is(2));
        assertThat("First id is correct.", response.get(0).get(RestApiConstants.ID), is(1));
        assertThat("Second id is correct.", response.get(1).get(RestApiConstants.ID), is(2));
    }
}
