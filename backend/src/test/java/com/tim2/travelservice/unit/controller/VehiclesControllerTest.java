package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.VehicleDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.VehicleType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.VehicleService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.validator.dto.VehicleDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.StationDto;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class VehiclesControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private VehicleService vehicleServiceMock;

    @MockBean
    private VehicleDtoValidator vehicleDtoValidatorMock;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void findAllShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s", RestApiEndpoints.VEHICLES);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(vehicleServiceMock, times(0)).findAllPageable(PageRequest.of(0, 20));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void findAllShouldReturnOkAndVehicles() throws Exception {
        when(vehicleServiceMock.findAllPageable(PageRequest.of(0, 20))).thenReturn(initVehicleDtoPage());

        String url = String.format("%s", RestApiEndpoints.VEHICLES);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(vehicleServiceMock, times(1)).findAllPageable(PageRequest.of(0, 20));
        andAllVehiclesAreReturned(mvcResult);
    }

    private void andAllVehiclesAreReturned(MvcResult mvcResult) throws IOException {
        List<VehicleDto> vehicleDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<VehicleDto>>() {
        });

        assertThat("All vehicles are returned.", vehicleDtos.size(), is(3));
    }

    @Test
    public void createShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(vehicleDtoValidatorMock, times(0)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void createShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(vehicleDtoValidatorMock, times(0)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void createShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(vehicleDtoValidatorMock, times(0)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenVehicleIdIsNotNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(3L, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenVehiclePlateNumberIsNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, null, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PLATE_NUMBER)))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PLATE_NUMBER))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenVehicleTypeIsNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, null, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VEHICLE_TYPE)))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VEHICLE_TYPE))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenCityLineIsNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE)))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenCityLineIdIsNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE)))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenCityLineTypeIsNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, null));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE)))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnNotFoundWhenCityLineDoesNotExis() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.TRAM));

        doNothing().when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(vehicleServiceMock).create(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(1)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenVehicleTypeAndCityLineTypeDoNotMatch() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.TRAM));

        doThrow(new InvalidRequestDataException(RestApiErrors.TYPES_DO_NOT_MATCH))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.TYPES_DO_NOT_MATCH)));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenStationIsNotNull() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS), new StationDto());

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.STATION)))
                .when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.STATION))));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(0)).create(vehicleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnCreatedAndVehicleId() throws Exception {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));
        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 9L);

        doNothing().when(vehicleDtoValidatorMock).validateAddVehicleRequest(vehicleDto);
        when(vehicleServiceMock.create(vehicleDto)).thenReturn(dynamicResponse);

        mockMvc.perform(post(RestApiEndpoints.VEHICLES)
                .with(csrf())
                .content(objectMapper.writeValueAsString(vehicleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(9)));

        verify(vehicleDtoValidatorMock, times(1)).validateAddVehicleRequest(vehicleDto);
        verify(vehicleServiceMock, times(1)).create(vehicleDto);
    }

    private Page<VehicleDto> initVehicleDtoPage() {
        VehicleDto vehicleDto1 = VehicleDataBuilder.buildVehicleDto(1L, "NS 001 AA", VehicleType.BUS);
        VehicleDto vehicleDto2 = VehicleDataBuilder.buildVehicleDto(2L, "NS 002 AA", VehicleType.BUS);
        VehicleDto vehicleDto3 = VehicleDataBuilder.buildVehicleDto(3L, "NS 003 AA", VehicleType.BUS);

        return new PageImpl<>(Arrays.asList(vehicleDto1, vehicleDto2, vehicleDto3));
    }
}
