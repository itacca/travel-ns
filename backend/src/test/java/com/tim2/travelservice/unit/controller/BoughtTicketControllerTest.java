package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.BoughtTicketBuilder;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.BoughtTicketService;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class BoughtTicketControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private BoughtTicketService boughtTicketServiceMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void validateShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/validate/%d", RestApiEndpoints.BOUGHT_TICKET, 1L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(boughtTicketServiceMock, times(0)).validate(anyLong());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void validateShouldReturnForbiddenWhenCallerIsPassenger() throws Exception {
        String url = String.format("%s/validate/%d", RestApiEndpoints.BOUGHT_TICKET, 1L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(boughtTicketServiceMock, times(0)).validate(anyLong());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void validateShouldReturnForbiddenWhenCallerIsVerifier() throws Exception {
        String url = String.format("%s/validate/%d", RestApiEndpoints.BOUGHT_TICKET, 1L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(boughtTicketServiceMock, times(0)).validate(anyLong());
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void validateShouldReturnNotFoundWhenValidatingNonExistingBoughtTicket() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID))).when(boughtTicketServiceMock).validate(999L);

        String url = String.format("%s/validate/%d", RestApiEndpoints.BOUGHT_TICKET, 999L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID))));

        verify(boughtTicketServiceMock, times(1)).validate(999L);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void validateShouldReturnOkAndBooleanAsTicketValidity() throws Exception {
        when(boughtTicketServiceMock.validate(1L)).thenReturn(new DynamicResponse(RestApiConstants.IS_ACTIVE, true));

        String url = String.format("%s/validate/%d", RestApiEndpoints.BOUGHT_TICKET, 1L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.isActive", is(true)));

        verify(boughtTicketServiceMock, times(1)).validate(1L);
    }

    @Test
    public void findByIdShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.BOUGHT_TICKET, 1L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(boughtTicketServiceMock, times(0)).findById(anyLong());
    }

    @Test
    @WithMockUser
    public void findByIdShouldReturnNotFoundWhenGettingNonExistingBoughtTicket() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID)))
                .when(boughtTicketServiceMock).findById(999L);

        String url = String.format("%s/%d", RestApiEndpoints.BOUGHT_TICKET, 999L);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID))));

        verify(boughtTicketServiceMock, times(1)).findById(999L);
    }

    @Test
    @WithMockUser
    public void findByIdShouldReturnOkAndBoughtTicket() throws Exception {
        BoughtTicketDto boughtTicketDto = BoughtTicketBuilder.buildBoughtTicketDto(1L);
        when(boughtTicketServiceMock.findById(1L)).thenReturn(boughtTicketDto);

        String url = String.format("%s/%d", RestApiEndpoints.BOUGHT_TICKET, 1L);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(boughtTicketServiceMock, times(1)).findById(1L);
        andExpectedBoughtTicketIsReturned(mvcResult, 1L);
    }

    private void andExpectedBoughtTicketIsReturned(MvcResult mvcResult, Long id) throws IOException {
        BoughtTicketDto boughtTicketDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), BoughtTicketDto.class);

        assertThat("Bought ticket id", boughtTicketDto.getId(), is(id));
    }
}
