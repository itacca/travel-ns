package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.data.builder.VehicleDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.entity.VehicleType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.VehicleService;
import com.tim2.travelservice.store.VehicleStore;
import com.tim2.travelservice.validator.db.VehicleDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.StationDto;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class VehicleServiceTest {

    @Autowired
    private VehicleService vehicleService;

    @MockBean
    private VehicleStore vehicleStoreMock;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private VehicleDbValidator vehicleDbValidatorMock;

    @Test
    public void findAllPageableShouldReturnAllVehicles() {
        Page<Vehicle> vehiclePage = initVehiclePage();
        when(vehicleStoreMock.findAllPageable(PageRequest.of(0, 20))).thenReturn(vehiclePage);

        Page<VehicleDto> vehicleDtoPage = vehicleService.findAllPageable(PageRequest.of(0, 20));

        verify(vehicleStoreMock, times(1)).findAllPageable(PageRequest.of(0, 20));
        assertThat("All vehicles are returned.", vehicleDtoPage.getContent().size(), is(vehicleDtoPage.getContent().size()));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void deleteShouldThrowNotFoundWithExplanationExceptionWhenDeletingNonExistingStation() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID)))
                .when(vehicleDbValidatorMock).validateDeleteVehicleRequest(1L);

        vehicleService.delete(1L);

        verify(vehicleDbValidatorMock, times(1)).validateDeleteVehicleRequest(1L);
        verify(vehicleStoreMock, times(0)).delete(any(Vehicle.class));
    }

    @Test
    public void deleteShouldDeleteExistingStation() {
        Vehicle existingVehicle = VehicleDataBuilder.buildVehicle(1L, "NS 001 AA", VehicleType.BUS);
        when(vehicleDbValidatorMock.validateDeleteVehicleRequest(1L)).thenReturn(existingVehicle);

        vehicleService.delete(1L);

        verify(vehicleDbValidatorMock, times(1)).validateDeleteVehicleRequest(1L);
        verify(vehicleStoreMock, times(1)).delete(existingVehicle);
    }

    @Test(expected = InvalidRequestDataException.class)
    public void createShouldThrowBadRequestWithExplanationExceptionWhenVehicleWithGivenPlateAlreadyExists() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.VEHICLE, RestApiConstants.PLATE_NUMBER)))
                .when(vehicleDbValidatorMock).validateCreateVehicleRequest(vehicleDto);

        vehicleService.create(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateCreateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(0)).map(any(VehicleDto.class), Vehicle.class);
        verify(vehicleStoreMock, times(0)).save(any(Vehicle.class));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void createShouldThrowNotFoundWithExplanationExceptionWhenCityLineDoesNotExist() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(vehicleDbValidatorMock).validateCreateVehicleRequest(vehicleDto);

        vehicleService.create(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateCreateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(0)).map(any(VehicleDto.class), Vehicle.class);
        verify(vehicleStoreMock, times(0)).save(any(Vehicle.class));
    }

    @Test(expected = InvalidRequestDataException.class)
    public void createShouldThrowBadRequestWithExplanationExceptionWhenCityLineDoesNotHaveLineStations() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.CITY_LINE_HAS_NO_STATIONS))
                .when(vehicleDbValidatorMock).validateCreateVehicleRequest(vehicleDto);

        vehicleService.create(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateCreateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(0)).map(any(VehicleDto.class), Vehicle.class);
        verify(vehicleStoreMock, times(0)).save(any(Vehicle.class));
    }

    @Test(expected = InvalidRequestDataException.class)
    public void createShouldThrowBadRequestWithExplanationExceptionWhenPlateNumberIsExisting() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER_EXISTING, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.VEHICLE, RestApiConstants.PLATE_NUMBER)))
                .when(vehicleDbValidatorMock).validateCreateVehicleRequest(vehicleDto);

        vehicleService.create(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateCreateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(0)).map(any(VehicleDto.class), Vehicle.class);
        verify(vehicleStoreMock, times(0)).save(any(Vehicle.class));
    }

    @Test
    public void createShouldReturnVehicleIdAfterSuccessfulCreation() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        Vehicle vehicleWithStation = VehicleDataBuilder.buildVehicle(null, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS), StationDataBuilder.buildStation(1L));

        Vehicle vehicleWithId = VehicleDataBuilder.buildVehicle(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS), StationDataBuilder.buildStation(1L));

        doNothing().when(vehicleDbValidatorMock).validateCreateVehicleRequest(vehicleDto);
        when(modelMapperMock.map(vehicleDto, Vehicle.class)).thenReturn(vehicleWithStation);
        when(vehicleStoreMock.save(any(Vehicle.class))).thenReturn(vehicleWithId);

        DynamicResponse dynamicResponse = vehicleService.create(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateCreateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(1)).map(vehicleDto, Vehicle.class);
        verify(vehicleStoreMock, times(1)).save(vehicleWithStation);
        Assert.assertThat("Vehicle id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateShouldThrowNotFoundWithExplanationExceptionWhenUpdatingNonExistingVehicle() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID)))
                .when(vehicleDbValidatorMock).validateUpdateVehicleRequest(vehicleDto);

        vehicleService.update(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateUpdateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(0)).map(any(StationDto.class), eq(Station.class));
        verify(vehicleStoreMock, times(0)).save(any(Vehicle.class));
    }

    @Test(expected = InvalidRequestDataException.class)
    public void updateShouldThrowNotFoundWithExplanationExceptionWhenUpdatingWithExistingPlateNumber() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        doThrow(new InvalidRequestDataException(RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.VEHICLE, RestApiConstants.PLATE_NUMBER)))
                .when(vehicleDbValidatorMock).validateUpdateVehicleRequest(vehicleDto);

        vehicleService.update(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateUpdateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(0)).map(any(StationDto.class), eq(Station.class));
        verify(vehicleStoreMock, times(0)).save(any(Vehicle.class));
    }

    @Test
    public void updateShouldUpdateExistingVehicle() {
        VehicleDto vehicleDto = VehicleDataBuilder.buildVehicleDto(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS));

        Vehicle existingVehicle = VehicleDataBuilder.buildVehicle(VEHICLE_ID, VEHICLE_PLATE_NUMBER, VehicleType.BUS, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS), StationDataBuilder.buildStation(1L));
        Vehicle updatedVehicle = VehicleDataBuilder.buildVehicle(VEHICLE_ID, VEHICLE_PLATE_NUMBER_UPDATED, VehicleType.BUS, CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.BUS), StationDataBuilder.buildStation(1L));

        when(vehicleDbValidatorMock.validateUpdateVehicleRequest(vehicleDto)).thenReturn(existingVehicle);
        when(modelMapperMock.map(vehicleDto, Vehicle.class)).thenReturn(updatedVehicle);

        vehicleService.update(vehicleDto);

        verify(vehicleDbValidatorMock, times(1)).validateUpdateVehicleRequest(vehicleDto);
        verify(modelMapperMock, times(1)).map(vehicleDto, Vehicle.class);
        verify(vehicleStoreMock, times(1)).save(updatedVehicle);
    }

    private Page<Vehicle> initVehiclePage() {
        Vehicle vehicle1 = VehicleDataBuilder.buildVehicle(1L, "NS 001 AA", VehicleType.BUS);
        Vehicle vehicle2 = VehicleDataBuilder.buildVehicle(2L, "NS 002 AA", VehicleType.BUS);
        Vehicle vehicle3 = VehicleDataBuilder.buildVehicle(3L, "NS 003 AA", VehicleType.BUS);

        return new PageImpl<>(Arrays.asList(vehicle1, vehicle2, vehicle3));
    }
}
