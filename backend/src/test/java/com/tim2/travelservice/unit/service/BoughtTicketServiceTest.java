package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.data.builder.BoughtTicketBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.BoughtTicket;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.BoughtTicketService;
import com.tim2.travelservice.store.BoughtTicketStore;
import com.tim2.travelservice.validator.db.BoughtTicketDbValidator;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class BoughtTicketServiceTest {

    @Autowired
    private BoughtTicketService boughtTicketService;

    @MockBean
    private BoughtTicketStore boughtTicketStoreMock;

    @MockBean
    private BoughtTicketDbValidator boughtTicketDbValidatorMock;

    @Test
    public void findByAccountEmailShouldReturnBoughtTickets() {
        Page<BoughtTicket> boughtTickets = initBoughtTicketPage();
        when(boughtTicketStoreMock.findByAccountEmail(SYSTEM_ADMINISTRATOR_EMAIL, PageRequest.of(0, 20))).thenReturn(boughtTickets);

        Page<BoughtTicketDto> boughtTicketDtos = boughtTicketService.findByAccountEmail(SYSTEM_ADMINISTRATOR_EMAIL, PageRequest.of(0, 20));

        verify(boughtTicketStoreMock, times(1)).findByAccountEmail(SYSTEM_ADMINISTRATOR_EMAIL, PageRequest.of(0, 20));
        boughtTicketDtos.getContent().forEach(boughtTicketDto -> assertThat("Principal is ticket owner.", boughtTicketDto.getAccount().getEmail(), is(SYSTEM_ADMINISTRATOR_EMAIL)));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void validateShouldThrowNotFoundWithExplanationWhenValidatingNonExistingBoughtTicket() {
        when(boughtTicketDbValidatorMock.validateValidateRequest(1L))
                .thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID)));

        boughtTicketService.validate(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateValidateRequest(1L);
    }

    @Test
    public void validateShouldValidateActiveSingleRideTicket() {
        BoughtTicket boughtTicket = BoughtTicketBuilder.buildBoughtTicket(1L, true, TicketDataBuilder.buildTicket(TicketDuration.SINGLE_RIDE));
        when(boughtTicketDbValidatorMock.validateValidateRequest(1L)).thenReturn(boughtTicket);

        DynamicResponse dynamicResponse = boughtTicketService.validate(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateValidateRequest(1L);
        verify(boughtTicketStoreMock, times(1)).save(boughtTicket);

        assertTrue("Ticket validity is returned.", (Boolean) dynamicResponse.get(RestApiConstants.IS_ACTIVE));
        assertFalse("Ticket validity is set to false after being used.", boughtTicket.getActive());
    }

    @Test
    public void validateShouldValidateActiveDailyTicket() {
        BoughtTicket boughtTicket = BoughtTicketBuilder.buildBoughtTicket(1L, true, DateTime.now(DateTimeZone.UTC), TicketDataBuilder.buildTicket(TicketDuration.DAILY));
        when(boughtTicketDbValidatorMock.validateValidateRequest(1L)).thenReturn(boughtTicket);

        DynamicResponse dynamicResponse = boughtTicketService.validate(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateValidateRequest(1L);
        verify(boughtTicketStoreMock, times(0)).save(any(BoughtTicket.class));

        assertTrue("Ticket validity is returned.", (Boolean) dynamicResponse.get(RestApiConstants.IS_ACTIVE));
        assertTrue("Ticket validity is true after being used.", boughtTicket.getActive());
    }

    @Test
    public void validateShouldValidateActiveAnnualTicket() {
        BoughtTicket boughtTicket = BoughtTicketBuilder.buildBoughtTicket(1L, true, DateTime.now(DateTimeZone.UTC), TicketDataBuilder.buildTicket(TicketDuration.ANNUAL));
        when(boughtTicketDbValidatorMock.validateValidateRequest(1L)).thenReturn(boughtTicket);

        DynamicResponse dynamicResponse = boughtTicketService.validate(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateValidateRequest(1L);
        verify(boughtTicketStoreMock, times(0)).save(any(BoughtTicket.class));

        assertTrue("Ticket validity is returned.", (Boolean) dynamicResponse.get(RestApiConstants.IS_ACTIVE));
        assertTrue("Ticket validity is true after being used.", boughtTicket.getActive());
    }

    @Test
    public void validateShouldValidateInactiveDailyTicket() {
        BoughtTicket boughtTicket = BoughtTicketBuilder.buildBoughtTicket(1L, true, DateTime.now(DateTimeZone.UTC).minusDays(1), TicketDataBuilder.buildTicket(TicketDuration.DAILY));
        when(boughtTicketDbValidatorMock.validateValidateRequest(1L)).thenReturn(boughtTicket);

        DynamicResponse dynamicResponse = boughtTicketService.validate(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateValidateRequest(1L);
        verify(boughtTicketStoreMock, times(1)).save(boughtTicket);

        assertFalse("Ticket validity is returned.", (Boolean) dynamicResponse.get(RestApiConstants.IS_ACTIVE));
        assertFalse("Ticket validity is false after validation.", boughtTicket.getActive());
    }

    @Test
    public void validateShouldValidateInactiveMonthlyTicket() {
        BoughtTicket boughtTicket = BoughtTicketBuilder.buildBoughtTicket(1L, true, DateTime.now(DateTimeZone.UTC).minusMonths(2), TicketDataBuilder.buildTicket(TicketDuration.MONTHLY));
        when(boughtTicketDbValidatorMock.validateValidateRequest(1L)).thenReturn(boughtTicket);

        DynamicResponse dynamicResponse = boughtTicketService.validate(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateValidateRequest(1L);
        verify(boughtTicketStoreMock, times(1)).save(boughtTicket);

        assertFalse("Ticket validity is returned.", (Boolean) dynamicResponse.get(RestApiConstants.IS_ACTIVE));
        assertFalse("Ticket validity is false after validation.", boughtTicket.getActive());
    }

    @Test
    public void validateShouldValidateInactiveAnnualTicket() {
        BoughtTicket boughtTicket = BoughtTicketBuilder.buildBoughtTicket(1L, true, DateTime.now(DateTimeZone.UTC).minusYears(2), TicketDataBuilder.buildTicket(TicketDuration.ANNUAL));
        when(boughtTicketDbValidatorMock.validateValidateRequest(1L)).thenReturn(boughtTicket);

        DynamicResponse dynamicResponse = boughtTicketService.validate(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateValidateRequest(1L);
        verify(boughtTicketStoreMock, times(1)).save(boughtTicket);

        assertFalse("Ticket validity is returned.", (Boolean) dynamicResponse.get(RestApiConstants.IS_ACTIVE));
        assertFalse("Ticket validity is false after validation.", boughtTicket.getActive());
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void findByIdShouldThrowNotFoundWithExplanationExceptionWhenFindingNonExistingBoughtTicket() {
        when(boughtTicketDbValidatorMock.validateFindByIdRequest(1L))
                .thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID)));

        boughtTicketService.findById(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateFindByIdRequest(1L);
    }

    @Test
    public void findByIdShouldReturnBoughtTicket() {
        BoughtTicket boughtTicket = BoughtTicketBuilder.buildBoughtTicket(1L);
        when(boughtTicketDbValidatorMock.validateFindByIdRequest(1L)).thenReturn(boughtTicket);

        BoughtTicketDto boughtTicketDto = boughtTicketService.findById(1L);

        verify(boughtTicketDbValidatorMock, times(1)).validateFindByIdRequest(1L);
        assertThat("Bought ticket with given id is returned.", boughtTicketDto.getId(), is(1L));
    }


    private Page<BoughtTicket> initBoughtTicketPage() {
        BoughtTicket boughtTicket1 = BoughtTicketBuilder.buildBoughtTicket(1L, AccountDataBuilder.buildAccount(SYSTEM_ADMINISTRATOR_EMAIL));
        BoughtTicket boughtTicket2 = BoughtTicketBuilder.buildBoughtTicket(2L, AccountDataBuilder.buildAccount(SYSTEM_ADMINISTRATOR_EMAIL));
        BoughtTicket boughtTicket3 = BoughtTicketBuilder.buildBoughtTicket(3L, AccountDataBuilder.buildAccount(SYSTEM_ADMINISTRATOR_EMAIL));

        return new PageImpl<>(Arrays.asList(boughtTicket1, boughtTicket2, boughtTicket3));
    }
}
