package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.PriceListService;
import com.tim2.travelservice.store.PriceListStore;
import com.tim2.travelservice.validator.db.PriceListDbValidator;
import com.tim2.travelservice.web.dto.PriceListDto;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class PriceListServiceTest {

    @Autowired
    private PriceListService priceListService;

    @MockBean
    private PriceListStore priceListStoreMock;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private PriceListDbValidator priceListDbValidatorMock;

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateBasicInformationShouldThrowNotFoundWithExplanationWhenPriceListIsNotExisting() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);

        when(priceListDbValidatorMock.validateUpdateRequest(priceListDto))
                .thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)));

        priceListService.updateBasicInformation(priceListDto);

        verify(priceListDbValidatorMock, times(1)).validateUpdateRequest(priceListDto);
    }

    @Test
    public void updateShouldNotUpdateNonUpdatableFieldsIfTheyAreProvided() {
        ArrayList<PriceListItemDto> priceListItems = new ArrayList<>();
        ArrayList<PriceListItem> priceListItemsExisting = new ArrayList<>();
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_END_DATE, priceListItems);
        PriceList existingPriceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE, priceListItemsExisting);
        PriceList updatedPriceList = PriceListDataBuilder.buildPriceList(null);

        when(priceListDbValidatorMock.validateUpdateRequest(priceListDto)).thenReturn(existingPriceList);
        when(modelMapperMock.map(priceListDto, PriceList.class)).thenReturn(updatedPriceList);

        priceListService.updateBasicInformation(priceListDto);

        verify(priceListDbValidatorMock, times(1)).validateUpdateRequest(priceListDto);
        verify(modelMapperMock, times(1)).map(priceListDto, PriceList.class);
        verify(priceListStoreMock, times(1)).save(existingPriceList);

        assertThat("Price list item list is not updated", existingPriceList.getPriceListItems(), is(priceListItemsExisting));
    }

    @Test
    public void updateShouldUpdatePriceList() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceList existingPriceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_START_DATE_UPDATED, PRICE_LIST_END_DATE_UPDATED);
        PriceList updatedPriceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_START_DATE_UPDATED, PRICE_LIST_END_DATE_UPDATED);

        when(priceListDbValidatorMock.validateUpdateRequest(priceListDto)).thenReturn(existingPriceList);
        when(modelMapperMock.map(priceListDto, PriceList.class)).thenReturn(updatedPriceList);

        priceListService.updateBasicInformation(priceListDto);

        verify(priceListDbValidatorMock, times(1)).validateUpdateRequest(priceListDto);
        verify(modelMapperMock, times(1)).map(priceListDto, PriceList.class);
        verify(priceListStoreMock, times(1)).save(existingPriceList);
        assertThat("Start date is updated.", existingPriceList.getStartDate(), is(DateTime.parse(PRICE_LIST_START_DATE_UPDATED)));
        assertThat("End date is updated.", existingPriceList.getEndDate(), is(DateTime.parse(PRICE_LIST_END_DATE_UPDATED)));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void deleteByIdShouldThrowNotFoundWithExplanationWhenPriceListIsNotExisting() {
        when(priceListDbValidatorMock.validateDeleteByIdRequest(PRICE_LIST_ID)).thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)));
        priceListService.deleteById(PRICE_LIST_ID);

        verify(priceListDbValidatorMock, times(1)).validateDeleteByIdRequest(PRICE_LIST_ID);
    }

    @Test
    public void deleteByIdShouldDeletePriceList() {
        PriceList existingPriceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_START_DATE_UPDATED, PRICE_LIST_END_DATE_UPDATED);

        when(priceListDbValidatorMock.validateDeleteByIdRequest(PRICE_LIST_ID)).thenReturn(existingPriceList);

        priceListService.deleteById(PRICE_LIST_ID);

        verify(priceListDbValidatorMock, times(1)).validateDeleteByIdRequest(PRICE_LIST_ID);
        verify(priceListStoreMock, times(1)).delete(existingPriceList);
    }

    @Test
    public void createShouldAddPriceList() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE_UPDATED, PRICE_LIST_END_DATE_UPDATED);
        PriceList priceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE_UPDATED, PRICE_LIST_END_DATE_UPDATED);

        when(modelMapperMock.map(priceListDto, PriceList.class)).thenReturn(priceList);
        when(priceListStoreMock.save(priceList)).thenReturn(priceList);

        priceListService.create(priceListDto);

        verify(modelMapperMock, times(1)).map(priceListDto, PriceList.class);
        verify(priceListStoreMock, times(1)).save(priceList);
    }

    @Test
    public void findPriceListByIdShouldReturnPriceList() {
        PriceList existingPriceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE_UPDATED, PRICE_LIST_END_DATE_UPDATED);

        when(priceListDbValidatorMock.validateFindByIdRequest(PRICE_LIST_ID)).thenReturn(existingPriceList);

        PriceListDto priceListDto = priceListService.findById(PRICE_LIST_ID);

        assertThat("Start date is correct.", priceListDto.getStartDate(), is(DateTime.parse(PRICE_LIST_START_DATE_UPDATED)));
        assertThat("End date is correct.", priceListDto.getEndDate(), is(DateTime.parse(PRICE_LIST_END_DATE_UPDATED)));
    }


    @Test(expected = NotFoundWithExplanationException.class)
    public void findByIdShouldThrowNotFoundWithExplanationWhenPriceListIsNotExisting() {
        when(priceListDbValidatorMock.validateFindByIdRequest(PRICE_LIST_ID))
                .thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)));

        priceListService.findById(PRICE_LIST_ID);

        verify(priceListDbValidatorMock, times(1)).validateFindByIdRequest(PRICE_LIST_ID);
    }

    @Test
    public void findByIdShouldReturnPriceList() {
        PriceList priceList = PriceListDataBuilder.buildPriceList(1L, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        when(priceListDbValidatorMock.validateFindByIdRequest(1L)).thenReturn(priceList);

        PriceListDto priceListDto = priceListService.findById(1L);

        verify(priceListDbValidatorMock, times(1)).validateFindByIdRequest(1L);
        assertThat("Price list with given id is returned.", priceListDto.getId(), is(1L));
    }

    @Test
    public void findAllShouldReturnPriceLists() {
        Page<PriceList> priceLists = initPriceListPage();

        when(priceListStoreMock.findAll(PageRequest.of(0, 20))).thenReturn(priceLists);

        Page<PriceListDto> priceListDtos = priceListService.findAll(PageRequest.of(0, 20));

        verify(priceListStoreMock, times(1)).findAll(PageRequest.of(0, 20));
        assertThat("Right number of price lists is returned.", priceListDtos.getContent().size(), is(3));
    }

    private Page<PriceList> initPriceListPage() {
        PriceList priceList1 = PriceListDataBuilder.buildPriceList(1L, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceList priceList2 = PriceListDataBuilder.buildPriceList(2L, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceList priceList3 = PriceListDataBuilder.buildPriceList(3L, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        return new PageImpl<>(Arrays.asList(priceList1, priceList2, priceList3));
    }
}