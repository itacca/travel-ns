package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.StationService;
import com.tim2.travelservice.validator.dto.StationDtoValidator;
import com.tim2.travelservice.web.dto.LineStationDto;
import com.tim2.travelservice.web.dto.StationDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Collections;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class StationControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private StationService stationServiceMock;

    @MockBean
    private StationDtoValidator stationDtoValidatorMock;

    private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser
    public void findByIdShouldReturnNotFoundWhenGettingNonExistingStation() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(stationServiceMock).findById(999L);

        String url = String.format("%s/%d", RestApiEndpoints.STATION, 999L);
        mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID))));

        verify(stationServiceMock, times(1)).findById(999L);
    }

    @Test
    @WithMockUser
    public void findByIdShouldReturnOkAndStation() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(1L, STATION_NAME, STATION_LONGITUDE, STATION_LATITUDE, Collections.emptyList());

        when(stationServiceMock.findById(1L)).thenReturn(stationDto);

        String url = String.format("%s/%d", RestApiEndpoints.STATION, 1L);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(stationServiceMock, times(1)).findById(1L);
        andExpectedStationIsReturned(mvcResult);
    }

    @Test
    public void deleteByIdShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.STATION, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(stationServiceMock, times(0)).deleteById(anyLong());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void deleteByIdShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.STATION, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(stationServiceMock, times(0)).deleteById(anyLong());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void deleteByIdShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.STATION, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(stationServiceMock, times(0)).deleteById(anyLong());
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteByIdShouldReturnNotFoundWhenDeletingNonExistingStation() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(stationServiceMock).deleteById(999L);

        String url = String.format("%s/%d", RestApiEndpoints.STATION, 999L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID))));

        verify(stationServiceMock, times(1)).deleteById(999L);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteByIdShouldReturnNoContentWhenDeletingExistingStation() throws Exception {
        doNothing().when(stationServiceMock).deleteById(1L);

        String url = String.format("%s/%d", RestApiEndpoints.STATION, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(stationServiceMock, times(1)).deleteById(1L);
    }

    @Test
    public void updateShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(stationDtoValidatorMock, times(0)).validateUpdateRequest(any(StationDto.class));
        verify(stationServiceMock, times(0)).update(any(StationDto.class));
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void updateShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(stationDtoValidatorMock, times(0)).validateUpdateRequest(any(StationDto.class));
        verify(stationServiceMock, times(0)).update(any(StationDto.class));
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void updateShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(stationDtoValidatorMock, times(0)).validateUpdateRequest(any(StationDto.class));
        verify(stationServiceMock, times(0)).update(any(StationDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenIdIsNull() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(stationDtoValidatorMock).validateUpdateRequest(stationDto);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(stationDtoValidatorMock, times(1)).validateUpdateRequest(stationDto);
        verify(stationServiceMock, times(0)).update(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenNameIsEmptyString() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, "", STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME)))
                .when(stationDtoValidatorMock).validateUpdateRequest(stationDto);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME))));

        verify(stationDtoValidatorMock, times(1)).validateUpdateRequest(stationDto);
        verify(stationServiceMock, times(0)).update(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnBadRequestWhenLineStationsIsNotNull() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, Collections.singletonList(new LineStationDto()));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS)))
                .when(stationDtoValidatorMock).validateUpdateRequest(stationDto);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS))));

        verify(stationDtoValidatorMock, times(1)).validateUpdateRequest(stationDto);
        verify(stationServiceMock, times(0)).update(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnNotFoundWhenUpdatingNonExistingStation() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(9999L, STATION_NAME_UPDATED, STATION_LONGITUDE_UPDATED, STATION_LATITUDE_UPDATED, null);

        doNothing().when(stationDtoValidatorMock).validateUpdateRequest(stationDto);
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(stationServiceMock).update(stationDto);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID))));

        verify(stationDtoValidatorMock, times(1)).validateUpdateRequest(stationDto);
        verify(stationServiceMock, times(1)).update(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateShouldReturnNoContentAndUpdateStation() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(STATION_ID, STATION_NAME_UPDATED, null, null, null);

        doNothing().when(stationDtoValidatorMock).validateUpdateRequest(stationDto);

        String url = String.format("%s", RestApiEndpoints.STATION);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(stationDtoValidatorMock, times(1)).validateUpdateRequest(stationDto);
        verify(stationServiceMock, times(1)).update(stationDto);
    }

    private void andExpectedStationIsReturned(MvcResult mvcResult) throws IOException {
        StationDto stationDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), StationDto.class);

        assertThat("Correct id is returned.", stationDto.getId(), is(1L));
        assertThat("Correct station name is returned.", stationDto.getName(), is(STATION_NAME));
        assertThat("Correct station longitude is returned.", stationDto.getLongitude(), is(STATION_LONGITUDE));
        assertThat("Correct station latitude is returned.", stationDto.getLatitude(), is(STATION_LATITUDE));
    }
}
