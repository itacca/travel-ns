package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.PriceListService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.validator.dto.PriceListDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.PriceListDto;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class PriceListsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private PriceListService priceListServiceMock;

    @MockBean
    private PriceListDtoValidator priceListDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnCreatedWhenPriceListIsSuccessfullyAdded() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE, Collections.emptyList());
        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 6L);

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        when(priceListServiceMock.create(priceListDto)).thenReturn(dynamicResponse);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(6)));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void createShouldReturnForbiddenWhenRoleIsVerifier() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE, Collections.emptyList());

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListDtoValidatorMock, times(0)).validateCreateRequest(any(PriceListDto.class));
        verify(priceListServiceMock, times(0)).create(any(PriceListDto.class));
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void createShouldReturnForbiddenWhenRoleIsPassenger() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE, Collections.emptyList());

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListDtoValidatorMock, times(0)).validateCreateRequest(any(PriceListDto.class));
        verify(priceListServiceMock, times(0)).create(any(PriceListDto.class));
    }

    @Test
    @WithMockUser()
    public void createShouldReturnForbiddenWhenRoleIsGuest() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE, Collections.emptyList());

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(priceListDtoValidatorMock, times(0)).validateCreateRequest(any(PriceListDto.class));
        verify(priceListServiceMock, times(0)).create(any(PriceListDto.class));
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenStartingDateIsNull() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_END_DATE, Collections.emptyList());

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.START_DATE)))
                .when(priceListServiceMock).create(priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.START_DATE))));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenEndingDateIsNull() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDtoWithStartDate(PRICE_LIST_START_DATE, Collections.emptyList());

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE)))
                .when(priceListServiceMock).create(priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE))));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenDateIntervalIsBad() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_END_DATE, PRICE_LIST_START_DATE, Collections.emptyList());

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.INVALID_DATE_INTERVAL))
                .when(priceListServiceMock).create(priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.INVALID_DATE_INTERVAL)));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenDateIntervalIsOverlappingWithAnother() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_END_DATE, PRICE_LIST_START_DATE, Collections.emptyList());

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.INTERVALS_ARE_OVERLAPPING))
                .when(priceListServiceMock).create(priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.INTERVALS_ARE_OVERLAPPING)));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenPriceListItemListIsNull() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST_ITEMS)))
                .when(priceListServiceMock).create(priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST_ITEMS))));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenPriceListItemListIsNotEmpty() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_END_DATE, PRICE_LIST_START_DATE, Collections.singletonList(new PriceListItemDto()));

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.collectionShouldBeEmpty(RestApiConstants.PRICE_LIST_ITEMS)))
                .when(priceListServiceMock).create(priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.collectionShouldBeEmpty(RestApiConstants.PRICE_LIST_ITEMS))));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenIdIsProvided() throws Exception {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_END_DATE, PRICE_LIST_START_DATE);

        doNothing().when(priceListDtoValidatorMock).validateCreateRequest(priceListDto);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(priceListServiceMock).create(priceListDto);

        mockMvc.perform(post(RestApiEndpoints.PRICE_LISTS)
                .content(objectMapper.writeValueAsString(priceListDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(priceListDtoValidatorMock, times(1)).validateCreateRequest(priceListDto);
        verify(priceListServiceMock, times(1)).create(priceListDto);
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void findAllShouldReturnOkAndPriceLists() throws Exception {
        Page<PriceListDto> priceListDtos = initTestData();

        when(priceListServiceMock.findAll(PageRequest.of(0, 20))).thenReturn(priceListDtos);

        String url = String.format("%s", RestApiEndpoints.PRICE_LISTS);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(priceListServiceMock, times(1)).findAll(PageRequest.of(0, 20));
        andExpectedPriceListsAreReturned(mvcResult);
    }

    private void andExpectedPriceListsAreReturned(MvcResult mvcResult) throws IOException {
        List<PriceListDto> priceListDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<PriceListDto>>() {
        });
        assertThat("Price lists length is correct.", priceListDtos.size(), is(3));
    }

    private Page<PriceListDto> initTestData() {
        PriceListDto priceListDto1 = PriceListDataBuilder.buildPriceListDto(1L, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListDto priceListDto2 = PriceListDataBuilder.buildPriceListDto(2L, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListDto priceListDto3 = PriceListDataBuilder.buildPriceListDto(3L, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        return new PageImpl<>(Arrays.asList(priceListDto1, priceListDto2, priceListDto3));
    }
}