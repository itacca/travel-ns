package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.AccountDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.AccountService;
import com.tim2.travelservice.validator.dto.AccountDtoValidator;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class AccountControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private AccountService accountServiceMock;

    @MockBean
    private AccountDtoValidator accountDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void getLoggedUserInfoShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/me", RestApiEndpoints.ACCOUNT);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(accountServiceMock, times(0)).findByEmail(anyString());
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void getLoggedUserInfoShouldReturnOkAndAccount() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(SYSTEM_ADMINISTRATOR_ID, SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_FIRST_NAME, SYSTEM_ADMINISTRATOR_LAST_NAME);

        when(accountServiceMock.findByEmail(SYSTEM_ADMINISTRATOR_EMAIL)).thenReturn(accountDto);

        String url = String.format("%s/me", RestApiEndpoints.ACCOUNT);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(accountServiceMock, times(1)).findByEmail(SYSTEM_ADMINISTRATOR_EMAIL);
        andExpectedAccountIsReturned(mvcResult);
    }

    @Test
    public void findByEmailShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s?email=%s", RestApiEndpoints.ACCOUNT, SYSTEM_ADMINISTRATOR_EMAIL);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(accountServiceMock, times(0)).findByEmail(anyString());
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void findByEmailShouldReturnNotFoundWhenGettingNonExistingAccount() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL)))
                .when(accountServiceMock).findByEmail("random@email.com");

        String url = String.format("%s?email=%s", RestApiEndpoints.ACCOUNT, "random@email.com");
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL))));

        verify(accountServiceMock, times(1)).findByEmail("random@email.com");
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void findByEmailShouldReturnOkAndAccountWhenGettingExistingAccount() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(SYSTEM_ADMINISTRATOR_ID, SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_FIRST_NAME, SYSTEM_ADMINISTRATOR_LAST_NAME);

        when(accountServiceMock.findByEmail(SYSTEM_ADMINISTRATOR_EMAIL)).thenReturn(accountDto);

        String url = String.format("%s?email=%s", RestApiEndpoints.ACCOUNT, SYSTEM_ADMINISTRATOR_EMAIL);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(accountServiceMock, times(1)).findByEmail(SYSTEM_ADMINISTRATOR_EMAIL);
        andExpectedAccountIsReturned(mvcResult);
    }

    @Test
    public void updateLoggedUserShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED);

        mockMvc.perform(put(RestApiEndpoints.ACCOUNT)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(accountDtoValidatorMock, times(0)).validateUpdateRequest(any(AccountDto.class));
        verify(accountServiceMock, times(0)).updateByEmail(anyString(), any(AccountDto.class));
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void updateLoggedUserShouldReturnBadRequestWhenPasswordIsLessThanEightCharacters() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_SHORT_PASSWORD_UPDATE, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH)))
                .when(accountDtoValidatorMock).validateUpdateRequest(accountDto);

        mockMvc.perform(put(RestApiEndpoints.ACCOUNT)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH))));

        verify(accountDtoValidatorMock, times(1)).validateUpdateRequest(accountDto);
        verify(accountServiceMock, times(0)).updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, accountDto);
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void updateLoggedUserShouldReturnBadRequestWhenFirstNameIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, "", null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME)))
                .when(accountDtoValidatorMock).validateUpdateRequest(accountDto);

        mockMvc.perform(put(RestApiEndpoints.ACCOUNT)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateUpdateRequest(accountDto);
        verify(accountServiceMock, times(0)).updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, accountDto);
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void updateLoggedUserShouldReturnBadRequestWhenLastNameIsEmptyString() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, null, "");

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME)))
                .when(accountDtoValidatorMock).validateUpdateRequest(accountDto);

        mockMvc.perform(put(RestApiEndpoints.ACCOUNT)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME))));

        verify(accountDtoValidatorMock, times(1)).validateUpdateRequest(accountDto);
        verify(accountServiceMock, times(0)).updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, accountDto);
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void updateLoggedUserShouldReturnBadRequestWhenDateOfBirthHasInvalidFormat() throws Exception {
        DynamicResponse accountDto = AccountDataBuilder.buildAccountDtoJson(null, null, null, ACCOUNT_INVALID_DATE_OF_BIRTH_UPDATED);

        mockMvc.perform(put(RestApiEndpoints.ACCOUNT)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void shouldReturnBadRequestWhenDateOfBirthIsInTheFuture() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(null, ACCOUNT_REGISTRATION_FUTURE_DATE_OF_BIRTH);

        doThrow(new InvalidRequestDataException(RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST))
                .when(accountDtoValidatorMock).validateUpdateRequest(accountDto);

        mockMvc.perform(put(RestApiEndpoints.ACCOUNT)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST)));

        verify(accountDtoValidatorMock, times(1)).validateUpdateRequest(accountDto);
        verify(accountServiceMock, times(0)).updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, accountDto);
    }

    @Test
    @WithMockUser(username = SYSTEM_ADMINISTRATOR_EMAIL, roles = "SYSTEM_ADMINISTRATOR")
    public void updateLoggedUserShouldReturnNoContentAndUpdateAccount() throws Exception {
        AccountDto accountDto = AccountDataBuilder.buildAccountDto(ACCOUNT_PASSWORD_UPDATED, ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED);

        doNothing().when(accountDtoValidatorMock).validateUpdateRequest(accountDto);

        mockMvc.perform(put(RestApiEndpoints.ACCOUNT)
                .with(csrf())
                .content(objectMapper.writeValueAsString(accountDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(accountDtoValidatorMock, times(1)).validateUpdateRequest(accountDto);
        verify(accountServiceMock, times(1)).updateByEmail(SYSTEM_ADMINISTRATOR_EMAIL, accountDto);
    }

    @Test
    public void deleteTravelAdminByIdShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(accountServiceMock, times(0)).deleteTravelAdminById(anyLong());
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void deleteTravelAdminByIdShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(accountServiceMock, times(0)).deleteTravelAdminById(anyLong());
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void deleteTravelAdminByIdShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(accountServiceMock, times(0)).deleteTravelAdminById(anyLong());
    }

    @Test
    @WithMockUser(roles = "ADMINISTRATOR")
    public void deleteTravelAdminByIdShouldReturnForbiddenWhenUserIsTravelAdmin() throws Exception {
        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(accountServiceMock, times(0)).deleteTravelAdminById(anyLong());
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteTravelAdminByIdShouldReturnBadRequestWhenGettingUserThatIsNotTravelAdmin() throws Exception {
        doThrow(new InvalidRequestDataException(RestApiErrors.USER_IS_NOT_TRAVEL_ADMIN))
                .when(accountServiceMock).deleteTravelAdminById(1L);

        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, 1L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        verify(accountServiceMock, times(1)).deleteTravelAdminById(1L);
    }

    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteTravelAdminByIdShouldReturnNotFoundWhenGettingNonExistingTravelAdmin() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID)))
                .when(accountServiceMock).deleteTravelAdminById(999L);

        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, 999L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());

        verify(accountServiceMock, times(1)).deleteTravelAdminById(999L);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void deleteTravelAdminByIdShouldReturnNoContentWhenDeletingExistingTravelAdmin() throws Exception {
        doNothing().when(accountServiceMock).deleteTravelAdminById(2L);

        String url = String.format("%s/travel-admin/%d", RestApiEndpoints.ACCOUNT, 2L);
        mockMvc.perform(delete(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(accountServiceMock, times(1)).deleteTravelAdminById(2L);
    }

    private void andExpectedAccountIsReturned(MvcResult mvcResult) throws IOException {
        AccountDto accountDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), AccountDto.class);

        assertThat("Account Id", accountDto.getId(), is(SYSTEM_ADMINISTRATOR_ID));
        assertThat("Account email", accountDto.getEmail(), is(SYSTEM_ADMINISTRATOR_EMAIL));
        assertThat("First name", accountDto.getFirstName(), is(SYSTEM_ADMINISTRATOR_FIRST_NAME));
        assertThat("Last name", accountDto.getLastName(), is(SYSTEM_ADMINISTRATOR_LAST_NAME));
        assertThat("Password", accountDto.getPassword(), isEmptyOrNullString());
    }
}
