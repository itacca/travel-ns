package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.PriceListDataBuilder;
import com.tim2.travelservice.data.builder.PriceListItemDataBuilder;
import com.tim2.travelservice.data.builder.TicketDataBuilder;
import com.tim2.travelservice.entity.*;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.PriceListItemService;
import com.tim2.travelservice.store.PriceListItemStore;
import com.tim2.travelservice.validator.db.PriceListItemDbValidator;
import com.tim2.travelservice.web.dto.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class PriceListItemServiceTest {
    @Autowired
    private PriceListItemService priceListItemService;

    @MockBean
    private PriceListItemStore priceListItemStoreMock;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private PriceListItemDbValidator priceListItemDbValidatorMock;

    @Test(expected = NotFoundWithExplanationException.class)
    public void createShouldShouldThrowNotFoundWithExplanationWhenPriceListIsNotExisting() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, new TicketDto(), priceListDto);

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID)))
                .when(priceListItemDbValidatorMock).validateCreateRequest(priceListItemDto);

        priceListItemService.create(priceListItemDto);

        verify(priceListItemDbValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
    }

    @Test(expected = InvalidRequestDataException.class)
    public void createShouldShouldThrowBadRequestExceptionWhenPriceListItemForGivenTicketAlreadyExists() {
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, new TicketDto(), priceListDto);

        doThrow(new InvalidRequestDataException(RestApiErrors.CREATE_PRICE_LIST_ITEM_ERROR))
                .when(priceListItemDbValidatorMock).validateCreateRequest(priceListItemDto);

        priceListItemService.create(priceListItemDto);

        verify(priceListItemDbValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
    }

    @Test
    public void createShouldAddPriceListItemAndReturnPriceListItemId() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        CityLine cityLine = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        Ticket ticket = TicketDataBuilder.buildTicket(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLine);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceList priceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);
        PriceListItem priceListItem = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE, ticket, priceList);

        doNothing().when(priceListItemDbValidatorMock).validateCreateRequest(priceListItemDto);
        doNothing().when(priceListItemDbValidatorMock).validatePriceListItemForGivenTicketDoesNotExist(priceList, priceListItemDto);
        when(modelMapperMock.map(priceListItemDto, PriceListItem.class)).thenReturn(priceListItem);
        when(priceListItemStoreMock.save(priceListItem)).thenReturn(priceListItem);

        DynamicResponse dynamicResponse = priceListItemService.create(priceListItemDto);

        verify(priceListItemDbValidatorMock, times(1)).validateCreateRequest(priceListItemDto);
        verify(modelMapperMock, times(1)).map(priceListItemDto, PriceListItem.class);
        verify(priceListItemStoreMock, times(1)).save(priceListItem);
        assertThat("Price list item id is returned.", dynamicResponse.get(RestApiConstants.ID), is(6L));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void deleteByIdShouldThrowNotFoundWithExplanationWhenPriceListItemIsNotExisting() {
        when(priceListItemDbValidatorMock.validateDeletePriceListItemRequest(PRICE_LIST_ITEM_ID)).thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST_ITEM, RestApiConstants.ID)));
        priceListItemService.deleteById(PRICE_LIST_ITEM_ID);

        verify(priceListItemDbValidatorMock, times(1)).validateDeletePriceListItemRequest(PRICE_LIST_ITEM_ID);
    }

    @Test
    public void deletePriceListShouldDeletePriceList() {
        CityLine cityLine = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        Ticket ticket = TicketDataBuilder.buildTicket(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLine);
        PriceList priceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItem existingPriceListItem = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE, ticket, priceList);
        PriceListItem deletedPriceListItem = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE, ticket, false, priceList);

        when(priceListItemDbValidatorMock.validateDeletePriceListItemRequest(PRICE_LIST_ITEM_ID)).thenReturn(existingPriceListItem);
        when(priceListItemStoreMock.save(existingPriceListItem)).thenReturn(existingPriceListItem);

        priceListItemService.deleteById(PRICE_LIST_ITEM_ID);

        verify(priceListItemDbValidatorMock, times(1)).validateDeletePriceListItemRequest(PRICE_LIST_ITEM_ID);
        verify(priceListItemStoreMock, times(1)).save(existingPriceListItem);
        assertThat("Price list item is deleted.", deletedPriceListItem.getActive(), is(false));
    }

    @Test
    public void findByPriceListIdShouldReturnActivePriceListItems() {
        Page<PriceListItem> priceListItems = initPriceListItemPage();

        when(priceListItemStoreMock.findActiveByPriceListId(3L, PageRequest.of(0, 20))).thenReturn(priceListItems);

        Page<PriceListItemDto> priceListItemDtos = priceListItemService.findByPriceListId(3L, PageRequest.of(0, 20));

        verify(priceListItemStoreMock, times(1)).findActiveByPriceListId(3L, PageRequest.of(0, 20));
        priceListItemDtos.getContent().forEach(priceListItemDto -> assertThat("Price list id is correct.", priceListItemDto.getPriceList().getId(), is(PRICE_LIST_ID)));
        priceListItemDtos.getContent().forEach(priceListItemDto -> assertThat("Active is true.", priceListItemDto.getActive(), is(true)));
    }

    @Test
    public void findAllByPriceListAndTicketShouldReturnAllPriceListItems() {
        Page<PriceListItem> priceListItems = initAllPriceListItemPage();

        when(priceListItemStoreMock.findByPriceListIdAndTicketId(PRICE_LIST_ID, 5L, PageRequest.of(0, 20))).thenReturn(priceListItems);

        Page<PriceListItemDto> priceListItemDtos = priceListItemService.findAllByPriceListAndTicket(PRICE_LIST_ID, 5L, PageRequest.of(0, 20));

        verify(priceListItemStoreMock, times(1)).findByPriceListIdAndTicketId(PRICE_LIST_ID, 5L, PageRequest.of(0, 20));
        priceListItemDtos.getContent().forEach(priceListItemDto -> assertThat("Price list id is correct.", priceListItemDto.getPriceList().getId(), is(PRICE_LIST_ID)));
        priceListItemDtos.getContent().forEach(priceListItemDto -> assertThat("Ticket id is correct.", priceListItemDto.getTicket().getId(), is(5L)));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateShouldThrowNotFoundWithExplanationWhenPriceListItemIsNotExisting() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE, ticketDto, priceListDto);

        when(priceListItemDbValidatorMock.validateUpdatePriceListItemRequest(priceListItemDto))
                .thenThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST_ITEM, RestApiConstants.ID)));

        priceListItemService.update(priceListItemDto);

        verify(priceListItemDbValidatorMock, times(1)).validateUpdatePriceListItemRequest(priceListItemDto);
    }

    @Test
    public void updateShouldUpdatePriceList() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        CityLine cityLine = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        TicketDto ticketDto = TicketDataBuilder.buildTicketDto(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLineDto);
        Ticket ticket = TicketDataBuilder.buildTicket(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLine);
        PriceListDto priceListDto = PriceListDataBuilder.buildPriceListDto(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceList priceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);
        PriceListItemDto priceListItemDto = PriceListItemDataBuilder.buildPriceListItemDto(PRICE_LIST_ITEM_PRICE_NEW, ticketDto, priceListDto);
        PriceListItem existingPriceListItem = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_ID, PRICE_LIST_ITEM_PRICE, ticket, true, priceList);
        PriceListItem newPriceListItem = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_PRICE_NEW, ticket, true, priceList);
        PriceListItem newPriceListItemSaved = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_ID_NEW, PRICE_LIST_ITEM_PRICE_NEW, ticket, true, priceList);

        when(priceListItemDbValidatorMock.validateUpdatePriceListItemRequest(priceListItemDto)).thenReturn(existingPriceListItem);
        existingPriceListItem.setActive(false);
        when(priceListItemStoreMock.save(existingPriceListItem)).thenReturn(existingPriceListItem);
        when(priceListItemStoreMock.save(any(PriceListItem.class))).thenReturn(newPriceListItemSaved);

        DynamicResponse dynamicResponse = priceListItemService.update(priceListItemDto);

        verify(priceListItemDbValidatorMock, times(1)).validateUpdatePriceListItemRequest(priceListItemDto);
        verify(priceListItemStoreMock, times(1)).save(existingPriceListItem);
        verify(priceListItemStoreMock, times(2)).save(any(PriceListItem.class));
        assertThat("Price list item id is returned.", dynamicResponse.get(RestApiConstants.ID), is(PRICE_LIST_ITEM_ID_NEW));
    }

    private Page<PriceListItem> initPriceListItemPage() {
        CityLine cityLine = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        Ticket ticket1 = TicketDataBuilder.buildTicket(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLine);
        Ticket ticket2 = TicketDataBuilder.buildTicket(6L, TicketType.SENIOR, TicketDuration.DAILY, cityLine);
        Ticket ticket3 = TicketDataBuilder.buildTicket(7L, TicketType.STUDENT, TicketDuration.DAILY, cityLine);
        PriceList priceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        PriceListItem priceListItem1 = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_PRICE, ticket1, true, priceList);
        PriceListItem priceListItem2 = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_PRICE, ticket2, true, priceList);
        PriceListItem priceListItem3 = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_PRICE, ticket3, true, priceList);

        return new PageImpl<>(Arrays.asList(priceListItem1, priceListItem2, priceListItem3));
    }

    private Page<PriceListItem> initAllPriceListItemPage() {
        CityLine cityLine = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.BUS);
        Ticket ticket1 = TicketDataBuilder.buildTicket(5L, TicketType.REGULAR, TicketDuration.DAILY, cityLine);
        PriceList priceList = PriceListDataBuilder.buildPriceList(PRICE_LIST_ID, PRICE_LIST_START_DATE, PRICE_LIST_END_DATE);

        PriceListItem priceListItem1 = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_PRICE, ticket1, true, priceList);
        PriceListItem priceListItem2 = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_PRICE, ticket1, false, priceList);
        PriceListItem priceListItem3 = PriceListItemDataBuilder.buildPriceListItem(PRICE_LIST_ITEM_PRICE, ticket1, false, priceList);

        return new PageImpl<>(Arrays.asList(priceListItem1, priceListItem2, priceListItem3));
    }
}
