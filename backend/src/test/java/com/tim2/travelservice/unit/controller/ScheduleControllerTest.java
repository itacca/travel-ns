package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.data.builder.ScheduleDataBuilder;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.ScheduleService;
import com.tim2.travelservice.validator.dto.ScheduleDtoValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class ScheduleControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private ScheduleService scheduleServiceMock;

    @MockBean
    private ScheduleDtoValidator scheduleDtoValidatorMock;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void getScheduleByCityLineIdAndTypeShouldReturnBadRequestWhenInvalidType() throws Exception {
        doThrow(new InvalidRequestDataException(RestApiErrors.invalidFieldFormat(RestApiConstants.SCHEDULE_TYPE)))
                .when(scheduleDtoValidatorMock).validateGetScheduleByTypeRequest(RANDOM_TYPE);

        String url = String.format("%s?city_line_id=%d&schedule_type=%s", RestApiEndpoints.SCHEDULE, FIRST_CITY_LINE_TRAM_TYPE_ID, RANDOM_TYPE);
        mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

        verify(scheduleDtoValidatorMock, times(1)).validateGetScheduleByTypeRequest(RANDOM_TYPE);
        verify(scheduleServiceMock, times(0)).findByCityLineIdAndType(FIRST_CITY_LINE_TRAM_TYPE_ID, RANDOM_TYPE);
    }

    @Test
    public void getScheduleByCityLineIdAndTypeShouldReturnRequestedSchedule() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        doNothing().when(scheduleDtoValidatorMock).validateGetScheduleByTypeRequest(WORK_DAY);

        when(scheduleServiceMock.findByCityLineIdAndType(FIRST_CITY_LINE_ID, WORK_DAY)).thenReturn(scheduleDto);

        String url = String.format("%s?city_line_id=%d&schedule_type=%s", RestApiEndpoints.SCHEDULE, FIRST_CITY_LINE_ID, WORK_DAY);
        MvcResult mvcResult = mockMvc.perform(get(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(scheduleDtoValidatorMock, times(1)).validateGetScheduleByTypeRequest(WORK_DAY);
        verify(scheduleServiceMock, times(1)).findByCityLineIdAndType(FIRST_CITY_LINE_ID, WORK_DAY);
        andExpectedScheduleIsReturned(mvcResult);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateScheduleShouldReturnBadRequestWhenScheduleIdIsNotNull() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        scheduleDto.setId(null);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(scheduleDtoValidatorMock).validateUpdateScheduleRequest(scheduleDto);

        String url = String.format("%s", RestApiEndpoints.SCHEDULE);
        mockMvc.perform(put(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andReturn();

        verify(scheduleDtoValidatorMock, times(1)).validateUpdateScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(0)).update(scheduleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateScheduleShouldReturnBadRequestWhenScheduleTypeIsNull() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        scheduleDto.setScheduleType(null);
        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.SCHEDULE_TYPE)))
                .when(scheduleDtoValidatorMock).validateUpdateScheduleRequest(scheduleDto);

        String url = String.format("%s", RestApiEndpoints.SCHEDULE);
        mockMvc.perform(put(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andReturn();

        verify(scheduleDtoValidatorMock, times(1)).validateUpdateScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(0)).update(scheduleDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void updateScheduleShouldReturnReturnNoContentAndUpdateSchedule() throws Exception {
        ScheduleDto scheduleDto = prepareTestData();
        doNothing().when(scheduleDtoValidatorMock).validateUpdateScheduleRequest(scheduleDto);

        String url = String.format("%s", RestApiEndpoints.SCHEDULE);
        mockMvc.perform(put(url)
                .content(objectMapper.writeValueAsString(scheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())
                .andReturn();

        verify(scheduleDtoValidatorMock, times(1)).validateUpdateScheduleRequest(scheduleDto);
        verify(scheduleServiceMock, times(1)).update(scheduleDto);
    }


    private void andExpectedScheduleIsReturned(MvcResult mvcResult) throws IOException {
        ScheduleDto scheduleDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), ScheduleDto.class);

        assertThat("City line id", scheduleDto.getCityLine().getId(), is(FIRST_CITY_LINE_ID));
        assertThat("Schedule id", scheduleDto.getId(), is(SCHEDULE_ID));
        assertThat("Schedule type", scheduleDto.getScheduleType(), is(SCHEDULE_TYPE));
    }

    private ScheduleDto prepareTestData() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(FIRST_CITY_LINE_ID, FIRST_CITY_LINE_NAME,
                CityLineType.valueOf(CITY_LINE_VALID_BUS_TYPE));
        return ScheduleDataBuilder.buildScheduleDto(SCHEDULE_ID, ScheduleType.WORK_DAY, cityLineDto);
    }
}
