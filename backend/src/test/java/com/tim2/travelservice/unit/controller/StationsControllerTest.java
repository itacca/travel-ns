package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.StationDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.service.StationService;
import com.tim2.travelservice.utils.CommonFunctions;
import com.tim2.travelservice.validator.dto.StationDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.LineStationDto;
import com.tim2.travelservice.web.dto.StationDto;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class StationsControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private StationService stationServiceMock;

    @MockBean
    private StationDtoValidator stationDtoValidatorMock;

    private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void createShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(stationDtoValidatorMock, times(0)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "VERIFIER")
    public void createShouldReturnForbiddenWhenUserIsVerifier() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(stationDtoValidatorMock, times(0)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void createShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(stationDtoValidatorMock, times(0)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenStationIdIsNotNull() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(1L, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID)))
                .when(stationDtoValidatorMock).validateCreateRequest(stationDto);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.ID))));

        verify(stationDtoValidatorMock, times(1)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenStationNameIsNull() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, null, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME)))
                .when(stationDtoValidatorMock).validateCreateRequest(stationDto);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME))));

        verify(stationDtoValidatorMock, times(1)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenLongitudeIsNull() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, null, ADD_STATION_LATITUDE, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LONGITUDE)))
                .when(stationDtoValidatorMock).validateCreateRequest(stationDto);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LONGITUDE))));

        verify(stationDtoValidatorMock, times(1)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenLatitudeIsNull() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, null, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LATITUDE)))
                .when(stationDtoValidatorMock).validateCreateRequest(stationDto);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LATITUDE))));

        verify(stationDtoValidatorMock, times(1)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnBadRequestWhenLineStationsAreNotNull() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, Collections.singletonList(new LineStationDto()));

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS)))
                .when(stationDtoValidatorMock).validateCreateRequest(stationDto);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS))));

        verify(stationDtoValidatorMock, times(1)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(0)).create(stationDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void createShouldReturnCreatedAndStationId() throws Exception {
        StationDto stationDto = StationDataBuilder.buildStationDto(null, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);
        DynamicResponse dynamicResponse = new DynamicResponse(RestApiConstants.ID, 9L);

        doNothing().when(stationDtoValidatorMock).validateCreateRequest(stationDto);
        doNothing().when(stationDtoValidatorMock).validateCreateRequest(stationDto);
        when(stationServiceMock.create(stationDto)).thenReturn(dynamicResponse);

        mockMvc.perform(post(RestApiEndpoints.STATIONS)
                .with(csrf())
                .content(objectMapper.writeValueAsString(stationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(9)));

        verify(stationDtoValidatorMock, times(1)).validateCreateRequest(stationDto);
        verify(stationServiceMock, times(1)).create(stationDto);
    }

    @Test
    public void findAllShouldReturnAllStations() throws Exception {
        when(stationServiceMock.findAll(PageRequest.of(0, 20))).thenReturn(initStationDtoPage());

        MvcResult mvcResult = mockMvc.perform(get(RestApiEndpoints.STATIONS)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        verify(stationServiceMock, times(1)).findAll(PageRequest.of(0, 20));
        andAllStationsAreReturned(mvcResult);
    }

    private void andAllStationsAreReturned(MvcResult mvcResult) throws IOException {
        List<StationDto> stationDtos = CommonFunctions.getContentListFromPageObject(mvcResult, new TypeReference<List<StationDto>>() {
        });

        assertThat("All vehicles are returned.", stationDtos.size(), is(3));
    }

    private Page<StationDto> initStationDtoPage() {
        StationDto stationDto1 = StationDataBuilder.buildStationDto(1L, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);
        StationDto stationDto2 = StationDataBuilder.buildStationDto(2L, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);
        StationDto stationDto3 = StationDataBuilder.buildStationDto(3L, ADD_STATION_NAME, ADD_STATION_LONGITUDE, ADD_STATION_LATITUDE, null);

        return new PageImpl<>(Arrays.asList(stationDto1, stationDto2, stationDto3));
    }
}
