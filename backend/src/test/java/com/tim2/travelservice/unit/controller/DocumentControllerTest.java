package com.tim2.travelservice.unit.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.DocumentDataBuilder;
import com.tim2.travelservice.exception.InvalidRequestDataException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.DocumentService;
import com.tim2.travelservice.validator.dto.DocumentDtoValidator;
import com.tim2.travelservice.web.dto.DocumentDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(value = "test")
public class DocumentControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private DocumentService documentServiceMock;

    @MockBean
    private DocumentDtoValidator documentDtoValidatorMock;

    private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void findImageBlobByDocumentIdShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        String url = String.format("%s/%d", RestApiEndpoints.DOCUMENT, 1L);
        mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(documentServiceMock, times(0)).findImageBlobByDocumentId(anyLong());
    }

    @Test
    @WithMockUser
    public void findImageBlobByDocumentIdShouldReturnNotFoundWhenDocumentDoesNotExist() throws Exception {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID)))
                .when(documentServiceMock).findImageBlobByDocumentId(999L);

        String url = String.format("%s/%d", RestApiEndpoints.DOCUMENT, 999L);
        mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID))));

        verify(documentServiceMock, times(1)).findImageBlobByDocumentId(999L);
    }

    @Test
    @WithMockUser
    public void findImageBlobByDocumentIdShouldReturnOkAndImageBlob() throws Exception {
        DynamicResponse response = new DynamicResponse(RestApiConstants.IMAGE_BLOB, "image-blob".getBytes());

        when(documentServiceMock.findImageBlobByDocumentId(1L)).thenReturn(response);

        String url = String.format("%s/%d", RestApiEndpoints.DOCUMENT, 1L);
        mockMvc.perform(get(url)
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.imageBlob", is("aW1hZ2UtYmxvYg==")));

        verify(documentServiceMock, times(1)).findImageBlobByDocumentId(1L);
    }

    @Test
    public void verifyShouldReturnLoginPageWhenUserIsGuest() throws Exception {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(documentDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());

        verify(documentDtoValidatorMock, times(0)).validateVerifyRequest(any(DocumentDto.class));
        verify(documentServiceMock, times(0)).verify(any(DocumentDto.class));
    }

    @Test
    @WithMockUser(roles = "PASSENGER")
    public void verifyShouldReturnForbiddenWhenUserIsPassenger() throws Exception {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(documentDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());

        verify(documentDtoValidatorMock, times(0)).validateVerifyRequest(any(DocumentDto.class));
        verify(documentServiceMock, times(0)).verify(any(DocumentDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void verifyShouldReturnBadRequestWhenIdIsNull() throws Exception {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(null, true);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID)))
                .when(documentDtoValidatorMock).validateVerifyRequest(documentDto);

        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(documentDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID))));

        verify(documentDtoValidatorMock, times(1)).validateVerifyRequest(documentDto);
        verify(documentServiceMock, times(0)).verify(any(DocumentDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void verifyShouldReturnBadRequestWhenVerifiedIsNull() throws Exception {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, null);

        doThrow(new InvalidRequestDataException(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VERIFIED)))
                .when(documentDtoValidatorMock).validateVerifyRequest(documentDto);

        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(documentDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VERIFIED))));

        verify(documentDtoValidatorMock, times(1)).validateVerifyRequest(documentDto);
        verify(documentServiceMock, times(0)).verify(any(DocumentDto.class));
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void verifyShouldReturnNotFoundWhenNonExistingDocument() throws Exception {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(999L, true);

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID)))
                .when(documentServiceMock).verify(documentDto);

        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(documentDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID))));

        verify(documentDtoValidatorMock, times(1)).validateVerifyRequest(documentDto);
        verify(documentServiceMock, times(1)).verify(documentDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void verifyShouldReturnBadRequestWhenVerifyingInactiveTicket() throws Exception {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(1L, true);

        doThrow(new InvalidRequestDataException(RestApiErrors.INACTIVE_DOCUMENT_VERIFICATION))
                .when(documentServiceMock).verify(documentDto);

        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(documentDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.data.explanation", is(RestApiErrors.INACTIVE_DOCUMENT_VERIFICATION)));

        verify(documentDtoValidatorMock, times(1)).validateVerifyRequest(documentDto);
        verify(documentServiceMock, times(1)).verify(documentDto);
    }

    @Test
    @WithMockUser(roles = "SYSTEM_ADMINISTRATOR")
    public void verifyShouldReturnNoContentAndVerifyDocument() throws Exception {
        DocumentDto documentDto = DocumentDataBuilder.buildDocumentDto(3L, true);

        doNothing().when(documentDtoValidatorMock).validateVerifyRequest(documentDto);

        String url = String.format("%s/verify", RestApiEndpoints.DOCUMENT);
        mockMvc.perform(put(url)
                .with(csrf())
                .content(objectMapper.writeValueAsString(documentDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

        verify(documentDtoValidatorMock, times(1)).validateVerifyRequest(documentDto);
        verify(documentServiceMock, times(1)).verify(documentDto);
    }
}
