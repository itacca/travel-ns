package com.tim2.travelservice.unit.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.data.builder.CityLineDataBuilder;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import com.tim2.travelservice.service.CityLineService;
import com.tim2.travelservice.store.CityLineStore;
import com.tim2.travelservice.validator.db.CityLineDbValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.tim2.travelservice.data.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
public class CityLineServiceTest {

    @Autowired
    private CityLineService cityLineService;

    @MockBean
    private CityLineStore cityLineStoreMock;

    @MockBean
    private ModelMapper modelMapperMock;

    @MockBean
    private CityLineDbValidator cityLineDbValidatorMock;

    @Test
    public void findByTypeShouldReturnListOfCityLinesOfTramType() {
        List<CityLine> cityLines = prepareCityLinesTramTypeTestData();

        when(cityLineStoreMock.findByType(PageRequest.of(0, 20), CityLineType.TRAM)).thenReturn(new PageImpl<>(cityLines));

        Page<CityLineDto> cityLineDtos = cityLineService.findByType(PageRequest.of(0, 20), CITY_LINE_VALID_TRAM_TYPE);

        verify(cityLineStoreMock, times(1)).findByType(PageRequest.of(0, 20), CityLineType.TRAM);
        cityLineDtos.forEach(cityLineDto -> assertThat("Returned city line has expected type",
                cityLineDto.getCityLineType().toString(), is(CITY_LINE_VALID_TRAM_TYPE)));
        cityLineDtos.forEach(cityLineDto -> assertThat("Returned city line has expected type.", cityLineDto.getCityLineType().toString(), is(CITY_LINE_VALID_TRAM_TYPE)));
    }

    @Test
    public void findByTypeShouldReturnListOfCityLinesOfBusType() {
        List<CityLine> cityLines = prepareCityLinesBusTypeTestData();

        when(cityLineStoreMock.findByType(PageRequest.of(0, 20), CityLineType.BUS)).thenReturn(new PageImpl<>(cityLines));

        Page<CityLineDto> cityLineDtos = cityLineService.findByType(PageRequest.of(0, 20), CITY_LINE_VALID_BUS_TYPE);

        verify(cityLineStoreMock, times(1)).findByType(PageRequest.of(0, 20), CityLineType.BUS);
        cityLineDtos.forEach(cityLineDto -> assertThat("Returned city line has expected type",
                cityLineDto.getCityLineType().toString(), is(CITY_LINE_VALID_BUS_TYPE)));
    }

    @Test
    public void shouldReturnListOfAllCityLines() {
        List<CityLine> cityLines = prepareCityLinesBusTypeTestData();

        when(cityLineStoreMock.findAll(PageRequest.of(0, 20))).thenReturn(new PageImpl<>(cityLines));

        Page<CityLineDto> cityLineDtos = cityLineService.findAll(PageRequest.of(0, 20));

        verify(cityLineStoreMock, times(1)).findAll(PageRequest.of(0, 20));
        assertThat("Number of returned city lines is valid.", (int) cityLineDtos.getTotalElements(), is(NUMBER_OF_CITY_LINES_OF_BUS_TYPE));
        cityLineDtos.forEach(cityLineDto -> assertThat("Returned city line has expected type.", cityLineDto.getCityLineType().toString(), is(CITY_LINE_VALID_BUS_TYPE)));
    }

    @Test
    public void createShouldReturnCityLineIdAfterSuccessfulInsert() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
        CityLine cityLine = CityLineDataBuilder.buildCityLine(null, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());
        CityLine cityLineWithId = CityLineDataBuilder.buildCityLine(1L, CITY_LINE_TRAM_NAME1, FIRST_CITY_LINE_TRAM_TYPE, Collections.emptyList(), Collections.emptyList(), Collections.emptyList());

        when(modelMapperMock.map(cityLineDto, CityLine.class)).thenReturn(cityLine);
        when(cityLineStoreMock.save(cityLine)).thenReturn(cityLineWithId);

        DynamicResponse dynamicResponse = cityLineService.create(cityLineDto);

        verify(modelMapperMock, times(1)).map(cityLineDto, CityLine.class);
        verify(cityLineStoreMock, times(1)).save(cityLine);
        assertThat("City line id is returned.", dynamicResponse.get(RestApiConstants.ID), is(1L));
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void findByIdShouldThrowNotFoundWithExplanationExceptionWhenGettingNonExistingCityLine() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(cityLineDbValidatorMock).validateFindByIdRequest(1L);

        cityLineService.findById(1L);

        verify(cityLineDbValidatorMock, times(1)).validateFindByIdRequest(1L);
    }

    @Test
    public void findByIdShouldReturnExistingCityLine() {
        CityLine CityLine = CityLineDataBuilder.buildCityLine(1L, CITY_LINE_BUS_NAME1, CityLineType.BUS);

        when(cityLineDbValidatorMock.validateFindByIdRequest(1L)).thenReturn(CityLine);

        CityLineDto CityLineDto = cityLineService.findById(1L);

        verify(cityLineDbValidatorMock, times(1)).validateFindByIdRequest(1L);
        andExpectedCityLineIsReturned(CityLineDto);
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void deleteByIdShouldThrowNotFoundWithExplanationExceptionWhenDeletingNonExistingCityLine() {
        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(cityLineDbValidatorMock).validateDeleteByIdRequest(1L);

        cityLineService.deleteById(1L);

        verify(cityLineDbValidatorMock, times(1)).validateDeleteByIdRequest(1L);
        verify(cityLineStoreMock, times(0)).delete(any(CityLine.class));
    }

    @Test
    public void deleteByIdShouldDeleteExistingCityLine() {
        CityLine existingCityLine = CityLineDataBuilder.buildCityLine(1L, CITY_LINE_BUS_NAME1, CityLineType.BUS);

        when(cityLineDbValidatorMock.validateDeleteByIdRequest(1L)).thenReturn(existingCityLine);

        cityLineService.deleteById(1L);

        verify(cityLineDbValidatorMock, times(1)).validateDeleteByIdRequest(1L);
        verify(cityLineStoreMock, times(1)).delete(existingCityLine);
    }

    @Test(expected = NotFoundWithExplanationException.class)
    public void updateShouldThrowNotFoundWithExplanationExceptionWhenUpdatingNonExistingCityLine() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        doThrow(new NotFoundWithExplanationException(RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID)))
                .when(cityLineDbValidatorMock).validateUpdateRequest(cityLineDto);

        cityLineService.update(cityLineDto);

        verify(cityLineDbValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(modelMapperMock, times(0)).map(any(CityLineDto.class), eq(CityLine.class));
        verify(cityLineStoreMock, times(0)).save(any(CityLine.class));
    }

    @Test
    public void updateShouldUpdateExistingCityLine() {
        CityLineDto cityLineDto = CityLineDataBuilder.buildCityLineDto(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);
        CityLine existingCityLine = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, FIRST_CITY_LINE_TYPE, null, null, null);
        CityLine updatedCityLine = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME_1_UPDATED, FIRST_CITY_LINE_TYPE_UPDATED, null, null, null);

        when(cityLineDbValidatorMock.validateUpdateRequest(cityLineDto)).thenReturn(existingCityLine);
        when(modelMapperMock.map(cityLineDto, CityLine.class)).thenReturn(updatedCityLine);

        cityLineService.update(cityLineDto);

        verify(cityLineDbValidatorMock, times(1)).validateUpdateRequest(cityLineDto);
        verify(modelMapperMock, times(1)).map(cityLineDto, CityLine.class);
        verify(cityLineStoreMock, times(1)).save(updatedCityLine);
    }

    private void andExpectedCityLineIsReturned(CityLineDto cityLineDto) {
        assertThat("Correct id is returned.", cityLineDto.getId(), is(1L));
        assertThat("Correct station name is returned.", cityLineDto.getName(), is(CITY_LINE_BUS_NAME1));
        assertThat("Correct station longitude is returned.", cityLineDto.getCityLineType(), is(CityLineType.BUS));
    }

    private List<CityLine> prepareCityLinesTramTypeTestData() {
        CityLine cityLineTram1 = CityLineDataBuilder.buildCityLine(CITY_LINE_TRAM_ID_1, CITY_LINE_TRAM_NAME1, CityLineType.valueOf(CITY_LINE_VALID_TRAM_TYPE));
        CityLine cityLineTram2 = CityLineDataBuilder.buildCityLine(CITY_LINE_TRAM_ID_2, CITY_LINE_TRAM_NAME2, CityLineType.valueOf(CITY_LINE_VALID_TRAM_TYPE));
        CityLine cityLineTram3 = CityLineDataBuilder.buildCityLine(CITY_LINE_TRAM_ID_3, CITY_LINE_TRAM_NAME3, CityLineType.valueOf(CITY_LINE_VALID_TRAM_TYPE));

        return Arrays.asList(cityLineTram1, cityLineTram2, cityLineTram3);
    }

    private List<CityLine> prepareCityLinesBusTypeTestData() {
        CityLine cityLineBus1 = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_1, CITY_LINE_BUS_NAME1, CityLineType.valueOf(CITY_LINE_VALID_BUS_TYPE));
        CityLine cityLineBus2 = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_2, CITY_LINE_BUS_NAME2, CityLineType.valueOf(CITY_LINE_VALID_BUS_TYPE));
        CityLine cityLineBus3 = CityLineDataBuilder.buildCityLine(CITY_LINE_BUS_ID_3, CITY_LINE_BUS_NAME3, CityLineType.valueOf(CITY_LINE_VALID_BUS_TYPE));

        return Arrays.asList(cityLineBus1, cityLineBus2, cityLineBus3);
    }
}
