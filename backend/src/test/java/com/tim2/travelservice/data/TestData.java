package com.tim2.travelservice.data;

import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.entity.ScheduleType;
import org.joda.time.DateTime;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.entity.TicketType;

public final class TestData {

    public static final Long SYSTEM_ADMINISTRATOR_ID = 1L;
    public static final String SYSTEM_ADMINISTRATOR_EMAIL = "sys@ns.com";
    public static final String SYSTEM_ADMINISTRATOR_FIRST_NAME = "System";
    public static final String SYSTEM_ADMINISTRATOR_LAST_NAME = "Admin";
    public static final String SYSTEM_ADMINISTRATOR_DATE_OF_BIRTH = "1996-01-30T00:10:02+0100";

    public static final Long TRAVEL_ADMINISTRATOR_ID = 2L;
    public static final String TRAVEL_ADMINISTRATOR_EMAIL = "adm@ns.com";
    public static final String TRAVEL_ADMINISTRATOR_FIRST_NAME = "Travel";
    public static final String TRAVEL_ADMINISTRATOR_LAST_NAME = "Admin";

    public static final Long PASSENGER_ID = 3L;
    public static final String PASSENGER_EMAIL = "pass@ns.com";
    public static final String PASSENGER_FIRST_NAME = "Just";
    public static final String PASSENGER_LAST_NAME = "Passenger";

    public static final Long VERIFIER_ID = 4L;
    public static final String VERIFIER_EMAIL = "ver@ns.com";
    public static final String VERIFIER_FIRST_NAME = "Just";
    public static final String VERIFIER_LAST_NAME = "Verifier";

    public static final Long ACCOUNT_REGISTRATION_RANDOM_ID = 1996L;
    public static final String ACCOUNT_REGISTRATION_EMAIL = "nikzeljkovic@gmail.com";
    public static final String ACCOUNT_REGISTRATION_INVALID_EMAIL = "not_email.form@t";
    public static final String ACCOUNT_REGISTRATION_PASSWORD = "password";
    public static final String ACCOUNT_REGISTRATION_SHORT_PASSWORD = "short";
    public static final String ACCOUNT_REGISTRATION_FIRST_NAME = "Nikola";
    public static final String ACCOUNT_REGISTRATION_LAST_NAME = "Zeljkovic";
    public static final String ACCOUNT_REGISTRATION_DATE_OF_BIRTH = "1996-01-30T00:10:02.000Z";
    public static final String ACCOUNT_REGISTRATION_FUTURE_DATE_OF_BIRTH = "2099-01-30T00:10:02.000Z";
    public static final String ACCOUNT_REGISTRATION_INVALID_DATE_OF_BIRTH = "invalid_time_format";
    public static final String DEFAULT_ACCOUNT_ROLE_LABEL = "ROLE_PASSENGER";
    public static final String ADMIN_ROLE_LABEL = "ROLE_ADMINISTRATOR";
    public static final Long TRAVEL_ADMIN_ID = 2L;
    public static final Long TRAVEL_ADMIN_ID_DELETE = 6L;
    public static final Long REFERENCED_TRAVEL_ADMIN_ID = 2L;
    public static final Long NON_REFERENCED_TRAVEL_ADMIN_ID = 7L;

    public static final String ACCOUNT_FIRST_NAME_UPDATED = "FirstNameUpdated";
    public static final String ACCOUNT_LAST_NAME_UPDATED = "LastNameUpdated";
    public static final String ACCOUNT_PASSWORD_UPDATED = "NewPassword";
    public static final String ACCOUNT_SHORT_PASSWORD_UPDATE = "short";
    public static final String ACCOUNT_DATE_OF_BIRTH_UPDATED = "1998-01-30T00:10:02.000Z";
    public static final String ACCOUNT_INVALID_DATE_OF_BIRTH_UPDATED = "invalid_time_format";

    public static final String DEFAULT_ENCODED_PASSWORD = "$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.";
    public static final String UPDATED_ENCODED_PASSWORD = "$2a$10$ZEUZK9c7HZ5R62/yFJ5RyurA/hwy1/.Tf6tX2dRWrz7TFwV3TwHDC";

    public static final Long PRICE_LIST_ID = 4L;
    public static final String PRICE_LIST_START_DATE = "2019-02-12T00:10:02.000Z";
    public static final String PRICE_LIST_END_DATE = "2022-01-30T00:10:02.000Z";

    public static final Long PRICE_LIST_ID_NEW = 7L;
    public static final String PRICE_LIST_START_DATE_NEW = "2025-02-12T00:10:02.000Z";
    public static final String PRICE_LIST_END_DATE_NEW = "2026-01-30T00:10:02.000Z";
    public static final String PRICE_LIST_OLD_START_DATE = "2022-05-23T00:00:00.000Z";
    public static final String PRICE_LIST_OLD_END_DATE = "2023-05-15T00:00:00.000Z";

    public static final String PRICE_LIST_PAST_DATE = "2017-02-12T00:10:02.000Z";

    public static final String PRICE_LIST_START_DATE_UPDATED = "2018-01-30T00:10:02.000Z";
    public static final String PRICE_LIST_END_DATE_UPDATED = "2020-01-30T00:10:02.000Z";

    public static final Long PRICE_LIST_ITEM_ID = 6L;
    public static final Long PRICE_LIST_ITEM_ID_NEW = 8L;
    public static final Double PRICE_LIST_ITEM_PRICE = 55.5;
    public static final Double PRICE_LIST_ITEM_PRICE_NEW = 66.5;
    public static final Double INVALID_PRICE_LIST_ITEM_PRICE = -55.5;
    public static final Long TICKET_ID = 5L;

    public static final String CITY_LINE_VALID_BUS_TYPE = "BUS";
    public static final String CITY_LINE_INVALID_TYPE = "INVALID_TYPE";
    public static final int NUMBER_OF_CITY_LINES = 6;
    public static final int NUMBER_OF_CITY_LINES_OF_BUS_TYPE = 3;
    public static final Long FIRST_CITY_LINE_ID = 1L;
    public static final CityLineType FIRST_CITY_LINE_TYPE = CityLineType.BUS;
    public static final CityLineType FIRST_CITY_LINE_TYPE_UPDATED = CityLineType.TRAM;
    public static final String FIRST_CITY_LINE_NAME = "Linija 1";

    public static final String CITY_LINE_VALID_TRAM_TYPE = "TRAM";
    public static final Integer NUMBER_OF_CITY_LINES_OF_TRAM_TYPE = 3;
    public static final Long FIRST_CITY_LINE_TRAM_TYPE_ID = 2L;
    public static final CityLineType FIRST_CITY_LINE_TRAM_TYPE = CityLineType.TRAM;
    public static final String FIRST_CITY_LINE_TRAM_TYPE_NAME = "Linija 101";

    public static final Long CITY_LINE_BUS_ID_1 = 1L;
    public static final Long CITY_LINE_BUS_ID_2 = 2L;
    public static final Long CITY_LINE_BUS_ID_3 = 3L;

    public static final Long CITY_LINE_TRAM_ID_1 = 4L;
    public static final Long CITY_LINE_TRAM_ID_2 = 5L;
    public static final Long CITY_LINE_TRAM_ID_3 = 6L;

    public static final Long NON_REFERENCED_CITY_LINE_ID = 996L;

    public static final String CITY_LINE_BUS_NAME1 = "Linija 1";
    public static final String CITY_LINE_BUS_NAME_1_UPDATED = "Novo ime linije 1";
    public static final String CITY_LINE_BUS_NAME2 = "Linija 2";
    public static final String CITY_LINE_BUS_NAME3 = "Linija 3";

    public static final String CITY_LINE_TRAM_NAME1 = "Linija 4";
    public static final String CITY_LINE_TRAM_NAME2 = "Linija 5";
    public static final String CITY_LINE_TRAM_NAME3 = "Linija 6";

    public static final String CITY_LINE_TRAM_NAME_ADD = "Linija 102";

    public static final String RANDOM_TYPE = "RANDOM_TYPE";
    public static final String TRAM = "TRAM";

    public static final String ADD_STATION_NAME = "USPENSKA - POZORIŠNI TRG";
    public static final Double ADD_STATION_LONGITUDE = 19.8422463011;
    public static final Double ADD_STATION_LATITUDE = 45.2548499029;

    public static final Long STATION_ID = 1L;
    public static final Long STATION_ID_2 = 2L;
    public static final Long NON_REFERENCED_STATION_ID = 996L;
    public static final Double STATION_LATITUDE = 45.2489224745;
    public static final Double STATION_LONGITUDE = 19.791754249;
    public static final String STATION_NAME = "NOVO NASELJE - BISTRICA - OKRETNICA";

    public static final Double STATION_LATITUDE_UPDATED = 45.4545454545;
    public static final Double STATION_LONGITUDE_UPDATED = 19.19191919;
    public static final String STATION_NAME_UPDATED = "KUL IME STANICE";

    public static final Long SCHEDULE_ID = 1L;
    public static final Long NOT_EXISTING_SCHEDULE_ID = 999L;
    public static final Boolean SCHEDULE_ACTIVE = true;
    public static final ScheduleType SCHEDULE_TYPE = ScheduleType.WORK_DAY;
    public static final ScheduleType SCHEDULE_TYPE_UPDATED = ScheduleType.WEEKEND;
    public static final Integer NUMBER_OF_SCHEDULE_ITEMS_PART_UPDATED = 3;
    public static final Integer NUMBER_OF_SCHEDULE_ITEMS_PART_UPDATED_TOTALLY = 1;
    public static final Long NEW_SCHEDULE_ID = 5L;
    public static final Boolean NEW_SCHEDULE_ACTIVE = true;
    public static final Long SCHEDULE_ITEM_ID = 1L;
    public static final DateTime SCHEDULE_ITEM_DEPARTURE_TIME = DateTime.parse("2000-01-01T14:10:00.000Z");
    public static final Long NON_EXISTING_CITY_LINE_ID = 999L;
    public static final Integer NUMBER_OF_SCHEDULES = 1;
    public static final DateTime SCHEDULE_VALID_FROM = DateTime.parse("2018-12-12T00:00:00.000Z");
    public static final Integer NUMBER_OF_SCHEDULE_ITEMS = 4;
    public static final Integer NUMBER_OF_SCHEDULE_ITEMS_IS_ONE = 1;
    public static final String WORK_DAY = "WORK_DAY";

    public static final Long VEHICLE_ID = 1L;
    public static final String VEHICLE_PLATE_NUMBER = "plate number";
    public static final String VEHICLE_PLATE_NUMBER_EXISTING = "NS-003-TR";
    public static final String VEHICLE_PLATE_NUMBER_UPDATED = "new plate number";

    public static final TicketDuration FIRST_TICKET_DURATION = TicketDuration.MONTHLY;
    public static final TicketDuration FIRST_TICKET_UPDATED_DURATION = TicketDuration.ANNUAL;
    public static final TicketType FIRST_TICKET_TYPE = TicketType.REGULAR;
    public static final Long FIRST_TICKET_ID = 1l;
    public static final Long NON_REFERENCED_TICKET_ID = 100L;
    public static final Long ADMINISTRATOR_ROLE_ID = 2L;

    public static final String INVALID_DOCUMENT_TYPE = "random_invalid_type";
    public static final String VALID_DOCUMENT_TYPE = "SENIOR";
    public static final String IMAGE_TYPE = "jpg";
    public static final String ID_PICTURE_FILE_NAME = "id_picture_test.jpg";
}


