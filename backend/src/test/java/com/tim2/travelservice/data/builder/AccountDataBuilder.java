package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Role;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.joda.time.DateTime;

import java.util.List;

public class AccountDataBuilder {

    public static AccountDto buildAccountDto(Long id) {
        return AccountDto
                .builder()
                .id(id)
                .build();
    }

    public static AccountDto buildAccountDto(String email) {
        return AccountDto
                .builder()
                .email(email)
                .build();
    }

    public static Account buildAccount(String email) {
        return Account
                .builder()
                .email(email)
                .build();
    }

    public static AccountDto buildAccountDto(String email, String password, String firstName, String lastName, String dateOfBirth) {
        return AccountDto
                .builder()
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .build();
    }

    public static AccountDto buildAccountDto(String email, String firstName, String lastName, String dateOfBirth) {
        return AccountDto
                .builder()
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .build();
    }

    public static AccountDto buildAccountDto(Long id, String email, String firstName, String lastName) {
        return AccountDto
                .builder()
                .id(id)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }

    public static AccountDto buildAccountDto(String password, String firstName, String lastName) {
        return AccountDto
                .builder()
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }

    public static Account buildAccount(String email, String password, String firstName, String lastName, String dateOfBirth) {
        return Account
                .builder()
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .build();
    }

    public static AccountDto buildAccountDto(String email, boolean idValidated, boolean validated) {
        return AccountDto
                .builder()
                .email(email)
                .idValidated(idValidated)
                .validated(validated)
                .build();
    }

    public static Account buildAccount(String email, Boolean idValidated, Boolean validated) {
        return Account
                .builder()
                .email(email)
                .idValidated(idValidated)
                .validated(validated)
                .build();
    }

    public static Account buildAccount(String email, String password, String firstName, String lastName, String dateOfBirth, boolean idValidate, boolean validated) {
        return Account
                .builder()
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .idValidated(idValidate)
                .validated(validated)
                .validated(validated)
                .build();
    }

    public static AccountDto buildAccountDto(Long id, String email, String password, String firstName, String lastName, String dateOfBirth, Boolean validated, Boolean idValidated, List<Role> roles) {
        return AccountDto
                .builder()
                .id(id)
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .idValidated(idValidated)
                .validated(validated)
                .validated(validated)
                .idValidated(idValidated)
                .roles(roles)
                .build();
    }

    public static AccountDto buildAccountDto(String password, String firstName, String lastName, List<Role> roles) {
        return AccountDto
                .builder()
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .roles(roles)
                .build();
    }

    public static Account buildAccount(String password, String firstName, String lastName, List<Role> roles) {
        return Account
                .builder()
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .roles(roles)
                .build();
    }

    public static AccountDto buildAccountDto(Long id, String email, String firstName, String lastName, String dateOfBirth, Boolean validated, Boolean idValidated, List<Role> roles) {
        return AccountDto
                .builder()
                .id(id)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .idValidated(idValidated)
                .validated(validated)
                .validated(validated)
                .idValidated(idValidated)
                .roles(roles)
                .build();
    }


    public static DynamicResponse buildAccountDtoJson(Long id, String email, String password, String firstName, String lastName, String dateOfBirth, Boolean validated, Boolean idValidated, List<Role> roles) {
        DynamicResponse dynamicResponse = new DynamicResponse();
        dynamicResponse.put(RestApiConstants.ID, id);
        dynamicResponse.put(RestApiConstants.EMAIL, email);
        dynamicResponse.put(RestApiConstants.PASSWORD, password);
        dynamicResponse.put(RestApiConstants.FIRST_NAME, firstName);
        dynamicResponse.put(RestApiConstants.LAST_NAME, lastName);
        dynamicResponse.put(RestApiConstants.DATE_OF_BIRTH, dateOfBirth);
        dynamicResponse.put(RestApiConstants.VALIDATED, validated);
        dynamicResponse.put(RestApiConstants.ID_VALIDATED, idValidated);
        dynamicResponse.put(RestApiConstants.ROLES, roles);
        return dynamicResponse;
    }

    public static DynamicResponse buildAccountDtoJson(Long id, String email, String firstName, String lastName, String dateOfBirth, Boolean validated, Boolean idValidated, List<Role> roles) {
        DynamicResponse dynamicResponse = new DynamicResponse();
        dynamicResponse.put(RestApiConstants.ID, id);
        dynamicResponse.put(RestApiConstants.EMAIL, email);
        dynamicResponse.put(RestApiConstants.FIRST_NAME, firstName);
        dynamicResponse.put(RestApiConstants.LAST_NAME, lastName);
        dynamicResponse.put(RestApiConstants.DATE_OF_BIRTH, dateOfBirth);
        dynamicResponse.put(RestApiConstants.VALIDATED, validated);
        dynamicResponse.put(RestApiConstants.ID_VALIDATED, idValidated);
        dynamicResponse.put(RestApiConstants.ROLES, roles);
        return dynamicResponse;
    }

    public static Account buildAccount(Long id, String email, String password, String firstName, String lastName, String dateOfBirth, Boolean validated, Boolean idValidated, List<Role> roles) {
        return Account
                .builder()
                .id(id)
                .email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .idValidated(idValidated)
                .validated(validated)
                .validated(validated)
                .idValidated(idValidated)
                .roles(roles)
                .build();
    }

    public static Account buildAccount(Long id, String email, String firstName, String lastName, String dateOfBirth, Boolean validated, Boolean idValidated, List<Role> roles) {
        return Account
                .builder()
                .id(id)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .idValidated(idValidated)
                .validated(validated)
                .validated(validated)
                .idValidated(idValidated)
                .roles(roles)
                .build();
    }

    public static DynamicResponse buildAccountDtoJson(String password, String firstName, String lastName, String dateOfBirth) {
        DynamicResponse dynamicResponse = new DynamicResponse();
        dynamicResponse.put(RestApiConstants.PASSWORD, password);
        dynamicResponse.put(RestApiConstants.FIRST_NAME, firstName);
        dynamicResponse.put(RestApiConstants.LAST_NAME, lastName);
        dynamicResponse.put(RestApiConstants.DATE_OF_BIRTH, dateOfBirth);
        return dynamicResponse;
    }

    public static Account buildAccount(Long id) {
        return Account
                .builder()
                .id(id)
                .build();
    }

    public static AccountDto buildAccountDto(String firstName, String dateOfBirth) {
        return AccountDto
                .builder()
                .firstName(firstName)
                .dateOfBirth(DateTime.parse(dateOfBirth))
                .build();
    }
}
