package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.entity.TicketType;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.TicketDto;

public class TicketDataBuilder {

    public static Ticket buildTicket(TicketDuration ticketDuration) {
        return Ticket
                .builder()
                .ticketDuration(ticketDuration)
                .build();
    }

    public static TicketDto buildTicketDto(Long id, TicketDuration ticketDuration, TicketType ticketType, CityLineDto cityLineDto) {
        return TicketDto
                .builder()
                .id(id)
                .ticketDuration(ticketDuration)
                .ticketType(ticketType)
                .cityLine(cityLineDto)
                .build();
    }

    public static Ticket buildTicket(Long id, TicketDuration ticketDuration, TicketType ticketType, CityLine cityLine) {
        return Ticket
                .builder()
                .id(id)
                .ticketDuration(ticketDuration)
                .ticketType(ticketType)
                .cityLine(cityLine)
                .build();
    }

    public static TicketDto buildTicketDto(Long id, TicketType ticketType, TicketDuration ticketDuration, CityLineDto cityLineDto) {
        return TicketDto
                .builder()
                .id(id)
                .cityLine(cityLineDto)
                .ticketDuration(ticketDuration)
                .ticketType(ticketType)
                .build();
    }

    public static Ticket buildTicket(Long id, TicketType ticketType, TicketDuration ticketDuration, CityLine cityLine) {
        return Ticket
                .builder()
                .id(id)
                .cityLine(cityLine)
                .ticketDuration(ticketDuration)
                .ticketType(ticketType)
                .build();
    }

    public static TicketDto buildTicketDto(TicketType ticketType, TicketDuration ticketDuration, CityLineDto cityLineDto) {
        return TicketDto
                .builder()
                .cityLine(cityLineDto)
                .ticketDuration(ticketDuration)
                .ticketType(ticketType)
                .build();
    }

    public static TicketDto buildTicketDto(Long id, TicketDuration ticketDuration, CityLineDto cityLineDto) {
        return TicketDto
                .builder()
                .id(id)
                .cityLine(cityLineDto)
                .ticketDuration(ticketDuration)
                .build();
    }

    public static TicketDto buildTicketDto(Long id, TicketType ticketType, CityLineDto cityLineDto) {
        return TicketDto
                .builder()
                .id(id)
                .cityLine(cityLineDto)
                .ticketType(ticketType)
                .build();
    }

    public static TicketDto buildTicketDto(Long id) {
        return TicketDto
                .builder()
                .id(id)
                .build();
    }
}
