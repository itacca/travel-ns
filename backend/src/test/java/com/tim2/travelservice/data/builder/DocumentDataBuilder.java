package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.entity.DocumentType;
import com.tim2.travelservice.web.dto.DocumentDto;

public class DocumentDataBuilder {

    public static Document buildDocument(Long id, String imageType, DocumentType documentType) {
        return Document
                .builder()
                .id(id)
                .imageType(imageType)
                .documentType(documentType)
                .build();
    }

    public static Document buildDocument(byte[] imageBlob) {
        return Document
                .builder()
                .imageBlob(imageBlob)
                .build();
    }

    public static DocumentDto buildDocumentDto(Long id, Boolean verified) {
        return DocumentDto
                .builder()
                .id(id)
                .verified(verified)
                .build();
    }

    public static DocumentDto buildDocumentDto(Long id, Boolean active, Boolean verified) {
        return DocumentDto
                .builder()
                .id(id)
                .active(active)
                .verified(verified)
                .build();
    }

    public static Document buildDocument(Long id, Boolean active, Boolean verified) {
        return Document
                .builder()
                .id(id)
                .active(active)
                .verified(verified)
                .build();
    }
}
