package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.web.dto.PriceListDto;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import com.tim2.travelservice.web.dto.TicketDto;

public class PriceListItemDataBuilder {

    public static PriceListItemDto buildPriceListItemDto(Long id, Double price, TicketDto ticketDto) {
        return PriceListItemDto
                .builder()
                .id(id)
                .price(price)
                .ticket(ticketDto)
                .build();
    }

    public static PriceListItemDto buildPriceListItemDto(Long id, Double price, TicketDto ticketDto, PriceListDto priceListDto) {
        return PriceListItemDto
                .builder()
                .id(id)
                .price(price)
                .ticket(ticketDto)
                .priceList(priceListDto)
                .build();
    }

    public static PriceListItemDto buildPriceListItemDto(Long id, TicketDto ticketDto, PriceListDto priceListDto) {
        return PriceListItemDto
                .builder()
                .id(id)
                .ticket(ticketDto)
                .priceList(priceListDto)
                .build();
    }

    public static PriceListItemDto buildPriceListItemDto(Double price, TicketDto ticketDto, PriceListDto priceListDto) {
        return PriceListItemDto
                .builder()
                .price(price)
                .ticket(ticketDto)
                .priceList(priceListDto)
                .build();
    }

    public static PriceListItem buildPriceListItem(Long id, Double price, Ticket ticket, Boolean active, PriceList priceList) {
        return PriceListItem
                .builder()
                .id(id)
                .price(price)
                .ticket(ticket)
                .active(active)
                .priceList(priceList)
                .build();
    }

    public static PriceListItemDto buildPriceListItemDto(Double price, TicketDto ticketDto, Boolean active, PriceListDto priceListDto) {
        return PriceListItemDto
                .builder()
                .price(price)
                .ticket(ticketDto)
                .active(active)
                .priceList(priceListDto)
                .build();
    }

    public static PriceListItem buildPriceListItem(Long id, Double price, Ticket ticket, PriceList priceList) {
        return PriceListItem
                .builder()
                .id(id)
                .price(price)
                .ticket(ticket)
                .priceList(priceList)
                .build();
    }

    public static PriceListItem buildPriceListItem(Double price, Ticket ticket, Boolean active, PriceList priceList) {
        return PriceListItem
                .builder()
                .price(price)
                .ticket(ticket)
                .active(active)
                .priceList(priceList)
                .build();
    }

    public static PriceListItemDto buildPriceListItemDto(TicketDto ticketDto, PriceListDto priceListDto) {
        return PriceListItemDto
                .builder()
                .ticket(ticketDto)
                .priceList(priceListDto)
                .build();
    }

    public static PriceListItemDto buildPriceListItemDto(Long id, Double price) {
        return PriceListItemDto
                .builder()
                .id(id)
                .price(price)
                .build();

    }

    public static PriceListItemDto buildPriceListItemDto(Double price) {
        return PriceListItemDto
                .builder()
                .price(price)
                .build();
    }
}
