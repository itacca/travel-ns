package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.entity.VehicleType;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.StationDto;
import com.tim2.travelservice.web.dto.VehicleDto;

public class VehicleDataBuilder {

    public static VehicleDto buildVehicleDto(Long id, String plateNumber, VehicleType vehicleType, CityLineDto cityLineDto) {
        return VehicleDto
                .builder()
                .id(id)
                .plateNumber(plateNumber)
                .vehicleType(vehicleType)
                .cityLine(cityLineDto)
                .build();
    }

    public static VehicleDto buildVehicleDto(Long id, String plateNumber, VehicleType vehicleType, CityLineDto cityLineDto, StationDto stationDto) {
        return VehicleDto
                .builder()
                .id(id)
                .plateNumber(plateNumber)
                .vehicleType(vehicleType)
                .cityLine(cityLineDto)
                .station(stationDto)
                .build();
    }

    public static VehicleDto buildVehicleDto(Long id, String plateNumber, VehicleType vehicleType) {
        return VehicleDto
                .builder()
                .id(id)
                .plateNumber(plateNumber)
                .vehicleType(vehicleType)
                .build();
    }

    public static Vehicle buildVehicle(Long id, String plateNumber, VehicleType vehicleType) {
        return Vehicle
                .builder()
                .id(id)
                .plateNumber(plateNumber)
                .vehicleType(vehicleType)
                .build();
    }

    public static Vehicle buildVehicle(Long id, String plateNumber, VehicleType vehicleType, CityLine cityLine, Station station) {
        return Vehicle
                .builder()
                .id(id)
                .plateNumber(plateNumber)
                .vehicleType(vehicleType)
                .cityLine(cityLine)
                .station(station)
                .build();
    }
}
