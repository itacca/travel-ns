package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.BoughtTicket;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import org.joda.time.DateTime;

public class BoughtTicketBuilder {

    public static BoughtTicketDto buildBoughtTicketDto(Long id, AccountDto accountDto) {
        return BoughtTicketDto
                .builder()
                .id(id)
                .account(accountDto)
                .build();
    }

    public static BoughtTicketDto buildBoughtTicketDto(Long id) {
        return BoughtTicketDto
                .builder()
                .id(id)
                .build();
    }

    public static BoughtTicket buildBoughtTicket(Long id, Account account) {
        return BoughtTicket
                .builder()
                .id(id)
                .account(account)
                .build();
    }

    public static BoughtTicket buildBoughtTicket(Long id) {
        return BoughtTicket
                .builder()
                .id(id)
                .build();
    }

    public static BoughtTicket buildBoughtTicket(Long id, Boolean active, Ticket ticket) {
        return BoughtTicket
                .builder()
                .id(id)
                .active(active)
                .ticket(ticket)
                .build();
    }

    public static BoughtTicket buildBoughtTicket(Long id, Boolean active, DateTime boughtDate, Ticket ticket) {
        return BoughtTicket
                .builder()
                .id(id)
                .active(active)
                .ticket(ticket)
                .boughtDate(boughtDate)
                .build();
    }
}
