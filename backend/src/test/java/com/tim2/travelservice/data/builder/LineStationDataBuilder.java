package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.LineStationDto;
import com.tim2.travelservice.web.dto.StationDto;

public class LineStationDataBuilder {

    public static LineStationDto buildLineStationDto(Long id, Integer relativePosition, CityLineDto cityLineDto, StationDto stationDto) {
        return LineStationDto
                .builder()
                .id(id)
                .relativePosition(relativePosition)
                .cityLine(cityLineDto)
                .station(stationDto)
                .build();
    }

    public static LineStation buildLineStation(Long id, Integer relativePosition, CityLine cityLine, Station station) {
        return LineStation
                .builder()
                .id(id)
                .relativePosition(relativePosition)
                .cityLine(cityLine)
                .station(station)
                .build();
    }
}
