package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.web.dto.ScheduleDto;
import com.tim2.travelservice.web.dto.ScheduleItemDto;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.joda.time.DateTime;

public class ScheduleItemDataBuilder {

    public static ScheduleItemDto buildScheduleItemDto(Long id, DateTime departureTime) {
        return ScheduleItemDto
                .builder()
                .id(id)
                .departureTime(departureTime)
                .build();
    }

    public static ScheduleItemDto buildScheduleItemDto(Long id, DateTime departureTime, ScheduleDto scheduleDto) {
        return ScheduleItemDto
                .builder()
                .id(id)
                .departureTime(departureTime)
                .schedule(scheduleDto)
                .build();
    }

    public static ScheduleItemDto buildScheduleItemDto(Long scheduleItemId, DateTime departureTime, ScheduleDto scheduleDto, VehicleDto vehicleDto) {
        return ScheduleItemDto
                .builder()
                .id(scheduleItemId)
                .departureTime(departureTime)
                .schedule(scheduleDto)
                .vehicle(vehicleDto)
                .build();
    }
}
