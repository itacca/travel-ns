package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.web.dto.LineStationDto;
import com.tim2.travelservice.web.dto.StationDto;

import java.util.List;

public class StationDataBuilder {

    public static StationDto buildStationDto(Long id, String name, Double longitude, Double latitude, List<LineStationDto> lineStations) {
        return StationDto
                .builder()
                .id(id)
                .name(name)
                .longitude(longitude)
                .latitude(latitude)
                .lineStations(lineStations)
                .build();
    }

    public static Station buildStation(Long id, String name, Double longitude, Double latitude, List<LineStation> lineStations) {
        return Station
                .builder()
                .id(id)
                .name(name)
                .longitude(longitude)
                .latitude(latitude)
                .lineStations(lineStations)
                .build();
    }

    public static StationDto buildStationDto(Long id) {
        return StationDto
                .builder()
                .id(id)
                .build();
    }

    public static Station buildStation(Long id) {
        return Station
                .builder()
                .id(id)
                .build();
    }
}
