package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.ScheduleDto;

public class ScheduleDataBuilder {

    public static ScheduleDto buildScheduleDto(Long id, ScheduleType scheduleType) {
        return ScheduleDto
                .builder()
                .id(id)
                .scheduleType(scheduleType)
                .build();
    }

    public static ScheduleDto buildScheduleDto(Long id, ScheduleType scheduleType, CityLineDto cityLineDto) {
        return ScheduleDto
                .builder()
                .id(id)
                .scheduleType(scheduleType)
                .cityLine(cityLineDto)
                .build();
    }

    public static Schedule buildSchedule(Long id, ScheduleType scheduleType, CityLine cityLine) {
        return Schedule
                .builder()
                .id(id)
                .scheduleType(scheduleType)
                .cityLine(cityLine)
                .build();

    }
}
