package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.web.dto.PriceListDto;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.joda.time.DateTime;

import java.util.List;

public class PriceListDataBuilder {

    public static PriceListDto buildPriceListDto(Long id, String startDate, String endDate, List<PriceListItemDto> priceListItems) {
        return PriceListDto
                .builder()
                .id(id)
                .startDate(DateTime.parse(startDate))
                .endDate(DateTime.parse(endDate))
                .priceListItems(priceListItems)
                .build();
    }

    public static PriceListDto buildPriceListDto(Long id, String startDate) {
        return PriceListDto
                .builder()
                .id(id)
                .startDate(DateTime.parse(startDate))
                .build();
    }

    public static PriceListDto buildPriceListDto(Long id, String startDate, String endDate) {
        return PriceListDto
                .builder()
                .id(id)
                .startDate(DateTime.parse(startDate))
                .endDate(DateTime.parse(endDate))
                .build();
    }

    public static PriceListDto buildPriceListDto(Long id) {
        return PriceListDto
                .builder()
                .id(id)
                .build();
    }

    public static PriceListDto buildPriceListDto(String startDate, String endDate, List<PriceListItemDto> priceListItems) {
        return PriceListDto
                .builder()
                .startDate(DateTime.parse(startDate))
                .endDate(DateTime.parse(endDate))
                .priceListItems(priceListItems)
                .build();
    }

    public static PriceListDto buildPriceListDto(String endDate, List<PriceListItemDto> priceListItems) {
        return PriceListDto
                .builder()
                .endDate(DateTime.parse(endDate))
                .priceListItems(priceListItems)
                .build();
    }

    public static PriceListDto buildPriceListDtoWithStartDate(String startDate, List<PriceListItemDto> priceListItems) {
        return PriceListDto
                .builder()
                .startDate(DateTime.parse(startDate))
                .priceListItems(priceListItems)
                .build();
    }

    public static PriceListDto buildPriceListDtoWithEndDate(String endDate, List<PriceListItemDto> priceListItems) {
        return PriceListDto
                .builder()
                .endDate(DateTime.parse(endDate))
                .priceListItems(priceListItems)
                .build();
    }

    public static PriceListDto buildPriceListDto(String startDate, String endDate) {
        return PriceListDto
                .builder()
                .startDate(DateTime.parse(startDate))
                .endDate(DateTime.parse(endDate))
                .build();
    }

    public static PriceList buildPriceList(Long id, String startDate, String endDate, List<PriceListItem> priceListItems) {
        return PriceList
                .builder()
                .id(id)
                .startDate(DateTime.parse(startDate))
                .endDate(DateTime.parse(endDate))
                .priceListItems(priceListItems)
                .build();
    }

    public static PriceList buildPriceList(Long id, String startDate, String endDate) {
        return PriceList
                .builder()
                .id(id)
                .startDate(DateTime.parse(startDate))
                .endDate(DateTime.parse(endDate))
                .build();
    }

    public static PriceList buildPriceList(String startDate, String endDate) {
        return PriceList
                .builder()
                .startDate(DateTime.parse(startDate))
                .endDate(DateTime.parse(endDate))
                .build();
    }

    public static PriceList buildPriceList(List<PriceListItem> priceListItems) {
        return PriceList
                .builder()
                .priceListItems(priceListItems)
                .build();
    }
}
