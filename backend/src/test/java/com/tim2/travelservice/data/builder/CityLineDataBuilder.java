package com.tim2.travelservice.data.builder;

import com.tim2.travelservice.entity.*;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.LineStationDto;
import com.tim2.travelservice.web.dto.ScheduleDto;
import com.tim2.travelservice.web.dto.VehicleDto;

import java.util.List;

public class CityLineDataBuilder {

    public static CityLine buildCityLine(Long id, String name, CityLineType cityLineType) {
        return CityLine
                .builder()
                .id(id)
                .name(name)
                .cityLineType(cityLineType)
                .build();
    }

    public static CityLineDto buildCityLineDto(Long id, String name, CityLineType cityLineType) {
        return CityLineDto
                .builder()
                .id(id)
                .name(name)
                .cityLineType(cityLineType)
                .build();
    }

    public static CityLineDto buildCityLineDto(Long id, CityLineType cityLineType) {
        return CityLineDto
                .builder()
                .id(id)
                .cityLineType(cityLineType)
                .build();
    }

    public static CityLineDto buildCityLineDto(Long id, String name) {
        return CityLineDto
                .builder()
                .id(id)
                .name(name)
                .build();
    }

    public static CityLineDto buildCityLineDto(Long id, String name, CityLineType cityLineType, List<VehicleDto> vehicleDtos, List<ScheduleDto> scheduleDtos, List<LineStationDto> lineStationDtos) {
        return CityLineDto
                .builder()
                .id(id)
                .name(name)
                .cityLineType(cityLineType)
                .vehicles(vehicleDtos)
                .schedules(scheduleDtos)
                .lineStations(lineStationDtos)
                .build();
    }

    public static CityLineDto buildCityLineDto(Long id) {
        return CityLineDto
                .builder()
                .id(id)
                .build();
    }

    public static CityLine buildCityLine(Long id, String name, CityLineType cityLineType, List<Vehicle> vehicles, List<Schedule> schedules, List<LineStation> lineStations) {
        return CityLine
                .builder()
                .id(id)
                .name(name)
                .cityLineType(cityLineType)
                .vehicles(vehicles)
                .schedules(schedules)
                .lineStations(lineStations)
                .build();
    }

    public static CityLine buildCityLine(Long id) {
        return CityLine
                .builder()
                .id(id)
                .build();
    }
}
