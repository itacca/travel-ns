SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE `account`;
TRUNCATE TABLE `account_role`;
TRUNCATE TABLE `bought_ticket`;
TRUNCATE TABLE `city_line`;
TRUNCATE TABLE `city_line_station`;
TRUNCATE TABLE `price_list`;
TRUNCATE TABLE `price_list_item`;
TRUNCATE TABLE `role`;
TRUNCATE TABLE `schedule`;
TRUNCATE TABLE `schedule_item`;
TRUNCATE TABLE `station`;
TRUNCATE TABLE `ticket`;
TRUNCATE TABLE `vehicle`;
TRUNCATE TABLE `document`;

INSERT INTO role (id_role, label, is_default) VALUES (1, 'ROLE_SYSTEM_ADMINISTRATOR', FALSE);
INSERT INTO role (id_role, label, is_default) VALUES (2, 'ROLE_ADMINISTRATOR', FALSE);
INSERT INTO role (id_role, label, is_default) VALUES (3, 'ROLE_PASSENGER', TRUE);
INSERT INTO role (id_role, label, is_default) VALUES (4, 'ROLE_VERIFIER', FALSE);

INSERT INTO account (email, password, first_name, last_name, validated, id_validated, date_of_birth) VALUES ('sys@ns.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'System', 'Admin', TRUE, TRUE, '1996-01-30 00:00:00');
INSERT INTO account_role (id_account, id_role) VALUES (1, 1);

INSERT INTO account (email, password, first_name, last_name, validated, id_validated, date_of_birth) VALUES ('adm@ns.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Travel', 'Admin', TRUE, TRUE, '1996-01-30 00:00:00');
INSERT INTO account_role (id_account, id_role) VALUES (2, 2);

INSERT INTO account (email, password, first_name, last_name, validated, id_validated, date_of_birth) VALUES ('pass@ns.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Just', 'Passenger', TRUE, FALSE, '1996-01-30 00:00:00');
INSERT INTO account_role (id_account, id_role) VALUES (3, 3);

INSERT INTO account (email, password, first_name, last_name, validated, id_validated, date_of_birth) VALUES ('ver@ns.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Just', 'Verifier', TRUE, TRUE, '1996-01-30 00:00:00');
INSERT INTO account_role (id_account, id_role) VALUES (4, 4);

INSERT INTO account (email, password, first_name, last_name, validated, id_validated, date_of_birth) VALUES ('nikola@ns.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Nikola', 'Zeljkovic', TRUE, TRUE, '1996-01-30 00:00:00');
INSERT INTO account_role (id_account, id_role) VALUES (5, 3);

INSERT INTO account (email, password, first_name, last_name, validated, id_validated, date_of_birth) VALUES ('adm1@ns.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Travel', 'Admin', TRUE, TRUE, '1996-01-30 00:00:00');
INSERT INTO account_role (id_account, id_role) VALUES (6, 2);

INSERT INTO account (email, password, first_name, last_name, validated, id_validated, date_of_birth) VALUES ('adm2@ns.com', '$2a$10$IfTbqUI6X/e7d72O9kSZkel8MAHUMY7gSkyg5w5qGA.sBDbvUKUE.', 'Travel', 'Admin', TRUE, TRUE, '1996-01-30 00:00:00');
INSERT INTO account_role (id_account, id_role) VALUES (7, 2);

INSERT INTO city_line (id_city_line, city_line_type, name) VALUES (1, 'BUS', 'Linija 1');
INSERT INTO city_line (id_city_line, city_line_type, name) VALUES (2, 'TRAM', 'Linija 101');
INSERT INTO city_line (id_city_line, city_line_type, name) VALUES (3, 'BUS', 'Linija 2');
INSERT INTO city_line (id_city_line, city_line_type, name) VALUES (4, 'TRAM', 'Linija 102');
INSERT INTO city_line (id_city_line, city_line_type, name) VALUES (5, 'BUS', 'Linija 3');
INSERT INTO city_line (id_city_line, city_line_type, name) VALUES (6, 'TRAM', 'Linija 103');

INSERT INTO schedule (id_schedule, active, schedule_type, valid_from, fk_id_city_line) VALUES (1, true, 'WORK_DAY', '2018-12-12 00:00:00', 1);
INSERT INTO schedule (id_schedule, active, schedule_type, valid_from, fk_id_city_line) VALUES (2, true, 'WORK_DAY', '2018-12-12 00:00:00', 2);
INSERT INTO schedule (id_schedule, active, schedule_type, valid_from, fk_id_city_line) VALUES (3, true, 'WORK_DAY', '2018-12-12 00:00:00', 3);
INSERT INTO schedule (id_schedule, active, schedule_type, valid_from, fk_id_city_line) VALUES (4, true, 'WEEKEND', '2018-12-12 00:00:00', 3);
INSERT INTO schedule (id_schedule, active, schedule_type, valid_from, fk_id_city_line) VALUES (5, true, 'WORK_DAY', '2018-12-12 00:00:00', 4);
INSERT INTO schedule (id_schedule, active, schedule_type, valid_from, fk_id_city_line) VALUES (6, true, 'WEEKEND', '2018-12-12 00:00:00', 5);
INSERT INTO schedule (id_schedule, active, schedule_type, valid_from, fk_id_city_line) VALUES (7, true, 'WORK_DAY', '2018-12-12 00:00:00', 6);

INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (1, 'MONTHLY', 'REGULAR', 1);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (2, 'DAILY', 'SENIOR', 1);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (3, 'MONTHLY', 'STUDENT', 2);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (4, 'ANNUAL', 'REGULAR', 2);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (5, 'SINGLE_RIDE', 'STUDENT', 3);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (6, 'DAILY', 'SENIOR', 3);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (7, 'SINGLE_RIDE', 'REGULAR', 3);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (8, 'DAILY', 'REGULAR', 3);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (9, 'DAILY', 'STUDENT', null);
INSERT INTO ticket (id_ticket, duration, ticket_type, fk_id_city_line) VALUES (100, 'SINGLE_RIDE', 'STUDENT', null);

INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (1, '2000-01-01 14:10:00', 1);
INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (2, '2000-01-01 14:15:00', 1);
INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (3, '2000-01-01 14:20:00', 1);
INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (4, '2000-01-01 13:21:00', 1);
INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (5, '2000-01-01 18:42:00', 1);
INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (6, '2000-01-01 15:20:00', 1);
INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (7, '2000-01-01 16:39:00', 1);
INSERT INTO schedule_item (id_schedule_item, departure_time, fk_id_schedule) VALUES (8, '2000-01-01 19:19:00', 1);

INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (1, '2018-12-12 00:00:00', 1, 4, TRUE, 11.11);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (2, '2018-12-12 00:00:00', 1, 7, TRUE, 22.22);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (3, '2018-12-12 00:00:00', 2, 3, TRUE, 33.54);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (4, '2018-12-12 00:00:00', 2, 5, TRUE, 44.11);

INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (5, NOW(), 5, 7, TRUE, 35.11);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (6, NOW(), 5, 2, TRUE, 123.11);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (7, '2010-12-12 00:00:00', 5, 2, TRUE, 552.22);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (8, NOW(), 5, 1, TRUE, 23.11);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (9, '2010-12-12 00:00:00', 5, 1, TRUE, 341.22);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (10, NOW(), 5, 4, TRUE, 11.11);
INSERT INTO bought_ticket (id_bought_ticket, bought_date, fk_id_account, fk_id_ticket, active, sold_price) VALUES (11, '2010-12-12 00:00:00', 5, 4, TRUE, 23.11);

INSERT INTO station (id_station, latitude, longitude, station_name) VALUES (1, 45.2489224745, 19.791754249, 'NOVO NASELJE - BISTRICA - OKRETNICA');
INSERT INTO station (id_station, latitude, longitude, station_name) VALUES (2, 45.2488161894, 19.8177958089, 'FUTOŠKA - HIGIJENSKI ZAVOD');
INSERT INTO station (id_station, latitude, longitude, station_name) VALUES (3, 45.2548499029, 19.8422463011, 'USPENSKA - POZORIŠNI TRG');
INSERT INTO station (id_station, latitude, longitude, station_name) VALUES (4, 45.2529483166, 19.8031021067, 'BUL. J. DUČIĆA - BUL. S. JOVANOVIĆA');
INSERT INTO station (id_station, latitude, longitude, station_name) VALUES (5, 45.2388659784, 19.833210388, 'NARODNOG FRONTA - BALZAKOVA');
INSERT INTO station (id_station, latitude, longitude, station_name) VALUES (996, 45.123, 19.123, 'Ne referencirana stanica');

INSERT INTO price_list (id_price_list, end_date, start_date) VALUES (1, '2020-05-15 00:00:00', '2018-01-01 00:00:00');
INSERT INTO price_list (id_price_list, end_date, start_date) VALUES (2, '2021-05-15 00:00:00', '2020-05-23 00:00:00');
INSERT INTO price_list (id_price_list, end_date, start_date) VALUES (3, '2022-05-15 00:00:00', '2021-05-23 00:00:00');
INSERT INTO price_list (id_price_list, end_date, start_date) VALUES (4, '2023-05-15 00:00:00', '2022-05-23 00:00:00');

INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (1, 55, TRUE, '2019-01-12 00:00:00', 4, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (11, 66, FALSE, '2018-12-12 00:00:00', 4, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (12, 77, FALSE, '2018-09-12 00:00:00', 4, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (13, 88, FALSE, '2018-03-12 00:00:00', 4, 1);

INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (2, 99, TRUE, '2018-12-12 00:00:00', 4, 2);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (3, 55, TRUE, '2018-12-12 00:00:00', 4, 3);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (4, 66, TRUE, '2018-12-12 00:00:00', 4, 4);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (5, 77, TRUE, '2018-12-12 00:00:00', 4, 5);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (6, 88, FALSE, '2018-12-12 00:00:00', 4, 6);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (7, 99, FALSE, '2018-12-12 00:00:00', 4, 7);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (8, 55, TRUE, '2018-12-12 00:00:00', 3, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (9, 66, TRUE, '2018-12-12 00:00:00', 3, 2);

INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (14, 77, TRUE, '2019-01-12 00:00:00', 1, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (15, 88, FALSE, '2018-12-12 00:00:00', 1, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (16, 99, FALSE, '2018-09-12 00:00:00', 1, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (17, 55, FALSE, '2018-03-12 00:00:00', 1, 1);

INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (18, 55, TRUE, '2018-12-12 00:00:00', 1, 2);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (19, 55, TRUE, '2018-12-12 00:00:00', 1, 3);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (20, 55, TRUE, '2018-12-12 00:00:00', 1, 4);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (21, 55, TRUE, '2018-12-12 00:00:00', 1, 5);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (22, 55, FALSE, '2018-12-12 00:00:00', 2, 6);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (23, 55, FALSE, '2018-12-12 00:00:00', 2, 7);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (24, 55, TRUE, '2018-12-12 00:00:00', 2, 1);
INSERT INTO price_list_item (id_price_list_item, price, active, date_active, fk_id_price_list, id_ticket) VALUES (10, 55, TRUE, '2018-12-12 00:00:00', 2, 3);

INSERT INTO city_line_station (id_city_line_station, relative_position, fk_id_city_line, fk_id_station) VALUES (1, 1, 1, 1);
INSERT INTO city_line_station (id_city_line_station, relative_position, fk_id_city_line, fk_id_station) VALUES (2, 1, 2, 2);

INSERT INTO vehicle (id_vehicle, plate_number, vehicle_type, fk_id_city_line, fk_id_station) VALUES (1, 'NS-001-BU', 'BUS', 1, 1);
INSERT INTO vehicle (id_vehicle, plate_number, vehicle_type, fk_id_city_line, fk_id_station) VALUES (2, 'NS-001-TR', 'TRAM', 2, 1);
INSERT INTO vehicle (id_vehicle, plate_number, vehicle_type, fk_id_city_line, fk_id_station) VALUES (3, 'NS-002-BU', 'BUS', 3, 1);
INSERT INTO vehicle (id_vehicle, plate_number, vehicle_type, fk_id_city_line, fk_id_station) VALUES (4, 'NS-002-TR', 'TRAM', 4, 2);
INSERT INTO vehicle (id_vehicle, plate_number, vehicle_type, fk_id_city_line, fk_id_station) VALUES (5, 'NS-003-BU', 'BUS', 5, 2);
INSERT INTO vehicle (id_vehicle, plate_number, vehicle_type, fk_id_city_line, fk_id_station) VALUES (6, 'NS-003-TR', 'TRAM', 6, 2);

SET FOREIGN_KEY_CHECKS = 1;