package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.store.LineStationStore;
import com.tim2.travelservice.validator.db.LineStationDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LineStationService {

    private final LineStationStore lineStationStore;

    private final ModelMapper modelMapper;

    private final LineStationDbValidator lineStationDbValidator;

    public LineStationService(LineStationStore lineStationStore,
                              ModelMapper modelMapper,
                              LineStationDbValidator lineStationDbValidator) {
        this.lineStationStore = lineStationStore;
        this.modelMapper = modelMapper;
        this.lineStationDbValidator = lineStationDbValidator;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<DynamicResponse> createAll(List<LineStationDto> lineStationDtos) {
        lineStationDbValidator.validateCreateAllRequest(lineStationDtos);
        List<LineStation> lineStations = lineStationDtos.stream().map(lineStationDto -> modelMapper.map(lineStationDto, LineStation.class)).collect(Collectors.toList());

        lineStations = lineStationStore.saveAll(lineStations);

        List<DynamicResponse> dynamicResponses = new ArrayList<>();
        lineStations.forEach(lineStation -> dynamicResponses.add(new DynamicResponse(RestApiConstants.ID, lineStation.getId())));
        return dynamicResponses;
    }

    @Transactional
    public void updateAll(List<LineStationDto> lineStationDtos) {
        if (CollectionUtils.isNotEmpty(lineStationDtos)) {
            lineStationDbValidator.validateUpdateAllRequest(lineStationDtos);

            List<LineStation> existingLineStationsForGivenCityLine = lineStationStore.findByCityLineId(lineStationDtos.get(0).getCityLine().getId());
            List<LineStation> updatedLineStations = lineStationDtos.stream().map(lineStationDto -> modelMapper.map(lineStationDto, LineStation.class)).collect(Collectors.toList());

            List<LineStation> stationsForDeletion = getStationsForDeletion(existingLineStationsForGivenCityLine, updatedLineStations);
            List<LineStation> stationsForCreation = getStationsForCreation(updatedLineStations);
            List<LineStation> stationForUpdate = getStationsForUpdate(updatedLineStations);

            lineStationStore.deleteAll(stationsForDeletion);
            lineStationStore.saveAll(stationsForCreation);
            lineStationStore.saveAll(stationForUpdate);
        }
    }

    private List<LineStation> getStationsForUpdate(List<LineStation> updatedLineStations) {
        return updatedLineStations
                .stream()
                .filter(lineStation -> lineStation.getId() != null)
                .collect(Collectors.toList());
    }

    private List<LineStation> getStationsForCreation(List<LineStation> updatedLineStations) {
        return updatedLineStations
                .stream()
                .filter(lineStation -> lineStation.getId() == null)
                .collect(Collectors.toList());
    }

    private List<LineStation> getStationsForDeletion(List<LineStation> existingLineStationsForGivenCityLine, List<LineStation> updatedLineStations) {
        List<LineStation> lineStationForDeletion = new ArrayList<>();
        Set<Long> updatedLineStationsIds = updatedLineStations
                .stream()
                .filter(lineStation -> lineStation.getId() != null)
                .map(LineStation::getId)
                .collect(Collectors.toSet());

        existingLineStationsForGivenCityLine.forEach(lineStation -> {
            if (!updatedLineStationsIds.contains(lineStation.getId())) {
                lineStationForDeletion.add(lineStation);
            }
        });

        return lineStationForDeletion;
    }

    @Transactional(readOnly = true)
    public List<LineStationDto> findByCityLineId(Long cityLineId) {
        lineStationDbValidator.validateFindByCiteLineIdRequest(cityLineId);

        return lineStationStore
                .findByCityLineId(cityLineId)
                .stream().map(JsonUtil::map)
                .collect(Collectors.toList());
    }
}
