package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.store.PriceListItemStore;
import com.tim2.travelservice.validator.db.PriceListItemDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PriceListItemService {

    private final PriceListItemStore priceListItemStore;

    private final PriceListItemDbValidator priceListItemDbValidator;

    private final ModelMapper modelMapper;

    public PriceListItemService(PriceListItemStore priceListItemStore,
                                PriceListItemDbValidator priceListItemDbValidator,
                                ModelMapper modelMapper) {
        this.priceListItemStore = priceListItemStore;
        this.priceListItemDbValidator = priceListItemDbValidator;
        this.modelMapper = modelMapper;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse create(PriceListItemDto priceListItemDto) {
        priceListItemDbValidator.validateCreateRequest(priceListItemDto);

        return savePriceListItem(priceListItemDto);
    }

    private DynamicResponse savePriceListItem(PriceListItemDto priceListItemDto) {
        PriceListItem priceListItem = modelMapper.map(priceListItemDto, PriceListItem.class);

        addDefaultFieldValues(priceListItem);
        priceListItem = priceListItemStore.save(priceListItem);

        return new DynamicResponse(RestApiConstants.ID, priceListItem.getId());
    }

    private void addDefaultFieldValues(PriceListItem priceListItem) {
        priceListItem.setActive(true);
        priceListItem.setDateActive(DateTime.now(DateTimeZone.UTC));
    }

    @Transactional
    public DynamicResponse update(PriceListItemDto priceListItemDto) {
        PriceListItem existingPriceListItem = priceListItemDbValidator.validateUpdatePriceListItemRequest(priceListItemDto);
        existingPriceListItem.setActive(false);
        priceListItemStore.save(existingPriceListItem);

        PriceListItem newPriceListItem = createNewPriceListItemForUpdate(priceListItemDto, existingPriceListItem);
        newPriceListItem = priceListItemStore.save(newPriceListItem);

        return new DynamicResponse(RestApiConstants.ID, newPriceListItem.getId());
    }

    private PriceListItem createNewPriceListItemForUpdate(PriceListItemDto priceListItemDto, PriceListItem existingPriceListItem) {
        PriceListItem newPriceListItem = new PriceListItem();
        RestApiUtils.copyNonNullProperties(existingPriceListItem, newPriceListItem);
        newPriceListItem.setId(null);
        newPriceListItem.setActive(true);
        newPriceListItem.setDateActive(DateTime.now(DateTimeZone.UTC));
        newPriceListItem.setPrice(priceListItemDto.getPrice());
        return newPriceListItem;
    }

    @Transactional
    public PriceListItem deleteById(Long id) {
        PriceListItem priceListItem = priceListItemDbValidator.validateDeletePriceListItemRequest(id);

        priceListItem.setActive(false);
        return priceListItemStore.save(priceListItem);
    }

    @Transactional(readOnly = true)
    public Page<PriceListItemDto> findByPriceListId(Long priceListId, Pageable pageable) {
        priceListItemDbValidator.validateFindByPriceListIdRequest(priceListId);

        return priceListItemStore
                .findActiveByPriceListId(priceListId, pageable)
                .map(JsonUtil::map);
    }

    @Transactional(readOnly = true)
    public Page<PriceListItemDto> findAllByPriceListAndTicket(Long priceListId, Long ticketId, Pageable pageable) {
        priceListItemDbValidator.validateFindByPriceListAndTicketIdRequest(priceListId, ticketId);

        return priceListItemStore
                .findByPriceListIdAndTicketId(priceListId, ticketId, pageable)
                .map(JsonUtil::map);
    }
}
