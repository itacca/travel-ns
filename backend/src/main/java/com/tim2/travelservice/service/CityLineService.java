package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.store.CityLineStore;
import com.tim2.travelservice.validator.db.CityLineDbValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CityLineService {

    private final CityLineStore cityLineStore;

    private final CityLineDbValidator cityLineDbValidator;

    private ModelMapper modelMapper;

    public CityLineService(CityLineStore cityLineStore,
                           CityLineDbValidator cityLineDbValidator,
                           ModelMapper modelMapper) {
        this.cityLineStore = cityLineStore;
        this.cityLineDbValidator = cityLineDbValidator;
        this.modelMapper = modelMapper;
    }

    @Transactional(readOnly = true)
    public Page<CityLineDto> findByType(Pageable pageable, String type) {
        CityLineType cityLineType = CityLineType.valueOf(type.toUpperCase());

        return cityLineStore.findByType(pageable, cityLineType)
                .map(JsonUtil::map);
    }

    @Transactional(readOnly = true)
    public Page<CityLineDto> findAll(Pageable pageable) {
        return cityLineStore.findAll(pageable)
                .map(JsonUtil::map);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse create(CityLineDto cityLineDto) {
        CityLine cityLine = modelMapper.map(cityLineDto, CityLine.class);

        cityLine = cityLineStore.save(cityLine);

        return new DynamicResponse(RestApiConstants.ID, cityLine.getId());
    }

    @Transactional
    public void update(CityLineDto cityLineDto) {
        CityLine existingCityLine = cityLineDbValidator.validateUpdateRequest(cityLineDto);
        CityLine updatedCityLine = modelMapper.map(cityLineDto, CityLine.class);

        RestApiUtils.copyNonNullProperties(updatedCityLine, existingCityLine);
        cityLineStore.save(existingCityLine);
    }

    @Transactional(readOnly = true)
    public CityLineDto findById(Long id) {
        CityLine cityLine = cityLineDbValidator.validateFindByIdRequest(id);

        return JsonUtil.map(cityLine);
    }

    @Transactional
    public void deleteById(Long id) {
        CityLine cityLine = cityLineDbValidator.validateDeleteByIdRequest(id);

        cityLineStore.delete(cityLine);
    }
}
