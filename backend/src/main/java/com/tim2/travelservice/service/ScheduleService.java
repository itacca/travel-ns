package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.store.ScheduleStore;
import com.tim2.travelservice.validator.db.ScheduleDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ScheduleService {

    private final ScheduleStore scheduleStore;

    private final VehicleService vehicleService;

    private final ModelMapper modelMapper;

    private final ScheduleDbValidator scheduleDbValidator;

    public ScheduleService(ScheduleStore scheduleStore,
                           VehicleService vehicleService,
                           ModelMapper modelMapper,
                           ScheduleDbValidator scheduleDbValidator) {
        this.scheduleStore = scheduleStore;
        this.vehicleService = vehicleService;
        this.modelMapper = modelMapper;
        this.scheduleDbValidator = scheduleDbValidator;
    }

    @Transactional
    public void update(ScheduleDto scheduleDto) {
        Schedule existingSchedule = scheduleDbValidator.validateUpdateRequest(scheduleDto);

        setNonModifiableFieldsToNull(scheduleDto);
        Schedule updatedSchedule = modelMapper.map(scheduleDto, Schedule.class);
        RestApiUtils.copyNonNullProperties(updatedSchedule, existingSchedule);

        scheduleStore.save(existingSchedule);
    }

    private void setNonModifiableFieldsToNull(ScheduleDto scheduleDto) {
        scheduleDto.setActive(null);
        scheduleDto.setId(null);
        scheduleDto.setCityLine(null);
        scheduleDto.setValidFrom(null);
        scheduleDto.setValidUntil(null);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse create(ScheduleDto scheduleDto) {
        scheduleDbValidator.validateCreateRequest(scheduleDto.getCityLine().getId());

        setAllSchedulesInactive(scheduleDto.getCityLine().getId(), scheduleDto.getScheduleType());
        Schedule schedule = addSchedule(scheduleDto);

        return new DynamicResponse(RestApiConstants.ID, schedule.getId());
    }

    private void setAllSchedulesInactive(Long lineId, ScheduleType scheduleType) {
        Optional<Schedule> activeSchedule = scheduleStore.findActiveForGivenCityLineAndType(lineId, scheduleType);
        activeSchedule.ifPresent(this::setToInactive);
    }

    private Schedule setToInactive(Schedule schedule) {
        schedule.setValidUntil(new DateTime());
        schedule.setActive(false);
        scheduleStore.save(schedule);

        return schedule;
    }

    private Schedule addSchedule(ScheduleDto scheduleDto) {
        Schedule schedule = modelMapper.map(scheduleDto, Schedule.class);
        schedule.setValidFrom(DateTime.now(DateTimeZone.UTC));
        schedule.setActive(true);

        return scheduleStore.save(schedule);
    }

    @Transactional(readOnly = true)
    public List<ScheduleDto> findByCityLineId(Long cityLineId) {
        scheduleDbValidator.validateFindByCityLineId(cityLineId);

        return scheduleStore
                .findByLineId(cityLineId)
                .stream()
                .map(JsonUtil::map)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public ScheduleDto findByCityLineIdAndType(Long lineId, String type) {
        Optional<Schedule> schedule = scheduleStore.findActiveForGivenCityLineAndType(lineId, ScheduleType.valueOf(type.toUpperCase()));

        return schedule.isPresent() ? JsonUtil.map(schedule.get()) : JsonUtil.map(new Schedule());
    }

    public Optional<Schedule> findById(Long id) {
        return scheduleStore.findById(id);
    }
}
