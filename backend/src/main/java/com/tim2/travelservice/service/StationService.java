package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.store.StationStore;
import com.tim2.travelservice.validator.db.StationDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.StationDto;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StationService {

    private final StationStore stationStore;

    private final ModelMapper modelMapper;

    private final StationDbValidator stationDbValidator;

    public StationService(StationStore stationStore,
                          ModelMapper modelMapper,
                          StationDbValidator stationDbValidator) {
        this.stationStore = stationStore;
        this.modelMapper = modelMapper;
        this.stationDbValidator = stationDbValidator;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse create(StationDto stationDto) {
        Station station = modelMapper.map(stationDto, Station.class);

        station = stationStore.save(station);

        return new DynamicResponse(RestApiConstants.ID, station.getId());
    }

    @Transactional(readOnly = true)
    public StationDto findById(Long id) {
        Station station = stationDbValidator.validateFindByIdRequest(id);

        return JsonUtil.map(station);
    }

    @Transactional(readOnly = true)
    public Page<StationDto> findAll(Pageable pageable) {
        return stationStore.findAll(pageable)
                .map(JsonUtil::map);
    }

    @Transactional
    public void update(StationDto stationDto) {
        Station existingStation = stationDbValidator.validateUpdateStationRequest(stationDto);
        Station updatedStation = modelMapper.map(stationDto, Station.class);

        RestApiUtils.copyNonNullProperties(updatedStation, existingStation);
        stationStore.save(existingStation);
    }

    @Transactional
    public void deleteById(Long stationId) {
        Station station = stationDbValidator.validateDeleteByIdRequest(stationId);

        stationStore.delete(station);
    }
}
