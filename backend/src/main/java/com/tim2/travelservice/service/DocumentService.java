package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.entity.DocumentType;
import com.tim2.travelservice.store.DocumentStore;
import com.tim2.travelservice.validator.db.DocumentDbValidator;
import com.tim2.travelservice.web.dto.DocumentDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class DocumentService {

    private final DocumentStore documentStore;

    private final AccountService accountService;

    private final DocumentDbValidator documentDbValidator;

    public DocumentService(DocumentStore documentStore,
                           AccountService accountService,
                           DocumentDbValidator documentDbValidator) {
        this.documentStore = documentStore;
        this.accountService = accountService;
        this.documentDbValidator = documentDbValidator;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse saveDocument(MultipartFile imageBlob, String imageType, String documentType, String userEmail) throws IOException {
        Account account = accountService.findEntityByEmail(userEmail);
        Document document = new Document(null, imageType, imageBlob.getBytes(), true, false, DocumentType.valueOf(documentType.toUpperCase()), account);

        return saveImage(document);
    }

    private DynamicResponse saveImage(Document document) {
        deleteActiveDocumentsForUser(document.getAccount());
        document = documentStore.save(document);

        return new DynamicResponse(RestApiConstants.ID, document.getId());
    }

    private void deleteActiveDocumentsForUser(Account account) {
        List<Document> activeDocuments = documentStore.findByAccountAndActive(account, true);
        activeDocuments.forEach(activeDocument -> {
            activeDocument.setActive(false);
            documentStore.save(activeDocument);
        });
    }

    @Transactional(readOnly = true)
    public DynamicResponse findImageBlobByDocumentId(Long documentId) {
        Document document = documentDbValidator.validateFindImageBlobByDocumentId(documentId);

        return new DynamicResponse(RestApiConstants.IMAGE_BLOB, document.getImageBlob());
    }

    @Transactional(readOnly = true)
    public Page<DocumentDto> findByActiveAndVerified(Boolean active, Boolean verified, Pageable pageable) {
        return documentStore.findByActiveAndVerified(active, verified, pageable)
                .map(JsonUtil::map);
    }

    @Transactional
    public void verify(DocumentDto documentDto) {
        Document document = documentDbValidator.validateVerifyRequest(documentDto);

        document.setVerified(documentDto.getVerified());
        documentStore.save(document);
    }
}
