package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleItem;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.store.ScheduleItemStore;
import com.tim2.travelservice.validator.db.ScheduleItemDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleDto;
import com.tim2.travelservice.web.dto.ScheduleItemDto;
import org.joda.time.DateTime;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ScheduleItemService {

    private final ScheduleItemStore scheduleItemStore;

    private final ScheduleService scheduleService;

    private final VehicleService vehicleService;

    private final ScheduleItemDbValidator scheduleItemDbValidator;

    private final ModelMapper modelMapper;

    public ScheduleItemService(ScheduleItemStore scheduleItemStore,
                               ScheduleService scheduleService,
                               VehicleService vehicleService,
                               ScheduleItemDbValidator scheduleItemDbValidator,
                               ModelMapper modelMapper) {
        this.scheduleItemStore = scheduleItemStore;
        this.scheduleService = scheduleService;
        this.vehicleService = vehicleService;
        this.scheduleItemDbValidator = scheduleItemDbValidator;
        this.modelMapper = modelMapper;
    }

    @Transactional(readOnly = true)
    public List<ScheduleItemDto> findByScheduleId(Long scheduleId) {
        scheduleItemDbValidator.validateFindByScheduleIdRequest(scheduleId);

        List<ScheduleItem> scheduleItems = scheduleItemStore.findByScheduleId(scheduleId);

        return scheduleItems
                .stream()
                .map(JsonUtil::map)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteById(Long id) {
        ScheduleItem scheduleItem = scheduleItemDbValidator.validateDeleteByIdRequest(id);

        scheduleItemStore.delete(scheduleItem);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<DynamicResponse> createAll(List<ScheduleItemDto> scheduleItemDtos) {
        List<ScheduleItem> scheduleItems = scheduleItemDtos.stream().map(scheduleItemDto -> modelMapper.map(scheduleItemDto,
                ScheduleItem.class)).collect(Collectors.toList());

        scheduleItems = scheduleItemStore.saveAll(scheduleItems);
        scheduleItems = setUpScheduleItems(scheduleItems);

        List<DynamicResponse> dynamicResponses = new ArrayList<>();
        scheduleItems.forEach(scheduleItem -> dynamicResponses.add(new DynamicResponse(RestApiConstants.ID, scheduleItem.getId())));
        return dynamicResponses;
    }

    private List<ScheduleItem> setUpScheduleItems(List<ScheduleItem> scheduleItems) {
        if (scheduleItems.isEmpty()) {
            return scheduleItems;
        }
        scheduleItems.forEach(this::setVehicle);
        scheduleItems = scheduleItemStore.saveAll(scheduleItems);
        return scheduleItems;
    }

    private void setVehicle(ScheduleItem scheduleItem) {
        List<Vehicle> vehicles = vehicleService.findAllCollection();
        if (vehicles.isEmpty()) {
            return;
        }
        Schedule itemParent = scheduleService.findById(scheduleItem.getSchedule().getId()).get();
        for (Vehicle vehicle : vehicles) {
            if (!vehicle.getVehicleType().toString().equals(itemParent.getCityLine().getCityLineType().toString())) {
                continue;
            }
            if (isFree(vehicle, itemParent, scheduleItem)) {
                scheduleItem.setVehicle(vehicle);
                vehicle.getScheduleItems().add(scheduleItem);
                vehicleService.saveVehicle(vehicle);
                return;
            }
        }
    }

    private boolean isFree(Vehicle vehicle, Schedule itemParent, ScheduleItem scheduleItem) {
        return vehicle.getScheduleItems().stream()
                .filter(item -> checkIfTimeIsOk(item, itemParent, scheduleItem))
                .collect(Collectors.toList())
                .isEmpty();
    }

    private boolean checkIfTimeIsOk(ScheduleItem scheduleItem, Schedule itemParent, ScheduleItem item) {
        ScheduleType startingType = itemParent.getScheduleType();
        Schedule vehicleItemParent = scheduleService.findById(scheduleItem.getSchedule().getId()).get();
        if (vehicleItemParent.getScheduleType().toString().equals(startingType.toString())) {
            DateTime vehicleItemTime = scheduleItem.getDepartureTime();
            DateTime startingItemTime = item.getDepartureTime();

            return Math.abs(startingItemTime.getHourOfDay() - vehicleItemTime.getHourOfDay()) <= 2;
        } else {
            return false;
        }
    }
}
