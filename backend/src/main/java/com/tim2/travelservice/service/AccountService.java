package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Role;
import com.tim2.travelservice.store.AccountStore;
import com.tim2.travelservice.validator.db.AccountDbValidator;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;

@Service
public class AccountService {

    private final AccountStore accountStore;

    private final ModelMapper modelMapper;

    private final AccountDbValidator accountDbValidator;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private static final Long DEFAULT_USER_ROLE_ID = 3L;

    private static final Long ADMINISTRATOR_ROLE_ID = 2L;

    private static final String DEFAULT_ADMINISTRATOR_PASSWORD = "password";

    public AccountService(AccountStore accountStore, ModelMapper modelMapper, AccountDbValidator accountDbValidator, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.accountStore = accountStore;
        this.modelMapper = modelMapper;
        this.accountDbValidator = accountDbValidator;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse register(AccountDto accountDto) {
        accountDbValidator.validateRegistrationRequest(accountDto);

        return addAccount(accountDto);
    }

    private DynamicResponse addAccount(AccountDto accountDto) {
        Account account = modelMapper.map(accountDto, Account.class);

        addDefaultFieldValuesPassenger(account);
        encodePassword(account);
        account = accountStore.create(account);

        return new DynamicResponse(RestApiConstants.ID, account.getId());
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse registerAdmin(AccountDto accountDto) {
        accountDbValidator.validateRegistrationRequest(accountDto);

        return addAdminAccount(accountDto);
    }

    private DynamicResponse addAdminAccount(AccountDto accountDto) {
        Account account = modelMapper.map(accountDto, Account.class);

        addDefaultFieldValuesAdmin(account);
        encodePassword(account);
        account = accountStore.create(account);

        return new DynamicResponse(RestApiConstants.ID, account.getId());
    }

    private void addDefaultFieldValuesPassenger(Account account) {
        account.setValidated(true);
        account.setIdValidated(false);
        account.setRoles(new ArrayList<>());
        account.getRoles().add(new Role(DEFAULT_USER_ROLE_ID));
    }

    private void addDefaultFieldValuesAdmin(Account account) {
        account.setValidated(true);
        account.setIdValidated(true);
        account.setRoles(new ArrayList<>());
        account.getRoles().add(new Role(ADMINISTRATOR_ROLE_ID));
        account.setPassword(DEFAULT_ADMINISTRATOR_PASSWORD);
    }

    private void encodePassword(Account account) {
        account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
    }

    @Transactional(readOnly = true)
    public AccountDto findByEmail(String email) {
        Account account = accountDbValidator.validateFindByEmailRequest(email);

        return JsonUtil.map(account);
    }

    @Transactional
    public void updateByEmail(String email, AccountDto accountDto) {
        Account existingAccount = accountDbValidator.validateUpdateByEmailRequest(email);

        setNonModifiableFieldsToNull(accountDto);
        Account updatedAccount = modelMapper.map(accountDto, Account.class);
        encryptPasswordIfProvided(updatedAccount);

        RestApiUtils.copyNonNullProperties(updatedAccount, existingAccount);
        accountStore.save(existingAccount);
    }

    private void setNonModifiableFieldsToNull(AccountDto accountDto) {
        accountDto.setEmail(null);
        accountDto.setRoles(null);
        accountDto.setIdValidated(null);
        accountDto.setValidated(null);
        accountDto.setId(null);
    }

    private void encryptPasswordIfProvided(Account updatedAccount) {
        if (!StringUtils.isEmpty(updatedAccount.getPassword())) {
            updatedAccount.setPassword(bCryptPasswordEncoder.encode(updatedAccount.getPassword()));
        }
    }

    public Account findEntityByEmail(String email) {
        return accountDbValidator.validateFindByEmailRequest(email);
    }

    @Transactional(readOnly = true)
    public Page<AccountDto> findByRoleId(Long roleId, Pageable pageable) {
        return accountStore
                .findByRoleId(roleId, pageable)
                .map(JsonUtil::map);
    }

    @Transactional
    public void deleteTravelAdminById(Long id) {
        Account account = accountDbValidator.validateDeleteTravelAdminById(id);

        accountStore.delete(account);
    }
}
