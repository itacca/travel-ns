package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.repository.CityLineRepository;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.store.VehicleStore;
import com.tim2.travelservice.validator.db.VehicleDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import java.util.Optional;

@Service
public class VehicleService {

    private final VehicleStore vehicleStore;

    private final VehicleDbValidator vehicleDbValidator;

    private final ModelMapper modelMapper;

    private final CityLineRepository cityLineRepository;

    public VehicleService(VehicleStore vehicleStore,
                          VehicleDbValidator vehicleDbValidator,
                          ModelMapper modelMapper,
                          CityLineRepository cityLineRepository) {
        this.vehicleStore = vehicleStore;
        this.vehicleDbValidator = vehicleDbValidator;
        this.modelMapper = modelMapper;
        this.cityLineRepository = cityLineRepository;
    }

    @Transactional(readOnly = true)
    public Page<VehicleDto> findAllPageable(Pageable pageable) {
        return vehicleStore.findAllPageable(pageable)
                .map(JsonUtil::map);
    }

    @Transactional(readOnly = true)
    public List<Vehicle> findAllCollection() {
        return vehicleStore.findAllCollection();
    }

    public Vehicle saveVehicle(Vehicle vehicle) {
        return vehicleStore.saveVehicle(vehicle);
    }

    @Transactional
    public void delete(Long id) {
        Vehicle vehicle = vehicleDbValidator.validateDeleteVehicleRequest(id);

        vehicleStore.delete(vehicle);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse create(VehicleDto vehicleDto) {
        vehicleDbValidator.validateCreateVehicleRequest(vehicleDto);
        Vehicle vehicle = modelMapper.map(vehicleDto, Vehicle.class);
        setStartingStation(vehicle);
        vehicle = vehicleStore.save(vehicle);

        return new DynamicResponse(RestApiConstants.ID, vehicle.getId());
    }

    private void setStartingStation(Vehicle vehicle) {
        Optional<CityLine> cityLine = cityLineRepository.findById(vehicle.getCityLine().getId());
        Station startingStation = cityLine.get().getLineStations().get(0).getStation();
        vehicle.setStation(startingStation);
    }

    @Transactional
    public void update(VehicleDto vehicleDto) {
        Vehicle existingVehicle = vehicleDbValidator.validateUpdateVehicleRequest(vehicleDto);

        setNonModifiableFieldsToNull(vehicleDto);
        Vehicle updatedVehicle = modelMapper.map(vehicleDto, Vehicle.class);

        RestApiUtils.copyNonNullProperties(updatedVehicle, existingVehicle);
        vehicleStore.save(existingVehicle);
    }

    private void setNonModifiableFieldsToNull(VehicleDto vehicleDto) {
        vehicleDto.setCityLine(null);
        vehicleDto.setStation(null);
        vehicleDto.setVehicleType(null);
    }

}
