package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.BoughtTicket;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.store.BoughtTicketStore;
import com.tim2.travelservice.validator.db.BoughtTicketDbValidator;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BoughtTicketService {

    private final BoughtTicketStore boughtTicketStore;

    private final BoughtTicketDbValidator boughtTicketDbValidator;

    public BoughtTicketService(BoughtTicketStore boughtTicketStore,
                               BoughtTicketDbValidator boughtTicketDbValidator) {
        this.boughtTicketStore = boughtTicketStore;
        this.boughtTicketDbValidator = boughtTicketDbValidator;
    }

    @Transactional(readOnly = true)
    public Page<BoughtTicketDto> findByAccountEmail(String email, Pageable pageable) {
        return boughtTicketStore.findByAccountEmail(email, pageable)
                .map(JsonUtil::map);
    }

    @Transactional
    public DynamicResponse validate(Long id) {
        BoughtTicket boughtTicket = boughtTicketDbValidator.validateValidateRequest(id);

        Boolean isActive = false;
        if (boughtTicket.getActive()) {
            isActive = calculateIsTicketActive(boughtTicket);
            if (!isActive) {
                boughtTicket.setActive(isActive);
                boughtTicketStore.save(boughtTicket);
                isActive = setIsActiveToTrueForSingleRideFirstValidation(boughtTicket, isActive);
            }
        }

        return new DynamicResponse(RestApiConstants.IS_ACTIVE, isActive);
    }

    private Boolean setIsActiveToTrueForSingleRideFirstValidation(BoughtTicket boughtTicket, Boolean isActive) {
        if (boughtTicket.getTicket().getTicketDuration() == TicketDuration.SINGLE_RIDE) {
            isActive = true;
        }
        return isActive;
    }

    private Boolean calculateIsTicketActive(BoughtTicket boughtTicket) {
        switch (boughtTicket.getTicket().getTicketDuration()) {
            case SINGLE_RIDE:
                return false;
            case DAILY:
                return DateTime.now(DateTimeZone.UTC).withTimeAtStartOfDay().equals(boughtTicket.getBoughtDate().withTimeAtStartOfDay());
            case MONTHLY:
                return DateTime.now(DateTimeZone.UTC).getMonthOfYear() == boughtTicket.getBoughtDate().getMonthOfYear()
                        && DateTime.now(DateTimeZone.UTC).getYear() == boughtTicket.getBoughtDate().getYear();
            case ANNUAL:
                return DateTime.now(DateTimeZone.UTC).getYear() == boughtTicket.getBoughtDate().getYear();
            default:
                return false;
        }
    }

    @Transactional(readOnly = true)
    public BoughtTicketDto findById(Long id) {
        BoughtTicket boughtTicket = boughtTicketDbValidator.validateFindByIdRequest(id);

        return JsonUtil.map(boughtTicket);
    }
}
