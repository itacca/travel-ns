package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.store.PriceListStore;
import com.tim2.travelservice.validator.db.PriceListDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PriceListService {

    private final PriceListStore priceListStore;

    private final PriceListDbValidator priceListDbValidator;

    private final ModelMapper modelMapper;


    public PriceListService(PriceListStore priceListStore,
                            PriceListDbValidator priceListDbValidator,
                            ModelMapper modelMapper) {
        this.priceListStore = priceListStore;
        this.priceListDbValidator = priceListDbValidator;
        this.modelMapper = modelMapper;

    }

    @Transactional
    public void updateBasicInformation(PriceListDto priceListDto) {
        PriceList existingPriceList = priceListDbValidator.validateUpdateRequest(priceListDto);

        setNonModifiableFieldsToNull(priceListDto);

        PriceList updatedPriceList = modelMapper.map(priceListDto, PriceList.class);

        RestApiUtils.copyNonNullProperties(updatedPriceList, existingPriceList);
        priceListStore.save(existingPriceList);
    }

    private void setNonModifiableFieldsToNull(PriceListDto priceListDto) {
        priceListDto.setPriceListItems(null);
        priceListDto.setStartDate(null);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse create(PriceListDto priceListDto) {
        return savePriceList(priceListDto);
    }

    private DynamicResponse savePriceList(PriceListDto priceListDto) {
        PriceList priceList = modelMapper.map(priceListDto, PriceList.class);
        priceList = priceListStore.save(priceList);

        return new DynamicResponse(RestApiConstants.ID, priceList.getId());
    }

    @Transactional
    public void deleteById(Long id) {
        PriceList priceList = priceListDbValidator.validateDeleteByIdRequest(id);

        priceListStore.delete(priceList);
    }

    @Transactional(readOnly = true)
    public PriceListDto findById(Long priceListId) {
        PriceList priceList = priceListDbValidator.validateFindByIdRequest(priceListId);

        return JsonUtil.map(priceList);
    }

    @Transactional(readOnly = true)
    public Page<PriceListDto> findAll(Pageable pageable) {
        return priceListStore
                .findAll(pageable)
                .map(JsonUtil::map);
    }
}
