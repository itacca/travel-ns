package com.tim2.travelservice.service;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.common.utils.JsonUtil;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.store.TicketStore;
import com.tim2.travelservice.validator.db.TicketDbValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.TicketDto;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TicketService {

    private final TicketStore ticketStore;

    private final ModelMapper modelMapper;

    private final TicketDbValidator ticketDbValidator;

    public TicketService(TicketStore ticketStore, ModelMapper modelMapper, TicketDbValidator ticketDbValidator) {
        this.ticketStore = ticketStore;
        this.modelMapper = modelMapper;
        this.ticketDbValidator = ticketDbValidator;
    }

    @Transactional(readOnly = true)
    public Page<TicketDto> findAll(Pageable pageable) {
        return ticketStore.findAll(pageable)
                .map(JsonUtil::map);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DynamicResponse addTicket(TicketDto ticketDto) {
        Ticket ticket = modelMapper.map(ticketDto, Ticket.class);

        ticket = ticketStore.save(ticket);

        return new DynamicResponse(RestApiConstants.ID, ticket.getId());
    }

    @Transactional(readOnly = true)
    public TicketDto findById(Long id) {
        Ticket ticket = ticketDbValidator.validateFindByIdRequest(id);

        return JsonUtil.map(ticket);
    }

    @Transactional
    public void updateTicket(TicketDto ticketDto) {
        Ticket existingTicket = ticketDbValidator.validateUpdateTicketRequest(ticketDto);
        Ticket updatedTicket = modelMapper.map(ticketDto, Ticket.class);

        RestApiUtils.copyNonNullProperties(updatedTicket, existingTicket);
        ticketStore.save(existingTicket);
    }

    @Transactional
    public void deleteTicket(Long id) {
        Ticket ticket = ticketDbValidator.validateDeleteTicketRequest(id);

        ticketStore.delete(ticket);
    }

    public Page<TicketDto> findWithoutDefinedPriceForGivenPriceList(Long priceListId, Pageable pageable) {
        return ticketStore
                .findWithoutDefinedPriceForGivenPriceList(priceListId, pageable)
                .map(JsonUtil::map);
    }

    public Page<TicketDto> findWithDefinedPriceForGivenPriceList(Long priceListId, Pageable pageable) {
        return ticketStore
                .findWithDefinedPriceForGivenPriceList(priceListId, pageable)
                .map(JsonUtil::map);
    }
}
