package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.repository.LineStationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LineStationStore {

    private final LineStationRepository lineStationRepository;

    public LineStationStore(LineStationRepository lineStationRepository) {
        this.lineStationRepository = lineStationRepository;
    }

    public List<LineStation> saveAll(List<LineStation> lineStations) {
        return lineStationRepository.saveAll(lineStations);
    }

    public List<LineStation> findByCityLineId(Long id) {
        return lineStationRepository.findByCityLineId(id);
    }

    public void deleteAll(List<LineStation> stationsForDeletion) {
        lineStationRepository.deleteAll(stationsForDeletion);
    }
}
