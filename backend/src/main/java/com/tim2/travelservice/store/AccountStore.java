package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.repository.AccountRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AccountStore {

    private final AccountRepository accountRepository;

    public AccountStore(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account create(Account account) {
        return accountRepository.save(account);
    }

    public void save(Account account) {
        accountRepository.save(account);
    }

    public Page<Account> findByRoleId(Long roleId, Pageable pageable) {
        return accountRepository.findByRolesId(roleId, pageable);
    }

    public void delete(Account account) {
        accountRepository.delete(account);
    }
}
