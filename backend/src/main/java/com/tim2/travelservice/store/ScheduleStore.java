package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.repository.ScheduleRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScheduleStore {

    private final ScheduleRepository scheduleRepository;

    public ScheduleStore(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    public List<Schedule> findByLineId(Long lineId) {
        return scheduleRepository.findByCityLineId(lineId);
    }

    public Optional<Schedule> findById(Long scheduleId) {
        return scheduleRepository.findById(scheduleId);
    }

    public Schedule save(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }

    public Optional<Schedule> findActiveForGivenCityLineAndType(Long lineId, ScheduleType scheduleType) {
        return scheduleRepository.findByCityLineIdAndActiveAndScheduleType(lineId, true, scheduleType);
    }
}
