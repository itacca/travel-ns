package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.ScheduleItem;
import com.tim2.travelservice.repository.ScheduleItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleItemStore {

    private final ScheduleItemRepository scheduleItemRepository;

    public ScheduleItemStore(ScheduleItemRepository scheduleItemRepository) {
        this.scheduleItemRepository = scheduleItemRepository;
    }

    public List<ScheduleItem> findByScheduleId(Long scheduleId) {
        return scheduleItemRepository.findByScheduleId(scheduleId);
    }

    public void delete(ScheduleItem scheduleItem) {
        scheduleItemRepository.delete(scheduleItem);
    }

    public ScheduleItem save(ScheduleItem scheduleItem) {
        return scheduleItemRepository.save(scheduleItem);
    }

    public List<ScheduleItem> saveAll(List<ScheduleItem> scheduleItems) {
        return scheduleItemRepository.saveAll(scheduleItems);
    }

    public ScheduleItem findById(Long id) {
        return scheduleItemRepository.findById(id).get();
    }
}
