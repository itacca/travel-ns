package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.exception.StoreException;
import com.tim2.travelservice.repository.PriceListItemRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PriceListItemStore {

    private final PriceListItemRepository priceListItemRepository;

    public PriceListItemStore(PriceListItemRepository priceListItemRepository) {
        this.priceListItemRepository = priceListItemRepository;
    }

    public PriceListItem save(PriceListItem priceListItem) {
        return priceListItemRepository.save(priceListItem);
    }

    public PriceListItem findById(Long id) throws StoreException {
        return priceListItemRepository.findById(id).orElseThrow(StoreException::new);
    }

    public Page<PriceListItem> findActiveByPriceListId(Long priceListId, Pageable pageable) {
        return priceListItemRepository.findByPriceListIdAndActive(priceListId, true, pageable);
    }

    public Page<PriceListItem> findByPriceListIdAndTicketId(Long priceListId, Long ticketId, Pageable pageable) {
        return priceListItemRepository.findByPriceListIdAndTicketId(priceListId, ticketId, pageable);
    }

    public void delete(PriceListItem priceListItem) {
        priceListItemRepository.delete(priceListItem);
    }
}
