package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.repository.CityLineRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityLineStore {

    private final CityLineRepository cityLineRepository;

    public CityLineStore(CityLineRepository cityLineRepository) {
        this.cityLineRepository = cityLineRepository;
    }

    public Page<CityLine> findAll(Pageable pageable) {
        return cityLineRepository.findAll(pageable);
    }

    public Page<CityLine> findByType(Pageable pageable, CityLineType cityLineType) {
        return cityLineRepository.findByCityLineType(pageable, cityLineType);
    }

    public CityLine save(CityLine cityLine) {
        return cityLineRepository.save(cityLine);
    }

    public void delete(CityLine cityLine) {
        cityLineRepository.delete(cityLine);
    }
}
