package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.repository.DocumentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentStore {

    private final DocumentRepository documentRepository;

    public DocumentStore(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    public Document save(Document document) {
        return documentRepository.save(document);
    }

    public List<Document> findByAccountAndActive(Account account, Boolean active) {
        return documentRepository.findByAccountAndActive(account, active);
    }

    public Page<Document> findByActiveAndVerified(Boolean active, Boolean verified, Pageable pageable) {
        return documentRepository.findByActiveAndVerified(active, verified, pageable);
    }
}
