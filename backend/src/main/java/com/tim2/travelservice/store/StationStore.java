package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.repository.StationRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class StationStore {

    private final StationRepository stationRepository;

    public StationStore(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    public Station save(Station station) {
        return stationRepository.save(station);
    }

    public Page<Station> findAll(Pageable pageable) {
        return stationRepository.findAll(pageable);
    }

    public void delete(Station station) {
        stationRepository.delete(station);
    }
}
