package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.exception.StoreException;
import com.tim2.travelservice.repository.PriceListRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PriceListStore {

    private final PriceListRepository priceListRepository;

    public PriceListStore(PriceListRepository priceListRepository) {
        this.priceListRepository = priceListRepository;
    }

    public PriceList findById(Long id) throws StoreException {
        return priceListRepository.findById(id).orElseThrow(StoreException::new);
    }

    public PriceList save(PriceList priceList) {
        return priceListRepository.save(priceList);
    }

    public void delete(PriceList priceList) {
        priceListRepository.delete(priceList);
    }

    public Page<PriceList> findAll(Pageable pageable) {
        return priceListRepository.findAll(pageable);
    }
}
