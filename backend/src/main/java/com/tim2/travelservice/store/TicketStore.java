package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.repository.TicketRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TicketStore {

    private final TicketRepository ticketRepository;

    public TicketStore(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public Page<Ticket> findAll(Pageable pageable) {
        return ticketRepository.findAll(pageable);
    }

    public Ticket save(Ticket ticket) {
        return ticketRepository.save(ticket);
    }

    public void delete(Ticket ticket) {
        ticketRepository.delete(ticket);
    }

    public Page<Ticket> findWithoutDefinedPriceForGivenPriceList(Long priceListId, Pageable pageable) {
        return ticketRepository.findWithoutDefinedPriceForGivenPriceList(priceListId, pageable);
    }

    public Page<Ticket> findWithDefinedPriceForGivenPriceList(Long priceListId, Pageable pageable) {
        return ticketRepository.findWithDefinedPriceForGivenPriceList(priceListId, pageable);
    }
}
