package com.tim2.travelservice.store;

import com.tim2.travelservice.entity.BoughtTicket;
import com.tim2.travelservice.repository.BoughtTicketRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class BoughtTicketStore {

    private final BoughtTicketRepository boughtTicketRepository;

    public BoughtTicketStore(BoughtTicketRepository boughtTicketRepository) {
        this.boughtTicketRepository = boughtTicketRepository;
    }

    public Page<BoughtTicket> findByAccountEmail(String email, Pageable pageable) {
        return boughtTicketRepository.findByAccountEmail(email, pageable);
    }

    public void save(BoughtTicket boughtTicket) {
        boughtTicketRepository.save(boughtTicket);
    }
}
