package com.tim2.travelservice.exception;

import com.tim2.travelservice.common.api.ReturnCode;

public class MissingRequestDataException extends BadRequestWithExplanationException {

    public MissingRequestDataException(String explanation) {
        super(ReturnCode.MISSING_REQUEST_DATA, explanation);
    }
}
