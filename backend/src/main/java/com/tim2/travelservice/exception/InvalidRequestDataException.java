package com.tim2.travelservice.exception;

import com.tim2.travelservice.common.api.ReturnCode;

public class InvalidRequestDataException extends BadRequestWithExplanationException {

    public InvalidRequestDataException(String explanation) {
        super(ReturnCode.INVALID_REQUEST_DATA, explanation);
    }
}
