package com.tim2.travelservice.exception.handler;

import com.tim2.travelservice.common.api.ReturnCode;
import com.tim2.travelservice.exception.ApiError;
import com.tim2.travelservice.exception.BadRequestWithExplanationException;
import com.tim2.travelservice.exception.NotFoundWithExplanationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;

import java.sql.SQLException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SpecificExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(SpecificExceptionHandler.class);

    @ExceptionHandler(BadRequestWithExplanationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleBadRequestWithExplanation(BadRequestWithExplanationException e) {
        return ApiError.fromBadRequestWithExplanation(e);
    }

    @ExceptionHandler(SQLException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleSQLException(SQLException e) {
        LOG.error("{}", e);

        return ApiError.fromIntegrityViolationException();
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        LOG.error("{}", e);

        return ApiError.fromIntegrityViolationException();
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        LOG.error("{}", e);

        return ApiError
                .builder()
                .code(ReturnCode.METHOD_NOT_SUPPORTED.getCode())
                .message(ReturnCode.METHOD_NOT_SUPPORTED.getMessage())
                .build();
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleInvalidJson(HttpMessageNotReadableException e) {
        LOG.error("{}", e);

        return ApiError
                .builder()
                .code(ReturnCode.INVALID_JSON.getCode())
                .message(ReturnCode.INVALID_JSON.getMessage())
                .build();
    }

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException e) {
        LOG.error("{}", e);

        return ApiError
                .builder()
                .code(ReturnCode.INVALID_MEDIA_TYPE.getCode())
                .message(ReturnCode.INVALID_MEDIA_TYPE.getMessage())
                .build();
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleMediaTypeNotSupported(HttpMediaTypeNotSupportedException e) {
        LOG.error("{}", e);

        return ApiError
                .builder()
                .code(ReturnCode.INVALID_MEDIA_TYPE.getCode())
                .message(ReturnCode.INVALID_MEDIA_TYPE.getMessage())
                .build();
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        LOG.error("{}", e);

        return ApiError
                .builder()
                .code(ReturnCode.INVALID_METHOD_ARGUMENT.getCode())
                .message(ReturnCode.INVALID_METHOD_ARGUMENT.getMessage())
                .build();
    }

    @ExceptionHandler(NotFoundWithExplanationException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ApiError handleNotFoundWithExplanationException(NotFoundWithExplanationException e) {
        return ApiError.fromNotFoundWithExplanationException(e);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ApiError handleAccessDeniedException(AccessDeniedException e) {
        LOG.info("Unauthorized request.");
        return ApiError.fromAccessDeniedException();
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
        LOG.error("{}", e);

        return ApiError
                .builder()
                .code(ReturnCode.MISSING_REQUEST_PARAM.getCode())
                .message(ReturnCode.MISSING_REQUEST_PARAM.getMessage())
                .build();
    }

    @ExceptionHandler(MultipartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleMultipartException(MultipartException e) {
        LOG.error("{}", e);

        return ApiError
                .builder()
                .code(ReturnCode.MULTIPART_REQUEST_REQUIRED.getCode())
                .message(ReturnCode.MULTIPART_REQUEST_REQUIRED.getMessage())
                .build();
    }
}
