package com.tim2.travelservice.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.ReturnCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiError {

    @JsonProperty(RestApiConstants.CODE)
    private Integer code;

    @JsonProperty(RestApiConstants.MESSAGE)
    private String message;

    @JsonProperty(RestApiConstants.DATA)
    private Map<String, String> data = new HashMap<>();

    public static ApiError fromBadRequestWithExplanation(BadRequestWithExplanationException e) {
        ApiError apiError = new ApiError();
        apiError.setCode(e.getReturnCode().getCode());
        apiError.setMessage(e.getReturnCode().getMessage());
        apiError.getData().putIfAbsent(RestApiConstants.EXPLANATION, e.getExplanation());
        return apiError;
    }

    public static ApiError fromNotFoundWithExplanationException(NotFoundWithExplanationException e) {
        ApiError apiError = new ApiError();
        apiError.getData().putIfAbsent(RestApiConstants.EXPLANATION, e.getExplanation());
        return apiError;
    }

    public static ApiError fromAccessDeniedException() {
        return ApiError.builder()
                .code(ReturnCode.INSUFFICIENT_PRIVILEGES.getCode())
                .message(ReturnCode.INSUFFICIENT_PRIVILEGES.getMessage())
                .build();
    }

    public static ApiError fromIntegrityViolationException() {
        return ApiError.builder()
                .code(ReturnCode.INTEGRITY_VIOLATION_EXCEPTION.getCode())
                .message(ReturnCode.INTEGRITY_VIOLATION_EXCEPTION.getMessage())
                .build();
    }
}
