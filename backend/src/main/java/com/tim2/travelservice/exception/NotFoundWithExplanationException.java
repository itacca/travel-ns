package com.tim2.travelservice.exception;

public class NotFoundWithExplanationException extends RuntimeException {

    private final String explanation;

    public NotFoundWithExplanationException(String explanation) {
        this.explanation = explanation;
    }

    public String getExplanation() {
        return explanation;
    }
}
