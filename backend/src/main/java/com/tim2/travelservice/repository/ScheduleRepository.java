package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    Optional<Schedule> findByCityLineIdAndActiveAndScheduleType(Long cityLineId, boolean active, ScheduleType scheduleType);

    List<Schedule> findByCityLineId(Long lineId);
}
