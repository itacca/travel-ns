package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.entity.PriceListItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PriceListItemRepository extends JpaRepository<PriceListItem, Long> {

    Optional<PriceListItem> findByPriceListAndTicketIdAndActive(PriceList priceList, Long ticketId, Boolean active);

    Page<PriceListItem> findByPriceListIdAndActive(Long priceListId, Boolean active, Pageable pageable);

    Page<PriceListItem> findByPriceListIdAndTicketId(Long priceListId, Long ticketId, Pageable pageable);
}
