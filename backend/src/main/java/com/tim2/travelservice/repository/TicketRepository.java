package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    @Query("SELECT t FROM Ticket t WHERE NOT EXISTS " +
            "(SELECT pli FROM PriceListItem pli WHERE pli.ticket.id = t.id AND pli.active = TRUE AND pli.priceList.id = :priceListId)")
    Page<Ticket> findWithoutDefinedPriceForGivenPriceList(@Param("priceListId") Long priceListId,
                                                          Pageable pageable);

    @Query("SELECT t FROM Ticket t WHERE EXISTS " +
            "(SELECT pli FROM PriceListItem pli WHERE pli.ticket.id = t.id AND pli.priceList.id = :priceListId)")
    Page<Ticket> findWithDefinedPriceForGivenPriceList(@Param("priceListId") Long priceListId,
                                                       Pageable pageable);
}
