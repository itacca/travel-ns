package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.BoughtTicket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoughtTicketRepository extends JpaRepository<BoughtTicket, Long> {

    Page<BoughtTicket> findByAccountEmail(String email, Pageable pageable);
}
