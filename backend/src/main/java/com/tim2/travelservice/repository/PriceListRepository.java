package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.PriceList;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PriceListRepository extends JpaRepository<PriceList, Long> {

    @Override
    Optional<PriceList> findById(Long id);

    @Query("SELECT p FROM PriceList p WHERE " +
            "(:startDate <= p.endDate) AND (p.startDate >= :endDate)")
    List<PriceList> findPriceListsWithOverlappingIntervalsWithGivenInterval(@Param("startDate") DateTime startDate,
                                                                            @Param("endDate") DateTime endDate);
}
