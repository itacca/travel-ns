package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    List<Document> findByAccountAndActive(Account account, Boolean active);

    Page<Document> findByActiveAndVerified(Boolean active, Boolean verified, Pageable pageable);
}
