package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.CityLineType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityLineRepository extends JpaRepository<CityLine, Long> {

    Page<CityLine> findByCityLineType(Pageable pageable, CityLineType cityLineType);
}
