package com.tim2.travelservice.repository;

import com.tim2.travelservice.entity.LineStation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LineStationRepository extends JpaRepository<LineStation, Long> {

    List<LineStation> findByCityLineId(Long id);
}
