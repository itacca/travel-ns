package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.repository.StationRepository;
import com.tim2.travelservice.web.dto.StationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StationDbValidator {

    @Autowired
    private StationRepository stationRepository;

    public Station validateFindByIdRequest(Long id) {
        return validateStationExists(id);
    }

    private Station validateStationExists(Long id) {
        Optional<Station> station = stationRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!station.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID));

        return station.get();
    }

    public Station validateUpdateStationRequest(StationDto stationDto) {
        return validateStationExists(stationDto.getId());
    }

    public Station validateDeleteByIdRequest(Long stationId) {
        return validateStationExists(stationId);
    }
}
