package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.web.dto.ScheduleItemDto;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ScheduleItemDtoValidator {

    public void validateAddScheduleItemRequest(ScheduleItemDto scheduleItemDto) {
        BadRequestUtils.throwInvalidRequestDataIf(scheduleItemDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(scheduleItemDto.getVehicle() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.VEHICLE));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(scheduleItemDto.getDepartureTime()), RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DEPARTURE_TIME));
        BadRequestUtils.throwInvalidRequestDataIf(scheduleItemDto.getSchedule() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.SCHEDULE));
        BadRequestUtils.throwInvalidRequestDataIf(scheduleItemDto.getSchedule().getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    public void validateAddScheduleItemsRequest(List<ScheduleItemDto> scheduleItemDtos) {
        if (scheduleItemDtos.isEmpty()) {
            return;
        }
        scheduleItemDtos.forEach(this::validateAddScheduleItemRequest);
    }
}
