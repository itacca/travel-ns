package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LineStationDtoValidator {

    public void validateCreateAllRequest(List<LineStationDto> lineStationDtos) {
        Set<Long> cityLineIds = new HashSet<>();
        lineStationDtos.forEach(lineStationDto -> {
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getRelativePosition() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getCityLine() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getCityLine().getId() == null, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getStation() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getStation().getId() == null, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION));
            cityLineIds.add(lineStationDto.getCityLine().getId());
        });
        BadRequestUtils.throwInvalidRequestDataIf(cityLineIds.size() != 1 && CollectionUtils.isNotEmpty(lineStationDtos), RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID);
    }

    public void validateUpdateAllRequest(List<LineStationDto> lineStationDtos) {
        Set<Long> cityLineIds = new HashSet<>();
        lineStationDtos.forEach(lineStationDto -> {
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getRelativePosition() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.RELATIVE_POSITION));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getCityLine() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getCityLine().getId() == null, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getStation() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.STATION));
            BadRequestUtils.throwInvalidRequestDataIf(lineStationDto.getStation().getId() == null, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.STATION));
            cityLineIds.add(lineStationDto.getCityLine().getId());
        });
        BadRequestUtils.throwInvalidRequestDataIf(cityLineIds.size() != 1 && CollectionUtils.isNotEmpty(lineStationDtos), RestApiErrors.LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID);
    }
}
