package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.repository.CityLineRepository;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CityLineDbValidator {

    private final CityLineRepository cityLineRepository;

    public CityLineDbValidator(CityLineRepository cityLineRepository) {
        this.cityLineRepository = cityLineRepository;
    }

    public CityLine validateUpdateRequest(CityLineDto cityLineDto) {
        return validateCityLineExists(cityLineDto.getId());
    }

    private CityLine validateCityLineExists(Long id) {
        Optional<CityLine> cityLine = cityLineRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!cityLine.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));

        return cityLine.get();
    }

    public CityLine validateFindByIdRequest(Long id) {
        return validateCityLineExists(id);
    }

    public CityLine validateDeleteByIdRequest(Long id) {
        return validateCityLineExists(id);
    }

    public void validateFindAllSchedulesRequest(Long lineId) {
        validateCityLineExists(lineId);
    }
}
