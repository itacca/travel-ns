package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.repository.TicketRepository;
import com.tim2.travelservice.web.dto.TicketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TicketDbValidator {

    @Autowired
    private TicketRepository ticketRepository;

    public Ticket validateFindByIdRequest(Long id) {
        return validateTicketExists(id);
    }

    private Ticket validateTicketExists(Long id) {
        Optional<Ticket> ticket = ticketRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!ticket.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID));

        return ticket.get();
    }

    public Ticket validateUpdateTicketRequest(TicketDto ticketDto) {
        return validateTicketExists(ticketDto.getId());
    }

    public Ticket validateDeleteTicketRequest(Long id) {
        return validateTicketExists(id);
    }


}
