package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.entity.PriceListItem;
import com.tim2.travelservice.entity.Ticket;
import com.tim2.travelservice.repository.PriceListItemRepository;
import com.tim2.travelservice.repository.PriceListRepository;
import com.tim2.travelservice.repository.TicketRepository;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PriceListItemDbValidator {

    private final PriceListRepository priceListRepository;

    private final TicketRepository ticketRepository;

    private final PriceListItemRepository priceListItemRepository;

    public PriceListItemDbValidator(PriceListRepository priceListRepository,
                                    TicketRepository ticketRepository,
                                    PriceListItemRepository priceListItemRepository) {
        this.priceListRepository = priceListRepository;
        this.ticketRepository = ticketRepository;
        this.priceListItemRepository = priceListItemRepository;
    }

    public void validateCreateRequest(PriceListItemDto priceListItemDto) {
        PriceList priceList = validatePriceListExists(priceListItemDto.getPriceList().getId());
        validatePriceListItemForGivenTicketDoesNotExist(priceList, priceListItemDto);
        validateDateIsBeforeEndDateOfPriceList(priceListItemDto);
    }

    private void validateDateIsBeforeEndDateOfPriceList(PriceListItemDto priceListItemDto) {
        BadRequestUtils.throwInvalidRequestDataIf(dateIsNotBefore(DateTime.now(DateTimeZone.UTC), priceListItemDto.getPriceList().getEndDate()), RestApiErrors.DATE_IS_AFTER_END_DATE_OD_PRICE_LIST);
    }

    private PriceList validatePriceListExists(Long id) {
        Optional<PriceList> priceListOptional = priceListRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!priceListOptional.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID));
        return priceListOptional.get();
    }

    private Ticket validateTicketExists(Long id) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!ticketOptional.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.TICKET, RestApiConstants.ID));
        return ticketOptional.get();
    }

    public void validatePriceListItemForGivenTicketDoesNotExist(PriceList priceList, PriceListItemDto priceListItemDto) {
        Optional<PriceListItem> priceListItemOptional = priceListItemRepository.findByPriceListAndTicketIdAndActive(priceList, priceListItemDto.getTicket().getId(), true);
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemOptional.isPresent(), RestApiErrors.CREATE_PRICE_LIST_ITEM_ERROR);
    }

    public PriceListItem validateUpdatePriceListItemRequest(PriceListItemDto priceListItemDto) {
        PriceListItem priceListItem = validatePriceListItemExists(priceListItemDto.getId());
        validateDateIsBeforeEndDateOfPriceList(priceListItem);
        return priceListItem;
    }

    private PriceListItem validatePriceListItemExists(Long id) {
        Optional<PriceListItem> priceListItemOptional = priceListItemRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!priceListItemOptional.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST_ITEM, RestApiConstants.ID));
        return priceListItemOptional.get();
    }

    private void validateDateIsBeforeEndDateOfPriceList(PriceListItem priceListItem) {
        BadRequestUtils.throwInvalidRequestDataIf(dateIsNotBefore(DateTime.now(DateTimeZone.UTC), priceListItem.getPriceList().getEndDate()), RestApiErrors.DATE_IS_AFTER_END_DATE_OD_PRICE_LIST);
    }

    public PriceListItem validateDeletePriceListItemRequest(Long id) {
        return validatePriceListItemExists(id);
    }

    public void validateFindByPriceListIdRequest(Long priceListId) {
        validatePriceListExists(priceListId);
    }

    public void validateFindByPriceListAndTicketIdRequest(Long priceListId, Long ticketId) {
        validatePriceListExists(priceListId);
        validateTicketExists(ticketId);
    }

    private static boolean dateIsNotBefore(DateTime start, DateTime end) {
        return !start.isBefore(end);
    }
}
