package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.springframework.stereotype.Service;

@Service
public class PriceListItemDtoValidator {

    public void validateCreateRequest(PriceListItemDto priceListItemDto) {
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getTicket() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getTicket().getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getTicket().getTicketDuration() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getTicket().getTicketType() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getTicket().getCityLine() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getTicket().getCityLine().getCityLineType() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getTicket().getCityLine().getName() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getPrice() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getPrice() < 0, RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER);
        validatePriceListIsProvidedAndContainsId(priceListItemDto);
    }

    public void validateUpdateRequest(PriceListItemDto priceListItemDto) {
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        validatePriceListIsProvidedAndContainsId(priceListItemDto);
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getPrice() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getPrice() < 0, RestApiErrors.PRICE_SHOULD_BE_POSITIVE_NUMBER);
    }

    private void validatePriceListIsProvidedAndContainsId(PriceListItemDto priceListItemDto) {
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getPriceList() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST));
        BadRequestUtils.throwInvalidRequestDataIf(priceListItemDto.getPriceList().getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }
}