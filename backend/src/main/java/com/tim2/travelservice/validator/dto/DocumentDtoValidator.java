package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.entity.DocumentType;
import com.tim2.travelservice.web.dto.DocumentDto;
import org.springframework.stereotype.Service;

@Service
public class DocumentDtoValidator {

    public void validateUploadRequest(String imageType) {
        try {
            DocumentType.valueOf(imageType.toUpperCase());
        } catch (IllegalArgumentException e) {
            BadRequestUtils.throwInvalidRequestData(RestApiErrors.invalidRequestParamFormat(RestApiRequestParams.DOCUMENT_TYPE));
        }
    }

    public void validateVerifyRequest(DocumentDto documentDto) {
        BadRequestUtils.throwInvalidRequestDataIf(documentDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(documentDto.getVerified() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VERIFIED));
    }
}
