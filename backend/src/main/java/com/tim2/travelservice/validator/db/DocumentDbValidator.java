package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.Document;
import com.tim2.travelservice.repository.DocumentRepository;
import com.tim2.travelservice.web.dto.DocumentDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DocumentDbValidator {

    private final DocumentRepository documentRepository;

    public DocumentDbValidator(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    public Document validateFindImageBlobByDocumentId(Long documentId) {
        return validateDocumentExists(documentId);
    }

    private Document validateDocumentExists(Long documentId) {
        Optional<Document> documentOptional = documentRepository.findById(documentId);
        NotFoundUtils.throwNotFoundWithExplanationIf(!documentOptional.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.DOCUMENT, RestApiConstants.ID));

        return documentOptional.get();
    }

    public Document validateVerifyRequest(DocumentDto documentDto) {
        Document document = validateDocumentExists(documentDto.getId());
        BadRequestUtils.throwInvalidRequestDataIf(!document.getActive(), RestApiErrors.INACTIVE_DOCUMENT_VERIFICATION);

        return document;
    }
}
