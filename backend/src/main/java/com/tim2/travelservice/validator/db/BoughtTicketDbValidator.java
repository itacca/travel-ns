package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.BoughtTicket;
import com.tim2.travelservice.repository.BoughtTicketRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BoughtTicketDbValidator {

    private final BoughtTicketRepository boughtTicketRepository;

    public BoughtTicketDbValidator(BoughtTicketRepository boughtTicketRepository) {
        this.boughtTicketRepository = boughtTicketRepository;
    }

    public BoughtTicket validateValidateRequest(Long id) {
        return validateTicketExistsById(id);
    }

    private BoughtTicket validateTicketExistsById(Long id) {
        Optional<BoughtTicket> boughtTicket = boughtTicketRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!boughtTicket.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.BOUGHT_TICKET, RestApiConstants.ID));

        return boughtTicket.get();
    }

    public BoughtTicket validateFindByIdRequest(Long id) {
        return validateTicketExistsById(id);
    }
}
