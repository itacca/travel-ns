package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.entity.CityLineType;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class CityLineDtoValidator {

    public void validateGetCityLineByTypeRequest(String type) {
        try {
            CityLineType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            BadRequestUtils.throwInvalidRequestData(RestApiErrors.invalidRequestParamFormat(RestApiConstants.TYPE));
        }
    }

    public void validateCreateRequest(CityLineDto cityLineDto) {
        BadRequestUtils.throwInvalidRequestDataIf(cityLineDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(cityLineDto.getName() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(cityLineDto.getName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
        BadRequestUtils.throwInvalidRequestDataIf(!CollectionUtils.isEmpty(cityLineDto.getVehicles()), RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.VEHICLES));
        BadRequestUtils.throwInvalidRequestDataIf(!CollectionUtils.isEmpty(cityLineDto.getSchedules()), RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.SCHEDULES));
        BadRequestUtils.throwInvalidRequestDataIf(!CollectionUtils.isEmpty(cityLineDto.getLineStations()), RestApiErrors.fieldShouldBeNullOrEmptyCollection(RestApiConstants.LINE_STATIONS));
    }

    public void validateUpdateRequest(CityLineDto cityLineDto) {
        BadRequestUtils.throwInvalidRequestDataIf(cityLineDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(cityLineDto.getLineStations() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS));
        BadRequestUtils.throwInvalidRequestDataIf(cityLineDto.getVehicles() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.VEHICLES));
        BadRequestUtils.throwInvalidRequestDataIf(cityLineDto.getSchedules() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.SCHEDULES));

        if (cityLineDto.getName() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(cityLineDto.getName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
        }
    }
}
