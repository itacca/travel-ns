package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.repository.AccountRepository;
import com.tim2.travelservice.web.dto.AccountDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountDbValidator {

    private final AccountRepository accountRepository;

    public AccountDbValidator(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void validateRegistrationRequest(AccountDto accountDto) {
        Optional<Account> account = accountRepository.findByEmail(accountDto.getEmail());
        BadRequestUtils.throwInvalidRequestDataIf(account.isPresent(), RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL));
    }

    public Account validateFindByEmailRequest(String email) {
        return validateAccountExists(email);
    }

    public Account validateUpdateByEmailRequest(String email) {
        return validateAccountExists(email);
    }

    private Account validateAccountExists(String email) {
        Optional<Account> account = accountRepository.findByEmail(email);
        NotFoundUtils.throwNotFoundWithExplanationIf(!account.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.EMAIL));

        return account.get();
    }

    public Account validateDeleteTravelAdminById(Long id) {
        Optional<Account> account = accountRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!account.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.ACCOUNT, RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(account.get().getRoles().get(0).getId() != 2L, RestApiErrors.USER_IS_NOT_TRAVEL_ADMIN);

        return account.get();
    }
}
