package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.web.dto.AccountDto;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class AccountDtoValidator {

    public void validateRegistrationRequest(AccountDto accountDto) {
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getEmail() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getEmail()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL));
        BadRequestUtils.throwInvalidRequestDataIf(!EmailValidator.getInstance().isValid(accountDto.getEmail()), RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getPassword()), RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PASSWORD));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getPassword().length() < 8, RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getFirstName() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getFirstName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getLastName() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getLastName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getDateOfBirth()), RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DATE_OF_BIRTH));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getDateOfBirth().isAfterNow(), RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST);
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getValidated() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.VALIDATED));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getIdValidated() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID_VALIDATED));
        BadRequestUtils.throwInvalidRequestDataIf(CollectionUtils.isNotEmpty(accountDto.getRoles()), RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES));
    }

    public void validateTravelAdminRegistrationRequest(AccountDto accountDto) {
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getEmail() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.EMAIL));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getEmail()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.EMAIL));
        BadRequestUtils.throwInvalidRequestDataIf(!EmailValidator.getInstance().isValid(accountDto.getEmail()), RestApiErrors.invalidFieldFormat(RestApiConstants.EMAIL));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getPassword() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.PASSWORD));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getFirstName() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.FIRST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getFirstName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getLastName() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LAST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getLastName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getDateOfBirth()), RestApiErrors.fieldShouldNotBeNull(RestApiConstants.DATE_OF_BIRTH));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getDateOfBirth().isAfterNow(), RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST);
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getValidated() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.VALIDATED));
        BadRequestUtils.throwInvalidRequestDataIf(accountDto.getIdValidated() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID_VALIDATED));
        BadRequestUtils.throwInvalidRequestDataIf(CollectionUtils.isNotEmpty(accountDto.getRoles()), RestApiErrors.fieldShouldBeNull(RestApiConstants.ROLES));
    }


    public void validateUpdateRequest(AccountDto accountDto) {
        if (accountDto.getPassword() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(accountDto.getPassword().length() < 8, RestApiErrors.fieldLengthShouldBeAtLeast(RestApiConstants.PASSWORD, RestApiConstants.PASSWORD_MIN_LENGTH));
        }
        if (accountDto.getFirstName() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getFirstName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.FIRST_NAME));
        }
        if (accountDto.getLastName() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(accountDto.getLastName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.LAST_NAME));
        }
        if (accountDto.getDateOfBirth() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(accountDto.getDateOfBirth().isAfterNow(), RestApiErrors.DATE_SHOULD_BE_IN_THE_PAST);
        }
    }
}
