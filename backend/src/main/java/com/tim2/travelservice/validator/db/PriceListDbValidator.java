package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.repository.PriceListRepository;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PriceListDbValidator {

    private final PriceListRepository priceListRepository;

    public PriceListDbValidator(PriceListRepository priceListRepository) {
        this.priceListRepository = priceListRepository;
    }

    public PriceList validateUpdateRequest(PriceListDto priceListDto) {
        return validatePriceListExists(priceListDto.getId());
    }

    public PriceList validateDeleteByIdRequest(Long priceListId) {
        return validatePriceListExists(priceListId);

    }

    public PriceList validateFindByIdRequest(Long priceListId) {
        return validatePriceListExists(priceListId);
    }

    private PriceList validatePriceListExists(Long id) {
        Optional<PriceList> priceListOptional = priceListRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!priceListOptional.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.PRICE_LIST, RestApiConstants.ID));

        return priceListOptional.get();
    }
}
