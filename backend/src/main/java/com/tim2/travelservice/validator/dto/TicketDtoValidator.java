package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.web.dto.TicketDto;
import org.springframework.stereotype.Service;

@Service
public class TicketDtoValidator {

    public void validateAddTicketRequest(TicketDto ticketDto) {
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getTicketDuration() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getTicketType() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getCityLine() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getCityLine().getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_ID));
    }

    public void validateUpdateTicketRequest(TicketDto ticketDto) {
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getTicketDuration() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_DURATION));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getTicketType() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.TICKET_TYPE));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getCityLine() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
        BadRequestUtils.throwInvalidRequestDataIf(ticketDto.getCityLine().getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_ID));
    }

}
