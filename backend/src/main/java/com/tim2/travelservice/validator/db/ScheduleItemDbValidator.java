package com.tim2.travelservice.validator.db;


import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.entity.ScheduleItem;
import com.tim2.travelservice.repository.ScheduleItemRepository;
import com.tim2.travelservice.repository.ScheduleRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ScheduleItemDbValidator {

    private final ScheduleItemRepository scheduleItemRepository;

    private final ScheduleRepository scheduleRepository;

    public ScheduleItemDbValidator(ScheduleItemRepository scheduleItemRepository, ScheduleRepository scheduleRepository) {
        this.scheduleItemRepository = scheduleItemRepository;
        this.scheduleRepository = scheduleRepository;
    }

    private ScheduleItem validateScheduleItemExists(Long id) {
        Optional<ScheduleItem> scheduleItem = scheduleItemRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!scheduleItem.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.SCHEDULE_ITEM, RestApiConstants.ID));

        return scheduleItem.get();
    }

    public Schedule validateFindByScheduleIdRequest(Long scheduleId) {
        return validateScheduleExists(scheduleId);
    }

    public ScheduleItem validateDeleteByIdRequest(Long id) {
        return validateScheduleItemExists(id);
    }

    public Schedule validateCreateRequest(Long scheduleId) {
        return validateScheduleExists(scheduleId);
    }

    private Schedule validateScheduleExists(Long scheduleId) {
        Optional<Schedule> schedule = scheduleRepository.findById(scheduleId);
        NotFoundUtils.throwNotFoundWithExplanationIf(!schedule.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.SCHEDULE, RestApiConstants.ID));

        return schedule.get();
    }
}
