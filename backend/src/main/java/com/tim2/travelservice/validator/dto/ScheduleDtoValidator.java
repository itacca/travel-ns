package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.entity.ScheduleType;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class ScheduleDtoValidator {

    public void validateNewScheduleRequest(ScheduleDto scheduleDto) {
        BadRequestUtils.throwInvalidRequestDataIf(scheduleDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(scheduleDto.getScheduleType()), RestApiErrors.fieldShouldNotBeNull(RestApiConstants.SCHEDULE_TYPE));
        BadRequestUtils.throwInvalidRequestDataIf(scheduleDto.getCityLine() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
        BadRequestUtils.throwInvalidRequestDataIf(scheduleDto.getCityLine().getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    public void validateNewScheduleItemsRequest(ScheduleDto scheduleDto) {
        BadRequestUtils.throwInvalidRequestDataIf(scheduleDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
    }

    public void validateUpdateScheduleRequest(ScheduleDto scheduleDto) {
        BadRequestUtils.throwInvalidRequestDataIf(scheduleDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(scheduleDto.getScheduleType() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.SCHEDULE_TYPE));
    }

    public void validateGetScheduleByTypeRequest(String type) {
        try {
            ScheduleType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            BadRequestUtils.throwInvalidRequestData(RestApiErrors.invalidFieldFormat(RestApiConstants.SCHEDULE_TYPE));
        }
    }
}
