package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.entity.PriceList;
import com.tim2.travelservice.repository.PriceListRepository;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PriceListDtoValidator {

    private final PriceListRepository priceListRepository;

    public PriceListDtoValidator(PriceListRepository priceListRepository) {
        this.priceListRepository = priceListRepository;
    }

    public void validateUpdateRequest(PriceListDto priceListDto) {
        BadRequestUtils.throwInvalidRequestDataIf(priceListDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        if (priceListDto.getEndDate() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(!priceListDto.getEndDate().isAfterNow(), RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE);
            BadRequestUtils.throwInvalidRequestDataIf(intervalIsInvalid(priceListDto.getStartDate(), priceListDto.getEndDate()), RestApiErrors.INVALID_DATE_INTERVAL);
        }
    }

    public void validateCreateRequest(PriceListDto priceListDto) {
        BadRequestUtils.throwInvalidRequestDataIf(priceListDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(priceListDto.getStartDate() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.START_DATE));
        BadRequestUtils.throwInvalidRequestDataIf(!priceListDto.getStartDate().isAfterNow(), RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE);
        BadRequestUtils.throwInvalidRequestDataIf(priceListDto.getEndDate() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.END_DATE));
        BadRequestUtils.throwInvalidRequestDataIf(!priceListDto.getEndDate().isAfterNow(), RestApiErrors.DATE_SHOULD_BE_IN_THE_FUTURE);
        BadRequestUtils.throwInvalidRequestDataIf(intervalIsInvalid(priceListDto.getStartDate(), priceListDto.getEndDate()), RestApiErrors.INVALID_DATE_INTERVAL);
        BadRequestUtils.throwInvalidRequestDataIf(intervalsAreOverlapping(priceListDto.getStartDate(), priceListDto.getEndDate()), RestApiErrors.INTERVALS_ARE_OVERLAPPING);
        BadRequestUtils.throwInvalidRequestDataIf(priceListDto.getPriceListItems() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PRICE_LIST_ITEMS));
        BadRequestUtils.throwInvalidRequestDataIf(!CollectionUtils.isEmpty(priceListDto.getPriceListItems()), RestApiErrors.collectionShouldBeEmpty(RestApiConstants.PRICE_LIST_ITEMS));
    }

    private boolean intervalsAreOverlapping(DateTime startDate, DateTime endDate) {
        List<PriceList> priceLists = priceListRepository.findPriceListsWithOverlappingIntervalsWithGivenInterval(startDate, endDate);
        return !CollectionUtils.isEmpty(priceLists);
    }

    private static boolean intervalIsInvalid(DateTime start, DateTime end) {
        return !start.isBefore(end);
    }
}
