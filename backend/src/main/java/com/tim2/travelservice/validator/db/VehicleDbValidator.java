package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.Vehicle;
import com.tim2.travelservice.repository.CityLineRepository;
import com.tim2.travelservice.repository.VehicleRepository;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VehicleDbValidator {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private CityLineRepository cityLineRepository;

    private Vehicle validateVehicleExists(Long id) {
        Optional<Vehicle> vehicle = vehicleRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!vehicle.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.VEHICLE, RestApiConstants.ID));

        return vehicle.get();
    }

    public Vehicle validateDeleteVehicleRequest(Long id) {
        return validateVehicleExists(id);
    }

    public void validateCreateVehicleRequest(VehicleDto vehicleDto) {
        Optional<Vehicle> vehicle = vehicleRepository.findByPlateNumber(vehicleDto.getPlateNumber());
        BadRequestUtils.throwInvalidRequestDataIf(vehicle.isPresent(), RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.VEHICLE, RestApiConstants.PLATE_NUMBER));
        CityLine cityLine = validateCityLineExists(vehicleDto);
        BadRequestUtils.throwInvalidRequestDataIf(CollectionUtils.isEmpty(cityLine.getLineStations()), RestApiErrors.CITY_LINE_HAS_NO_STATIONS);
    }

    private CityLine validateCityLineExists(VehicleDto vehicleDto) {
        Optional<CityLine> cityLine = cityLineRepository.findById(vehicleDto.getCityLine().getId());
        NotFoundUtils.throwNotFoundWithExplanationIf(!cityLine.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));

        return cityLine.get();
    }

    public Vehicle validateUpdateVehicleRequest(VehicleDto vehicleDto) {
        Vehicle vehicle = validateVehicleExists(vehicleDto.getId());
        Optional<Vehicle> vehicleOptional = vehicleRepository.findByPlateNumber(vehicleDto.getPlateNumber());
        BadRequestUtils.throwInvalidRequestDataIf(vehicleOptional.isPresent(), RestApiErrors.entityWithGivenFieldAlreadyExists(RestApiConstants.VEHICLE, RestApiConstants.PLATE_NUMBER));
        return vehicle;
    }
}
