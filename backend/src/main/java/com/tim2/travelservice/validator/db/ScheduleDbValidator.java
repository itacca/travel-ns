package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.Schedule;
import com.tim2.travelservice.repository.CityLineRepository;
import com.tim2.travelservice.repository.ScheduleRepository;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ScheduleDbValidator {

    private final ScheduleRepository scheduleRepository;

    private final CityLineRepository cityLineRepository;

    public ScheduleDbValidator(ScheduleRepository scheduleRepository,
                               CityLineRepository cityLineRepository) {
        this.scheduleRepository = scheduleRepository;
        this.cityLineRepository = cityLineRepository;
    }

    public void validateNewScheduleItemRequest(Long scheduleId) {

        Schedule schedule = validateScheduleExists(scheduleId);
    }

    public Schedule validateUpdateRequest(ScheduleDto scheduleDto) {
        return validateScheduleExists(scheduleDto.getId());
    }

    private Schedule validateScheduleExists(Long scheduleId) {
        Optional<Schedule> schedule = scheduleRepository.findById(scheduleId);
        NotFoundUtils.throwNotFoundWithExplanationIf(!schedule.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.SCHEDULE, RestApiConstants.ID));

        return schedule.get();
    }

    public void validateFindByCityLineId(Long cityLineId) {
        validateCityLineExist(cityLineId);
    }

    public CityLine validateCreateRequest(Long cityLineId) {
        return validateCityLineExist(cityLineId);
    }

    private CityLine validateCityLineExist(Long lineId) {
        Optional<CityLine> cityLine = cityLineRepository.findById(lineId);
        NotFoundUtils.throwNotFoundWithExplanationIf(!cityLine.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));
        return cityLine.get();
    }
}
