package com.tim2.travelservice.validator.db;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.NotFoundUtils;
import com.tim2.travelservice.entity.CityLine;
import com.tim2.travelservice.entity.LineStation;
import com.tim2.travelservice.entity.Station;
import com.tim2.travelservice.repository.CityLineRepository;
import com.tim2.travelservice.repository.LineStationRepository;
import com.tim2.travelservice.repository.StationRepository;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LineStationDbValidator {

    private final CityLineRepository cityLineRepository;

    private final StationRepository stationRepository;

    private final LineStationRepository lineStationRepository;

    public LineStationDbValidator(CityLineRepository cityLineRepository,
                                  StationRepository stationRepository,
                                  LineStationRepository lineStationRepository) {
        this.cityLineRepository = cityLineRepository;
        this.stationRepository = stationRepository;
        this.lineStationRepository = lineStationRepository;
    }

    public void validateCreateAllRequest(List<LineStationDto> lineStationDtos) {
        lineStationDtos.forEach(lineStationDto -> {
            validateCityLineExists(lineStationDto.getCityLine().getId());
            validateStationExists(lineStationDto.getStation().getId());
        });
    }

    private CityLine validateCityLineExists(Long id) {
        Optional<CityLine> cityLine = cityLineRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!cityLine.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.CITY_LINE, RestApiConstants.ID));

        return cityLine.get();
    }

    private Station validateStationExists(Long id) {
        Optional<Station> station = stationRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!station.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.STATION, RestApiConstants.ID));

        return station.get();
    }

    public void validateUpdateAllRequest(List<LineStationDto> lineStationDtos) {
        lineStationDtos.forEach(lineStationDto -> {
            validateCityLineExists(lineStationDto.getCityLine().getId());
            validateStationExists(lineStationDto.getStation().getId());
            if (lineStationDto.getId() != null) {
                validateLineStationExists(lineStationDto.getId());
            }
        });
    }

    private LineStation validateLineStationExists(Long id) {
        Optional<LineStation> lineStation = lineStationRepository.findById(id);
        NotFoundUtils.throwNotFoundWithExplanationIf(!lineStation.isPresent(), RestApiErrors.entityWithGivenFieldDoesNotExist(RestApiConstants.LINE_STATION, RestApiConstants.ID));

        return lineStation.get();
    }

    public void validateFindByCiteLineIdRequest(Long cityLineId) {
        validateCityLineExists(cityLineId);
    }
}
