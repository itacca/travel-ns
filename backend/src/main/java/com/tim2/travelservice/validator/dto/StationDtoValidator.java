package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.web.dto.StationDto;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class StationDtoValidator {

    public void validateCreateRequest(StationDto stationDto) {
        BadRequestUtils.throwInvalidRequestDataIf(stationDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(stationDto.getName() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.NAME));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(stationDto.getName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
        BadRequestUtils.throwInvalidRequestDataIf(stationDto.getLatitude() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LATITUDE));
        BadRequestUtils.throwInvalidRequestDataIf(stationDto.getLongitude() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.LONGITUDE));
        BadRequestUtils.throwInvalidRequestDataIf(stationDto.getLineStations() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS));
    }

    public void validateUpdateRequest(StationDto stationDto) {
        BadRequestUtils.throwInvalidRequestDataIf(stationDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(stationDto.getLineStations() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.LINE_STATIONS));

        if (stationDto.getName() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(stationDto.getName()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.NAME));
        }
    }
}
