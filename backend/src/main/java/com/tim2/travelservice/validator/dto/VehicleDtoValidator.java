package com.tim2.travelservice.validator.dto;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiErrors;
import com.tim2.travelservice.common.utils.BadRequestUtils;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class VehicleDtoValidator {

    public void validateAddVehicleRequest(VehicleDto vehicleDto) {
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getId() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.ID));
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getPlateNumber() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.PLATE_NUMBER));
        BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(vehicleDto.getPlateNumber()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.PLATE_NUMBER));
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getVehicleType() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.VEHICLE_TYPE));
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getCityLine() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE));
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getCityLine().getId() == null, RestApiErrors.fieldInsideFieldShouldNotBeNull(RestApiConstants.ID, RestApiConstants.CITY_LINE));
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getCityLine().getCityLineType() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.CITY_LINE_TYPE));
        BadRequestUtils.throwInvalidRequestDataIf(!vehicleDto.getCityLine().getCityLineType().toString().equals(vehicleDto.getVehicleType().toString()), RestApiErrors.TYPES_DO_NOT_MATCH);
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getStation() != null, RestApiErrors.fieldShouldBeNull(RestApiConstants.STATION));
    }

    public void validateUpdateVehicleRequest(VehicleDto vehicleDto) {
        BadRequestUtils.throwInvalidRequestDataIf(vehicleDto.getId() == null, RestApiErrors.fieldShouldNotBeNull(RestApiConstants.ID));
        if (vehicleDto.getPlateNumber() != null) {
            BadRequestUtils.throwInvalidRequestDataIf(StringUtils.isEmpty(vehicleDto.getPlateNumber()), RestApiErrors.fieldShouldNotBeEmptyString(RestApiConstants.PLATE_NUMBER));
        }
    }
}
