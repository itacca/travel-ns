package com.tim2.travelservice.common.utils;

import com.tim2.travelservice.exception.InvalidRequestDataException;

public final class BadRequestUtils {

    private BadRequestUtils() {
    }

    public static void throwInvalidRequestDataIf(boolean condition, String explanation) {
        if (condition) {
            throwInvalidRequestData(explanation);
        }
    }

    public static void throwInvalidRequestData(String explanation) {
        throw new InvalidRequestDataException(explanation);
    }
}
