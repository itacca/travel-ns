package com.tim2.travelservice.common.utils;

import java.lang.reflect.Field;

public final class ReflectionUtil {

    private ReflectionUtil() {
    }

    public static Object getFieldValue(Field field, Object object) throws IllegalAccessException {
        field.setAccessible(true);
        Object result = field.get(object);
        field.setAccessible(false);
        return result;
    }

    public static void setFieldValue(Field field, Object object, Object fieldValue) throws IllegalAccessException {
        field.setAccessible(true);
        field.set(object, fieldValue);
        field.setAccessible(false);
    }
}
