package com.tim2.travelservice.common.api;

public final class RestApiRequestParams {

    private RestApiRequestParams() {
    }

    public static final String CITY_LINE_ID = "city_line_id";
    public static final String CITY_LINE_TYPE = "type";
    public static final String EMAIL = "email";
    public static final String ROLE_ID = "role_id";
    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String IMAGE_TYPE = "image_type";
    public static final String DOCUMENT_TYPE = "document_type";
    public static final String ACTIVE = "active";
    public static final String VERIFIED = "verified";
    public static final String PRICE_LIST_ID = "price_list_id";
    public static final String TICKET_ID = "ticket_id";
    public static final String SCHEDULE_ID = "schedule_id";
    public static final String SCHEDULE_TYPE = "schedule_type";
}
