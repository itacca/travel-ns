package com.tim2.travelservice.common.api;

import com.tim2.travelservice.common.utils.ReflectionUtil;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Collection;

public final class RestApiUtils {

    private RestApiUtils() {
    }

    private static final Logger LOG = LoggerFactory.getLogger(RestApiUtils.class);

    public static void copyNonNullProperties(Object source, Object target) {
        if (!source.getClass().equals(target.getClass())) {

            return;
        }

        Field[] sourceFields = source.getClass().getDeclaredFields();
        Field[] targetFields = target.getClass().getDeclaredFields();

        try {
            for (int i = 0; i < sourceFields.length; i++) {
                Field sourceField = sourceFields[i];
                Field targetField = targetFields[i];

                Object sourceFieldValue = ReflectionUtil.getFieldValue(sourceField, source);
                Object targetFieldValue = ReflectionUtil.getFieldValue(targetField, target);

                if (Collection.class.isAssignableFrom(sourceField.getType())) {
                    if (sourceFieldValue != null) {
                        Collection sourceFieldCollection = (Collection) sourceFieldValue;
                        Collection targetFieldCollection = (Collection) targetFieldValue;

                        targetFieldCollection.clear();
                        targetFieldCollection.addAll(sourceFieldCollection);
                    }
                } else {
                    if (sourceFieldValue != null) {
                        ReflectionUtil.setFieldValue(targetField, target, sourceFieldValue);
                    }
                }
            }
        } catch (Exception exception) {
            LOG.error("Copy non null properties", exception);
        }
    }

    public static ResponseEntity<DynamicResponse> buildCreatedEntityResponse(String endpoint, DynamicResponse body) {
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path(endpoint + "/{id}")
                .buildAndExpand(body.get(RestApiConstants.ID)).toUri();

        return ResponseEntity
                .created(location)
                .body(body);
    }
}
