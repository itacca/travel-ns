package com.tim2.travelservice.common.utils;

import com.tim2.travelservice.exception.NotFoundWithExplanationException;

public final class NotFoundUtils {

    private NotFoundUtils() {
    }

    public static void throwNotFoundWithExplanationIf(boolean condition, String explanation) {
        if (condition) {
            throw new NotFoundWithExplanationException(explanation);
        }
    }
}
