package com.tim2.travelservice.common.db;

public final class DbColumnConstants {

    private DbColumnConstants() {
    }

    public static final String ACCOUNT_ID = "id_account";
    public static final String ACTIVE = "active";
    public static final String BOUGHT_DATE = "bought_date";
    public static final String BOUGHT_TICKET_ID = "id_bought_ticket";
    public static final String CITY_LINE_ID = "id_city_line";
    public static final String CITY_LINE_STATION_ID = "id_city_line_station";
    public static final String CITY_LINE_TYPE = "city_line_type";
    public static final String DATE_ACTIVE = "date_active";
    public static final String DATE_OF_BIRTH = "date_of_birth";
    public static final String DOCUMENT_ID = "id_document";
    public static final String DEPARTURE_TIME = "departure_time";
    public static final String DURATION = "duration";
    public static final String EMAIL = "email";
    public static final String END_DATE = "end_date";
    public static final String FIRST_NAME = "first_name";
    public static final String FK_ACCOUNT_ID = "fk_id_account";
    public static final String FK_CITY_LINE_ID = "fk_id_city_line";
    public static final String FK_PRICE_LIST_ID = "fk_id_price_list";
    public static final String FK_SCHEDULE_ID = "fk_id_schedule";
    public static final String FK_STATION_ID = "fk_id_station";
    public static final String FK_TICKET_ID = "fk_id_ticket";
    public static final String FK_VEHICLE_ID = "fk_vehicle_id";
    public static final String ID_VALIDATED = "id_validated";
    public static final String IS_DEFAULT = "is_default";
    public static final String IMAGE_BLOB = "image_blob";
    public static final String IMAGE_TYPE = "image_type";
    public static final String LABEL = "label";
    public static final String LAST_NAME = "last_name";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String PASSWORD = "password";
    public static final String PLATE_NUMBER = "plate_number";
    public static final String PRICE = "price";
    public static final String PRICE_LIST_ID = "id_price_list";
    public static final String PRICE_LIST_ITEM_ID = "id_price_list_item";
    public static final String RELATIVE_POSITION = "relative_position";
    public static final String ROLE_ID = "id_role";
    public static final String SCHEDULE_ID = "id_schedule";
    public static final String SCHEDULE_ITEM_ID = "id_schedule_item";
    public static final String SCHEDULE_TYPE = "schedule_type";
    public static final String SOLD_PRICE = "sold_price";
    public static final String START_DATE = "start_date";
    public static final String STATION_ID = "id_station";
    public static final String STATION_NAME = "station_name";
    public static final String TICKET_ID = "id_ticket";
    public static final String TICKET_TYPE = "ticket_type";
    public static final String VALIDATED = "validated";
    public static final String VALID_FROM = "valid_from";
    public static final String VALID_UNTIL = "valid_until";
    public static final String VEHICLE_ID = "id_vehicle";
    public static final String VEHICLE_TYPE = "vehicle_type";
    public static final String VERIFIED = "verified";
    public static final String DOCUMENT_TYPE = "document_type";
}
