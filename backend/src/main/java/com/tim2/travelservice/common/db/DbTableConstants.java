package com.tim2.travelservice.common.db;

public final class DbTableConstants {

    private DbTableConstants() {
    }

    public static final String ACCOUNT_ROLE_TABLE = "account_role";
    public static final String ACCOUNT_TABLE = "account";
    public static final String BOUGHT_TICKET_TABLE = "bought_ticket";
    public static final String CITY_LINE_STATION_TABLE = "city_line_station";
    public static final String CITY_LINE_TABLE = "city_line";
    public static final String DOCUMENT_TABLE = "document";
    public static final String PRICE_LIST_ITEM_TABLE = "price_list_item";
    public static final String PRICE_LIST_TABLE = "price_list";
    public static final String ROLE_TABLE = "role";
    public static final String SCHEDULE_ITEM_TABLE = "schedule_item";
    public static final String SCHEDULE_TABLE = "schedule";
    public static final String STATION_TABLE = "station";
    public static final String TICKET_TABLE = "ticket";
    public static final String VEHICLE_TABLE = "vehicle";
}
