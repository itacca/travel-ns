package com.tim2.travelservice.common.api;

public final class RestApiConstants {

    private RestApiConstants() {
    }

    public static final String ACCOUNT = "account";
    public static final String ACTIVE = "active";
    public static final String BOUGHT_DATE = "boughtDate";
    public static final String BOUGHT_TICKET = "boughTicket";
    public static final String BOUGHT_TICKETS = "boughtTickets";
    public static final String CITY_LINE = "cityLine";
    public static final String CITY_LINE_TYPE = "cityLineType";
    public static final String CODE = "code";
    public static final String DATA = "data";
    public static final String DATE_OF_BIRTH = "dateOfBirth";
    public static final String DATE_ACTIVE = "dateActive";
    public static final String DEPARTURE_TIME = "departureTime";
    public static final String EMAIL = "email";
    public static final String EXPLANATION = "explanation";
    public static final String FIRST_NAME = "firstName";
    public static final String ID = "id";
    public static final String ID_VALIDATED = "idValidated";
    public static final String IS_ACTIVE = "isActive";
    public static final String LAST_NAME = "lastName";
    public static final String LATITUDE = "latitude";
    public static final String LINE_STATION = "lineStation";
    public static final String LINE_STATIONS = "lineStations";
    public static final String LONGITUDE = "longitude";
    public static final String MESSAGE = "message";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final Integer PASSWORD_MIN_LENGTH = 8;
    public static final String PLATE_NUMBER = "plateNumber";
    public static final String PRICE = "price";
    public static final String PRICE_LIST = "priceList";
    public static final String PRICE_LIST_ITEMS = "priceListItems";
    public static final String RELATIVE_POSITION = "relativePosition";
    public static final String ROLES = "roles";
    public static final String SCHEDULE = "schedule";
    public static final String SCHEDULES = "schedules";
    public static final String SCHEDULE_ITEMS = "scheduleItems";
    public static final String SCHEDULE_TYPE = "scheduleType";
    public static final String SOLD_PRICE = "soldPrice";
    public static final String STATION = "station";
    public static final String TICKET = "ticket";
    public static final String TICKET_DURATION = "ticketDuration";
    public static final String TICKET_TYPE = "ticketType";
    public static final String TYPE = "type";
    public static final String VALIDATED = "validated";
    public static final String VALID_FROM = "validFrom";
    public static final String VALID_UNTIL = "validUntil";
    public static final String VEHICLE = "vehicle";
    public static final String VEHICLES = "vehicles";
    public static final String VEHICLE_TYPE = "vehicleType";
    public static final String CITY_LINE_ID = "lineId";
    public static final String SCHEDULE_ITEM = "scheduleItem";
    public static final String IMAGE_TYPE = "imageType";
    public static final String IMAGE_BLOB = "imageBlob";
    public static final String VERIFIED = "verified";
    public static final String DOCUMENT = "document";
    public static final String DOCUMENT_TYPE = "documentType";
    public static final String PRICE_LIST_ID = "priceListId";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String PRICE_LIST_ITEM = "priceListItem";
    public static final String PRICE_LIST_ITEM_ID = "priceListItemId";
}
