package com.tim2.travelservice.common.utils;

import com.tim2.travelservice.entity.*;
import com.tim2.travelservice.web.dto.*;
import org.modelmapper.ModelMapper;

public final class JsonUtil {

    private JsonUtil() {
    }

    private static final ModelMapper modelMapper = new ModelMapper();

    public static PriceListItemDto map(PriceListItem priceListItem) {
        hideUnimportantFields(priceListItem);
        return modelMapper.map(priceListItem, PriceListItemDto.class);
    }

    public static AccountDto map(Account account) {
        hideUnimportantFields(account);
        return modelMapper.map(account, AccountDto.class);
    }

    public static PriceListDto map(PriceList priceList) {
        hideUnimportantFields(priceList);
        return modelMapper.map(priceList, PriceListDto.class);
    }

    public static BoughtTicketDto map(BoughtTicket boughtTicket) {
        hideUnimportantFields(boughtTicket);
        return modelMapper.map(boughtTicket, BoughtTicketDto.class);
    }

    public static CityLineDto map(CityLine cityLine) {
        hideUnimportantFields(cityLine);
        return modelMapper.map(cityLine, CityLineDto.class);
    }

    public static TicketDto map(Ticket ticket) {
        hideUnimportantFields(ticket);
        return modelMapper.map(ticket, TicketDto.class);
    }

    public static VehicleDto map(Vehicle vehicle) {
        hideUnimportantFields(vehicle);
        return modelMapper.map(vehicle, VehicleDto.class);
    }

    public static StationDto map(Station station) {
        hideUnimportantFields(station);
        return modelMapper.map(station, StationDto.class);
    }

    public static ScheduleDto map(Schedule schedule) {
        hideUnimportantFields(schedule);
        return modelMapper.map(schedule, ScheduleDto.class);
    }

    public static ScheduleItemDto map(ScheduleItem scheduleItem) {
        hideUnimportantFields(scheduleItem);
        return modelMapper.map(scheduleItem, ScheduleItemDto.class);
    }

    private static void hideUnimportantFields(ScheduleItem scheduleItem) {
        if (scheduleItem == null) {
            return;
        }
        hideUnimportantFields(scheduleItem.getSchedule());
        hideUnimportantFields(scheduleItem.getVehicle());
    }

    public static LineStationDto map(LineStation lineStation) {
        hideUnimportantFields(lineStation);
        return modelMapper.map(lineStation, LineStationDto.class);
    }

    public static DocumentDto map(Document document) {
        hideUnimportantFields(document);
        return modelMapper.map(document, DocumentDto.class);
    }

    private static void hideUnimportantFields(PriceListItem priceListItem) {
        if (priceListItem == null) {
            return;
        }
        hideUnimportantFields(priceListItem.getPriceList());
        hideUnimportantFields(priceListItem.getTicket());
    }

    private static void hideUnimportantFields(Ticket ticket) {
        if (ticket == null) {
            return;
        }
        ticket.setBoughtTickets(null);
        ticket.setPriceListItems(null);
        hideUnimportantFields(ticket.getCityLine());
    }

    private static void hideUnimportantFields(CityLine cityLine) {
        if (cityLine == null) {
            return;
        }
        cityLine.setLineStations(null);
        cityLine.setSchedules(null);
        cityLine.setVehicles(null);
    }

    private static void hideUnimportantFields(PriceList priceList) {
        if (priceList == null) {
            return;
        }
        priceList.setPriceListItems(null);
    }

    private static void hideUnimportantFields(BoughtTicket boughtTicket) {
        if (boughtTicket == null) {
            return;
        }
        hideUnimportantFields(boughtTicket.getTicket());
        hideUnimportantFields(boughtTicket.getAccount());
    }

    private static void hideUnimportantFields(Account account) {
        if (account == null) {
            return;
        }
        account.setBoughtTickets(null);
        account.setPassword(null);
    }

    private static void hideUnimportantFields(Vehicle vehicle) {
        if (vehicle == null) {
            return;
        }
        hideUnimportantFields(vehicle.getCityLine());
        hideUnimportantFields(vehicle.getStation());
    }

    private static void hideUnimportantFields(Station station) {
        if (station == null) {
            return;
        }
        station.setLineStations(null);
    }

    private static void hideUnimportantFields(Schedule schedule) {
        if (schedule == null) {
            return;
        }
        schedule.setScheduleItems(null);
        hideUnimportantFields(schedule.getCityLine());
        hideUnimportantFields(schedule.getStartingStation());
    }

    private static void hideUnimportantFields(LineStation lineStation) {
        if (lineStation == null) {
            return;
        }
        hideUnimportantFields(lineStation.getCityLine());
        hideUnimportantFields(lineStation.getStation());
    }

    private static void hideUnimportantFields(Document document) {
        if (document == null) {
            return;
        }
        hideUnimportantFields(document.getAccount());
    }
}