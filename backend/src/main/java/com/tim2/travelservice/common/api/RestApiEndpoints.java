package com.tim2.travelservice.common.api;

public final class RestApiEndpoints {

    private RestApiEndpoints() {
    }

    private static final String API_ROOT = "/api/v1";

    public static final String ACCOUNT = API_ROOT + "/account";
    public static final String ACCOUNTS = API_ROOT + "/accounts";

    public static final String BOUGHT_TICKET = API_ROOT + "/bought-ticket";
    public static final String BOUGHT_TICKETS = API_ROOT + "/bought-tickets";

    public static final String CITY_LINE = API_ROOT + "/city-line";
    public static final String CITY_LINES = API_ROOT + "/city-lines";

    public static final String DOCUMENT = API_ROOT + "/document";
    public static final String DOCUMENTS = API_ROOT + "/documents";

    public static final String LINE_STATION = API_ROOT + "/line-station";
    public static final String LINE_STATIONS = API_ROOT + "/line-stations";

    public static final String PRICE_LIST = API_ROOT + "/price-list";
    public static final String PRICE_LISTS = API_ROOT + "/price-lists";

    public static final String PRICE_LIST_ITEM = API_ROOT + "/price-list-item";
    public static final String PRICE_LIST_ITEMS = API_ROOT + "/price-list-items";

    public static final String SCHEDULE = API_ROOT + "/schedule";
    public static final String SCHEDULES = API_ROOT + "/schedules";

    public static final String SCHEDULE_ITEM = API_ROOT + "/schedule-item";
    public static final String SCHEDULE_ITEMS = API_ROOT + "/schedule-items";

    public static final String STATION = API_ROOT + "/station";
    public static final String STATIONS = API_ROOT + "/stations";

    public static final String TEST_DATA = API_ROOT + "/test-data";

    public static final String TICKET = API_ROOT + "/ticket";
    public static final String TICKETS = API_ROOT + "/tickets";

    public static final String VEHICLE = API_ROOT + "/vehicle";
    public static final String VEHICLES = API_ROOT + "/vehicles";
}
