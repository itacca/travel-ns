package com.tim2.travelservice.common.api;

public final class RestApiErrors {

    private RestApiErrors() {
    }

    public static final String CREATE_PRICE_LIST_ITEM_ERROR = "Price list item for given ticket already exists.";
    public static final String DATE_IS_AFTER_END_DATE_OD_PRICE_LIST = "Date is after end date of price list.";
    public static final String INVALID_DATE_INTERVAL = "Date interval is invalid.";
    public static final String INTERVALS_ARE_OVERLAPPING = "Date interval is overlapping with another.";
    public static final String DATE_SHOULD_BE_IN_THE_PAST = "Date should be in the past.";
    public static final String USER_IS_NOT_TRAVEL_ADMIN = "User is not travel admin.";
    public static final String DATE_SHOULD_BE_IN_THE_FUTURE = "Date should be in the future.";
    public static final String PRICE_SHOULD_BE_POSITIVE_NUMBER = "Price should be positive number.";
    public static final String LINE_STATIONS_MUST_HAVE_SAME_CITY_LINE_ID = "Line stations should have same " + RestApiConstants.ID + " for " + RestApiConstants.CITY_LINE;
    public static final String INACTIVE_DOCUMENT_VERIFICATION = "Inactive document can not be verified.";
    public static final String TYPES_DO_NOT_MATCH = "Vehicle type and city line type do not match.";
    public static final String CITY_LINE_HAS_NO_STATIONS = "City line has no stations.";

    public static String fieldShouldBeNull(String fieldName) {
        return String.format("Field `%s` should be null.", fieldName);
    }

    public static String fieldShouldNotBeNull(String fieldName) {
        return String.format("Field `%s` should not be null.", fieldName);
    }

    public static String fieldShouldNotBeEmptyString(String fieldName) {
        return String.format("Field `%s` should not be empty string.", fieldName);
    }

    public static String fieldLengthShouldBeAtLeast(String fieldName, int length) {
        return String.format("Field `%s` should be at least %d characters long.", fieldName, length);
    }

    public static String invalidFieldFormat(String fieldName) {
        return String.format("Invalid format provided for field `%s`.", fieldName);
    }

    public static String entityWithGivenFieldAlreadyExists(String entityName, String fieldName) {
        return String.format("%s with given `%s` already exists.", entityName, fieldName);
    }

    public static String entityWithGivenFieldDoesNotExist(String entityName, String fieldName) {
        return String.format("%s with given `%s` does not exist.", entityName, fieldName);
    }

    public static String collectionShouldBeEmpty(String itemsName) {
        return String.format("Collection of %s should be empty.", itemsName);
    }

    public static String fieldShouldBeNullOrEmptyCollection(String fieldName) {
        return String.format("Field `%s` should be null or empty collection.", fieldName);
    }

    public static String fieldInsideFieldShouldNotBeNull(String fieldName, String insideFieldName) {
        return String.format("Field `%s` inside field `%s` should not be null.", fieldName, insideFieldName);
    }

    public static String invalidRequestParamFormat(String requestParam) {
        return String.format("Invalid format provided for request param `%s`.", requestParam);
    }
}
