package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.SCHEDULE_TABLE)
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.SCHEDULE_ID)
    private Long id;

    @Column(name = DbColumnConstants.ACTIVE)
    private Boolean active;

    @Column(name = DbColumnConstants.VALID_FROM)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")
    })
    private DateTime validFrom;

    @Column(name = DbColumnConstants.VALID_UNTIL)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")
    })
    private DateTime validUntil;

    @Column(name = DbColumnConstants.SCHEDULE_TYPE)
    @Enumerated(EnumType.STRING)
    private ScheduleType scheduleType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.FK_CITY_LINE_ID)
    private CityLine cityLine;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "schedule")
    private List<ScheduleItem> scheduleItems;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.STATION_ID)
    private Station startingStation;
}
