package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.STATION_TABLE)
public class Station {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.STATION_ID)
    private Long id;

    @Column(name = DbColumnConstants.STATION_NAME)
    private String name;

    @Column(name = DbColumnConstants.LONGITUDE)
    private Double longitude;

    @Column(name = DbColumnConstants.LATITUDE)
    private Double latitude;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "station")
    private List<LineStation> lineStations;
}
