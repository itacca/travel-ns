package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.TICKET_TABLE)
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.TICKET_ID)
    private Long id;

    @Column(name = DbColumnConstants.DURATION)
    @Enumerated(EnumType.STRING)
    private TicketDuration ticketDuration;

    @Column(name = DbColumnConstants.TICKET_TYPE)
    @Enumerated(EnumType.STRING)
    private TicketType ticketType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.FK_CITY_LINE_ID)
    private CityLine cityLine;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ticket")
    private List<BoughtTicket> boughtTickets;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ticket")
    private List<PriceListItem> priceListItems;
}

