package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.CITY_LINE_TABLE)
public class CityLine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.CITY_LINE_ID)
    private Long id;

    @Column
    private String name;

    @Column(name = DbColumnConstants.CITY_LINE_TYPE)
    @Enumerated(EnumType.STRING)
    private CityLineType cityLineType;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cityLine")
    private List<Vehicle> vehicles;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cityLine")
    private List<Schedule> schedules;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cityLine")
    private List<LineStation> lineStations;
}

