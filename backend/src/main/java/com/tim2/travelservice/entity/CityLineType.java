package com.tim2.travelservice.entity;

public enum CityLineType {

    BUS,
    TRAM
}
