package com.tim2.travelservice.entity;

public enum TicketDuration {

    SINGLE_RIDE,
    DAILY,
    MONTHLY,
    ANNUAL
}
