package com.tim2.travelservice.entity;

public enum VehicleType {

    BUS,
    TRAM
}
