package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.PRICE_LIST_ITEM_TABLE)
public class PriceListItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.PRICE_LIST_ITEM_ID)
    private Long id;

    @Column(name = DbColumnConstants.PRICE)
    private Double price;

    @Column(name = DbColumnConstants.ACTIVE)
    private Boolean active;

    @Column(name = DbColumnConstants.DATE_ACTIVE)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")
    })
    private DateTime dateActive;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.FK_PRICE_LIST_ID)
    private PriceList priceList;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.TICKET_ID)
    private Ticket ticket;
}