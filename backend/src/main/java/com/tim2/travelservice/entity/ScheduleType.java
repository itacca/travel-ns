package com.tim2.travelservice.entity;

public enum ScheduleType {

    WORK_DAY,
    WEEKEND
}
