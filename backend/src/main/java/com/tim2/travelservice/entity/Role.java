package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = DbTableConstants.ROLE_TABLE)
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.ROLE_ID)
    private Long id;

    @Size(min = 8, max = 255, message = "Account role have to be greater than 8 characters")
    @Column(name = DbColumnConstants.LABEL)
    private String label;

    @Column(name = DbColumnConstants.IS_DEFAULT)
    private boolean isDefault;

    public Role(Long id) {
        this.id = id;
    }
}
