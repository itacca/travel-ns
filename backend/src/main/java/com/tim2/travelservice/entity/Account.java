package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.ACCOUNT_TABLE)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.ACCOUNT_ID)
    private Long id;

    @Column(unique = true, name = DbColumnConstants.EMAIL)
    private String email;

    @Column(name = DbColumnConstants.PASSWORD)
    private String password;

    @Column(name = DbColumnConstants.FIRST_NAME)
    private String firstName;

    @Column(name = DbColumnConstants.LAST_NAME)
    private String lastName;

    @Column(name = DbColumnConstants.DATE_OF_BIRTH)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime", parameters = {
            @org.hibernate.annotations.Parameter(name = "databaseZone", value = "UTC"),
            @org.hibernate.annotations.Parameter(name = "javaZone", value = "UTC")
    })
    private DateTime dateOfBirth;

    @Column(name = DbColumnConstants.VALIDATED)
    private Boolean validated = true;

    @Column(name = DbColumnConstants.ID_VALIDATED)
    private Boolean idValidated = false;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = DbTableConstants.ACCOUNT_ROLE_TABLE, joinColumns = @JoinColumn(name = DbColumnConstants.ACCOUNT_ID),
            inverseJoinColumns = @JoinColumn(name = DbColumnConstants.ROLE_ID))
    private List<Role> roles = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<BoughtTicket> boughtTickets;
}
