package com.tim2.travelservice.entity;

public enum TicketType {

    STUDENT,
    REGULAR,
    SENIOR
}
