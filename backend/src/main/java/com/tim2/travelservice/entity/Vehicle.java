package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.VEHICLE_TABLE)
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.VEHICLE_ID)
    private Long id;

    @Column(unique = true, name = DbColumnConstants.PLATE_NUMBER)
    private String plateNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = DbColumnConstants.VEHICLE_TYPE)
    private VehicleType vehicleType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.FK_CITY_LINE_ID)
    private CityLine cityLine;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "vehicle")
    private List<ScheduleItem> scheduleItems;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.FK_STATION_ID)
    private Station station;
}
