package com.tim2.travelservice.entity;

import com.tim2.travelservice.common.db.DbColumnConstants;
import com.tim2.travelservice.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = DbTableConstants.DOCUMENT_TABLE)
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = DbColumnConstants.DOCUMENT_ID)
    private Long id;

    @Column(name = DbColumnConstants.IMAGE_TYPE)
    private String imageType;

    @Lob
    @Column(name = DbColumnConstants.IMAGE_BLOB)
    private byte[] imageBlob;

    @Column(name = DbColumnConstants.ACTIVE)
    private Boolean active;

    @Column(name = DbColumnConstants.VERIFIED)
    private Boolean verified;

    @Column(name = DbColumnConstants.DOCUMENT_TYPE)
    @Enumerated(EnumType.STRING)
    private DocumentType documentType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = DbColumnConstants.FK_ACCOUNT_ID)
    private Account account;
}
