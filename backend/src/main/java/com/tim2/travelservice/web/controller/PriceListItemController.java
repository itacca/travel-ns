package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.PriceListItemService;
import com.tim2.travelservice.validator.dto.PriceListItemDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.PRICE_LIST_ITEM)
public class PriceListItemController {

    private final PriceListItemService priceListItemService;

    private final PriceListItemDtoValidator priceListItemDtoValidator;

    public PriceListItemController(PriceListItemService priceListItemService,
                                   PriceListItemDtoValidator priceListItemDtoValidator) {
        this.priceListItemService = priceListItemService;
        this.priceListItemDtoValidator = priceListItemDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> update(@RequestBody PriceListItemDto priceListItemDto) {
        priceListItemDtoValidator.validateUpdateRequest(priceListItemDto);
        DynamicResponse body = priceListItemService.update(priceListItemDto);

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.PRICE_LIST_ITEM, body);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        priceListItemService.deleteById(id);

        return ResponseEntity
                .noContent()
                .build();
    }
}
