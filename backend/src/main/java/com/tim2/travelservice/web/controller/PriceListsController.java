package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.PriceListService;
import com.tim2.travelservice.validator.dto.PriceListDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.PRICE_LISTS)
public class PriceListsController {

    private final PriceListService priceListService;

    private final PriceListDtoValidator priceListDtoValidator;

    public PriceListsController(PriceListService priceListService,
                                PriceListDtoValidator priceListDtoValidator) {
        this.priceListService = priceListService;
        this.priceListDtoValidator = priceListDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> create(@RequestBody PriceListDto priceListDto) {
        priceListDtoValidator.validateCreateRequest(priceListDto);
        DynamicResponse body = priceListService.create(priceListDto);

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.PRICE_LIST, body);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<PriceListDto>> findAll(Pageable pageable) {
        Page<PriceListDto> priceLists = priceListService.findAll(pageable);

        return ResponseEntity
                .ok(priceLists);
    }
}
