package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.BoughtTicketService;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.BOUGHT_TICKETS)
public class BoughtTicketsController {

    private final BoughtTicketService boughtTicketService;

    public BoughtTicketsController(BoughtTicketService boughtTicketService) {
        this.boughtTicketService = boughtTicketService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<BoughtTicketDto>> findAllForPrincipal(Principal principal, Pageable pageable) {
        Page<BoughtTicketDto> boughtTickets = boughtTicketService.findByAccountEmail(principal.getName(), pageable);

        return ResponseEntity
                .ok(boughtTickets);
    }
}
