package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.BoughtTicketService;
import com.tim2.travelservice.web.dto.BoughtTicketDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.BOUGHT_TICKET)
public class BoughtTicketController {

    private final BoughtTicketService boughtTicketService;

    public BoughtTicketController(BoughtTicketService boughtTicketService) {
        this.boughtTicketService = boughtTicketService;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/validate/{id}")
    public ResponseEntity<DynamicResponse> validate(@PathVariable Long id) {
        DynamicResponse dynamicResponse = boughtTicketService.validate(id);

        return ResponseEntity
                .ok(dynamicResponse);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<BoughtTicketDto> findById(@PathVariable Long id) {
        BoughtTicketDto boughtTicketDto = boughtTicketService.findById(id);

        return ResponseEntity
                .ok(boughtTicketDto);
    }
}
