package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.VehicleService;
import com.tim2.travelservice.validator.dto.VehicleDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.VEHICLES)
public class VehiclesController {

    private final VehicleService vehicleService;

    private final VehicleDtoValidator vehicleDtoValidator;

    public VehiclesController(VehicleService vehicleService, VehicleDtoValidator vehicleDtoValidator) {
        this.vehicleService = vehicleService;
        this.vehicleDtoValidator = vehicleDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<VehicleDto>> findAll(Pageable pageable) {
        Page<VehicleDto> vehicles = vehicleService.findAllPageable(pageable);

        return ResponseEntity
                .ok(vehicles);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> create(@RequestBody VehicleDto vehicleDto) {
        vehicleDtoValidator.validateAddVehicleRequest(vehicleDto);
        DynamicResponse body = vehicleService.create(vehicleDto);

        return RestApiUtils.buildCreatedEntityResponse(RestApiEndpoints.STATION, body);
    }
}
