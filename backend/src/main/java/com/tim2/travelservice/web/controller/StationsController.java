package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.StationService;
import com.tim2.travelservice.validator.dto.StationDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.StationDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.STATIONS)
public class StationsController {

    private final StationService stationService;

    private final StationDtoValidator stationDtoValidator;

    public StationsController(StationService stationService,
                              StationDtoValidator stationDtoValidator) {
        this.stationService = stationService;
        this.stationDtoValidator = stationDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> create(@RequestBody StationDto stationDto) {
        stationDtoValidator.validateCreateRequest(stationDto);
        DynamicResponse body = stationService.create(stationDto);

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.STATION, body);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<StationDto>> findAll(Pageable pageable) {
        Page<StationDto> stations = stationService.findAll(pageable);

        return ResponseEntity
                .ok(stations);
    }
}
