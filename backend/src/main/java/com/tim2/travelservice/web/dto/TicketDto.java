package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.entity.TicketDuration;
import com.tim2.travelservice.entity.TicketType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TicketDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.TICKET_DURATION)
    private TicketDuration ticketDuration;

    @JsonProperty(RestApiConstants.TICKET_TYPE)
    private TicketType ticketType;

    @JsonProperty(RestApiConstants.CITY_LINE)
    private CityLineDto cityLine;

    @JsonProperty(RestApiConstants.BOUGHT_TICKETS)
    private List<BoughtTicketDto> boughtTickets;

    @JsonProperty(RestApiConstants.PRICE_LIST_ITEMS)
    private List<PriceListItemDto> priceListItems;
}
