package com.tim2.travelservice.web.controller;


import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.CityLineService;
import com.tim2.travelservice.validator.dto.CityLineDtoValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.CITY_LINES)
public class CityLinesController {

    private final CityLineService cityLineService;

    private final CityLineDtoValidator cityLineDtoValidator;

    public CityLinesController(CityLineService cityLineService, CityLineDtoValidator cityLineDtoValidator) {
        this.cityLineService = cityLineService;
        this.cityLineDtoValidator = cityLineDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<CityLineDto>> getCityLines(Pageable pageable) {
        Page<CityLineDto> cityLines = cityLineService.findAll(pageable);

        return ResponseEntity
                .ok(cityLines);
    }

    @GetMapping(params = {RestApiRequestParams.TYPE}, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<CityLineDto>> getCityLinesByType(Pageable pageable, @RequestParam(RestApiRequestParams.TYPE) String type) {
        cityLineDtoValidator.validateGetCityLineByTypeRequest(type);
        Page<CityLineDto> cityLines = cityLineService.findByType(pageable, type);

        return ResponseEntity
                .ok(cityLines);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> create(@RequestBody CityLineDto cityLineDto) {
        cityLineDtoValidator.validateCreateRequest(cityLineDto);
        DynamicResponse body = cityLineService.create(cityLineDto);

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.CITY_LINE, body);
    }
}
