package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.PriceListItemService;
import com.tim2.travelservice.validator.dto.PriceListItemDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.PriceListItemDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.PRICE_LIST_ITEMS)
public class PriceListItemsController {

    private final PriceListItemService priceListItemService;

    private final PriceListItemDtoValidator priceListItemDtoValidator;

    public PriceListItemsController(PriceListItemService priceListItemService,
                                    PriceListItemDtoValidator priceListItemDtoValidator) {
        this.priceListItemService = priceListItemService;
        this.priceListItemDtoValidator = priceListItemDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> create(@RequestBody PriceListItemDto priceListItemDto) {
        priceListItemDtoValidator.validateCreateRequest(priceListItemDto);
        DynamicResponse body = priceListItemService.create(priceListItemDto);

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.PRICE_LIST_ITEM, body);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.PRICE_LIST_ID})
    public ResponseEntity<Page<PriceListItemDto>> findByPriceListId(@RequestParam(RestApiRequestParams.PRICE_LIST_ID) Long priceListId,
                                                                    Pageable pageable) {
        Page<PriceListItemDto> priceListItems = priceListItemService.findByPriceListId(priceListId, pageable);

        return ResponseEntity
                .ok(priceListItems);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.PRICE_LIST_ID, RestApiRequestParams.TICKET_ID})
    public ResponseEntity<Page<PriceListItemDto>> findAllByPriceListAndTicket(@RequestParam(RestApiRequestParams.PRICE_LIST_ID) Long priceListId,
                                                                              @RequestParam(RestApiRequestParams.TICKET_ID) Long ticketId,
                                                                              Pageable pageable) {
        Page<PriceListItemDto> priceListItems = priceListItemService.findAllByPriceListAndTicket(priceListId, ticketId, pageable);

        return ResponseEntity
                .ok(priceListItems);
    }
}
