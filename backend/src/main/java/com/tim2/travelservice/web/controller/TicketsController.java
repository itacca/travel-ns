package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.TicketService;
import com.tim2.travelservice.validator.dto.TicketDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.TicketDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.TICKETS)
public class TicketsController {

    private final TicketService ticketService;

    private final TicketDtoValidator ticketDtoValidator;

    public TicketsController(TicketService ticketService, TicketDtoValidator ticketDtoValidator) {
        this.ticketService = ticketService;
        this.ticketDtoValidator = ticketDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<TicketDto>> findAll(Pageable pageable) {
        Page<TicketDto> tickets = ticketService.findAll(pageable);

        return ResponseEntity.ok(tickets);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> addTicket(@RequestBody TicketDto ticketDto) {
        ticketDtoValidator.validateAddTicketRequest(ticketDto);
        DynamicResponse body = ticketService.addTicket(ticketDto);

        return RestApiUtils.buildCreatedEntityResponse(RestApiConstants.STATION, body);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/not-in-price-list", params = {RestApiRequestParams.PRICE_LIST_ID})
    public ResponseEntity<Page<TicketDto>> findWithoutDefinedPriceForGivenPriceList(@RequestParam(RestApiRequestParams.PRICE_LIST_ID) Long priceListId,
                                                                                    Pageable pageable) {
        Page<TicketDto> tickets = ticketService.findWithoutDefinedPriceForGivenPriceList(priceListId, pageable);

        return ResponseEntity
                .ok(tickets);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/in-price-list", params = {RestApiRequestParams.PRICE_LIST_ID})
    public ResponseEntity<Page<TicketDto>> findWithDefinedPriceForGivenPriceList(@RequestParam(RestApiRequestParams.PRICE_LIST_ID) Long priceListId,
                                                                                 Pageable pageable) {
        Page<TicketDto> tickets = ticketService.findWithDefinedPriceForGivenPriceList(priceListId, pageable);

        return ResponseEntity
                .ok(tickets);
    }
}
