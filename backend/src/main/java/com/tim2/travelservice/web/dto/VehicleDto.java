package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.entity.VehicleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VehicleDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.PLATE_NUMBER)
    private String plateNumber;

    @JsonProperty(RestApiConstants.VEHICLE_TYPE)
    private VehicleType vehicleType;

    @JsonProperty(RestApiConstants.CITY_LINE)
    private CityLineDto cityLine;

    @JsonProperty(RestApiConstants.STATION)
    private StationDto station;
}


