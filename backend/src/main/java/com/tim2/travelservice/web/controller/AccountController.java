package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.service.AccountService;
import com.tim2.travelservice.validator.dto.AccountDtoValidator;
import com.tim2.travelservice.web.dto.AccountDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.ACCOUNT)
public class AccountController {

    private final AccountService accountService;

    private final AccountDtoValidator accountDtoValidator;

    public AccountController(AccountService accountService,
                             AccountDtoValidator accountDtoValidator) {
        this.accountService = accountService;
        this.accountDtoValidator = accountDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/me")
    public ResponseEntity<AccountDto> getLoggedUserInfo(Principal principal) {
        AccountDto body = accountService.findByEmail(principal.getName());

        return ResponseEntity
                .ok(body);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.EMAIL})
    public ResponseEntity<AccountDto> findByEmail(@RequestParam(RestApiRequestParams.EMAIL) String email) {
        AccountDto body = accountService.findByEmail(email);

        return ResponseEntity
                .ok(body);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity updateLoggedUser(Principal principal,
                                           @RequestBody AccountDto accountDto) {
        accountDtoValidator.validateUpdateRequest(accountDto);
        accountService.updateByEmail(principal.getName(), accountDto);

        return ResponseEntity
                .noContent()
                .build();
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR')")
    @DeleteMapping(value = "/travel-admin/{id}")
    public ResponseEntity deleteTravelAdminById(@PathVariable Long id) {
        accountService.deleteTravelAdminById(id);

        return ResponseEntity
                .noContent()
                .build();
    }
}
