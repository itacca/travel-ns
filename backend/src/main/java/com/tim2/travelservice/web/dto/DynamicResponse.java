package com.tim2.travelservice.web.dto;

import java.util.HashMap;

public class DynamicResponse extends HashMap<String, Object> {

    public DynamicResponse() {
    }

    public DynamicResponse(String key, Object value) {
        put(key, value);
    }
}
