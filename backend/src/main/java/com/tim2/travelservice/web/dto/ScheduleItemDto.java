package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tim2.travelservice.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScheduleItemDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.DEPARTURE_TIME)
    private DateTime departureTime;

    @JsonProperty(RestApiConstants.VEHICLE)
    private VehicleDto vehicle;

    @JsonProperty(RestApiConstants.SCHEDULE)
    private ScheduleDto schedule;
}
