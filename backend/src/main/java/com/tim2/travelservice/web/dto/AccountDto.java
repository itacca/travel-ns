package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonJodaDateTimeSerializer;
import com.tim2.travelservice.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.EMAIL)
    private String email;

    @JsonProperty(RestApiConstants.PASSWORD)
    private String password;

    @JsonProperty(RestApiConstants.FIRST_NAME)
    private String firstName;

    @JsonProperty(RestApiConstants.LAST_NAME)
    private String lastName;

    @JsonProperty(RestApiConstants.DATE_OF_BIRTH)
    @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
    private DateTime dateOfBirth;

    @JsonProperty(RestApiConstants.VALIDATED)
    private Boolean validated;

    @JsonProperty(RestApiConstants.ID_VALIDATED)
    private Boolean idValidated;

    @JsonProperty(RestApiConstants.ROLES)
    private List<Role> roles = new ArrayList<>();
}
