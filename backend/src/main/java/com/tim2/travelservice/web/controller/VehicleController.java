package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.VehicleService;
import com.tim2.travelservice.validator.dto.VehicleDtoValidator;
import com.tim2.travelservice.web.dto.VehicleDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.VEHICLE)
public class VehicleController {

    private final VehicleService vehicleService;

    private final VehicleDtoValidator vehicleDtoValidator;

    public VehicleController(VehicleService vehicleService,
                             VehicleDtoValidator vehicleDtoValidator) {
        this.vehicleService = vehicleService;
        this.vehicleDtoValidator = vehicleDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        vehicleService.delete(id);

        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody VehicleDto vehicleDto) {
        vehicleDtoValidator.validateUpdateVehicleRequest(vehicleDto);
        vehicleService.update(vehicleDto);

        return ResponseEntity.noContent().build();
    }

}
