package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.service.ScheduleItemService;
import com.tim2.travelservice.validator.dto.ScheduleItemDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleItemDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.SCHEDULE_ITEMS)
public class ScheduleItemsController {

    private final ScheduleItemService scheduleItemService;

    private final ScheduleItemDtoValidator scheduleItemDtoValidator;

    public ScheduleItemsController(ScheduleItemService scheduleItemService, ScheduleItemDtoValidator scheduleItemDtoValidator) {
        this.scheduleItemService = scheduleItemService;
        this.scheduleItemDtoValidator = scheduleItemDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.SCHEDULE_ID})
    public ResponseEntity<List<ScheduleItemDto>> findByScheduleId(@RequestParam(RestApiRequestParams.SCHEDULE_ID) Long scheduleId) {
        List<ScheduleItemDto> scheduleItemDtos = scheduleItemService.findByScheduleId(scheduleId);

        return ResponseEntity
                .ok(scheduleItemDtos);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DynamicResponse>> createAll(@RequestBody List<ScheduleItemDto> scheduleItemDto) {
        scheduleItemDtoValidator.validateAddScheduleItemsRequest(scheduleItemDto);
        List<DynamicResponse> body = scheduleItemService.createAll(scheduleItemDto);

        return ResponseEntity
                .created(URI.create(RestApiEndpoints.SCHEDULE_ITEMS))
                .body(body);
    }
}
