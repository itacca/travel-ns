package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.ScheduleService;
import com.tim2.travelservice.validator.dto.ScheduleDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.SCHEDULES)
public class SchedulesController {

    private final ScheduleService scheduleService;

    private final ScheduleDtoValidator scheduleDtoValidator;

    public SchedulesController(ScheduleService scheduleService, ScheduleDtoValidator scheduleDtoValidator) {
        this.scheduleService = scheduleService;
        this.scheduleDtoValidator = scheduleDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> create(@RequestBody ScheduleDto scheduleDto) {
        scheduleDtoValidator.validateNewScheduleRequest(scheduleDto);
        DynamicResponse body = scheduleService.create(scheduleDto);

        return RestApiUtils.buildCreatedEntityResponse(RestApiEndpoints.SCHEDULE, body);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.CITY_LINE_ID})
    public ResponseEntity findByCityLineId(@RequestParam(RestApiRequestParams.CITY_LINE_ID) Long lineId) {
        List<ScheduleDto> scheduleDtos = scheduleService.findByCityLineId(lineId);

        return ResponseEntity
                .ok(scheduleDtos);
    }
}
