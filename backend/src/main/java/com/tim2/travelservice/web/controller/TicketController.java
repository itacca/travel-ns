package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.TicketService;
import com.tim2.travelservice.validator.dto.TicketDtoValidator;
import com.tim2.travelservice.web.dto.TicketDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.TICKET)
public class TicketController {

    private final TicketService ticketService;

    private final TicketDtoValidator ticketDtoValidator;

    public TicketController(TicketService ticketService, TicketDtoValidator ticketDtoValidator) {
        this.ticketService = ticketService;
        this.ticketDtoValidator = ticketDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<TicketDto> findById(@PathVariable Long id) {
        TicketDto ticketDto = ticketService.findById(id);

        return ResponseEntity.ok(ticketDto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody TicketDto ticketDto) {
        ticketDtoValidator.validateUpdateTicketRequest(ticketDto);
        ticketService.updateTicket(ticketDto);

        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        ticketService.deleteTicket(id);

        return ResponseEntity.noContent().build();
    }
}
