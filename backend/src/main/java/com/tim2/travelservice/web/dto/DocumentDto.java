package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.IMAGE_TYPE)
    private String imageType;

    @JsonProperty(RestApiConstants.IMAGE_BLOB)
    private byte[] imageBlob;

    @JsonProperty(RestApiConstants.ACTIVE)
    private Boolean active;

    @JsonProperty(RestApiConstants.VERIFIED)
    private Boolean verified;

    @JsonProperty(RestApiConstants.DOCUMENT_TYPE)
    private DocumentType documentType;

    @JsonProperty(RestApiConstants.ACCOUNT)
    private Account account;
}
