package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.StationService;
import com.tim2.travelservice.validator.dto.StationDtoValidator;
import com.tim2.travelservice.web.dto.StationDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.STATION)
public class StationController {

    private final StationService stationService;

    private final StationDtoValidator stationDtoValidator;

    public StationController(StationService stationService,
                             StationDtoValidator stationDtoValidator) {
        this.stationService = stationService;
        this.stationDtoValidator = stationDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<StationDto> findById(@PathVariable Long id) {
        StationDto stationDto = stationService.findById(id);

        return ResponseEntity
                .ok(stationDto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody StationDto stationDto) {
        stationDtoValidator.validateUpdateRequest(stationDto);
        stationService.update(stationDto);

        return ResponseEntity
                .noContent().build();
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        stationService.deleteById(id);

        return ResponseEntity
                .noContent().build();
    }
}
