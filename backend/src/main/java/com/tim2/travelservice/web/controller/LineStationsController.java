package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.service.LineStationService;
import com.tim2.travelservice.validator.dto.LineStationDtoValidator;
import com.tim2.travelservice.web.dto.DynamicResponse;
import com.tim2.travelservice.web.dto.LineStationDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.LINE_STATIONS)
public class LineStationsController {

    private final LineStationService lineStationService;

    private final LineStationDtoValidator lineStationDtoValidator;

    public LineStationsController(LineStationService lineStationService,
                                  LineStationDtoValidator lineStationDtoValidator) {
        this.lineStationService = lineStationService;
        this.lineStationDtoValidator = lineStationDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DynamicResponse>> createAll(@RequestBody List<LineStationDto> lineStationDtos) {
        lineStationDtoValidator.validateCreateAllRequest(lineStationDtos);
        List<DynamicResponse> body = lineStationService.createAll(lineStationDtos);

        return ResponseEntity
                .created(URI.create(RestApiEndpoints.LINE_STATION))
                .body(body);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DynamicResponse>> updateAll(@RequestBody List<LineStationDto> lineStationDtos) {
        lineStationDtoValidator.validateUpdateAllRequest(lineStationDtos);
        lineStationService.updateAll(lineStationDtos);

        return ResponseEntity
                .noContent()
                .build();
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.CITY_LINE_ID})
    public ResponseEntity<List<LineStationDto>> findByCityLineId(@RequestParam(RestApiRequestParams.CITY_LINE_ID) Long cityLineId) {
        List<LineStationDto> boughtTickets = lineStationService.findByCityLineId(cityLineId);

        return ResponseEntity
                .ok(boughtTickets);
    }
}
