package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.CityLineService;
import com.tim2.travelservice.validator.dto.CityLineDtoValidator;
import com.tim2.travelservice.web.dto.CityLineDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.CITY_LINE)
public class CityLineController {

    private final CityLineService cityLineService;

    private final CityLineDtoValidator cityLineDtoValidator;

    public CityLineController(CityLineService cityLineService,
                              CityLineDtoValidator cityLineDtoValidator) {
        this.cityLineService = cityLineService;
        this.cityLineDtoValidator = cityLineDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody CityLineDto cityLineDto) {
        cityLineDtoValidator.validateUpdateRequest(cityLineDto);
        cityLineService.update(cityLineDto);

        return ResponseEntity
                .noContent()
                .build();
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<CityLineDto> findById(@PathVariable Long id) {
        CityLineDto cityLineDto = cityLineService.findById(id);

        return ResponseEntity
                .ok(cityLineDto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        cityLineService.deleteById(id);

        return ResponseEntity
                .noContent()
                .build();
    }
}
