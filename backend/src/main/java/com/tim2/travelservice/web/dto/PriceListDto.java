package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonJodaDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceListDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.START_DATE)
    @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
    private DateTime startDate;

    @JsonProperty(RestApiConstants.END_DATE)
    @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
    private DateTime endDate;

    @JsonProperty(RestApiConstants.PRICE_LIST_ITEMS)
    private List<PriceListItemDto> priceListItems;
}
