package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

@RestController
@RequestMapping(RestApiEndpoints.TEST_DATA)
public class TestDataController {

    @Autowired
    private DataSource dataSource;

    @PreAuthorize("hasAnyAuthority('ROLE_E2E_TEST')")
    @PutMapping
    @Transactional
    public ResponseEntity resetDatabase() {
        Resource resource = new ClassPathResource("data/mysql/e2e-data.sql");
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(resource);
        databasePopulator.execute(dataSource);

        return ResponseEntity
                .noContent()
                .build();
    }
}
