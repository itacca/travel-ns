package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tim2.travelservice.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StationDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.NAME)
    private String name;

    @JsonProperty(RestApiConstants.LONGITUDE)
    private Double longitude;

    @JsonProperty(RestApiConstants.LATITUDE)
    private Double latitude;

    @JsonProperty(RestApiConstants.LINE_STATIONS)
    private List<LineStationDto> lineStations;
}
