package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.service.ScheduleService;
import com.tim2.travelservice.validator.dto.CityLineDtoValidator;
import com.tim2.travelservice.validator.dto.ScheduleDtoValidator;
import com.tim2.travelservice.web.dto.ScheduleDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.SCHEDULE)
public class ScheduleController {

    private final ScheduleService scheduleService;

    private final ScheduleDtoValidator scheduleDtoValidator;

    private final CityLineDtoValidator cityLineDtoValidator;

    public ScheduleController(ScheduleService scheduleService, ScheduleDtoValidator scheduleDtoValidator,
                              CityLineDtoValidator cityLineDtoValidator) {
        this.scheduleService = scheduleService;
        this.scheduleDtoValidator = scheduleDtoValidator;
        this.cityLineDtoValidator = cityLineDtoValidator;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.CITY_LINE_ID, RestApiRequestParams.SCHEDULE_TYPE})
    public ResponseEntity<ScheduleDto> findByCityLineIdAndType(@RequestParam(RestApiRequestParams.CITY_LINE_ID) Long lineId,
                                                               @RequestParam(RestApiRequestParams.SCHEDULE_TYPE) String type) {
        scheduleDtoValidator.validateGetScheduleByTypeRequest(type);
        ScheduleDto scheduleDto = scheduleService.findByCityLineIdAndType(lineId, type);

        return ResponseEntity
                .ok(scheduleDto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody ScheduleDto scheduleDto) {
        scheduleDtoValidator.validateUpdateScheduleRequest(scheduleDto);
        scheduleService.update(scheduleDto);

        return ResponseEntity
                .noContent()
                .build();
    }
}
