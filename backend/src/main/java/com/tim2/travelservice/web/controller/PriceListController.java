package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.PriceListService;
import com.tim2.travelservice.validator.dto.PriceListDtoValidator;
import com.tim2.travelservice.web.dto.PriceListDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.PRICE_LIST)
public class PriceListController {

    private final PriceListService priceListService;

    private final PriceListDtoValidator priceListDtoValidator;

    public PriceListController(PriceListService priceListService,
                               PriceListDtoValidator priceListDtoValidator) {
        this.priceListService = priceListService;
        this.priceListDtoValidator = priceListDtoValidator;
    }

    @PreAuthorize("hasAnyAuthority('ROLE_ADMINISTRATOR', 'ROLE_SYSTEM_ADMINISTRATOR')")
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody PriceListDto priceListDto) {
        priceListDtoValidator.validateUpdateRequest(priceListDto);
        priceListService.updateBasicInformation(priceListDto);

        return ResponseEntity
                .noContent()
                .build();
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        priceListService.deleteById(id);

        return ResponseEntity
                .noContent()
                .build();
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<PriceListDto> findById(@PathVariable Long id) {
        PriceListDto priceListDto = priceListService.findById(id);

        return ResponseEntity
                .ok(priceListDto);
    }
}
