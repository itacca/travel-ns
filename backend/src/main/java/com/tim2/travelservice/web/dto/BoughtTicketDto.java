package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonJodaDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BoughtTicketDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.BOUGHT_DATE)
    @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
    private DateTime boughtDate;

    @JsonProperty(RestApiConstants.ACTIVE)
    private Boolean active;

    @JsonProperty(RestApiConstants.SOLD_PRICE)
    private Double soldPrice;

    @JsonProperty(RestApiConstants.ACCOUNT)
    private AccountDto account;

    @JsonProperty(RestApiConstants.TICKET)
    private TicketDto ticket;
}
