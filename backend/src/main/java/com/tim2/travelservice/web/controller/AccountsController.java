package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.AccountService;
import com.tim2.travelservice.validator.dto.AccountDtoValidator;
import com.tim2.travelservice.web.dto.AccountDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.ACCOUNTS)
public class AccountsController {

    private final AccountService accountService;

    private final AccountDtoValidator accountDtoValidator;

    public AccountsController(AccountService accountService, AccountDtoValidator accountDtoValidator) {
        this.accountService = accountService;
        this.accountDtoValidator = accountDtoValidator;
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> register(@RequestBody AccountDto accountDto) {
        accountDtoValidator.validateRegistrationRequest(accountDto);
        DynamicResponse body = accountService.register(accountDto);

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.ACCOUNT, body);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR')")
    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE, value = "/travel-admins")
    public ResponseEntity<DynamicResponse> registerTravelAdmin(@RequestBody AccountDto accountDto) {
        accountDtoValidator.validateTravelAdminRegistrationRequest(accountDto);
        DynamicResponse body = accountService.registerAdmin(accountDto);

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.ACCOUNT, body);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR')")
    @GetMapping(produces = APPLICATION_JSON_VALUE, params = {RestApiRequestParams.ROLE_ID})
    public ResponseEntity<Page<AccountDto>> findByRoleId(@RequestParam(RestApiRequestParams.ROLE_ID) Long roleId,
                                                         Pageable pageable) {
        Page<AccountDto> accountDtos = accountService.findByRoleId(roleId, pageable);

        return ResponseEntity
                .ok(accountDtos);
    }
}
