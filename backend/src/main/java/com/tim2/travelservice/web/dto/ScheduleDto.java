package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonJodaDateTimeSerializer;
import com.tim2.travelservice.entity.ScheduleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScheduleDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.ACTIVE)
    private Boolean active;

    @JsonProperty(RestApiConstants.VALID_FROM)
    @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
    private DateTime validFrom;

    @JsonProperty(RestApiConstants.VALID_UNTIL)
    @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
    private DateTime validUntil;

    @JsonProperty(RestApiConstants.SCHEDULE_TYPE)
    private ScheduleType scheduleType;

    @JsonProperty(RestApiConstants.CITY_LINE)
    private CityLineDto cityLine;

    @JsonProperty(RestApiConstants.SCHEDULE_ITEMS)
    private List<ScheduleItemDto> scheduleItems;
}
