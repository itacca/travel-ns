package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.entity.CityLineType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CityLineDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.NAME)
    private String name;

    @JsonProperty(RestApiConstants.CITY_LINE_TYPE)
    private CityLineType cityLineType;

    @JsonProperty(RestApiConstants.VEHICLES)
    private List<VehicleDto> vehicles;

    @JsonProperty(RestApiConstants.SCHEDULES)
    private List<ScheduleDto> schedules;

    @JsonProperty(RestApiConstants.LINE_STATIONS)
    private List<LineStationDto> lineStations;
}
