package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.service.DocumentService;
import com.tim2.travelservice.validator.dto.DocumentDtoValidator;
import com.tim2.travelservice.web.dto.DocumentDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.DOCUMENT)
public class DocumentController {

    private final DocumentService documentService;

    private final DocumentDtoValidator documentDtoValidator;

    public DocumentController(DocumentService documentService,
                              DocumentDtoValidator documentDtoValidator) {
        this.documentService = documentService;
        this.documentDtoValidator = documentDtoValidator;
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> findImageBlobByDocumentId(@PathVariable Long id) {
        DynamicResponse body = documentService.findImageBlobByDocumentId(id);

        return ResponseEntity
                .ok(body);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR', 'ROLE_VERIFIER')")
    @PutMapping(value = "/verify", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity verify(@RequestBody DocumentDto documentDto) {
        documentDtoValidator.validateVerifyRequest(documentDto);
        documentService.verify(documentDto);

        return ResponseEntity
                .noContent()
                .build();
    }
}
