package com.tim2.travelservice.web.controller;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import com.tim2.travelservice.common.api.RestApiRequestParams;
import com.tim2.travelservice.common.api.RestApiUtils;
import com.tim2.travelservice.service.DocumentService;
import com.tim2.travelservice.validator.dto.DocumentDtoValidator;
import com.tim2.travelservice.web.dto.DocumentDto;
import com.tim2.travelservice.web.dto.DynamicResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.DOCUMENTS)
public class DocumentsController {

    private final DocumentService documentService;

    private final DocumentDtoValidator documentDtoValidator;

    public DocumentsController(DocumentService documentService,
                               DocumentDtoValidator documentDtoValidator) {
        this.documentService = documentService;
        this.documentDtoValidator = documentDtoValidator;
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<DynamicResponse> uploadDocument(@RequestParam(RestApiRequestParams.IMAGE_TYPE) String imageType,
                                                          @RequestParam(RestApiRequestParams.DOCUMENT_TYPE) String documentType,
                                                          @RequestPart("file") MultipartFile file,
                                                          Principal principal) throws IOException {
        documentDtoValidator.validateUploadRequest(documentType);
        DynamicResponse body = documentService.saveDocument(file, imageType, documentType, principal.getName());

        return RestApiUtils
                .buildCreatedEntityResponse(RestApiEndpoints.DOCUMENTS, body);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_ADMINISTRATOR', 'ROLE_VERIFIER')")
    @GetMapping(params = {RestApiRequestParams.ACTIVE, RestApiRequestParams.VERIFIED})
    public ResponseEntity<Page<DocumentDto>> findByActiveAndVerified(@RequestParam(RestApiRequestParams.ACTIVE) Boolean active,
                                                                     @RequestParam(RestApiRequestParams.VERIFIED) Boolean verified,
                                                                     Pageable pageable) {
        Page<DocumentDto> documents = documentService.findByActiveAndVerified(active, verified, pageable);

        return ResponseEntity
                .ok(documents);
    }
}
