package com.tim2.travelservice.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tim2.travelservice.common.api.RestApiConstants;
import com.tim2.travelservice.common.utils.JsonJodaDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceListItemDto {

    @JsonProperty(RestApiConstants.ID)
    private Long id;

    @JsonProperty(RestApiConstants.PRICE)
    private Double price;

    @JsonProperty(RestApiConstants.ACTIVE)
    private Boolean active;

    @JsonProperty(RestApiConstants.DATE_ACTIVE)
    @JsonSerialize(using = JsonJodaDateTimeSerializer.class)
    private DateTime dateActive;

    @JsonProperty(RestApiConstants.PRICE_LIST)
    private PriceListDto priceList;

    @JsonProperty(RestApiConstants.TICKET)
    private TicketDto ticket;
}
