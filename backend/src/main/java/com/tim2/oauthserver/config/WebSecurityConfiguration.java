package com.tim2.oauthserver.config;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JpaUserDetailsService userDetailsService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/resources/**");
        web.ignoring().antMatchers("/webjars/**");
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.CITY_LINES);
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.CITY_LINE + "/{id}");
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.SCHEDULE);
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.SCHEDULES);
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.SCHEDULE_ITEMS);
        web.ignoring().antMatchers(HttpMethod.POST, RestApiEndpoints.ACCOUNTS);
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.STATIONS);
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.TICKET + "/{id}");
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.TICKETS);
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.TICKET + "/{id}");
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.STATION + "/{id}");
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.CITY_LINE + "/{id}");
        web.ignoring().antMatchers(HttpMethod.GET, RestApiEndpoints.LINE_STATIONS + "**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .and()
                .cors()
                .and()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
