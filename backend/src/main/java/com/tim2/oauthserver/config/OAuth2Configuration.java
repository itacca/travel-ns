package com.tim2.oauthserver.config;

import com.tim2.travelservice.common.api.RestApiEndpoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

@Configuration
@EnableAuthorizationServer
public class OAuth2Configuration extends AuthorizationServerConfigurerAdapter {

    @EnableResourceServer
    public static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

        @Autowired
        TokenStore tokenStore;

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .cors()
                    .and()
                    .headers().frameOptions().disable()
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .requestMatcher(new OAuthRequestedMatcher()).authorizeRequests()
                    .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                    .antMatchers("/login").permitAll()
                    .antMatchers("/logout").permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.CITY_LINES).permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.CITY_LINE + "/{id}").permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.SCHEDULE).permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.SCHEDULES).permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.SCHEDULE_ITEMS).permitAll()
                    .antMatchers(HttpMethod.POST, RestApiEndpoints.ACCOUNTS).permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.STATIONS).permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.TICKET + "/{id}").permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.TICKETS).permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.TICKET + "/{id}").permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.STATION + "/{id}").permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.CITY_LINE + "/{id}").permitAll()
                    .antMatchers(HttpMethod.GET, RestApiEndpoints.LINE_STATIONS + "**").permitAll()
                    .antMatchers("/").permitAll()
                    .anyRequest().authenticated();
        }

        private static class OAuthRequestedMatcher implements RequestMatcher {
            public boolean matches(HttpServletRequest request) {
                String auth = request.getHeader("Authorization");
                // Determine if the client request contained an OAuth Authorization
                boolean haveOauth2Token = (auth != null) && auth.startsWith("Bearer");
                boolean haveAccessToken = request.getParameter("access_token") != null;
                return haveOauth2Token || haveAccessToken;
            }
        }

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.tokenStore(tokenStore);
        }
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("web-client")
                .authorities("ROLE_CLIENT")
                .scopes("read", "write")
                .autoApprove(true)
                .accessTokenValiditySeconds(0)
                .authorizedGrantTypes("implicit", "authorization_code")
                .and()
                .withClient("trusted-client")
                .authorizedGrantTypes("client_credentials", "password")
                .authorities("ROLE_TRUSTED_CLIENT")
                .scopes("read", "write")
                .accessTokenValiditySeconds(0)
                .secret("secret");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .tokenStore(tokenStore()).tokenEnhancer(jwtTokenEnhancer())
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
        oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess(
                "isAuthenticated()");
    }

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    JpaUserDetailsService userDetailsService;

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtTokenEnhancer());
    }

    @Bean
    protected JwtAccessTokenConverter jwtTokenEnhancer() {
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("jwt.jks"),
                "mySecretKey".toCharArray());
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("jwt"));
        return converter;
    }
}
