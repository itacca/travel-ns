package com.tim2.oauthserver.config;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class LoginLogoutController {

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "/logout")
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpServletRequest request, HttpServletResponse response, @RequestParam("redirect_uri") String uri) throws IOException {
        Cookie cookieWithSlash = new Cookie("JSESSIONID", null);
        cookieWithSlash.setPath(request.getContextPath() + "/");
        cookieWithSlash.setMaxAge(0);

        Cookie cookieWithoutSlash = new Cookie("JSESSIONID", null);
        cookieWithoutSlash.setPath(request.getContextPath());
        cookieWithoutSlash.setMaxAge(0);

        response.addCookie(cookieWithSlash); //For Tomcat
        response.addCookie(cookieWithoutSlash); //For JBoss

        response.sendRedirect(uri);
    }
}
