package com.tim2.oauthserver.config;

import com.tim2.travelservice.entity.Account;
import com.tim2.travelservice.entity.Role;
import com.tim2.travelservice.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(JpaUserDetailsService.class);


    private final AccountRepository accountRepository;

    @Autowired
    public JpaUserDetailsService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {

        Optional<Account> accountOptional = accountRepository.findByEmail(email);

        if (!accountOptional.isPresent()) {
            throw new UsernameNotFoundException("Username not found.");
        }

        if (!accountOptional.get().getValidated()) {
            LOG.debug("Account {} tried to login without validating account first.", accountOptional.get().getEmail());

            throw new UsernameNotFoundException("Account not validated.");
        }

        List<GrantedAuthority> grantedAuthorities = accountOptional.get().getRoles().stream().map(Role::getLabel).map
                (SimpleGrantedAuthority::new).collect(Collectors.toList());

        return new User(accountOptional.get().getEmail(),
                accountOptional.get().getPassword(), true,
                true, true,
                true, grantedAuthorities);
    }
}
