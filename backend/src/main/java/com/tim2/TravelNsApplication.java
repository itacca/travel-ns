package com.tim2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelNsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravelNsApplication.class, args);
    }
}
