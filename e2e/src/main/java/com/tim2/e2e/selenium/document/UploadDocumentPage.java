package com.tim2.e2e.selenium.document;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;

public class UploadDocumentPage extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "mat-radio-button[value='STUDENT']")
    private WebElement studentRadioButton;

    @FindBy(css = "mat-radio-button[value='SENIOR']")
    private WebElement seniorRadioButton;

    @FindBy(css = "input[type='file']")
    private WebElement fileInput;

    @FindBy(css = "button.upload-document-btn")
    private WebElement uploadButton;

    public UploadDocumentPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-upload-document"));
    }

    public void setUploadPath() {
        File file = new File("src/main/resources/documents/test-document.gif");
        String absolutePath = file.getAbsolutePath();

        ensureIsDisplayed(uploadButton);
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.display = 'block';", fileInput);
        ensureIsDisplayed(fileInput);
        fileInput.sendKeys(absolutePath);
    }

    public void clickUploadButton() {
        clickElement(uploadButton);
    }

    public void clickStudentRadioButton() {
        clickElement(studentRadioButton);
    }

    public void clickSeniorRadioButton() {
        clickElement(seniorRadioButton);
    }

    public WebElement getUploadButton() {
        ensureIsDisplayed(uploadButton);
        return uploadButton;
    }
}
