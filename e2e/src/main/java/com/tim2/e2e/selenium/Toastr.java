package com.tim2.e2e.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Toastr extends BasePage {

    @FindBy(css = "div.toast-message")
    private WebElement toastMessage;

    @FindBy(css = "div.toast-title")
    private WebElement toastTitle;

    public Toastr(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return false;
    }

    public WebElement getToastMessage() {
        ensureIsDisplayed(toastMessage);
        return toastMessage;
    }

    public WebElement getToastTitle() {
        ensureIsDisplayed(toastTitle);
        return toastTitle;
    }
}
