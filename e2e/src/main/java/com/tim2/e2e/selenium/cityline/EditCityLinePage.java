package com.tim2.e2e.selenium.cityline;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class EditCityLinePage extends BasePage {

    @FindBy(css = "input[formcontrolname='nameCtrl']")
    private WebElement nameTextField;

    @FindBy(css = "mat-select[formcontrolname='cityLineTypeCtrl']")
    private WebElement cityLineTypeSelect;

    @FindBy(css = "div.mat-select-panel")
    private WebElement selectOptionsContainer;

    @FindAll(
            @FindBy(css = "mat-option")
    )
    private List<WebElement> cityLineTypeOptions;

    @FindBy(css = "button.save-btn")
    private WebElement saveButton;

    public EditCityLinePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-edit-city-line"));
    }

    public void enterCityLineName(String name) {
        enterText(nameTextField, name);
    }

    public void setCityLineTypeSelect(Integer option) {
        chooseSelectOption(cityLineTypeSelect, selectOptionsContainer, cityLineTypeOptions, option);
    }

    public void clickSaveButton() {
        clickElement(saveButton);
    }

    public WebElement getSaveButton() {
        ensureIsDisplayed(saveButton);
        return saveButton;
    }
}
