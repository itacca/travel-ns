package com.tim2.e2e.selenium.vehicle;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateVehiclePage extends BasePage {

    @FindBy(css = "mat-select[placeholder='Type']")
    private WebElement selectType;

    @FindBy(css = "mat-select[placeholder='Choose city line']")
    private WebElement selectLine;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/mat-option[1]")
    private WebElement matOptionType;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/mat-option[1]")
    private WebElement matOptionLine;

    @FindBy(xpath = "//button[@id='save']")
    private WebElement saveButton;

    @FindBy(xpath = "//button[@id='load']")
    private WebElement loadLinesButton;

    @FindBy(xpath = "//input[@id='mat-input-0']")
    private WebElement plateNumberInput;

    public CreateVehiclePage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.cssSelector("input[formcontrolname='plateNumberCtrl']"));
    }

    public void checkIfEnabledSaveButton() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(saveButton));
    }

    public void checkIfEnabledLoadButton() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(loadLinesButton));
    }

    public boolean isSaveButtonVisible() {
        return saveButton.isEnabled();
    }



    public void enterPlateNumber(String plateNumber) {
        enterText(plateNumberInput, plateNumber);
    }

    public void clickOptionType() {
        clickElement(matOptionType);
    }

    public void clickLoad() {
        clickElement(loadLinesButton);
    }

    public void clickOptionLine() {
        clickElement(matOptionLine);
    }

    public void clickSaveButton() {
        clickElement(saveButton);
    }

}
