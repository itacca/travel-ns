package com.tim2.e2e.selenium.pricelist;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddPriceListPage extends BasePage {

    @FindBy(xpath = "//input[@id='mat-input-0']")
    private WebElement startDateTextField;

    @FindBy(xpath = "//input[@id='mat-input-1']")
    private WebElement endDateTextField;

    @FindBy(xpath = "//button[@class='mat-raised-button mat-primary']")
    private WebElement addButton;

    public AddPriceListPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath("//input[@id='mat-input-0']"));
    }

    public void enterStartDate(String startDate) {
        enterText(startDateTextField, startDate);
    }

    public void enterEndDate(String endDate) {
        enterText(endDateTextField, endDate);
    }

    public WebElement getStartDateTextField() {
        ensureIsDisplayed(startDateTextField);
        return startDateTextField;
    }

    public WebElement getEndDateTextField() {
        ensureIsDisplayed(endDateTextField);
        return endDateTextField;
    }

    public void checkIfEnabledAddButtonButton() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(addButton));
    }

    public void clickAddButton() {
        clickElement(addButton);
    }
}
