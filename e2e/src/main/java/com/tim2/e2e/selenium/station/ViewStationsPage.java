package com.tim2.e2e.selenium.station;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ViewStationsPage extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "div.station-list")
    private WebElement stationsContainer;

    @FindAll(
            @FindBy(css = "div.station-wrapper")
    )
    private List<WebElement> stations;

    @FindAll(
            @FindBy(css = "a.edit-station-btn")
    )
    private List<WebElement> editStationButtons;

    @FindAll(
            @FindBy(css = "button.delete-station-btn")
    )
    private List<WebElement> deleteStationButtons;

    public ViewStationsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-stations-view"));
    }

    public WebElement getPageTitle() {
        return pageTitle;
    }

    public WebElement getStation(Integer index) {
        ensureIsDisplayed(stationsContainer);
        return stations.get(index);
    }

    public WebElement getDeleteButton(Integer index) {
        ensureIsDisplayed(stationsContainer);
        return stations.get(index);
    }

    public WebElement getEditStationButton(Integer index) {
        ensureIsDisplayed(stationsContainer);
        return stations.get(index);
    }

    public void clickDeleteButton(Integer index) {
        ensureIsDisplayed(stationsContainer);
        clickElement(deleteStationButtons.get(index));
    }

    public void clickEditButton(Integer index) {
        ensureIsDisplayed(stationsContainer);
        clickElement(editStationButtons.get(index));
    }
}
