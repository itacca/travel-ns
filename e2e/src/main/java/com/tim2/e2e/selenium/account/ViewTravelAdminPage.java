package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ViewTravelAdminPage extends BasePage {

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-admins-view[1]/div[1]/div[1]/div[1]/app-admin-list[1]/div[1]/div[1]/div[1]/app-admin-card[1]/mat-card[1]/mat-card-actions[1]/button[1]")
    private WebElement deleteButton;


    public ViewTravelAdminPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath("//h2[@class='mat-h2']"));
    }

    @FindAll(
            @FindBy(css = "button.delete-admin-btn")
    )
    private List<WebElement> deleteAdminButtons;

    public void clickDeleteButton(Integer index) {
        clickElement(deleteAdminButtons.get(index));
    }


    public boolean isDeleteButtonVisible() {
        return isVisible(By.cssSelector("button.delete-admin-btn"));
    }
}
