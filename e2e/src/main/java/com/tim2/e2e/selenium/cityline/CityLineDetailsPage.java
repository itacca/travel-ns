package com.tim2.e2e.selenium.cityline;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CityLineDetailsPage extends BasePage {


    @FindBy(xpath = "//h2[@class='mat-h2']")
    private WebElement pageTitle;

    @FindBy(xpath = "//h3[@class='mat-h3']")
    private WebElement lineName;

    public CityLineDetailsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-city-line-details"));
    }

    public void checkIfTitleIsVisible(){
        ensureIsDisplayed(pageTitle);
    }

    public void checkIfLineNameIsVisible(){
        ensureIsDisplayed(lineName);
    }




}
