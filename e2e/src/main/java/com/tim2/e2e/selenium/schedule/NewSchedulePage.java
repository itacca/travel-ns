package com.tim2.e2e.selenium.schedule;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewSchedulePage  extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(xpath = "//app-root/div/app-new-schedule/div/div/div[1]/span/select")
    private WebElement selectType;

    @FindBy(xpath = "//app-root/div/app-new-schedule/div/div/div[1]/span/select/option[1]")
    private WebElement selectWorkDay;

    @FindBy(xpath = "//*[@id=\"new-time\"]")
    private WebElement inputNewTime;

    @FindBy(xpath = "//app-new-schedule/div/div/div[2]/table/tbody/tr/td[3]/button")
    private WebElement addButton;

    @FindBy(xpath = "//app-new-schedule/div/div/div[4]/button")
    private WebElement saveButton;

    public NewSchedulePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return false;
    }

    public WebElement getPageTitle() {
        ensureIsDisplayed(pageTitle);
        return pageTitle;
    }

    public WebElement getSelectType() {
        ensureIsDisplayed(selectType);
        return selectType;
    }

    public WebElement getSelectWorkDay() {
        ensureIsDisplayed(selectWorkDay);
        return selectWorkDay;
    }

    public WebElement getInputNewTime() {
        ensureIsDisplayed(inputNewTime);
        return inputNewTime;
    }

    public WebElement getAddButton() {
        ensureIsDisplayed(addButton);
        return addButton;
    }

    public WebElement getSaveButton() {
        ensureIsDisplayed(saveButton);
        return saveButton;
    }
}
