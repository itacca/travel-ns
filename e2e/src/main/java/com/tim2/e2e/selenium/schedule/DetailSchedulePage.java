package com.tim2.e2e.selenium.schedule;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DetailSchedulePage extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "span.schedule-type")
    private WebElement scheduleType;

    @FindBy(css = "span.schedule-active")
    private WebElement scheduleActive;

    @FindBy(css = "span.schedule-validFrom")
    private WebElement scheduleValidFrom;

    @FindBy(xpath = "//app-schedule-detail/div/div/div[4]/span/button")
    private WebElement newButton;

    @FindBy(xpath = "//app-schedule-detail/div/div/div[3]/app-schedule-item-list/div/div/div/table/tbody/tr[last()]")
    private WebElement lastRowInTable;

    @FindBy(xpath = "//app-schedule-detail/div/div/div[3]/app-schedule-item-list/div/div/div/table/tbody/tr[last()]/td[3]/button")
    private WebElement deleteOnLastItem;

    public DetailSchedulePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-schedule-detail"));
    }

    public WebElement getPageTitle() {
        ensureIsDisplayed(pageTitle);
        return pageTitle;
    }

    public WebElement getScheduleType() {
        ensureIsDisplayed(scheduleType);
        return scheduleType;
    }

    public WebElement getScheduleActive() {
        ensureIsDisplayed(scheduleActive);
        return scheduleActive;
    }

    public WebElement getScheduleValidFrom() {
        ensureIsDisplayed(scheduleValidFrom);
        return scheduleValidFrom;
    }

    public WebElement getNewButton() {
        ensureIsDisplayed(newButton);
        return newButton;
    }

    public WebElement getLastRowInTable() {
        ensureIsDisplayed(lastRowInTable);
        return lastRowInTable;
    }

    public WebElement getDeleteOnLastItem() {
        ensureIsDisplayed(deleteOnLastItem);
        return deleteOnLastItem;
    }

    public int getScheduleItemsCount() {
        ensureIsDisplayed(lastRowInTable);
        // two less because oone belongs to table header and second to new Schedule item
        return driver.findElements(By.cssSelector("tr")).size() - 2;
    }
}
