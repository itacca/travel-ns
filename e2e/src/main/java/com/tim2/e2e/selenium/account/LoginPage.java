package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(id = "username")
    private WebElement emailTextField;

    @FindBy(id = "password")
    private WebElement passwordTextField;

    @FindBy(xpath = ".//button[contains(., 'Sign in')]")
    private WebElement signInButton;

    @FindBy(xpath = "//p")
    private WebElement loginErrorMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath(".//button[contains(., 'Sign in')]"))
                && isVisible(By.id("username"))
                && isVisible(By.id("password"));
    }

    public void enterEmail(String email) {
        enterText(emailTextField, email);
    }

    public void enterPassword(String password) {
        enterText(passwordTextField, password);
    }

    public void clickSignInButton() {
        clickElement(signInButton);
    }

    public WebElement getLoginErrorMessage() {
        ensureIsDisplayed(loginErrorMessage);
        return loginErrorMessage;
    }
}
