package com.tim2.e2e.selenium.vehicle;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class VehicleViewPage extends BasePage {

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-vehicles-view[1]/div[1]/div[1]/div[1]/app-vehicle-list[1]/div[1]/div[1]/div[1]/app-vehicle-card[1]/mat-card[1]/mat-card-actions[1]/button[1]")
    private WebElement deleteButton;

    @FindBy(xpath = "//input[@id='mat-input-0']")
    private WebElement plateNumberInput;

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-vehicles-view[1]/div[1]/div[1]/div[1]/app-vehicle-list[1]/div[1]/div[1]/div[1]/app-vehicle-card[1]/mat-card[1]/mat-card-actions[1]/button[2]")
    private WebElement updateButton;

    public VehicleViewPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath("//h2[@class='mat-h2']"));
    }

    public void clickDeleteButton() {
        clickElement(deleteButton);
    }

    public void clickUpdateButton() {
        clickElement(updateButton);
    }

    public void enterPlateNumber(String plateNumber) {
        enterText(plateNumberInput, plateNumber);
    }

    public boolean isUpdateButtonVisible() {
        return isVisible(By.xpath("/html[1]/body[1]/app-root[1]/div[1]/app-vehicles-view[1]/div[1]/div[1]/div[1]/app-vehicle-list[1]/div[1]/div[1]/div[1]/app-vehicle-card[1]/mat-card[1]/mat-card-actions[1]/button[2]"));
    }

}
