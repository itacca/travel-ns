package com.tim2.e2e.selenium.ticket;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ValidateBoughtTicketsPage extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "qrcode")
    private WebElement ticketQrCode;

    @FindBy(css = "span.ticket-date-of-purchase")
    private WebElement ticketDateOfPurchase;

    @FindBy(css = "span.ticket-type")
    private WebElement ticketType;

    @FindBy(css = "span.ticket-duration")
    private WebElement ticketDuration;

    @FindBy(css = "span.ticket-price")
    private WebElement ticketPrice;

    @FindBy(css = "h2.ticket-validity")
    private WebElement ticketValidity;

    public ValidateBoughtTicketsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-ticket-validation"));
    }

    public WebElement getPageTitle() {
        ensureIsDisplayed(pageTitle);
        return pageTitle;
    }

    public WebElement getTicketQrCode() {
        ensureIsDisplayed(ticketQrCode);
        return ticketQrCode;
    }

    public WebElement getTicketDateOfPurchase() {
        ensureIsDisplayed(ticketDateOfPurchase);
        return ticketDateOfPurchase;
    }

    public WebElement getTicketType() {
        ensureIsDisplayed(ticketType);
        return ticketType;
    }

    public WebElement getTicketDuration() {
        ensureIsDisplayed(ticketDuration);
        return ticketDuration;
    }

    public WebElement getTicketPrice() {
        ensureIsDisplayed(ticketPrice);
        return ticketPrice;
    }

    public WebElement getTicketValidity() {
        ensureIsDisplayed(ticketValidity);
        return ticketValidity;
    }
}
