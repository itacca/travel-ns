package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountInformationPage extends BasePage {

    @FindBy(css = "input[formcontrolname='firstNameCtrl']")
    private WebElement firstNameTextField;

    @FindBy(css = "input[formcontrolname='lastNameCtrl']")
    private WebElement lastNameTextField;

    @FindBy(css = "input[formcontrolname='dateOfBirthCtrl']")
    private WebElement dateTextField;

    @FindBy(css = "input[formcontrolname='passwordCtrl']")
    private WebElement passwordTextField;

    @FindBy(css = "input[formcontrolname='passwordConfirmCtrl']")
    private WebElement confirmPasswordTextField;

    @FindBy(xpath = "//button/span[contains(., 'Update password')]/..")
    private WebElement updatePasswordButton;

    @FindBy(xpath = "//button/span[contains(., 'Save')]/..")
    private WebElement saveButton;

    public AccountInformationPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath(".//input[@disabled and @placeholder='Email']"));
    }

    public void enterFirstName(String firstName) {
        enterText(firstNameTextField, firstName);
    }

    public void enterLastName(String lastName) {
        enterText(lastNameTextField, lastName);
    }

    public void enterPassword(String password) {
        enterText(passwordTextField, password);
        firstNameTextField.click();
    }

    public void enterConfirmPassword(String password) {
        enterText(confirmPasswordTextField, password);
        firstNameTextField.click();
    }

    public void enterDateOfBirth(String dateOfBirth) {
        enterText(dateTextField, dateOfBirth);
    }

    public void clickUpdatePasswordButton() {
        clickElement(updatePasswordButton);
    }

    public void clickSaveButton() {
        clickElement(saveButton);
    }

    public WebElement getFirstNameTextField() {
        ensureIsDisplayed(firstNameTextField);
        return firstNameTextField;
    }

    public WebElement getLastNameTextField() {
        ensureIsDisplayed(lastNameTextField);
        return lastNameTextField;
    }

    public WebElement getDateTextField() {
        ensureIsDisplayed(dateTextField);
        return dateTextField;
    }

    public WebElement getSaveButtonWithoutWaiting() {
        return saveButton;
    }
}
