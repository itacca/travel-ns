package com.tim2.e2e.selenium.cityline;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CityLineListPage extends BasePage {

    @FindBy(xpath = "//app-city-line-list/div/div[2]/div[1]/div[2]/input")
    private WebElement filterNameInput;

    @FindBy(xpath = "//app-city-line-list/div/div[2]/div[1]/div[4]/select")
    private WebElement selectType;

    @FindBy(xpath = "//app-city-line-list/div/div[2]/div[3]/table/tbody/tr[1]/td[2]")
    private WebElement firstTdName;

    @FindBy(xpath = "//app-city-line-list/div/div[2]/div[3]/table/tbody/tr[1]/td[3]")
    private WebElement firstTdType;

    @FindBy(xpath = "//app-city-line-list/div/div[2]/div[3]/table/tbody/tr[1]/td[4]/a")
    private WebElement linkToFirstSchedule;

    @FindBy(xpath = "//app-city-line-list/div/div[2]/div[1]/div[4]/select/option[2]")
    private WebElement busType;

    @FindBy(xpath = "//app-city-line-list/div/div[2]/div[3]/table/tbody/tr[last()]")
    private WebElement lastRow;

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-city-line-list[1]/div[1]/div[2]/div[3]/table[1]/tbody[1]/tr[1]/td[5]/button[1]")
    private WebElement showButton;

    public CityLineListPage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath("//app-city-line-list/div/div[2]/div[1]/div[2]/input"))
                && isVisible(By.xpath("//app-city-line-list/div/div[2]/div[1]/div[4]/select"))
                && isVisible(By.xpath("//div/div[2]/div[3]/table/tbody/tr[1]/td[2]"))
                && isVisible(By.xpath("//app-city-line-list/div/div[2]/div[3]/table/tbody/tr[1]/td[3]"))
                && isVisible(By.xpath("//app-city-line-list/div/div[2]/div[1]/div[4]/select/option[2]"))
                && isVisible(By.xpath("//app-city-line-list/div/div[2]/div[3]/table/tbody/tr[last()]"));
    }

    public WebElement getFilterNameInput() {
        ensureIsDisplayed(filterNameInput);
        return filterNameInput;
    }

    public WebElement getSelectType() {
        ensureIsDisplayed(selectType);
        return selectType;
    }

    public WebElement getFirstTdName() {
        ensureIsDisplayed(firstTdName);
        return firstTdName;
    }

    public WebElement getFirstTdType() {
        ensureIsDisplayed(firstTdType);
        return firstTdType;
    }

    public WebElement getBusType() {
        ensureIsDisplayed(busType);
        return busType;
    }

    public int getCityLinesCount() {
        // one less because it belongs to table header
        return driver.findElements(By.cssSelector("tr")).size() - 1;
    }

    public void setNameFilterInput(String value) {
        enterText(filterNameInput, value);
    }

    public void selectBusType() {
        clickElement(selectType);
        clickElement(busType);
    }

    public void ensureLastRowIsVisible() {
        ensureIsDisplayed(lastRow);
    }

    public WebElement getLinkToFirstSchedule() {
        ensureIsDisplayed(linkToFirstSchedule);
        return linkToFirstSchedule;
    }

    public WebElement getLastRow() {
        ensureIsDisplayed(lastRow);
        return lastRow;
    }

    public void clickShowButton() {
        clickElement(showButton);
    }
}
