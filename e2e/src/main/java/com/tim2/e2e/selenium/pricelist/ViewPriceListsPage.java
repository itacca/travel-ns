package com.tim2.e2e.selenium.pricelist;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ViewPriceListsPage extends BasePage {

    @FindBy(xpath = "//div[last()]/mat-card/mat-card-actions/button/span")
    private WebElement lastDeleteButton;

    @FindBy(xpath = "//div[@class='priceLists ng-star-inserted']//div[1]//mat-card[1]//mat-card-actions[1]//button[1]")
    private WebElement deleteButton;

    @FindBy(xpath = "//mat-expansion-panel-header[@id='mat-expansion-panel-header-0']")
    private WebElement extensionPanel;

    @FindBy(xpath = "//input[@id='mat-input-1']")
    private WebElement inputUpdateDate;

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-pricelist-view[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/mat-card-content[1]/app-price-list-card[1]/mat-accordion[1]/mat-expansion-panel[1]/div[1]/div[1]/button[1]")
    private WebElement updateButton;

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-pricelist-view[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/mat-card-content[1]/app-price-list-card[1]/mat-accordion[1]/mat-expansion-panel[1]/div[1]/div[1]/button[2]")
    private WebElement addItemButton;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/mat-dialog-container[1]/app-add-price-list-item[1]/div[1]/div[1]/form[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
    private WebElement priceInput;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[4]/div[1]/div[1]/mat-option[1]")
    private WebElement option;

    @FindBy(xpath = "//button[@id='addButton']")
    private WebElement addItemButtonInDialogue;

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-pricelist-view[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/mat-card-content[1]/app-price-list-card[1]/mat-accordion[1]/mat-expansion-panel[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/div[1]/span[1]/button[1]")
    private WebElement deletePriceListItemButton;

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-pricelist-view[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/mat-card-content[1]/app-price-list-card[1]/mat-accordion[1]/mat-expansion-panel[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/div[1]/span[2]/form[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
    private WebElement priceInputUpdate;

    @FindBy(xpath = "/html[1]/body[1]/app-root[1]/div[1]/app-pricelist-view[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/mat-card-content[1]/app-price-list-card[1]/mat-accordion[1]/mat-expansion-panel[1]/div[1]/div[1]/div[1]/div[1]/mat-card[1]/div[1]/span[3]/button[1]")
    private WebElement updatePriceListItemButton;

    public ViewPriceListsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath("//h2[@class='mat-h2']"));
    }

    public void clickLastDeleteButton() {
        clickElement(lastDeleteButton);
    }

    public void clickDeleteButton() {
        clickElement(deleteButton);
    }

    public void clickUpdateButton() {
        clickElement(updateButton);
    }

    public boolean isUpdateButtonEnabled() {
        return updateButton.isEnabled();
    }

    public void clickExtensionPanel() {
        clickElement(extensionPanel);
    }

    public void enterUpdateDate(String startDate) {
        enterText(inputUpdateDate, startDate);
    }

    public void clickAddItemButton() {
        clickElement(addItemButton);
    }

    public void enterPrice(String price) {
        enterText(priceInput, price);
    }

    public void clickAddItemButtonInDialogue() {
        clickElement(addItemButtonInDialogue);
    }

    public void clickOption() {
        clickElement(option);
    }

    public void clickDeletePriceListItemButton() {
        clickElement(deletePriceListItemButton);
    }

    public void enterPriceUpdate(String price) {
        enterText(priceInputUpdate, price);
    }

    public void clickUpdatePriceListItemButton() {
        clickElement(updatePriceListItemButton);
    }
}
