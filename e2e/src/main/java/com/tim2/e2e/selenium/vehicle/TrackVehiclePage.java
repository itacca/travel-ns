package com.tim2.e2e.selenium.vehicle;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class TrackVehiclePage extends BasePage {

    @FindBy(css = "div.form > h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "mat-select[placeholder='Select vehicle']")
    private WebElement vehicleSelect;

    @FindAll(
            @FindBy(css = "mat-option")
    )
    private List<WebElement> selectOptions;

    @FindBy(css = "div.mat-select-panel")
    private WebElement selectOptionsContainer;

    @FindBy(css = "div.vehicle-data > h2.mat-h2")
    private WebElement vehicleDataSectionTitle;

    @FindBy(css = "span.lat")
    private WebElement latitude;

    @FindBy(css = " span.lng")
    private WebElement longitude;

    @FindBy(css = "div.map")
    private WebElement map;

    public TrackVehiclePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-vehicle-map-view"));
    }

    public void setVehicleSelect(Integer optionIndex) {
        chooseSelectOption(vehicleSelect, selectOptionsContainer, selectOptions, optionIndex);
    }

    public WebElement getPageTitle() {
        ensureIsDisplayed(pageTitle);
        return pageTitle;
    }

    public WebElement getVehicleDataSectionTitle() {
        ensureIsDisplayed(vehicleDataSectionTitle);
        return vehicleDataSectionTitle;
    }

    public WebElement getLatitude() {
        ensureIsDisplayed(latitude);
        return latitude;
    }

    public WebElement getLongitude() {
        ensureIsDisplayed(longitude);
        return longitude;
    }

    public WebElement getMap() {
        ensureIsDisplayed(map);
        return map;
    }
}
