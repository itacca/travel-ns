package com.tim2.e2e.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageableControls extends BasePage {

    @FindBy(css = "button.previous")
    private WebElement previousPageButton;

    @FindBy(css = "button.next")
    private WebElement nextPageButton;

    @FindBy(css = "app-page-controls > div > div")
    private WebElement pageableStatusDiv;

    @FindBy(css = "mat-select[placeholder='Show per page']")
    private WebElement itemsPerPageSelect;

    @FindBy(css = "mat-select[placeholder='Field']")
    private WebElement sortByFieldSelect;

    @FindBy(css = "mat-select[placeholder='Order']")
    private WebElement sortOrderSelect;

    @FindAll(
            @FindBy(css = "mat-option")
    )
    private List<WebElement> selectOptions;

    @FindBy(css = "div.mat-select-panel")
    private WebElement selectOptionsContainer;

    public PageableControls(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return false;
    }

    public void clickOnNextPageButton() {
        clickElement(nextPageButton);
    }

    public void clickOnPreviousPageButton() {
        clickElement(previousPageButton);
    }

    public void setItemsPerPageSelect(Integer optionIndex) {
        chooseSelectOption(itemsPerPageSelect, selectOptionsContainer, selectOptions, optionIndex);
    }

    public void setSortByFieldSelect(Integer optionIndex) {
        chooseSelectOption(sortByFieldSelect, selectOptionsContainer, selectOptions, optionIndex);
    }

    public void setSortOrderSelect(Integer optionIndex) {
        chooseSelectOption(sortOrderSelect, selectOptionsContainer, selectOptions, optionIndex);
    }
}
