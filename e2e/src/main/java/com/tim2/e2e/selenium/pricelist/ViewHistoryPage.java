package com.tim2.e2e.selenium.pricelist;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ViewHistoryPage extends BasePage {

    @FindBy(xpath = "//span[@class='mat-select-placeholder ng-tns-c12-8 ng-star-inserted']")
    private WebElement choosePriceList;

    @FindBy(xpath = "//span[@class='mat-select-placeholder ng-tns-c12-9 ng-star-inserted']")
    private WebElement chooseTicket;

    @FindBy(xpath = "//button[@id='load1']")
    private WebElement loadPriceListButton;

    @FindBy(xpath = "//button[@id='load2']")
    private WebElement loadTicketButton;

    @FindBy(xpath = "//mat-card[@class='mat-card ng-star-inserted']//div[5]")
    private WebElement showPriceListItems;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/mat-option[1]")
    private WebElement optionOne;

    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/mat-option[1]")
    private WebElement optionTwo;

    public ViewHistoryPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath("//mat-card[@class='mat-card ng-star-inserted']//div[2]//button[1]"));
    }

    public void checkIfEnabledLoadPriceListButton() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(loadPriceListButton));
    }

    public void checkIfEnabledLoadTicketButton() {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(loadTicketButton));
    }

    public void clickLoadPriceListButton() {
        clickElement(loadPriceListButton);
    }

    public void clickLoadTicketButton() {
        clickElement(loadTicketButton);
    }

    public void clickOptionOne() {
        clickElement(optionOne);
    }

    public void clickOptionTwo() {
        clickElement(optionTwo);
    }

    public void checkIfVisible() {
        isVisible(By.xpath("//mat-card[@class='mat-card ng-star-inserted']//div[5]"));
    }
}
