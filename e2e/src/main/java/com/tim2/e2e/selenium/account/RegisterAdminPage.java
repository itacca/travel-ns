package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterAdminPage extends BasePage {

    @FindBy(css = "input[formcontrolname='firstNameCtrl']")
    private WebElement firstNameTextField;

    @FindBy(css = "input[formcontrolname='lastNameCtrl']")
    private WebElement lastNameTextField;

    @FindBy(css = "input[formcontrolname='emailCtrl']")
    private WebElement emailTextField;

    @FindBy(css = "input[formcontrolname='dateOfBirthCtrl']")
    private WebElement dateTextField;

    @FindBy(xpath = "//button/span[contains(., 'Register')]/..")
    private WebElement registerButton;


    public RegisterAdminPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.cssSelector("input[formcontrolname='firstNameCtrl']"))
                && isVisible(By.cssSelector("input[formcontrolname='lastNameCtrl']"))
                && isVisible(By.cssSelector("input[formcontrolname='emailCtrl']"))
                && isVisible(By.cssSelector("input[formcontrolname='dateOfBirthCtrl']"));
    }

    public void enterFirstName(String firstName) {
        enterText(firstNameTextField, firstName);
    }

    public void enterLastName(String lastName) {
        enterText(lastNameTextField, lastName);
    }

    public void enterEmail(String email) {
        enterText(emailTextField, email);
    }

    public void enterDate(String date) {
        enterText(dateTextField, date);
    }

    public void clickRegisterButton() {
        clickElement(registerButton);
    }

    public boolean isRegisterButtonVisible() {
        return isVisible(By.xpath("//button/span[contains(., 'Register')]/.."));
    }

}
