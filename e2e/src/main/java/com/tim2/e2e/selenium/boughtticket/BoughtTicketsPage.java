package com.tim2.e2e.selenium.boughtticket;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class BoughtTicketsPage extends BasePage {

    @FindAll(
            @FindBy(css = "app-ticket-card")
    )
    private List<WebElement> ticketCards;

    @FindBy(css = "div.tickets")
    private WebElement ticketContainer;

    public BoughtTicketsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.cssSelector("app-purchased-tickets-view"));
    }

    public List<WebElement> getTicketCards() {
        ensureIsDisplayed(ticketContainer);
        return ticketCards;
    }
}
