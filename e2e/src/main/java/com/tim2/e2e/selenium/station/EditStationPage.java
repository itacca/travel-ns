package com.tim2.e2e.selenium.station;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditStationPage extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "input[formcontrolname='nameCtrl']")
    private WebElement nameInput;

    @FindBy(css = "input[formcontrolname='latitudeCtrl']")
    private WebElement latitudeInput;

    @FindBy(css = "input[formcontrolname='longitudeCtrl']")
    private WebElement longitudeInput;

    @FindBy(css = "button.save-btn")
    private WebElement saveButton;

    public EditStationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-edit-station"));
    }

    public WebElement getPageTitle() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ensureIsDisplayed(pageTitle);
        return pageTitle;
    }

    public void clickSaveButton() {
        clickElement(saveButton);
    }

    public void setNameInputText(String text) {
        enterText(nameInput, text);
    }

    public void setLatitudeInputText(String text) {
        enterText(latitudeInput, text);
    }

    public void setLongitudeInputText(String text) {
        enterText(longitudeInput, text);
    }

    public WebElement getSaveButton() {
        ensureIsDisplayed(saveButton);
        return saveButton;
    }

    public WebElement getNameInput() {
        ensureIsDisplayed(nameInput);
        return nameInput;
    }

    public WebElement getLatitudeInput() {
        ensureIsDisplayed(latitudeInput);
        return latitudeInput;
    }

    public WebElement getLongitudeInput() {
        ensureIsDisplayed(longitudeInput);
        return longitudeInput;
    }
}
