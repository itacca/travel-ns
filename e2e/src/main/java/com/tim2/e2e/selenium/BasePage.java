package com.tim2.e2e.selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public abstract class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public abstract boolean isVisible();

    protected void ensureIsDisplayed(WebElement element) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(element));
    }

    protected void ensureIsDisplayed(List<WebElement> elements) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    protected void ensureTextIsEntered(WebElement element, String email) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.textToBePresentInElementValue(
                        element, email));
    }

    protected boolean isVisible(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected void enterText(WebElement element, String text) {
        ensureIsDisplayed(element);
        clear(element);
        element.sendKeys(text);
        ensureTextIsEntered(element, text);
    }

    protected void clickElement(WebElement element) {
        ensureIsDisplayed(element);
        element.click();
    }

    protected void clear(WebElement formField) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].value=\"\";arguments[0].dispatchEvent(new Event('input'))", formField);
    }

    public WebElement getToastMessage() {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.className("toast-message")));
    }

    protected void chooseSelectOption(WebElement select, WebElement selectOptionsContainer,
                                      List<WebElement> selectOptions, Integer optionIndex) {
        clickElement(select);
        ensureIsDisplayed(selectOptionsContainer);
        clickElement(selectOptions.get(optionIndex));
    }
}
