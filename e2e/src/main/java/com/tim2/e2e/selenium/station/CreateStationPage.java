package com.tim2.e2e.selenium.station;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateStationPage extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "input[formcontrolname='nameCtrl']")
    private WebElement nameInput;

    @FindBy(css = "input[formcontrolname='latitudeCtrl']")
    private WebElement latitudeInput;

    @FindBy(css = "input[formcontrolname='longitudeCtrl']")
    private WebElement longitudeInput;

    @FindBy(css = "button.save-btn")
    private WebElement saveButton;

    public CreateStationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-create-station"));
    }

    public WebElement getPageTitle() {
        ensureIsDisplayed(pageTitle);
        return pageTitle;
    }

    public void clickSaveButton() {
        clickElement(saveButton);
    }

    public void setNameInputText(String text) {
        enterText(nameInput, text);
    }

    public void setLatitudeInputText(String text) {
        enterText(latitudeInput, text);
    }

    public void setLongitudeInputText(String text) {
        enterText(longitudeInput, text);
    }

    public WebElement getSaveButton() {
        ensureIsDisplayed(saveButton);
        return saveButton;
    }
}
