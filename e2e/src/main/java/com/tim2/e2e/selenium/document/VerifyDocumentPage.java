package com.tim2.e2e.selenium.document;

import com.tim2.e2e.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class VerifyDocumentPage extends BasePage {

    @FindBy(css = "h2.mat-h2")
    private WebElement pageTitle;

    @FindBy(css = "div.documents")
    private WebElement documentContainer;

    @FindAll(
            @FindBy(css = "div.documents > div.document")
    )
    private List<WebElement> documents;

    @FindAll(
            @FindBy(css = "div.account-details")
    )
    private List<WebElement> accountDetails;

    @FindAll(
            @FindBy(css = "div.document > button")
    )
    private List<WebElement> verifyButtons;

    @FindAll(
            @FindBy(css = "div.document > img")
    )
    private List<WebElement> images;

    public VerifyDocumentPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isVisible() {
        return isVisible(By.cssSelector("app-verify-document"));
    }

    public WebElement getImage(Integer index) {
        ensureIsDisplayed(documentContainer);
        return images.get(index);
    }

    public WebElement getVerifyButton(Integer index) {
        ensureIsDisplayed(documentContainer);
        return verifyButtons.get(index);
    }

    public WebElement getAccountDetails(Integer index) {
        ensureIsDisplayed(documentContainer);
        return accountDetails.get(index);
    }

    public WebElement getPageTitle() {
        ensureIsDisplayed(pageTitle);
        return pageTitle;
    }

    public List<WebElement> getDocuments() {
        ensureIsDisplayed(documentContainer);
        return documents;
    }

    public void clickVerifyButton(int index) {
        ensureIsDisplayed(documentContainer);
        clickElement(verifyButtons.get(index));
    }
}
