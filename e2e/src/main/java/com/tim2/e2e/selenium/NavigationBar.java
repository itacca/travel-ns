package com.tim2.e2e.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationBar extends BasePage {

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Account')]")
    private WebElement accountNavigationButton;

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Home')]")
    private WebElement homeNavigationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Log In']")
    private WebElement accountLogInButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Log Out']")
    private WebElement accountLogOutButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Register']")
    private WebElement accountRegisterButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View Account Information']")
    private WebElement accountInformationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Upload document']")
    private WebElement uploadDocumentButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Verify documents']")
    private WebElement verifyDocumentButton;

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Travel admins')]")
    private WebElement travelAdminsNavigationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Register admin']")
    private WebElement registerAdminButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View admins']")
    private WebElement viewAdminsButton;

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Tickets')]")
    private WebElement ticketsNavigationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View purchased tickets']")
    private WebElement viewBoughtTicketsButton;

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'City lines')]")
    private WebElement cityLinesNavigationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View city lines']")
    private WebElement viewCityLinesButton;

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Price lists')]")
    private WebElement priceListNavigationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Add price list']")
    private WebElement addPriceListButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View history']")
    private WebElement viewHistoryButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View price lists']")
    private WebElement viewPriceListsButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Create city line']")
    private WebElement createCityLineButton;

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Vehicles')]")
    private WebElement vehiclesNavigationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Create vehicle']")
    private WebElement createVehicleButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View vehicles']")
    private WebElement vieVehiclesButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Track vehicle']")
    private WebElement trackVehicleButton;

    @FindBy(xpath = "//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Stations')]")
    private WebElement stationNavigationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'Create station']")
    private WebElement createStationButton;

    @FindBy(xpath = "//div[contains(@class,'mat-menu-content')] //a[text() = 'View stations']")
    private WebElement viewStationsButton;


    public NavigationBar(WebDriver driver) {
        super(driver);
    }

    public boolean isVisible() {
        return isVisible(By.xpath("//span[contains(@class, 'mat-button-wrapper') and contains(text(), 'Account')]"));
    }

    public void clickAccountNavigationButton() {
        clickElement(accountNavigationButton);
    }

    public void clickAccountLogInButton() {
        clickElement(accountLogInButton);
    }

    public void clickLogOutButton() {
        clickElement(accountLogOutButton);
    }

    public void clickAccountRegisterButton() {
        clickElement(accountRegisterButton);
    }

    public boolean isLogOutDisplayed() {
        return isVisible(By.xpath("//div[contains(@class,'mat-menu-content')] //a[text() = 'Log Out']"));
    }

    public void clickAccountInformationButton() {
        clickElement(accountInformationButton);
    }

    public void clickHomeButton() {
        clickElement(homeNavigationButton);
    }

    public void clickCityLinesNavigationButton() {
        clickElement(cityLinesNavigationButton);
    }

    public void clickViewCityLinesButton() {
        clickElement(viewCityLinesButton);
    }

    public void clickTravelAdminsNavigationButton() {
        clickElement(travelAdminsNavigationButton);
    }

    public void clickRegisterAdminButton() {
        clickElement(registerAdminButton);
    }

    public void clickViewAdminsButton() {
        clickElement(viewAdminsButton);
    }

    public void clickPriceListNavigationButton() {
        clickElement(priceListNavigationButton);
    }

    public void clickViewHistoryButton() {
        clickElement(viewHistoryButton);
    }

    public void clickAddPriceListButton() {
        clickElement(addPriceListButton);
    }

    public void clickViewPriceListsButton() {
        clickElement(viewPriceListsButton);
    }

    public void clickCreateCityLineButton() {
        clickElement(createCityLineButton);
    }

    public void clickTicketsNavigationButton() {
        clickElement(ticketsNavigationButton);
    }

    public void clickViewBoughtTicketsButton() {
        clickElement(viewBoughtTicketsButton);
    }

    public void clickVehiclesNavigationButton() {clickElement(vehiclesNavigationButton);}

    public void clickCreateVehicle() {clickElement(createVehicleButton);}

    public void clickViewVehiclesButton() {clickElement(vieVehiclesButton);}

    public void clickTrackVehicleButton() {
        clickElement(trackVehicleButton);
    }

    public void clickUploadDocumentButton() {
        clickElement(uploadDocumentButton);
    }

    public void clickVerifyDocumentButton() {
        clickElement(verifyDocumentButton);
    }

    public void clickStationNavigationButton() {
        clickElement(stationNavigationButton);
    }

    public void clickCreateStationButton() {
        clickElement(createStationButton);
    }

    public void clickViewStationsButton() {
        clickElement(viewStationsButton);
    }

}
