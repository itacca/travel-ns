package com.tim2.e2e.selenium.cityline;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.WebMessages;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tim2.e2e.selenium.TestData.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class CreateCityLineTest extends TestBaseResource {

    private static final Integer BUS_TYPE_OPTION = 0;

    @Test
    public void testCreateCityLinePage() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(emails, passwords, this::testSaveButtonDisabledWhenNoInformationIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonDisabledWhenCityLineNameIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonDisabledWhenCityLineTypeIsSelected);
        runWithCredentials(emails, passwords, this::testSaveButtonEnabledWhenCityLineNameAndTypeAreEntered);
        runWithCredentials(emails, passwords, this::testSuccessMessageIsShownWhenSaveButtonIsClicked);
    }

    private void testSuccessMessageIsShownWhenSaveButtonIsClicked() {
        navigateToCreateCityLinePage();

        createCityLinePage.enterCityLineName(CREATE_CITY_LINE_NAME);
        createCityLinePage.setCityLineTypeSelect(BUS_TYPE_OPTION);

        expectSaveButtonToBeEnabled();

        createCityLinePage.clickSaveButton();

        assertThat("Toast contains success text", toastr.getToastMessage().getText(),
                containsString(WebMessages.CREATE_CITY_LINE_SUCCESS));
        assertThat("Toast has success title", toastr.getToastTitle().getText(),
                containsString(WebMessages.TOAST_SUCCESS));
    }

    private void testSaveButtonEnabledWhenCityLineNameAndTypeAreEntered() {
        navigateToCreateCityLinePage();

        createCityLinePage.enterCityLineName(CREATE_CITY_LINE_NAME);
        createCityLinePage.setCityLineTypeSelect(BUS_TYPE_OPTION);

        expectSaveButtonToBeEnabled();
    }

    private void testSaveButtonDisabledWhenCityLineTypeIsSelected() {
        navigateToCreateCityLinePage();

        createCityLinePage.setCityLineTypeSelect(BUS_TYPE_OPTION);

        expectSaveButtonToBeDisabled();
    }

    private void testSaveButtonDisabledWhenCityLineNameIsEntered() {
        navigateToCreateCityLinePage();

        createCityLinePage.enterCityLineName(CREATE_CITY_LINE_NAME);

        expectSaveButtonToBeDisabled();
    }

    private void testSaveButtonDisabledWhenNoInformationIsEntered() {
        navigateToCreateCityLinePage();

        expectSaveButtonToBeDisabled();
    }

    private void navigateToCreateCityLinePage() {
        navigationBar.clickCityLinesNavigationButton();
        navigationBar.clickCreateCityLineButton();
        createCityLinePage.getSaveButton();
        expectCreateCityLinesPageToBeVisible();
    }

    private void expectCreateCityLinesPageToBeVisible() {
        assertTrue("Create city line page should be visible", createCityLinePage.isVisible());
    }

    private void expectSaveButtonToBeDisabled() {
        assertFalse("Save button should be disabled.", createCityLinePage.getSaveButton().isEnabled());
    }

    private void expectSaveButtonToBeEnabled() {
        assertTrue("Save button should be enabled.", createCityLinePage.getSaveButton().isEnabled());
    }

    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TRAVEL_ADMINISTRATOR_EMAIL);
        emails.add(SYSTEM_ADMINISTRATOR_EMAIL);
        return emails;
    }

    public List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TRAVEL_ADMINISTRATOR_PASSWORD);
        passwords.add(SYSTEM_ADMINISTRATOR_PASSWORD);
        return passwords;
    }
}
