package com.tim2.e2e.selenium.vehicle;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;
import static org.testng.AssertJUnit.assertFalse;

public class CreateVehicleTest extends TestBaseResource {

    @Test
    public void testCreateVehicle()  {
        testSuccessfulAdding();
        testAddExisting();
        testNoPlateNumberEntered();
        testNoLineChosen();
        testNoTypeChosen();
    }

    private void testSuccessfulAdding() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openCreateVehiclePage();
        createVehiclePage.enterPlateNumber("lalsss");
        findSelectByCss("mat-select[placeholder='Type']");
        createVehiclePage.clickOptionType();
        createVehiclePage.checkIfEnabledLoadButton();
        createVehiclePage.clickLoad();
        findSelectByCss("mat-select[placeholder='Choose city line']");
        createVehiclePage.clickOptionLine();
        createVehiclePage.checkIfEnabledSaveButton();
        createVehiclePage.clickSaveButton();
        verifyToastMessage(createVehiclePage.getToastMessage(), "Vehicle is added.");
        logOut();
    }

    private void testAddExisting() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openCreateVehiclePage();
        createVehiclePage.enterPlateNumber(EXISTING_PLATE_NUMBER);
        findSelectByCss("mat-select[placeholder='Type']");
        createVehiclePage.clickOptionType();
        createVehiclePage.checkIfEnabledLoadButton();
        createVehiclePage.clickLoad();
        findSelectByCss("mat-select[placeholder='Choose city line']");
        createVehiclePage.clickOptionLine();
        createVehiclePage.checkIfEnabledSaveButton();
        createVehiclePage.clickSaveButton();
        verifyToastMessage(createVehiclePage.getToastMessage(), "Unable to add vehicle");
        logOut();
    }

    private void testNoPlateNumberEntered() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openCreateVehiclePage();
        findSelectByCss("mat-select[placeholder='Type']");
        createVehiclePage.clickOptionType();
        createVehiclePage.checkIfEnabledLoadButton();
        createVehiclePage.clickLoad();
        findSelectByCss("mat-select[placeholder='Choose city line']");
        createVehiclePage.clickOptionLine();
        assertFalse("Save button is not visible.", createVehiclePage.isSaveButtonVisible());
        logOut();
    }

    private void testNoTypeChosen() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openCreateVehiclePage();
        createVehiclePage.enterPlateNumber(PLATE_NUMBER);
        assertFalse("Save button is not visible.", createVehiclePage.isSaveButtonVisible());
        logOut();
    }

    private void testNoLineChosen() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openCreateVehiclePage();
        createVehiclePage.enterPlateNumber(PLATE_NUMBER);
        findSelectByCss("mat-select[placeholder='Type']");
        createVehiclePage.clickOptionType();
        createVehiclePage.checkIfEnabledLoadButton();
        createVehiclePage.clickLoad();
        assertFalse("Save button is not visible.", createVehiclePage.isSaveButtonVisible());
        logOut();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }
    
    private void openCreateVehiclePage() {
        navigationBar.clickVehiclesNavigationButton();
        navigationBar.clickCreateVehicle();
    }
}
