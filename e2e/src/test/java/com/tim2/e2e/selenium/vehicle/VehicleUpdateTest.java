package com.tim2.e2e.selenium.vehicle;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;

public class VehicleUpdateTest extends TestBaseResource {

    @Test
    public void testVehicleUpdate()  {
        testUpdateSuccessful();
        testUpdateExistingPlateNumber();
    }

    private void testUpdateSuccessful() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewVehicleTest();
        vehicleViewPage.enterPlateNumber(PLATE_NUMBER);

        vehicleViewPage.isUpdateButtonVisible();
        vehicleViewPage.clickUpdateButton();
        verifyToastMessage(registerAdminPage.getToastMessage(), "Vehicle updated");
        logOut();
        navigationBar.isVisible();
    }

    private void testUpdateExistingPlateNumber() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewVehicleTest();
        vehicleViewPage.enterPlateNumber(EXISTING_PLATE_NUMBER);

        vehicleViewPage.isUpdateButtonVisible();
        vehicleViewPage.clickUpdateButton();
        verifyToastMessage(registerAdminPage.getToastMessage(), "Unable to update vehicle");
        logOut();
        navigationBar.isVisible();
    }


    private void openViewVehicleTest() {
        navigationBar.clickVehiclesNavigationButton();
        navigationBar.clickViewVehiclesButton();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }
}
