package com.tim2.e2e.selenium.vehicle;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;

public class DeleteVehicleTest extends TestBaseResource {

    @Test
    public void testDeleteVehicle()  {
        testDelete();
    }

    private void testDelete() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewVehicleTest();

        vehicleViewPage.clickDeleteButton();
        verifyToastMessage(registerAdminPage.getToastMessage(), "Vehicle deleted");
        logOut();
        navigationBar.isVisible();
    }


    private void openViewVehicleTest() {
        navigationBar.clickVehiclesNavigationButton();
        navigationBar.clickViewVehiclesButton();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }
}
