package com.tim2.e2e.selenium.pricelist;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;
import static org.testng.AssertJUnit.assertFalse;

public class UpdatePriceListTest extends TestBaseResource {

    @Test
    public void testUpdatePriceList() {
        testSuccessfulUpdate();
        testDateIsInThePast();
        testInvalidDateFormat();
    }

    private void testSuccessfulUpdate() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.enterUpdateDate(TestData.PRICE_LIST_UPDATE_DATE);
        viewPriceListsPage.clickUpdateButton();
        verifySuccessToast(addPriceListPage.getToastMessage(), "Price list successfuly updated");
        logOut();
        navigationBar.isVisible();
    }

    private void testDateIsInThePast() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.enterUpdateDate(TestData.PRICE_LIST_PAST_DATE);
        viewPriceListsPage.clickUpdateButton();
        verifyToastMessage(addPriceListPage.getToastMessage(), "Unable to update price list.");
        logOut();
        navigationBar.isVisible();
    }

    private void testInvalidDateFormat() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.enterUpdateDate(TestData.SYSTEM_ADMINISTRATOR_EMAIL);
        assertFalse("Update button is not visible.", viewPriceListsPage.isUpdateButtonEnabled());
        logOut();
        navigationBar.isVisible();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    protected void openViewPriceListPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickViewPriceListsButton();
    }
}
