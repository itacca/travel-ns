package com.tim2.e2e.selenium.cityline;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;
import static junit.framework.TestCase.assertEquals;

public class FindCityLinesByTypeTest extends TestBaseResource {

    @Test
    public void testFindCityLinesByTypeFeature() {
        openCityLinesListPage();
        verifyCurrentUrlAddress(CITY_LINE_URL_ADDRESS);
        loadCityLineListPage();

        checkNumberOfCityLines(NUM_OF_CITY_LINES);
        testShouldGetCityLinesByBusType();
        testShouldFilterCityLinesByName();
    }

    private void testShouldGetCityLinesByBusType() {
        cityLineListPage.selectBusType();
        loadCityLineListPage();

        checkNumberOfCityLines(NUM_OF_BUS_TYPE);
        checkFirstCityLine();
    }

    private void testShouldFilterCityLinesByName() {
        filterCityLinesByName(CITY_LINE_BUS_TYPE_NAME3);

        verifyFilter();
    }

    private void verifyFilter() {
        assertEquals(cityLineListPage.getFirstTdName().getText(), CITY_LINE_BUS_TYPE_NAME3);
        assertEquals(cityLineListPage.getFirstTdType().getText(), CITY_LINE_VALID_BUS_TYPE.toLowerCase());
    }

    private void filterCityLinesByName(String inputNameString) {
        cityLineListPage.setNameFilterInput(inputNameString);
        loadCityLineListPage();
    }

    private void checkFirstCityLine() {
        assertEquals(cityLineListPage.getFirstTdName().getText(), FIRST_CITY_LINE_NAME);
        assertEquals(cityLineListPage.getFirstTdType().getText(), CITY_LINE_VALID_BUS_TYPE.toLowerCase());
    }

    private void loadCityLineListPage() {
        cityLineListPage.ensureLastRowIsVisible();
    }

    private void checkNumberOfCityLines(int actualNumberOfCityLines) {
        assertEquals(cityLineListPage.getCityLinesCount(), actualNumberOfCityLines);
    }

    private void openCityLinesListPage() {
        navigationBar.clickCityLinesNavigationButton();
        navigationBar.clickViewCityLinesButton();
    }
}
