package com.tim2.e2e.selenium.station;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class CreateStationTest extends TestBaseResource {

    @Test
    public void createStationPageTest() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(emails, passwords, this::testShouldDisplayPageTitle);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfNoDataIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfStationNameIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfLatitudeIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfLongitudeIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfLongitudeAndLatitudeIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeEnabledIfAllDataIsEntered);
        runWithCredentials(emails, passwords, this::testToastSuccessShouldBeShownWhenSaveButtonIsClicked);
    }

    private void testShouldDisplayPageTitle() {
        navigateToCreateStationPage();

        assertThat("Should display page title", createStationPage.getPageTitle().getText(),
                containsString("Create station"));
    }

    private void testToastSuccessShouldBeShownWhenSaveButtonIsClicked() {
        navigateToCreateStationPage();

        createStationPage.setNameInputText("Test station name");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("123123");
        createStationPage.clickSaveButton();

        assertThat("Toast success title is shown", toastr.getToastTitle().getText(),
                containsString("Success"));
        assertThat("Toast success message is shown", toastr.getToastMessage().getText(),
                containsString("Station created"));
    }

    private void testSaveButtonShouldBeEnabledIfAllDataIsEntered() {
        navigateToCreateStationPage();

        createStationPage.setNameInputText("Test station name");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("123123");

        assertTrue("Save button should be enabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfLongitudeAndLatitudeIsEntered() {
        navigateToCreateStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("123123");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfLongitudeIsEntered() {
        navigateToCreateStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("");
        createStationPage.setLongitudeInputText("123123");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfLatitudeIsEntered() {
        navigateToCreateStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfStationNameIsEntered() {
        navigateToCreateStationPage();

        createStationPage.setNameInputText("Test station name");
        createStationPage.setLatitudeInputText("");
        createStationPage.setLongitudeInputText("");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfNoDataIsEntered() {
        navigateToCreateStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("");
        createStationPage.setLongitudeInputText("");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void navigateToCreateStationPage() {
        navigationBar.clickStationNavigationButton();
        navigationBar.clickCreateStationButton();
    }

    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TestData.TRAVEL_ADMINISTRATOR_EMAIL);
        emails.add(TestData.SYSTEM_ADMINISTRATOR_EMAIL);
        return emails;
    }

    public List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TestData.TRAVEL_ADMINISTRATOR_PASSWORD);
        passwords.add(TestData.SYSTEM_ADMINISTRATOR_PASSWORD);
        return passwords;
    }
}
