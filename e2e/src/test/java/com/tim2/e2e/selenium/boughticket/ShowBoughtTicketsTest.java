package com.tim2.e2e.selenium.boughticket;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.boughticket.model.BoughtTicketDisplayData;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.tim2.e2e.selenium.TestData.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ShowBoughtTicketsTest extends TestBaseResource {

    private static final Integer BOUGHT_PRICE_SORT_OPTION = 4;
    private static final Integer ASCENDING_SORT_OPTION = 0;
    private static final Integer DESCENDING_SORT_OPTION = 1;

    @Test
    public void testBoughtTicketsPage() {
        runWithCredentials(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD,
                this::testBoughtTicketsDisplayedCorrectlyForSystemAdminRole);

        runWithCredentials(TRAVEL_ADMINISTRATOR_EMAIL, TRAVEL_ADMINISTRATOR_PASSWORD,
                this::testBoughtTicketsDisplayedCorrectlyForTravelAdminRole);

        runWithCredentials(NIKOLA_EMAIL, NIKOLA_PASSWORD,
                this::testBoughtTicketsDisplayedCorrectlyForRegisteredUser);

        runWithCredentials(NIKOLA_EMAIL, NIKOLA_PASSWORD,
                this::testBoughtTicketsSortedInDescendingOrderCorrectly);

        runWithCredentials(NIKOLA_EMAIL, NIKOLA_PASSWORD,
                this::testBoughtTicketsSortedInAscendingOrderCorrectly);
    }

    private void testBoughtTicketsSortedInAscendingOrderCorrectly() {
        navigationBar.clickTicketsNavigationButton();
        navigationBar.clickViewBoughtTicketsButton();

        expectBoughtTicketsPageToBeVisible();

        List<BoughtTicketDisplayData> ticketData = getBoughtTicketDisplayDataForRegisteredUser();

        ticketData.sort(getAscendingBoughtPriceComparator());
        testBoughTicketsSortedCorrectlyWith(BOUGHT_PRICE_SORT_OPTION, ASCENDING_SORT_OPTION, ticketData);
    }

    private void testBoughtTicketsSortedInDescendingOrderCorrectly() {
        navigationBar.clickTicketsNavigationButton();
        navigationBar.clickViewBoughtTicketsButton();

        expectBoughtTicketsPageToBeVisible();

        List<BoughtTicketDisplayData> ticketData = getBoughtTicketDisplayDataForRegisteredUser();

        ticketData.sort(getDescendingBoughtPriceComparator());
        testBoughTicketsSortedCorrectlyWith(BOUGHT_PRICE_SORT_OPTION, DESCENDING_SORT_OPTION, ticketData);
    }

    private void testBoughTicketsSortedCorrectlyWith(Integer sortField, Integer sortOrder,
                                                     List<BoughtTicketDisplayData> ticketData) {
        pageableControls.setSortByFieldSelect(sortField);
        pageableControls.setSortOrderSelect(sortOrder);

        expectCorrectNumberOfTicketsToBeDisplayed(ticketData.size());
        expectTicketCardListToDisplayCorrectInformation(boughtTicketsPage.getTicketCards(), ticketData);
    }

    private void testBoughtTicketsDisplayedCorrectlyForRegisteredUser() {
        navigationBar.clickTicketsNavigationButton();
        navigationBar.clickViewBoughtTicketsButton();

        expectBoughtTicketsPageToBeVisible();
        expectCorrectNumberOfTicketsToBeDisplayed(REGISTERED_USER_BOUGHT_TICKETS_COUNT);

        expectTicketCardListToDisplayCorrectInformation(boughtTicketsPage.getTicketCards(),
                getBoughtTicketDisplayDataForRegisteredUser());
    }


    private void testBoughtTicketsDisplayedCorrectlyForTravelAdminRole() {
        navigationBar.clickTicketsNavigationButton();
        navigationBar.clickViewBoughtTicketsButton();

        expectBoughtTicketsPageToBeVisible();
        expectCorrectNumberOfTicketsToBeDisplayed(TRAVEL_ADMIN_BOUGHT_TICKETS_COUNT);

        expectTicketCardListToDisplayCorrectInformation(boughtTicketsPage.getTicketCards(),
                getBoughtTicketDisplayDataForTravelAdmin());
    }


    private void testBoughtTicketsDisplayedCorrectlyForSystemAdminRole() {
        navigationBar.clickTicketsNavigationButton();
        navigationBar.clickViewBoughtTicketsButton();

        expectBoughtTicketsPageToBeVisible();
        expectCorrectNumberOfTicketsToBeDisplayed(SYS_ADMIN_BOUGHT_TICKETS_COUNT);

        expectTicketCardListToDisplayCorrectInformation(boughtTicketsPage.getTicketCards(),
                getBoughtTicketDisplayDataForSystemAdmin());
    }

    private Comparator<BoughtTicketDisplayData> getAscendingBoughtPriceComparator() {
        return (o1, o2) -> {
            Double p1 = Double.parseDouble(o1.getPrice().substring(1));
            Double p2 = Double.parseDouble(o2.getPrice().substring(1));
            return (int) (p1 - p2);
        };
    }

    private Comparator<BoughtTicketDisplayData> getDescendingBoughtPriceComparator() {
        return (o1, o2) -> {
            Double p1 = Double.parseDouble(o1.getPrice().substring(1));
            Double p2 = Double.parseDouble(o2.getPrice().substring(1));
            return (int) (p2 - p1);
        };
    }

    private void expectCorrectNumberOfTicketsToBeDisplayed(int numberOfTickets) {
        assertThat("Correct number of tickets are displayed",
                boughtTicketsPage.getTicketCards().size(), is(numberOfTickets));
    }

    private void expectBoughtTicketsPageToBeVisible() {
        assertTrue("View bought tickets page is visible", boughtTicketsPage.isVisible());
    }

    private void expectTicketInformationToBeDisplayed(WebElement ticketCard, BoughtTicketDisplayData data) {
        assertThat("Ticket type should be displayed", ticketCard.getText(), containsString(data.getType()));
        assertThat("Ticket duration should be displayed", ticketCard.getText(), containsString(data.getDuration()));
        assertThat("Ticket price should be displayed", ticketCard.getText(), containsString(data.getPrice()));
        assertThat("Ticket date of purchase should be displayed", ticketCard.getText(), containsString(data.getBoughtDate()));
    }

    private List<BoughtTicketDisplayData> getBoughtTicketDisplayDataForRegisteredUser() {
        DateTime currentTime = DateTime.now(DateTimeZone.UTC);
        String visibleCurrentTime = currentTime.toString("MMM dd, YYYY");

        List<BoughtTicketDisplayData> boughtTicketsDisplayData = new ArrayList<>();
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("REGULAR", "SINGLE_RIDE",
                "€35.11", "Date of purchase: " + visibleCurrentTime));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("SENIOR", "DAILY",
                "€123.11", "Date of purchase: " + visibleCurrentTime));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("SENIOR", "DAILY",
                "€552.22", "Date of purchase: Dec 12, 2010"));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("REGULAR", "MONTHLY",
                "€23.11", "Date of purchase: " + visibleCurrentTime));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("REGULAR", "MONTHLY",
                "€341.22", "Date of purchase: Dec 12, 2010"));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("REGULAR", "ANNUAL",
                "€11.11", "Date of purchase: " + visibleCurrentTime));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("REGULAR", "ANNUAL",
                "€23.11", "Date of purchase: Dec 12, 2010"));

        return boughtTicketsDisplayData;
    }

    private List<BoughtTicketDisplayData> getBoughtTicketDisplayDataForTravelAdmin() {
        List<BoughtTicketDisplayData> boughtTicketsDisplayData = new ArrayList<>();
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("STUDENT", "MONTHLY",
                "€33.54", "Date of purchase: Dec 12, 2018"));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("STUDENT", "SINGLE_RIDE",
                "€44.11", "Date of purchase: Dec 12, 2018"));

        return boughtTicketsDisplayData;
    }

    private List<BoughtTicketDisplayData> getBoughtTicketDisplayDataForSystemAdmin() {
        List<BoughtTicketDisplayData> boughtTicketsDisplayData = new ArrayList<>();
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("REGULAR", "ANNUAL",
                "€11.11", "Date of purchase: Dec 12, 2018"));
        boughtTicketsDisplayData.add(new BoughtTicketDisplayData("REGULAR", "SINGLE_RIDE",
                "€22.22", "Date of purchase: Dec 12, 2018"));

        return boughtTicketsDisplayData;
    }

    private void expectTicketCardListToDisplayCorrectInformation(List<WebElement> ticketCards,
                                                                 List<BoughtTicketDisplayData> boughtTicketsDisplayData) {
        for (int i = 0; i < ticketCards.size(); i++) {
            expectTicketInformationToBeDisplayed(ticketCards.get(i), boughtTicketsDisplayData.get(i));
        }
    }
}
