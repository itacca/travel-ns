package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;

public class DeleteTravelAdminTest extends TestBaseResource {

    @Test
    public void testDeleteAdmin() {
        testDelete();
        testDeleteReferenced();
    }

    private void testDelete() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewAdminsPage();

        viewTravelAdminPage.isDeleteButtonVisible();
        viewTravelAdminPage.clickDeleteButton(1);
        verifyToastMessage(registerAdminPage.getToastMessage(), "Admin deleted");
        logOut();
        navigationBar.isVisible();
    }

    private void testDeleteReferenced() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewAdminsPage();

        viewTravelAdminPage.isDeleteButtonVisible();
        viewTravelAdminPage.clickDeleteButton(0);
        verifyToastMessage(registerAdminPage.getToastMessage(), "The entity is referenced by at least one other entity.");
        logOut();
        navigationBar.isVisible();
    }




    private void openViewAdminsPage() {
        navigationBar.clickTravelAdminsNavigationButton();
        navigationBar.clickViewAdminsButton();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }
}
