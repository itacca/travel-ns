package com.tim2.e2e.selenium.schedule;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.NEW_SCHEDULE_URL_ADDRESS;
import static com.tim2.e2e.selenium.TestData.NUM_OF_SCHEDULE_ITEMS;
import static com.tim2.e2e.selenium.TestData.SCHEDULE_URL_ADDRESS;
import static junit.framework.TestCase.assertEquals;

public class DeleteScheduleItem  extends TestBaseResource {

    @Test
    private void testSuccessfullyDeleteScheduleItem() {
        login(TestData.SYSTEM_ADMINISTRATOR_EMAIL, TestData.SYSTEM_ADMINISTRATOR_PASSWORD);
        navigateScheduleDetailPage();

        verifyCurrentUrlAddress(SCHEDULE_URL_ADDRESS);

        deleteLastItem();
        logout();
    }

    private void deleteLastItem() {
        int numberOfItemsStart = detailSchedulePage.getScheduleItemsCount();

        detailSchedulePage.getDeleteOnLastItem().click();

        int numberOfItemsEnd = detailSchedulePage.getScheduleItemsCount();

        assertEquals(numberOfItemsStart - 1, numberOfItemsEnd);
    }

    private void navigateScheduleDetailPage() {
        navigationBar.clickCityLinesNavigationButton();
        navigationBar.clickViewCityLinesButton();

        cityLineListPage.getLinkToFirstSchedule().click();
        detailSchedulePage.getLastRowInTable();
    }
}
