package com.tim2.e2e.selenium.pricelist;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;

public class ViewHistoryTest extends TestBaseResource {

    @Test
    public void testViewHistory() {
        testSuccessfulView();
    }

    private void testSuccessfulView() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewHistoryPage();
        waitForAddressToBe("http://localhost:4200/#/price-list-history");
        findSelectByCss("mat-select[placeholder='Choose price list']");
        viewHistoryPage.clickOptionOne();

        viewHistoryPage.checkIfEnabledLoadPriceListButton();
        viewHistoryPage.clickLoadPriceListButton();

        findSelectByCss("mat-select[placeholder='Choose ticket']");
        viewHistoryPage.clickOptionTwo();

        viewHistoryPage.checkIfEnabledLoadTicketButton();
        viewHistoryPage.clickLoadTicketButton();

        viewHistoryPage.checkIfVisible();

        logOut();
        navigationBar.isVisible();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    protected void openViewHistoryPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickViewHistoryButton();
    }
}
