package com.tim2.e2e.selenium.cityline;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.CITY_LINE_URL_ADDRESS;

public class ViewCityLineDetailsTest extends TestBaseResource {

    @Test
    public void testViewCityLineDetails() {
        testIfSuccessful();
        openCityLinesListPage();
        verifyCurrentUrlAddress(CITY_LINE_URL_ADDRESS);
    }

    private void testIfSuccessful() {
        openCityLinesListPage();
        verifyCurrentUrlAddress(CITY_LINE_URL_ADDRESS);
        cityLineListPage.clickShowButton();
        cityLineDetailsPage.checkIfTitleIsVisible();
        cityLineDetailsPage.checkIfLineNameIsVisible();
    }

    private void openCityLinesListPage() {
        navigationBar.clickCityLinesNavigationButton();
        navigationBar.clickViewCityLinesButton();
    }
}
