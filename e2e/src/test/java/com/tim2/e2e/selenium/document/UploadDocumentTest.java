package com.tim2.e2e.selenium.document;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tim2.e2e.selenium.TestData.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;

public class UploadDocumentTest extends TestBaseResource {

    @Test
    public void testUploadDocumentPage() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(emails, passwords, this::testShouldDisplaySuccessToastAfterStudentDocumentIsUploaded);
        runWithCredentials(emails, passwords, this::testShouldDisplaySuccessToastAfterSeniorDocumentIsUploaded);
        runWithCredentials(emails, passwords, this::testUploadDocumentButtonShouldBeDisabledIfDocumentTypeIsNotSelected);
        runWithCredentials(emails, passwords, this::testUploadDocumentButtonShouldBeDisabledIfFileIsNotSelected);
    }

    private void testUploadDocumentButtonShouldBeDisabledIfFileIsNotSelected() {
        navigateToUploadDocumentPage();

        uploadDocumentPage.clickSeniorRadioButton();

        assertFalse("Upload button should be disabled", uploadDocumentPage.getUploadButton().isEnabled());
    }

    private void testUploadDocumentButtonShouldBeDisabledIfDocumentTypeIsNotSelected() {
        navigateToUploadDocumentPage();

        uploadDocumentPage.setUploadPath();

        assertFalse("Upload button should be disabled", uploadDocumentPage.getUploadButton().isEnabled());
    }

    private void testShouldDisplaySuccessToastAfterSeniorDocumentIsUploaded() {
        navigateToUploadDocumentPage();

        uploadDocumentPage.clickStudentRadioButton();
        uploadDocumentPage.setUploadPath();
        uploadDocumentPage.clickUploadButton();

        assertThat("Toast should have 'Success' as title", toastr.getToastTitle().getText(),
                containsString("Success"));
        assertThat("Toast should have 'Document uploaded' as message", toastr.getToastMessage().getText(),
                containsString("Document uploaded"));
    }

    private void testShouldDisplaySuccessToastAfterStudentDocumentIsUploaded() {
        navigateToUploadDocumentPage();

        uploadDocumentPage.clickStudentRadioButton();
        uploadDocumentPage.setUploadPath();
        uploadDocumentPage.clickUploadButton();

        assertThat("Toast should have 'Success' as title", toastr.getToastTitle().getText(),
                containsString("Success"));
        assertThat("Toast should have 'Document uploaded' as message", toastr.getToastMessage().getText(),
                containsString("Document uploaded"));
    }

    private void navigateToUploadDocumentPage() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickUploadDocumentButton();
    }

    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TRAVEL_ADMINISTRATOR_EMAIL);
        emails.add(SYSTEM_ADMINISTRATOR_EMAIL);
        emails.add(VERIFIER_EMAIL);
        emails.add(NIKOLA_EMAIL);
        return emails;
    }

    public List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TRAVEL_ADMINISTRATOR_PASSWORD);
        passwords.add(SYSTEM_ADMINISTRATOR_PASSWORD);
        passwords.add(VERIFIER_PASSWORD);
        passwords.add(NIKOLA_PASSWORD);
        return passwords;
    }
}
