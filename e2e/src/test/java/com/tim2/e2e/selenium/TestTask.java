package com.tim2.e2e.selenium;

@FunctionalInterface
public interface TestTask {

    void execute();
}
