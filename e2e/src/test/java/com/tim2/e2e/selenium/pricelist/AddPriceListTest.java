package com.tim2.e2e.selenium.pricelist;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;

public class AddPriceListTest extends TestBaseResource {

    @Test
    public void testAddPriceList() {
        testSuccessfullyAddPriceList();
        testEnteringInvalidDateInterval();
        testEnteringEndDateInThePast();
        testEnteringStartDateInThePast();
    }

    private void testSuccessfullyAddPriceList() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAddPriceListPage();
        addPriceList();
        waitForAddressToBe("http://localhost:4200/#/pricelist");
        openViewPriceListPage();
        logOut();
        navigationBar.isVisible();
    }

    private void testEnteringInvalidDateInterval() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAddPriceListPage();
        addPriceListInvalidInterval();
        addPriceListPage.getToastMessage();
        verifyToastMessage(addPriceListPage.getToastMessage(), "Unable to add price list");
        logOut();
        navigationBar.isVisible();
    }

    private void testEnteringStartDateInThePast() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAddPriceListPage();
        addPriceListInvalidStartDate();
        addPriceListPage.getToastMessage();
        verifyToastMessage(addPriceListPage.getToastMessage(), "Unable to add price list");
        logOut();
        navigationBar.isVisible();
    }

    private void testEnteringEndDateInThePast() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAddPriceListPage();
        addPriceListInvalidStartDate();
        addPriceListPage.getToastMessage();
        verifyToastMessage(addPriceListPage.getToastMessage(), "Unable to add price list");
        logOut();
        navigationBar.isVisible();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    private void addPriceList() {
        addPriceListPage.enterStartDate(PRICE_LIST_START_DATE);
        addPriceListPage.enterEndDate(PRICE_LIST_END_DATE);
        addPriceListPage.checkIfEnabledAddButtonButton();
        addPriceListPage.clickAddButton();
    }

    private void addPriceListInvalidInterval() {
        addPriceListPage.enterStartDate(PRICE_LIST_END_DATE);
        addPriceListPage.enterEndDate(PRICE_LIST_START_DATE);
        addPriceListPage.checkIfEnabledAddButtonButton();
        addPriceListPage.clickAddButton();
    }

    private void addPriceListInvalidStartDate() {
        addPriceListPage.enterStartDate(PRICE_LIST_PAST_DATE);
        addPriceListPage.enterEndDate(PRICE_LIST_END_DATE);
        addPriceListPage.checkIfEnabledAddButtonButton();
        addPriceListPage.clickAddButton();
    }

    private void addPriceListInvalidEndDate() {
        addPriceListPage.enterStartDate(PRICE_LIST_START_DATE);
        addPriceListPage.enterEndDate(PRICE_LIST_PAST_DATE);
        addPriceListPage.checkIfEnabledAddButtonButton();
        addPriceListPage.clickAddButton();
    }


    protected void openAddPriceListPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickAddPriceListButton();
    }

    protected void openViewPriceListPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickViewPriceListsButton();
    }
}
