package com.tim2.e2e.selenium.ticket;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class ValidateTicketTest extends TestBaseResource {

    @Test
    public void testTicketValidationPage() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(emails, passwords, this::testTicketInformationIsDisplayedWhenTicketIsValid);
        runWithCredentials(emails, passwords, this::testTicketInformationIsDisplayedWhenTicketIsInvalid);
        runWithCredentials(emails, passwords, this::testValidationLabelDisplaysTicketIsValidWhenTicketIsValid);
        runWithCredentials(emails, passwords, this::testValidationLabelDisplaysTicketIsInvalidWhenTicketIsInvalid);
        runWithCredentials(emails, passwords, this::testPageTitleIsDisplayedWhenTicketIsValid);
    }

    private void testPageTitleIsDisplayedWhenTicketIsValid() {
        driver.navigate().to(TestData.VALIDATE_VALID_TICKET_URL_ADDRESS);

        assertThat("Page title is displayed", validateBoughtTicketsPage.getPageTitle().getText(),
                containsString("Ticket validation"));
    }

    private void testValidationLabelDisplaysTicketIsValidWhenTicketIsValid() {
        driver.navigate().to(TestData.VALIDATE_VALID_TICKET_URL_ADDRESS);

        assertThat("Validation label contains 'Ticket is valid'",
                validateBoughtTicketsPage.getTicketValidity().getText(), containsString("Ticket is valid"));
    }

    private void testValidationLabelDisplaysTicketIsInvalidWhenTicketIsInvalid() {
        driver.navigate().to(TestData.VALIDATE_INVALID_TICKET_URL_ADDRESS);

        assertThat("Validation label contains 'Ticket is invalid'",
                validateBoughtTicketsPage.getTicketValidity().getText(), containsString("Ticket is invalid"));
    }

    private void testTicketInformationIsDisplayedWhenTicketIsValid() {
        driver.navigate().to(TestData.VALIDATE_VALID_TICKET_URL_ADDRESS);

        DateTime currentTime = DateTime.now(DateTimeZone.UTC);
        String visibleCurrentTime = currentTime.toString("MMM dd, YYYY");

        assertThat("Date of purchase is displayed", validateBoughtTicketsPage.getTicketDateOfPurchase().getText(),
                containsString(visibleCurrentTime));
        assertThat("Ticket type is displayed", validateBoughtTicketsPage.getTicketType().getText(),
                containsString("SENIOR"));
        assertThat("Ticket duration is displayed", validateBoughtTicketsPage.getTicketDuration().getText(),
                containsString("DAILY"));
        assertThat("Ticket price is displayed", validateBoughtTicketsPage.getTicketPrice().getText(),
                containsString("Price: €123.11"));
    }

    private void testTicketInformationIsDisplayedWhenTicketIsInvalid() {
        driver.navigate().to(TestData.VALIDATE_INVALID_TICKET_URL_ADDRESS);

        assertThat("Date of purchase is displayed", validateBoughtTicketsPage.getTicketDateOfPurchase().getText(),
                containsString("Dec 12, 2010"));
        assertThat("Ticket type is displayed", validateBoughtTicketsPage.getTicketType().getText(),
                containsString("SENIOR"));
        assertThat("Ticket duration is displayed", validateBoughtTicketsPage.getTicketDuration().getText(),
                containsString("DAILY"));
        assertThat("Ticket price is displayed", validateBoughtTicketsPage.getTicketPrice().getText(),
                containsString("Price: €552.22"));
    }

    private List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TestData.SYSTEM_ADMINISTRATOR_PASSWORD);
        passwords.add(TestData.TRAVEL_ADMINISTRATOR_PASSWORD);
        return passwords;
    }

    private List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TestData.SYSTEM_ADMINISTRATOR_EMAIL);
        emails.add(TestData.TRAVEL_ADMINISTRATOR_EMAIL);
        return emails;
    }
}
