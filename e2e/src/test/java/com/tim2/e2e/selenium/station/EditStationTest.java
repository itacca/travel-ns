package com.tim2.e2e.selenium.station;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class EditStationTest extends TestBaseResource {

    private static final Integer SORT_BY_NAME_OPTION = 1;
    private static final Integer SORT_ASCENDING_OPTION = 0;

    @Test
    public void editStationPageTest() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(TestData.SYSTEM_ADMINISTRATOR_EMAIL, TestData.SYSTEM_ADMINISTRATOR_PASSWORD, this::createTestStation);
        runWithCredentials(emails, passwords, this::testShouldDisplayPageTitle);
        runWithCredentials(emails, passwords, this::testShouldDisplayStationName);
        runWithCredentials(emails, passwords, this::testShouldDisplayLatitude);
        runWithCredentials(emails, passwords, this::testShouldDisplayLongitude);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfNoDataIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfStationNameIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfLatitudeIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfLongitudeIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeDisabledIfLongitudeAndLatitudeIsEntered);
        runWithCredentials(emails, passwords, this::testSaveButtonShouldBeEnabledIfAllDataIsEntered);
        runWithCredentials(emails, passwords, this::testToastSuccessShouldBeShownWhenSaveButtonIsClicked);
        runWithCredentials(emails, passwords, this::testEditedStationAppearsWithUpdatedData);
        runWithCredentials(TestData.SYSTEM_ADMINISTRATOR_EMAIL, TestData.SYSTEM_ADMINISTRATOR_PASSWORD, this::deleteTestStation);
    }

    private void deleteTestStation() {
        navigationBar.clickStationNavigationButton();
        navigationBar.clickViewStationsButton();

        pageableControls.setSortByFieldSelect(SORT_BY_NAME_OPTION);
        pageableControls.setSortOrderSelect(SORT_ASCENDING_OPTION);

        viewStationsPage.clickDeleteButton(0);
    }

    private void testEditedStationAppearsWithUpdatedData() {
        navigationBar.clickStationNavigationButton();
        navigationBar.clickViewStationsButton();

        pageableControls.setSortByFieldSelect(SORT_BY_NAME_OPTION);
        pageableControls.setSortOrderSelect(SORT_ASCENDING_OPTION);

        assertThat("Edited station should display name", viewStationsPage.getStation(0).getText(),
                containsString("BBB - Edited and saved"));
        assertThat("Edited station should display latitude", viewStationsPage.getStation(0).getText(),
                containsString("Latitude: 321321"));
        assertThat("Edited station should display longitude", viewStationsPage.getStation(0).getText(),
                containsString("Longitude: 321321"));
    }

    private void testToastSuccessShouldBeShownWhenSaveButtonIsClicked() {
        navigateToEditStationPage();

        createStationPage.setNameInputText("BBB - Edited and saved");
        createStationPage.setLatitudeInputText("321321");
        createStationPage.setLongitudeInputText("321321");
        createStationPage.clickSaveButton();

        assertThat("Toast success title is shown", toastr.getToastTitle().getText(),
                containsString("Success"));
        assertThat("Toast success message is shown", toastr.getToastMessage().getText(),
                containsString("Station updated"));
    }

    private void testSaveButtonShouldBeEnabledIfAllDataIsEntered() {
        navigateToEditStationPage();

        createStationPage.setNameInputText("Test station name");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("123123");

        assertTrue("Save button should be enabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfLongitudeAndLatitudeIsEntered() {
        navigateToEditStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("123123");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfLongitudeIsEntered() {
        navigateToEditStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("");
        createStationPage.setLongitudeInputText("123123");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfLatitudeIsEntered() {
        navigateToEditStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfStationNameIsEntered() {
        navigateToEditStationPage();

        createStationPage.setNameInputText("Test station name");
        createStationPage.setLatitudeInputText("");
        createStationPage.setLongitudeInputText("");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testSaveButtonShouldBeDisabledIfNoDataIsEntered() {
        navigateToEditStationPage();

        createStationPage.setNameInputText("");
        createStationPage.setLatitudeInputText("");
        createStationPage.setLongitudeInputText("");

        assertFalse("Save button should be disabled", createStationPage.getSaveButton().isEnabled());
    }

    private void testShouldDisplayLongitude() {
        navigateToEditStationPage();

        assertThat("Should display longitude", editStationPage.getLongitudeInput().getAttribute("value"),
                containsString("123123"));
    }

    private void testShouldDisplayLatitude() {
        navigateToEditStationPage();

        assertThat("Should display latitude", editStationPage.getLatitudeInput().getAttribute("value"),
                containsString("123123"));
    }

    private void testShouldDisplayStationName() {
        navigateToEditStationPage();

        assertThat("Should display station name", editStationPage.getNameInput().getAttribute("value"),
                containsString("A - Edit Station Test"));
    }

    private void testShouldDisplayPageTitle() {
        navigateToEditStationPage();

        assertThat("Should display page title", editStationPage.getPageTitle().getText(),
                containsString("Edit station"));
    }

    private void createTestStation() {
        navigateToCreateStationPage();
        createStationPage.setNameInputText("A - Edit Station Test");
        createStationPage.setLatitudeInputText("123123");
        createStationPage.setLongitudeInputText("123123");
        createStationPage.clickSaveButton();
    }

    private void navigateToCreateStationPage() {
        navigationBar.clickStationNavigationButton();
        navigationBar.clickCreateStationButton();
    }

    public void navigateToEditStationPage() {
        navigationBar.clickStationNavigationButton();
        navigationBar.clickViewStationsButton();

        pageableControls.setSortByFieldSelect(SORT_BY_NAME_OPTION);
        pageableControls.setSortOrderSelect(SORT_ASCENDING_OPTION);
        viewStationsPage.clickEditButton(0);
    }

    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TestData.TRAVEL_ADMINISTRATOR_EMAIL);
        emails.add(TestData.SYSTEM_ADMINISTRATOR_EMAIL);
        return emails;
    }

    public List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TestData.TRAVEL_ADMINISTRATOR_PASSWORD);
        passwords.add(TestData.SYSTEM_ADMINISTRATOR_PASSWORD);
        return passwords;
    }
}
