package com.tim2.e2e.selenium;

public class WebMessages {

    public static final String EDIT_CITY_LINE_SUCCESS = "City line has been updated.";

    public static String ACCOUNT_REGISTRATION_ERROR = "The request contains invalid data.";

    public static final String ADMIN_REGISTRATION_ERROR = "Unable to register travel admin";

    public static final String LOGIN_ERROR = "Wrong username or password or account is not validated.";

    public static final String PASSWORDS_DO_NOT_MATCH = "passwords don't match";

    public static final String PASSWORD_LENGTH_ERROR = "password needs to be at least 8 characters long, your current password is 5";

    public static final String TOAST_SUCCESS = "Success";

    public static final String CREATE_CITY_LINE_SUCCESS = "City line has been created.";
}
