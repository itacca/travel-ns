package com.tim2.e2e.selenium;

public class TestData {

    public static final String SYSTEM_ADMINISTRATOR_EMAIL = "sys@ns.com";
    public static final String SYSTEM_ADMINISTRATOR_PASSWORD = "password";
    public static final String SYSTEM_ADMINISTRATOR_FIRST_NAME = "System";
    public static final String SYSTEM_ADMINISTRATOR_LAST_NAME = "Admin";
    public static final String SYSTEM_ADMINISTRATOR_DATE_OF_BIRTH = "1/30/1996";

    public static final String NIKOLA_EMAIL = "nikola@ns.com";
    public static final String NIKOLA_PASSWORD = "password";
    public static final String NIKOLA_FIRST_NAME = "Nikola";
    public static final String NIKOLA_LAST_NAME = "Zeljkovic";
    public static final String NIKOLA_DATE_OF_BIRTH = "1/30/1996";

    public static final String TRAVEL_ADMINISTRATOR_EMAIL = "adm@ns.com";
    public static final String TRAVEL_ADMINISTRATOR_PASSWORD = "password";

    public static final String PASSENGER_EMAIL = "pass@ns.com";
    public static final String PASSENGER_PASSWORD = "password";

    public static final String VERIFIER_EMAIL = "ver@ns.com";
    public static final String VERIFIER_PASSWORD = "password";

    public static final String ACCOUNT_REGISTRATION_EMAIL = "nikzeljkovic@gmail.com";
    public static final String ACCOUNT_INVALID_DATE_OF_BIRTH = "1/30/2500";

    public static String ACCOUNT_REGISTRATION_INVALID_EMAIL = "invalid_email_format";
    public static final String ACCOUNT_REGISTRATION_PASSWORD = "password";
    public static final String ACCOUNT_REGISTRATION_SHORT_PASSWORD = "short";
    public static final String ACCOUNT_REGISTRATION_FIRST_NAME = "Nikola";
    public static final String ACCOUNT_REGISTRATION_LAST_NAME = "Zeljkovic";
    public static final String ACCOUNT_REGISTRATION_DATE_OF_BIRTH = "1/30/1996";
    public static final String ACCOUNT_REGISTRATION_DATE_OF_BIRTH_IN_FUTURE = "1/30/2996";

    public static final String ACCOUNT_FIRST_NAME_UPDATED = "FirstNameUpdated";
    public static final String ACCOUNT_LAST_NAME_UPDATED = "LastNameUpdated";
    public static final String ACCOUNT_PASSWORD_UPDATED = "NewPassword";
    public static final String ACCOUNT_DATE_OF_BIRTH_UPDATED = "1/30/1998";

    public static final String CITY_LINE_URL_ADDRESS = "http://localhost:4200/#/city-lines";
    public static final String CITY_LINE_BUS_TYPE_NAME3 = "Linija 3";
    public static final String CITY_LINE_VALID_BUS_TYPE = "BUS";
    public static final String FIRST_CITY_LINE_NAME = "Linija 1";
    public static final int NUM_OF_CITY_LINES = 6;
    public static final int NUM_OF_BUS_TYPE = 3;
    public static final String PLATE_NUMBER = "NS05949";
    public static final String EXISTING_PLATE_NUMBER = "NS-003-TR";
    public static final String PRICE_LIST_START_DATE = "1/30/2026";
    public static final String PRICE_LIST_END_DATE = "1/30/2027";
    public static final String PRICE_LIST_UPDATE_DATE = "6/15/2020";
    public static final String PRICE_LIST_PAST_DATE = "6/15/2018";

    public static final String PRICE = "99";
    public static final String INVALID_PRICE = "-99";

    public static final String CREATE_CITY_LINE_NAME = "Create city line test name";
    public static final String EDIT_CITY_LINE_URL_ADDRESS = "http://localhost:4200/#/edit-city-line";
    public static final String EDIT_CITY_LINE_NAME = "Edit city line test name";

    public static final Integer SYS_ADMIN_BOUGHT_TICKETS_COUNT = 2;
    public static final Integer TRAVEL_ADMIN_BOUGHT_TICKETS_COUNT = 2;
    public static final Integer REGISTERED_USER_BOUGHT_TICKETS_COUNT = 7;

    public static final String VALIDATE_INVALID_TICKET_URL_ADDRESS = "http://localhost:4200/#/validate-ticket/7";
    public static final String VALIDATE_VALID_TICKET_URL_ADDRESS = "http://localhost:4200/#/validate-ticket/6";

    public static final String SCHEDULE_URL_ADDRESS = "http://localhost:4200/#/schedule/1";
    public static final String NEW_SCHEDULE_URL_ADDRESS = "http://localhost:4200/#/new-schedule/1/";
    public static final int NUM_OF_SCHEDULE_ITEMS = 4;
}
