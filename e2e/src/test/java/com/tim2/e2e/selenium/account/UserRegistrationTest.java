package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;
import static com.tim2.e2e.selenium.WebMessages.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.AssertJUnit.assertFalse;

public class UserRegistrationTest extends TestBaseResource {

    @Test
    public void testUserRegistration() {
        testEnteringInvalidEmailFormat();
        testEnteringDateInFuture();
        testEnteringShortPassword();
        testEnteringPasswordsThatDoNotMatch();
        testRegisteringWithEmailThatExists();
        testSuccessfulRegistration();
    }

    private void testRegisteringWithEmailThatExists() {
        openRegistrationPage();
        enterUserInformation(SYSTEM_ADMINISTRATOR_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_PASSWORD,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        registerPage.clickRegisterButton();

        assertThat("Toast title has error.", toastr.getToastTitle().getText(), containsString("Error"));
        assertThat("Toast message has error.", toastr.getToastMessage().getText(),
                containsString(ACCOUNT_REGISTRATION_ERROR));
    }

    private void testEnteringPasswordsThatDoNotMatch() {
        openRegistrationPage();
        enterUserInformation(ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_PASSWORD_UPDATED,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        assertThat("Passwords don't match error message is visible.", registerPage.getPasswordErrorMessage().getText(), is(PASSWORDS_DO_NOT_MATCH));
        assertFalse("Register button is not visible.", registerPage.isRegisterButtonVisible());
    }

    private void testEnteringShortPassword() {
        openRegistrationPage();
        enterUserInformation(ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_SHORT_PASSWORD, ACCOUNT_REGISTRATION_SHORT_PASSWORD,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        assertThat("Short password error message is visible.", registerPage.getPasswordErrorMessage().getText(), is(PASSWORD_LENGTH_ERROR));
        assertFalse("Register button is not visible.", registerPage.isRegisterButtonVisible());
    }

    private void testEnteringDateInFuture() {
        openRegistrationPage();
        enterUserInformation(ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_PASSWORD,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH_IN_FUTURE, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        assertFalse("Register button is not visible.", registerPage.isRegisterButtonVisible());
    }

    private void testEnteringInvalidEmailFormat() {
        openRegistrationPage();
        enterUserInformation(ACCOUNT_REGISTRATION_INVALID_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_PASSWORD,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        assertFalse("Register button is not visible.", registerPage.isRegisterButtonVisible());
    }

    private void testSuccessfulRegistration() {
        enterUserInformation(ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD, ACCOUNT_REGISTRATION_PASSWORD,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        registerPage.clickRegisterButton();
        driver.navigate().to("http://localhost:4200");
        login(ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD);
        navigationBar.clickAccountNavigationButton();
    }

    private void enterUserInformation(String email, String password, String confirmedPassword, String dateOfBirth, String firstName, String lastName) {
        registerPage.enterEmail(email);
        registerPage.enterPassword(password);
        registerPage.enterConfirmPassword(confirmedPassword);
        registerPage.enterDate(dateOfBirth);
        registerPage.enterFirstName(firstName);
        registerPage.enterLastName(lastName);
    }

    private void openRegistrationPage() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickAccountRegisterButton();
    }
}
