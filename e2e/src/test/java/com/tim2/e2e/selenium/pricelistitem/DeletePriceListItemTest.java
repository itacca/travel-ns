package com.tim2.e2e.selenium.pricelistitem;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;

public class DeletePriceListItemTest extends TestBaseResource {

    @Test
    public void testDeletePriceListItem() {
        testSuccessfulDeletePriceListItem();
    }

    private void testSuccessfulDeletePriceListItem() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.clickDeletePriceListItemButton();

        verifySuccessToast(viewPriceListsPage.getToastMessage(), "Price list item successfuly deleted");

        logOut();
        navigationBar.isVisible();
    }

    private void logOut() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    protected void openViewPriceListPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickViewPriceListsButton();
    }
}
