package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.TestBaseResource;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;
import static com.tim2.e2e.selenium.WebMessages.LOGIN_ERROR;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoginTest extends TestBaseResource {

    @Test
    public void testUserLogin() {
        testInvalidCredentialCombination();
        testSuccessfulLogin();
    }

    private void testInvalidCredentialCombination() {
        openLoginPage();
        enterEmailAndPassword(SYSTEM_ADMINISTRATOR_EMAIL, ACCOUNT_PASSWORD_UPDATED);
        loginPage.clickSignInButton();
        WebElement errorMessage = loginPage.getLoginErrorMessage();
        assertThat("Error text", errorMessage.getText(), is(LOGIN_ERROR));
    }

    private void testSuccessfulLogin() {
        enterEmailAndPassword(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        loginPage.clickSignInButton();
        navigationBar.isVisible();
        navigationBar.clickAccountNavigationButton();
        logOutIsDisplayed();
    }

    private void logOutIsDisplayed() {
        assertThat("Log out is displayed.", navigationBar.isLogOutDisplayed(), is(true));
    }

    private void enterEmailAndPassword(String email, String password) {
        loginPage.enterEmail(email);
        loginPage.enterPassword(password);
    }

    private void openLoginPage() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickAccountLogInButton();
    }
}
