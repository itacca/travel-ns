package com.tim2.e2e.selenium;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.tim2.e2e.selenium.account.*;
import com.tim2.e2e.selenium.boughtticket.BoughtTicketsPage;
import com.tim2.e2e.selenium.account.LoginPage;
import com.tim2.e2e.selenium.account.RegisterPage;
import com.tim2.e2e.selenium.cityline.CityLineDetailsPage;
import com.tim2.e2e.selenium.cityline.CityLineListPage;
import com.tim2.e2e.selenium.cityline.CreateCityLinePage;
import com.tim2.e2e.selenium.cityline.EditCityLinePage;
import com.tim2.e2e.selenium.document.UploadDocumentPage;
import com.tim2.e2e.selenium.document.VerifyDocumentPage;
import com.tim2.e2e.selenium.pricelist.AddPriceListPage;
import com.tim2.e2e.selenium.pricelist.ViewHistoryPage;
import com.tim2.e2e.selenium.pricelist.ViewPriceListsPage;
import com.tim2.e2e.selenium.schedule.DetailSchedulePage;
import com.tim2.e2e.selenium.schedule.NewSchedulePage;
import com.tim2.e2e.selenium.station.CreateStationPage;
import com.tim2.e2e.selenium.station.EditStationPage;
import com.tim2.e2e.selenium.station.ViewStationsPage;
import com.tim2.e2e.selenium.ticket.ValidateBoughtTicketsPage;
import com.tim2.e2e.selenium.vehicle.CreateVehiclePage;
import com.tim2.e2e.selenium.vehicle.TrackVehiclePage;
import com.tim2.e2e.selenium.vehicle.VehicleViewPage;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.List;
import java.util.stream.IntStream;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class TestBaseResource {

    protected WebDriver driver;

    protected NavigationBar navigationBar;

    protected PageableControls pageableControls;

    protected LoginPage loginPage;

    protected RegisterPage registerPage;

    protected RegisterAdminPage registerAdminPage;

    protected ViewTravelAdminPage viewTravelAdminPage;

    protected AccountInformationPage accountInformationPage;

    protected CityLineListPage cityLineListPage;

    protected AddPriceListPage addPriceListPage;

    protected ViewHistoryPage viewHistoryPage;

    protected ViewPriceListsPage viewPriceListsPage;

    protected BoughtTicketsPage boughtTicketsPage;

    protected CreateCityLinePage createCityLinePage;

    protected CityLineDetailsPage cityLineDetailsPage;

    protected CreateVehiclePage createVehiclePage;

    protected VehicleViewPage vehicleViewPage;

    protected EditCityLinePage editCityLinePage;

    protected ValidateBoughtTicketsPage validateBoughtTicketsPage;

    protected DetailSchedulePage detailSchedulePage;

    protected NewSchedulePage newSchedulePage;

    protected TrackVehiclePage trackVehiclePage;

    protected UploadDocumentPage uploadDocumentPage;

    protected VerifyDocumentPage verifyDocumentPage;

    protected CreateStationPage createStationPage;

    protected EditStationPage editStationPage;

    protected ViewStationsPage viewStationsPage;

    protected Toastr toastr;

    @BeforeMethod
    public void setupSelenium() {
        resetDatabase();
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("http://localhost:4200");

        navigationBar = PageFactory.initElements(driver, NavigationBar.class);
        toastr = PageFactory.initElements(driver, Toastr.class);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
        registerPage = PageFactory.initElements(driver, RegisterPage.class);
        accountInformationPage = PageFactory.initElements(driver, AccountInformationPage.class);
        cityLineListPage = PageFactory.initElements(driver, CityLineListPage.class);
        registerAdminPage = PageFactory.initElements(driver, RegisterAdminPage.class);
        viewTravelAdminPage = PageFactory.initElements(driver, ViewTravelAdminPage.class);
        addPriceListPage = PageFactory.initElements(driver, AddPriceListPage.class);
        viewHistoryPage = PageFactory.initElements(driver, ViewHistoryPage.class);
        viewPriceListsPage = PageFactory.initElements(driver, ViewPriceListsPage.class);
        boughtTicketsPage = PageFactory.initElements(driver, BoughtTicketsPage.class);
        pageableControls = PageFactory.initElements(driver, PageableControls.class);
        createCityLinePage = PageFactory.initElements(driver, CreateCityLinePage.class);
        cityLineDetailsPage = PageFactory.initElements(driver, CityLineDetailsPage.class);
        vehicleViewPage = PageFactory.initElements(driver, VehicleViewPage.class);
        createVehiclePage = PageFactory.initElements(driver, CreateVehiclePage.class);
        editCityLinePage = PageFactory.initElements(driver, EditCityLinePage.class);
        validateBoughtTicketsPage = PageFactory.initElements(driver, ValidateBoughtTicketsPage.class);
        detailSchedulePage = PageFactory.initElements(driver, DetailSchedulePage.class);
        newSchedulePage = PageFactory.initElements(driver, NewSchedulePage.class);
        trackVehiclePage = PageFactory.initElements(driver, TrackVehiclePage.class);
        uploadDocumentPage = PageFactory.initElements(driver, UploadDocumentPage.class);
        verifyDocumentPage = PageFactory.initElements(driver, VerifyDocumentPage.class);
        createStationPage = PageFactory.initElements(driver, CreateStationPage.class);
        editStationPage = PageFactory.initElements(driver, EditStationPage.class);
        viewStationsPage = PageFactory.initElements(driver, ViewStationsPage.class);
    }

    @AfterMethod
    public void closeSelenium() {
        resetDatabase();
        driver.quit();
    }

    public void login(String email, String password) {
        if (navigationBar.isVisible()) {
            navigationBar.clickAccountNavigationButton();
            navigationBar.clickAccountLogInButton();
        }
        loginPage.enterEmail(email);
        loginPage.enterPassword(password);
        loginPage.clickSignInButton();
    }

    public void logout() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    public void verifyTextIsLoaded(WebElement element, String text) {
        try {
            (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.textToBePresentInElementValue(
                            element, text));
        } catch (TimeoutException e) {
            assertThat("Element text is not loaded.", element.getText(), is(text));
        }
    }

    public void verifyCurrentUrlAddress(String expectedAddress) {
        assertEquals(expectedAddress, driver.getCurrentUrl());
    }

    public void verifyToastMessage(WebElement toast, String expectedError) {
        assertTrue("Error message", toast.getText().contains(expectedError));
        toast.click();
    }

    public void verifySuccessToast(WebElement toast, String expectedError) {
        assertTrue("Success message", toast.getText().contains(expectedError));
        toast.click();
    }

    public void waitForAddressToBe(String expectedAddress) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.urlToBe(expectedAddress));
    }

    public void findSelectByXpath(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public void findSelectByCss(String id) {
        driver.findElement(By.cssSelector(id)).click();
    }

    public void runWithCredentials(String email, String password, TestTask testTask) {
        login(email, password);
        testTask.execute();
        logout();
    }

    public void runWithCredentials(List<String> emails, List<String> passwords, TestTask testTask) {
        assertEquals("Emails and passwords list should have the same size", emails.size(), passwords.size());
        IntStream.range(0, emails.size()).forEach(index ->
                runWithCredentials(emails.get(index), passwords.get(index), testTask));
    }

    private void resetDatabase() {
        try {
            Unirest
                    .put("http://localhost:9190/api/v1/test-data")
                    .header("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJlMmVAbnMuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9FMkVfVEVTVCJdLCJqdGkiOiJmY2ZiYTRmMy1hODk1LTQzYzQtYWU1Ny04MDAyNTJjYWMzNmYiLCJjbGllbnRfaWQiOiJ3ZWItY2xpZW50Iiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl19.Ys7uQMEPrueZZFQuXpDJzIfAryCgaGbuw8nOAiaMNX4v2DX0ppgUR0xayukk9mBOsSFdUnqQVqVODQAMz0GBvr5FXyMgnhdBU5b4sm8Y9XgG8RIjVWSvcc_R_t2QL6y8frIXrKokoAC8BmUaLPDbgGcSbzg4e8Ciy7rriuJniApNCJhlzXCPA49YCltm0gXHHrKr1kw1TR7SsGxOUh9IyXXKoYdieez-ldeorL_8KkK-vS6WVAoW-qYdnmi5UI8fSx6j6IjJhTaDDgqgxPYZkmG0-GsJKHMca09Dc82ptaDSRFFJSumJy2uI-mmSkzRKoc-sqRF4j0WBaAlxhC86ow")
                    .asString();
        } catch (UnirestException e) {
            fail("Unable to reset database.");
        }
    }
}
