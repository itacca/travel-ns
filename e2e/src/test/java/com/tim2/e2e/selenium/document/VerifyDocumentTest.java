package com.tim2.e2e.selenium.document;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tim2.e2e.selenium.TestData.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class VerifyDocumentTest extends TestBaseResource {

    @Test
    public void testVerifyDocumentPage() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(emails, passwords, this::testShouldDisplayPageTitle);
        runWithCredentials(emails, passwords, this::uploadTestDocument);
        runWithCredentials(emails, passwords, this::testDocumentImageShouldBeVisible);
        runWithCredentials(emails, passwords, this::testDocumentTypeShouldBeVisible);
        runWithCredentials(emails, passwords, this::testDocumentFirstNameShouldBeVisible);
        runWithCredentials(emails, passwords, this::testDocumentLastNameShouldBeVisible);
        runWithCredentials(emails, passwords, this::testDocumentDateOfBirthShouldBeVisible);
        runWithCredentials(emails, passwords, this::testShouldDisplayADocumentForEachUser);
        runWithCredentials(emails, passwords, this::testShouldDisplaySuccessToastWhenVerifyIsClicked);
        runWithCredentials(emails, passwords, this::testShouldDisplayNoDocuments);
    }

    private void testShouldDisplayNoDocuments() {
        navigateToVerifyDocumentPage();

        assertThat("There should be no documents", verifyDocumentPage.getDocuments().size(), is(0));
    }

    private void testShouldDisplaySuccessToastWhenVerifyIsClicked() {
        navigateToVerifyDocumentPage();

        verifyDocumentPage.clickVerifyButton(0);

        assertThat("Toast should display success message", toastr.getToastMessage().getText(),
                containsString("Document verified"));
        assertThat("Toast should display success title", toastr.getToastTitle().getText(),
                containsString("Success"));
    }

    private void testShouldDisplayADocumentForEachUser() {
        navigateToVerifyDocumentPage();

        assertThat("There should be a document for each user", verifyDocumentPage.getDocuments().size(), is(3));
    }

    private void testDocumentDateOfBirthShouldBeVisible() {
        navigateToVerifyDocumentPage();

        assertThat("Document date of birth should be visible", verifyDocumentPage.getAccountDetails(0).getText(),
                containsString("Date of birth: Jan 30, 1996"));
    }

    private void testDocumentLastNameShouldBeVisible() {
        navigateToVerifyDocumentPage();

        assertThat("Document last name should be visible", verifyDocumentPage.getAccountDetails(0).getText(),
                containsString("Last name: Admin"));
    }

    private void testDocumentFirstNameShouldBeVisible() {
        navigateToVerifyDocumentPage();

        assertThat("Document first name should be visible", verifyDocumentPage.getAccountDetails(0).getText(),
                containsString("First name: Travel"));
    }

    private void testDocumentTypeShouldBeVisible() {
        navigateToVerifyDocumentPage();

        assertThat("Document type should be visible", verifyDocumentPage.getAccountDetails(0).getText(),
                containsString("Document type: STUDENT"));
    }

    private void testDocumentImageShouldBeVisible() {
        navigateToVerifyDocumentPage();

        assertNotNull("Document image should be visible", verifyDocumentPage.getImage(0));
    }

    private void uploadTestDocument() {
        navigateToUploadDocumentPage();

        uploadDocumentPage.clickStudentRadioButton();
        uploadDocumentPage.setUploadPath();
        uploadDocumentPage.clickUploadButton();
    }

    private void testShouldDisplayPageTitle() {
        navigateToVerifyDocumentPage();

        assertThat("Page title should be displayed", verifyDocumentPage.getPageTitle().getText(),
                containsString("Verify documents"));
    }

    private void navigateToVerifyDocumentPage() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickVerifyDocumentButton();
    }

    private void navigateToUploadDocumentPage() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickUploadDocumentButton();
    }

    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TRAVEL_ADMINISTRATOR_EMAIL);
        emails.add(SYSTEM_ADMINISTRATOR_EMAIL);
        emails.add(VERIFIER_EMAIL);
        return emails;
    }

    public List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TRAVEL_ADMINISTRATOR_PASSWORD);
        passwords.add(SYSTEM_ADMINISTRATOR_PASSWORD);
        passwords.add(VERIFIER_PASSWORD);
        return passwords;
    }
}
