package com.tim2.e2e.selenium.vehicle;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class TrackVehicleTest extends TestBaseResource {

    private static final Integer FIRST_VEHICLE = 0;

    @Test
    public void testTrackVehiclePage() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(emails, passwords, this::testPageTitleShouldBeVisible);
        runWithCredentials(emails, passwords, this::testShouldDisplayLatitudeWhenVehicleIsSelected);
        runWithCredentials(emails, passwords, this::testShouldDisplayLongitudeWhenVehicleIsSelected);
        runWithCredentials(emails, passwords, this::testShouldDisplayVehicleDataTitleWhenVehicleIsSelected);
        runWithCredentials(emails, passwords, this::testMapShouldBeVisible);
    }

    private void testMapShouldBeVisible() {
        navigateToTrackVehiclePage();
        assertNotNull("Map should be visible", trackVehiclePage.getMap());
    }

    private void testShouldDisplayVehicleDataTitleWhenVehicleIsSelected() {
        navigateToTrackVehiclePage();
        trackVehiclePage.setVehicleSelect(FIRST_VEHICLE);
        assertThat("Vehicle data title is displayed", trackVehiclePage.getVehicleDataSectionTitle().getText(),
                containsString("Vehicle data"));
    }

    private void testShouldDisplayLongitudeWhenVehicleIsSelected() {
        navigateToTrackVehiclePage();
        trackVehiclePage.setVehicleSelect(FIRST_VEHICLE);
        assertThat("Longitude is displayed", trackVehiclePage.getLongitude().getText(),
                containsString("Longitude: 19.791754249"));
    }

    private void testShouldDisplayLatitudeWhenVehicleIsSelected() {
        navigateToTrackVehiclePage();
        trackVehiclePage.setVehicleSelect(FIRST_VEHICLE);
        assertThat("Latitude is displayed", trackVehiclePage.getLatitude().getText(),
                containsString("Latitude: 45.2489224745"));
    }

    private void testPageTitleShouldBeVisible() {
        navigateToTrackVehiclePage();
        assertThat("Page title is visible", trackVehiclePage.getPageTitle().getText(),
                containsString("Track vehicle"));
    }

    private void navigateToTrackVehiclePage() {
        navigationBar.clickVehiclesNavigationButton();
        navigationBar.clickTrackVehicleButton();
    }

    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TestData.NIKOLA_EMAIL);
        emails.add(TestData.VERIFIER_EMAIL);
        emails.add(TestData.TRAVEL_ADMINISTRATOR_EMAIL);
        emails.add(TestData.SYSTEM_ADMINISTRATOR_EMAIL);
        return emails;
    }

    public List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TestData.NIKOLA_PASSWORD);
        passwords.add(TestData.VERIFIER_PASSWORD);
        passwords.add(TestData.TRAVEL_ADMINISTRATOR_PASSWORD);
        passwords.add(TestData.SYSTEM_ADMINISTRATOR_PASSWORD);
        return passwords;
    }
}
