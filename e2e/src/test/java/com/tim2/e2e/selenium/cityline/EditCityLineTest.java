package com.tim2.e2e.selenium.cityline;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.WebMessages;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.tim2.e2e.selenium.TestData.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class EditCityLineTest extends TestBaseResource {

    private static final Integer BUS_TYPE_OPTION = 0;

    @Test
    public void testEditCityLinePage() {
        List<String> emails = getEmails();
        List<String> passwords = getPasswords();

        runWithCredentials(emails, passwords, this::testSaveButtonDisabledWhenCityLineNameIsCleared);
        runWithCredentials(emails, passwords, this::testSuccessMessageIsShownWhenSaveButtonIsClicked);
        runWithCredentials(emails, passwords, this::testSaveButtonEnabledWhenCityLineNameAndTypeAreEntered);
    }

    private void testSaveButtonDisabledWhenCityLineNameIsCleared() {
        navigateToEditCityLinePage();

        editCityLinePage.enterCityLineName("");
        editCityLinePage.setCityLineTypeSelect(BUS_TYPE_OPTION);

        expectSaveButtonToBeDisabled();
    }

    private void testSuccessMessageIsShownWhenSaveButtonIsClicked() {
        navigateToEditCityLinePage();

        editCityLinePage.enterCityLineName(EDIT_CITY_LINE_NAME);
        editCityLinePage.setCityLineTypeSelect(BUS_TYPE_OPTION);

        expectSaveButtonToBeEnabled();

        editCityLinePage.clickSaveButton();

        assertThat("Toast contains success text", toastr.getToastMessage().getText(),
                containsString(WebMessages.EDIT_CITY_LINE_SUCCESS));
        assertThat("Toast has success title", toastr.getToastTitle().getText(),
                containsString(WebMessages.TOAST_SUCCESS));
    }

    private void testSaveButtonEnabledWhenCityLineNameAndTypeAreEntered() {
        navigateToEditCityLinePage();

        editCityLinePage.enterCityLineName(EDIT_CITY_LINE_NAME);
        editCityLinePage.setCityLineTypeSelect(BUS_TYPE_OPTION);

        expectSaveButtonToBeEnabled();
    }

    private void navigateToEditCityLinePage() {
        driver.navigate().to(EDIT_CITY_LINE_URL_ADDRESS + "/6");
        editCityLinePage.getSaveButton();
        expectEditCityLinesPageToBeVisible();
    }

    private void expectEditCityLinesPageToBeVisible() {
        assertTrue("Edit city line page should be visible", editCityLinePage.isVisible());
    }

    private void expectSaveButtonToBeDisabled() {
        assertFalse("Save button should be disabled.", editCityLinePage.getSaveButton().isEnabled());
    }

    private void expectSaveButtonToBeEnabled() {
        assertTrue("Save button should be enabled.", editCityLinePage.getSaveButton().isEnabled());
    }

    public List<String> getEmails() {
        List<String> emails = new ArrayList<>();
        emails.add(TRAVEL_ADMINISTRATOR_EMAIL);
        emails.add(SYSTEM_ADMINISTRATOR_EMAIL);
        return emails;
    }

    public List<String> getPasswords() {
        List<String> passwords = new ArrayList<>();
        passwords.add(TRAVEL_ADMINISTRATOR_PASSWORD);
        passwords.add(SYSTEM_ADMINISTRATOR_PASSWORD);
        return passwords;
    }
}
