package com.tim2.e2e.selenium.boughticket.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BoughtTicketDisplayData {

    private String type;

    private String duration;

    private String price;

    private String boughtDate;
}
