package com.tim2.e2e.selenium.pricelistitem;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;

public class UpdatePriceListItemTest extends TestBaseResource {

    @Test
    public void testUpdatePriceListItem() {
        testSuccessfulUpdatePriceListItem();
        testInvalidPrice();
        testNotANumber();
    }

    private void testSuccessfulUpdatePriceListItem() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.enterPriceUpdate(TestData.PRICE);
        viewPriceListsPage.clickUpdatePriceListItemButton();

        verifySuccessToast(viewPriceListsPage.getToastMessage(), "Price list item price successfuly updated");

        logOut();
        navigationBar.isVisible();
    }

    private void testInvalidPrice() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.enterPriceUpdate(TestData.INVALID_PRICE);
        viewPriceListsPage.clickUpdatePriceListItemButton();

        verifyToastMessage(viewPriceListsPage.getToastMessage(), "Unable to update price list item");

        logOut();
        navigationBar.isVisible();
    }

    private void testNotANumber() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.enterPriceUpdate(TestData.SYSTEM_ADMINISTRATOR_EMAIL);
        viewPriceListsPage.clickUpdatePriceListItemButton();

        verifyToastMessage(viewPriceListsPage.getToastMessage(), "Unable to update price list item");

        logOut();
        navigationBar.isVisible();
    }

    private void logOut() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    protected void openViewPriceListPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickViewPriceListsButton();
    }
}
