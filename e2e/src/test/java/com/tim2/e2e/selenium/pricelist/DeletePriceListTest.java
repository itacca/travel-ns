package com.tim2.e2e.selenium.pricelist;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;

public class DeletePriceListTest extends TestBaseResource {

    @Test
    public void testDeletePriceList() throws Exception {
        testSuccessfulDelete();
    }

    private void testSuccessfulDelete() throws Exception {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickDeleteButton();
        Thread.sleep(1000);
        verifySuccessToast(addPriceListPage.getToastMessage(), "Price list successfuly deleted");
        logOut();
        navigationBar.isVisible();
    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    protected void openViewPriceListPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickViewPriceListsButton();
    }
}
