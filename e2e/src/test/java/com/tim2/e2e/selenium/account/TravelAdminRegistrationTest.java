package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.TestBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;
import static com.tim2.e2e.selenium.WebMessages.ACCOUNT_REGISTRATION_ERROR;
import static com.tim2.e2e.selenium.WebMessages.ADMIN_REGISTRATION_ERROR;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.AssertJUnit.assertFalse;

public class TravelAdminRegistrationTest extends TestBaseResource {

    @Test
    public void testUserRegistration() {
        testEnteringInvalidEmailFormat();
        testEnteringDateInFuture();
        testRegisteringWithEmailThatExists();
        testSuccessfulRegistration();
    }

    private void testEnteringDateInFuture() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAdminRegistrationPage();
        enterUserInformation(ACCOUNT_REGISTRATION_EMAIL,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH_IN_FUTURE, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        registerAdminPage.clickRegisterButton();
        verifyToastMessage(registerAdminPage.getToastMessage(), ADMIN_REGISTRATION_ERROR);
        logOut();
    }

    private void testEnteringInvalidEmailFormat() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAdminRegistrationPage();
        enterUserInformation(ACCOUNT_REGISTRATION_INVALID_EMAIL,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        assertFalse("Register button is not visible.", registerAdminPage.isRegisterButtonVisible());
        logOut();
    }

    private void testSuccessfulRegistration() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAdminRegistrationPage();
        enterUserInformation(ACCOUNT_REGISTRATION_EMAIL,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        registerAdminPage.clickRegisterButton();
        logOut();
        login(ACCOUNT_REGISTRATION_EMAIL, ACCOUNT_REGISTRATION_PASSWORD);
        navigationBar.isVisible();
        logOut();
    }

    private void testRegisteringWithEmailThatExists() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAdminRegistrationPage();
        enterUserInformation(SYSTEM_ADMINISTRATOR_EMAIL,
                ACCOUNT_REGISTRATION_DATE_OF_BIRTH, ACCOUNT_REGISTRATION_FIRST_NAME, ACCOUNT_REGISTRATION_LAST_NAME);
        registerAdminPage.clickRegisterButton();

        assertThat("Toast title has error.", toastr.getToastTitle().getText(), containsString("Error"));
        //navigationBar.isVisible();
        logOut();

    }

    private void logOut() {
        navigationBar.clickHomeButton();
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    private void enterUserInformation(String email, String dateOfBirth, String firstName, String lastName) {
        registerAdminPage.enterEmail(email);
        registerAdminPage.enterDate(dateOfBirth);
        registerAdminPage.enterFirstName(firstName);
        registerAdminPage.enterLastName(lastName);
    }

    private void openAdminRegistrationPage() {
        navigationBar.clickTravelAdminsNavigationButton();
        navigationBar.clickRegisterAdminButton();
    }
}
