package com.tim2.e2e.selenium.schedule;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class AddSchedule extends TestBaseResource {

    @Test
    private void testSuccessfullyAddScheduleItems() {
        login(TestData.SYSTEM_ADMINISTRATOR_EMAIL, TestData.SYSTEM_ADMINISTRATOR_PASSWORD);
        navigateScheduleDetailPage();

        verifyCurrentUrlAddress(SCHEDULE_URL_ADDRESS);

        goToNewSchedulePage();

        verifyCurrentUrlAddress(NEW_SCHEDULE_URL_ADDRESS);

        enterNewTimeAndSave();

        assertEquals(1, detailSchedulePage.getScheduleItemsCount());

        logout();
    }

    @Test(expectedExceptions = org.openqa.selenium.TimeoutException.class)
    private void testShouldPresentScheduleDetailPageWithNoCreateButtonWhenUserDoesNotHaveAuthority() {
        navigateScheduleDetailPage();

        verifyCurrentUrlAddress(SCHEDULE_URL_ADDRESS);

        detailSchedulePage.getNewButton();
    }

    private void enterNewTimeAndSave() {
        newSchedulePage.getInputNewTime().clear();
        newSchedulePage.getInputNewTime().sendKeys("0610AM");
        newSchedulePage.getAddButton().click();
        newSchedulePage.getSaveButton().click();
    }

    private void goToNewSchedulePage() {
        detailSchedulePage.getNewButton().click();
    }

    private void navigateScheduleDetailPage() {
        navigationBar.clickCityLinesNavigationButton();
        navigationBar.clickViewCityLinesButton();

        cityLineListPage.getLinkToFirstSchedule().click();
        detailSchedulePage.getLastRowInTable();
    }
}
