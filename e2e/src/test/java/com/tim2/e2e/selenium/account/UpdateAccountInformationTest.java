package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.AccountInformationBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;
import static org.junit.Assert.assertFalse;

public class UpdateAccountInformationTest extends AccountInformationBaseResource {

    @Test
    public void testUpdateAccountInformation() {
        testSuccessfulUpdatingAccountInformation();
        resetPassword();
        testInvalidDateOfBirth();
    }

    private void resetPassword() {
        login(NIKOLA_EMAIL, ACCOUNT_PASSWORD_UPDATED);
        openAccountInformationPage();
        resetAccountData();
        logout();
    }


    private void testInvalidDateOfBirth() {
        login(NIKOLA_EMAIL, NIKOLA_PASSWORD);
        openAccountInformationPage();
        verifyAccountInformationIsLoaded(NIKOLA_FIRST_NAME, NIKOLA_LAST_NAME, NIKOLA_DATE_OF_BIRTH);
        accountInformationPage.enterDateOfBirth(TestData.ACCOUNT_INVALID_DATE_OF_BIRTH);
        assertFalse("Save button should be invisible", driver.findElements(By.xpath("//button/span[contains(., 'Save')]/..")).size() >= 1);
        logout();
    }

    private void testSuccessfulUpdatingAccountInformation() {
        login(NIKOLA_EMAIL, NIKOLA_PASSWORD);
        openAccountInformationPage();
        verifyAccountInformationIsLoaded(NIKOLA_FIRST_NAME, NIKOLA_LAST_NAME, NIKOLA_DATE_OF_BIRTH);
        updateAccount();
        navigationBar.clickHomeButton();
        openAccountInformationPage();
        verifyAccountInformationIsLoaded(ACCOUNT_FIRST_NAME_UPDATED, ACCOUNT_LAST_NAME_UPDATED, ACCOUNT_DATE_OF_BIRTH_UPDATED);
        logout();
        login(NIKOLA_EMAIL, ACCOUNT_PASSWORD_UPDATED);
        navigationBar.isVisible();
        logout();
    }

    private void updateAccount() {
        accountInformationPage.enterFirstName(ACCOUNT_FIRST_NAME_UPDATED);
        accountInformationPage.enterLastName(ACCOUNT_LAST_NAME_UPDATED);
        accountInformationPage.enterDateOfBirth(ACCOUNT_DATE_OF_BIRTH_UPDATED);
        accountInformationPage.clickUpdatePasswordButton();
        accountInformationPage.enterPassword(ACCOUNT_PASSWORD_UPDATED);
        accountInformationPage.enterConfirmPassword(ACCOUNT_PASSWORD_UPDATED);
        accountInformationPage.clickSaveButton();
    }

    private void resetAccountData() {
        accountInformationPage.enterFirstName(NIKOLA_FIRST_NAME);
        accountInformationPage.enterLastName(NIKOLA_LAST_NAME);
        accountInformationPage.enterDateOfBirth(NIKOLA_DATE_OF_BIRTH);
        accountInformationPage.clickUpdatePasswordButton();
        accountInformationPage.enterPassword(NIKOLA_PASSWORD);
        accountInformationPage.enterConfirmPassword(NIKOLA_PASSWORD);
        accountInformationPage.clickSaveButton();
    }
}
