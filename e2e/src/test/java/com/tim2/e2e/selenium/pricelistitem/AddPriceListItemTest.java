package com.tim2.e2e.selenium.pricelistitem;

import com.tim2.e2e.selenium.TestBaseResource;
import com.tim2.e2e.selenium.TestData;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_EMAIL;
import static com.tim2.e2e.selenium.TestData.SYSTEM_ADMINISTRATOR_PASSWORD;

public class AddPriceListItemTest extends TestBaseResource {

    @Test
    public void testAddPriceListItem() {
        testSuccessfulAddPriceListItem();
        testInvalidPrice();
        testNotANumber();
    }

    private void testSuccessfulAddPriceListItem() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.clickAddItemButton();
        viewPriceListsPage.enterPrice(TestData.PRICE);
        findSelectByXpath("//mat-select[@id='addSelect']");
        viewPriceListsPage.clickOption();
        viewPriceListsPage.clickAddItemButtonInDialogue();
        verifySuccessToast(viewPriceListsPage.getToastMessage(), "Price list item successfuly added");

        logOut();
        navigationBar.isVisible();
    }

    private void testInvalidPrice() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.clickAddItemButton();

        viewPriceListsPage.enterPrice(TestData.INVALID_PRICE);
        findSelectByXpath("//mat-select[@id='addSelect']");
        viewPriceListsPage.clickOption();
        viewPriceListsPage.clickAddItemButtonInDialogue();

        verifyToastMessage(viewPriceListsPage.getToastMessage(), "Unable to add price list item");
        logOut();
        navigationBar.isVisible();
    }

    private void testNotANumber() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openViewPriceListPage();
        viewPriceListsPage.clickExtensionPanel();
        viewPriceListsPage.clickAddItemButton();

        viewPriceListsPage.enterPrice(TestData.SYSTEM_ADMINISTRATOR_EMAIL);
        findSelectByXpath("//mat-select[@id='addSelect']");
        viewPriceListsPage.clickOption();
        viewPriceListsPage.clickAddItemButtonInDialogue();

        verifyToastMessage(viewPriceListsPage.getToastMessage(), "Unable to add price list item");

        logOut();
        navigationBar.isVisible();

    }

    private void logOut() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickLogOutButton();
    }

    protected void openViewPriceListPage() {
        navigationBar.clickPriceListNavigationButton();
        navigationBar.clickViewPriceListsButton();
    }
}
