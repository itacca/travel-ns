package com.tim2.e2e.selenium.account;

import com.tim2.e2e.selenium.AccountInformationBaseResource;
import org.testng.annotations.Test;

import static com.tim2.e2e.selenium.TestData.*;

public class ShowAccountInformationTest extends AccountInformationBaseResource {

    @Test
    public void shouldShowAccountInformation() {
        login(SYSTEM_ADMINISTRATOR_EMAIL, SYSTEM_ADMINISTRATOR_PASSWORD);
        openAccountInformationPage();
        verifyAccountInformationIsLoaded(SYSTEM_ADMINISTRATOR_FIRST_NAME, SYSTEM_ADMINISTRATOR_LAST_NAME, SYSTEM_ADMINISTRATOR_DATE_OF_BIRTH);
    }
}
