package com.tim2.e2e.selenium;

public class AccountInformationBaseResource extends TestBaseResource {

    protected void verifyAccountInformationIsLoaded(String firstName, String lastName, String dateOfBirth) {
        verifyTextIsLoaded(accountInformationPage.getFirstNameTextField(), firstName);
        verifyTextIsLoaded(accountInformationPage.getLastNameTextField(), lastName);
        verifyTextIsLoaded(accountInformationPage.getDateTextField(), dateOfBirth);
    }

    protected void openAccountInformationPage() {
        navigationBar.clickAccountNavigationButton();
        navigationBar.clickAccountInformationButton();
    }
}
