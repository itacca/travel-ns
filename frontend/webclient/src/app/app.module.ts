import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { GuardModule } from './guard/guard.module';
import { AccountModule } from './account/account.module';
import { PriceListModule } from './price-list/pricelist.module';
import { TicketModule } from './ticket/ticket.module';
import { CityLinesModule } from './city-lines/city-lines.module';
import { VehicleModule } from './vehicle/vehicle.module';
import { ScheduleDetailComponent } from './schedule/schedule-detail/schedule-detail.component';
import { ScheduleModule } from './schedule/schedule.module';
import { ConvertToSpacesPipe } from './shared/convert-to-spaces.pipe';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StationModule } from './station/station.module';
import { DocumentModule } from './document/document.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    GuardModule,
    HttpModule,
    AccountModule,
    CityLinesModule,
    PriceListModule,
    ReactiveFormsModule,
    FormsModule,
    TicketModule,
    StationModule,
    VehicleModule,
    DocumentModule,
    ScheduleModule,
    AppRoutingModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
