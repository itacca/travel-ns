import { Coordinates } from './coordinates.model';

export class Marker {

    constructor(
        public position: Coordinates,
        public settings?: any
    ) { }
}