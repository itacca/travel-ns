import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkerMapComponent } from './marker-map.component';
import { SharedModule } from '../shared.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';

import { Coordinates } from 'src/app/shared/marker-map/coordinates.model';
import { Marker } from './marker.model';
import { By } from '@angular/platform-browser';

describe('MarkerMapComponent', () => {
  let component: MarkerMapComponent;
  let fixture: ComponentFixture<MarkerMapComponent>;
  let center: Coordinates = new Coordinates(45.247803, 19.839120);
  let markers: Marker[] = [new Marker(center), new Marker(center), new Marker(center)];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkerMapComponent);
    component = fixture.componentInstance;
    component.center = center;
    component.markers = markers;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add point on map for each marker', () => {
    expect(fixture.debugElement.queryAll(By.css('.map-circle')).length).toEqual(markers.length);
  });

  it('increaseZoom() should increase zoom if zoom is less than 18', () => {
    component.zoom = 10;
    component.increaseZoom();
    expect(component.zoom).toEqual(11);
  });

  it('decreaseZoom() should decrease zoom if zoom is more than 1', () => {
    component.zoom = 10;
    component.decreaseZoom();
    expect(component.zoom).toEqual(9);
  });

  it('increaseOpacity() should increase opacity if opacity is less than 1', () => {
    component.opacity = 0.5;
    component.increaseOpacity();
    expect(component.opacity).toEqual(0.6);
  });

  it('decreaseOpacity() should decrease opacity if opacity is more than 0', () => {
    component.opacity = 0.5;
    component.decreaseOpacity();
    expect(component.opacity).toEqual(0.4);
  });
});
