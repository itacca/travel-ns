import { Component, OnInit, Input } from '@angular/core';
import { Coordinates } from './coordinates.model';
import { Marker } from './marker.model';

@Component({
  selector: 'app-marker-map',
  templateUrl: './marker-map.component.html',
  styleUrls: ['./marker-map.component.css']
})
export class MarkerMapComponent implements OnInit {

  @Input()
  center: Coordinates;

  @Input()
  markers: Marker[];

  zoom = 17;
  opacity = 1.0;
  strokeWidth = 2;

  constructor() { }

  ngOnInit() {
  }

  increaseZoom() {
    this.zoom  = Math.min(this.zoom + 1, 18);
  }

  decreaseZoom() {
    this.zoom  = Math.max(this.zoom - 1, 1);
  }

  increaseOpacity() {
    this.opacity  = Math.min(this.opacity + 0.1, 1);
  }

  decreaseOpacity() {
    this.opacity  = Math.max(this.opacity - 0.1, 0);
  }

}
