import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicMapComponent } from './dynamic-map.component';
import { SharedModule } from '../shared.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';

describe('DynamicMapComponent', () => {
  let component: DynamicMapComponent;
  let fixture: ComponentFixture<DynamicMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
