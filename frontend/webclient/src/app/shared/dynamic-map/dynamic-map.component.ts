import { Component, Input, OnDestroy, EventEmitter, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import Map from 'ol/Map';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { MapOption, MapOptionsConsumer } from './map-options/map-options';

@Component({
  selector: 'app-dynamic-map',
  templateUrl: './dynamic-map.component.html',
  styleUrls: ['./dynamic-map.component.css']
})
export class DynamicMapComponent implements OnDestroy, MapOptionsConsumer {
  
  private _mapOptions: MapOption[] = [];

  vectorSource: VectorSource;

  layerVector: VectorLayer;

  tileLayer: TileLayer;

  map: Map;

  mapId: string = 'dynamic-map-id[default]';
  
  destroyedEvent: EventEmitter<MapOption[]> = new EventEmitter();

  constructor(
    public changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnDestroy() {
    this.destroyedEvent.emit(this.mapOptions);
  }

  @Input()
  set mapOptions(mapOptions: MapOption[]) {
    this._mapOptions = mapOptions;
    this.mapOptions.forEach(option => option.addOptionToComponent(this));
  }

  get mapOptions(): MapOption[] {
    return this._mapOptions;
  }
}
