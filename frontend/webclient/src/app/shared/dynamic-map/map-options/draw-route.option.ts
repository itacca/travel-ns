import { MapOption, MapOptionsConsumer } from "./map-options";
import { Station } from "src/app/station/station.model";
import {fromLonLat} from 'ol/proj';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Stroke from 'ol/style/Stroke';
import LineString from 'ol/geom/LineString';
import Text from 'ol/style/Text';
import Fill from 'ol/style/Fill';


export class DrawRoute extends MapOption {
  
  lineStyle: Style = new Style({
    stroke: new Stroke({
      color: '#ff0000',
      width: 4
    })
  });

  private features: Feature[] = [];

  private component: MapOptionsConsumer;

  constructor(
    public points: Station[],
    lineStyle?: Style
  ) {
    super();
    if (lineStyle) {
      this.lineStyle = lineStyle;
    }
  }

  addOptionToComponent(component: MapOptionsConsumer) {
    this.validateMapInitialized(component);
    this.component = component;
    this.drawRoute(component);
    this.completeInitialization();
  }

  updateRoute(points: Station[]) {
    this.validateMapInitialized(this.component);
    this.points = points;
    this.drawRoute(this.component);
  }

  private drawRoute(component: MapOptionsConsumer) {
    this.clearFeatures(component);
    for (let i = 0; i < this.points.length - 1; i++) {
      let startPoint = fromLonLat([this.points[i].longitude, this.points[i].latitude]);
      let endPoint = fromLonLat([this.points[i + 1].longitude, this.points[i + 1].latitude]);
      this.drawLine(component, startPoint, endPoint);
    }
    this.points.forEach(point => this.drawStation(component, point.latitude, point.longitude, point.name));
  }

  private clearFeatures(component: MapOptionsConsumer) {
    this.features.forEach(feature => component.vectorSource.removeFeature(feature));
    this.features = [];
  }

  private drawLine(component: MapOptionsConsumer, startPoint, endPoint) {
    let line = new Feature({
      geometry: new LineString([startPoint, endPoint]),
      name: 'Line',
    });
    line.setStyle(this.lineStyle);
    component.vectorSource.addFeature(line);
    this.features.push(line);
  }

  private drawStation(component: MapOptionsConsumer, latitude: number, longitude: number, stationName: string) {
    let station = new Feature({
      geometry: new Point(fromLonLat([longitude, latitude])),
      name: 'Station'
    });
    station.setStyle(this.getStationStyle(stationName));
    component.vectorSource.addFeature(station);
    this.features.push(station);
  }

  private getStationStyle(stationName: string): Style {
    return new Style({
      image: new Icon({
        anchor: [0.5, 1],
        anchorXUnits: 'fraction',
        anchorYUnits: 'fraction',
        opacity: 1,
        src: '../../assets/images/map/bus-station.png',
        scale: 0.07
      }),
      text: new Text({
        text: stationName,
        stroke: new Stroke({
          color: '#fff' 
        }),
        fill: new Fill({
          color: '#3366cc'
        }),
        font: '9px sans-serif',
        offsetY: -45,
        backgroundFill: new Fill({
          color: 'white'
        })
      })
    });
  }

}