export class NamedPoint {

  constructor(
    public longitude: number,
    public latitude: number,
    public name?: string
  ) { }
}