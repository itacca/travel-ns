import { MapOption, MapOptionsConsumer } from "./map-options";
import { fromLonLat } from 'ol/proj';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Stroke from 'ol/style/Stroke';
import Text from 'ol/style/Text';
import Fill from 'ol/style/Fill';
import { EventEmitter } from "@angular/core";
import { Station } from "src/app/station/station.model";
import { NamedPoint } from "./named-point.model";

export class DrawMarkersAndNotifyWhenClicked extends MapOption {
  
  private features: Feature[] = [];

  private component: MapOptionsConsumer;

  markerClicked: EventEmitter<number> = new EventEmitter();

  constructor(
    public points: NamedPoint[]
  ) {
    super();
  }

  addOptionToComponent(component: MapOptionsConsumer) {
    this.validateMapInitialized(component);
    this.clearFeatures(component);
    this.drawMarkers(component);
    this.addEventListener(component);
    this.completeInitialization();
  }

  updatePoints(points: NamedPoint[]) {
    this.validateMapInitialized(this.component);
    this.clearFeatures(this.component);
    this.points = points;
    this.drawMarkers(this.component);
  }

  private addEventListener(component) {
    this.component = component;
    let opt = this;
    component.map.on("click", function(e) {
      component.map.forEachFeatureAtPixel(e.pixel, (feature, layer) => {
        let index = opt.features.findIndex(other => other === feature);
        if (index !== -1) {
          opt.markerClicked.emit(index)
        }
      });
  });
  }

  private drawMarkers(component: MapOptionsConsumer) {
    this.points.forEach(point => {
      this.drawStation(component, point.latitude, point.longitude, point.name);
    });
  }

  private clearFeatures(component: MapOptionsConsumer) {
    this.features.map(feature => component.vectorSource.removeFeature(feature));
    this.features = [];
  }

  private drawStation(component: MapOptionsConsumer, latitude: number, longitude: number, stationName: string) {
    let station = new Feature({
      geometry: new Point(fromLonLat([longitude, latitude])),
      name: 'Marker'
    });
    station.setStyle(this.getStationStyle(stationName));
    component.vectorSource.addFeature(station);
    this.features.push(station);
  }

  private getStationStyle(stationName: string): Style {
    return new Style({
      image: new Icon({
        anchor: [0.5, 1],
        anchorXUnits: 'fraction',
        anchorYUnits: 'fraction',
        opacity: 1,
        src: '../../assets/images/map/bus-station.png',
        scale: 0.07
      }),
      text: new Text({
        text: stationName,
        stroke: new Stroke({
          color: '#fff' 
        }),
        fill: new Fill({
          color: '#3366cc'
        }),
        font: '9px sans-serif',
        offsetY: -45,
        backgroundFill: new Fill({
          color: 'white'
        })
      })
    });
  }
}