import Map from 'ol/Map';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { EventEmitter, ChangeDetectorRef } from '@angular/core';
import { forkJoin, of, Observable, Subject } from 'rxjs';

export interface MapOptionsConsumer {
  
  mapOptions: MapOption[];

  vectorSource: VectorSource;

  layerVector: VectorLayer;

  tileLayer: TileLayer;

  map: Map;

  mapId: string;

  destroyedEvent: EventEmitter<MapOption[]>;

  changeDetectorRef: ChangeDetectorRef;
}

export abstract class MapOption {

  optionInitializedEvent: Subject<any> = new Subject();

  optionInitialized: boolean = false;

  abstract addOptionToComponent(component: MapOptionsConsumer);

  protected validateMapInitialized(component: MapOptionsConsumer) {
    if (!component || !component.map) {
      throw new Error('DynamicMapComponent has not been initialized, please provide an initialization' + 
        ' option before adding other options. You can use "InitilizeMapWithId" initializer for this. ' +
        'It is also possible that you forgot to include this option in your options list and tried to call an ' + 
        'update method on it.');
    }
  }

  protected completeInitialization() {
    this.optionInitialized = true;
    this.optionInitializedEvent.next();
    this.optionInitializedEvent.complete();
  }
}

export function optionsInitialized(options: MapOption[]): Observable<any> {
  let uninitialized = options.filter(option => !option.optionInitialized);
  if (uninitialized.length === 0) {
    return of({});
  }
  return forkJoin(uninitialized.map(option => option.optionInitializedEvent));
}
