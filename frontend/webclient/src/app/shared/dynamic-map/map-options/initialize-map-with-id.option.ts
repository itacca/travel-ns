import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import OSM from 'ol/source/OSM';
import { fromLonLat } from 'ol/proj';
import { MapOption, MapOptionsConsumer } from './map-options';

const __usedMapIds = {};
const __maxId = 100;

function getUniqueId(): string {
  for (let i = 0; i < __maxId; i++) {
    let id = `dynamic-map-unique-id-${i}`;
    if (!__usedMapIds[id]) {
      __usedMapIds[id] = true;
      return id;
    }
  }
  throw new Error(`${__maxId + 1} ids have been generated, please check if you are emitting the "destroyedEvent" correctly.`);
}

function removeId(id: string) {
  if (__usedMapIds[id]) {
    delete __usedMapIds[id];
  }
} 

function isUsedId(id: string) {
  return __usedMapIds[id];
}


/**
 * If usiing multiple dynamic map components each map should have a unique id.
 * You can use this option to initialize your maps with a user defined or automatically defined unique id.
 * If you pass 'unique' for the mapId parameter the id will be generated automatically.
 */
export class InitilizeMapWithId extends MapOption {

  constructor(
    public mapId: string | 'unique',
    public centerLatitude: number,
    public centerLongitude: number,
    public zoom: number
  ) {
    super();
    if (isUsedId(mapId)) {
      throw new Error('Id is already in use');
    }
    if (mapId === 'unique') {
      this.mapId = getUniqueId();
    }
  }

  addOptionToComponent(component: MapOptionsConsumer) {
    this.initializeMapId(component);
    this.initializeOpenLayers(component);
    this.initializeMap(component);
    this.subscribeToDestroyEvent(component);
    this.completeInitialization();
  }

  private initializeOpenLayers(component) {
    this.initializeVectorSource(component);
    this.initializeLayerVector(component);
    this.initializeTileLayer(component);
  }

  private initializeMapId(component: MapOptionsConsumer) {
    component.mapId = this.mapId;
    component.changeDetectorRef.detectChanges();
  }

  private initializeVectorSource(component: MapOptionsConsumer) {
    component.vectorSource = new VectorSource();
  }

  private initializeLayerVector(component: MapOptionsConsumer) {
    component.layerVector = new VectorLayer({
      source: component.vectorSource
    });
  }

  private initializeTileLayer(component: MapOptionsConsumer) {
    component.tileLayer = new TileLayer({
      source: new OSM()
    });
  }

  private initializeMap(component: MapOptionsConsumer) {
    component.map = new Map({
      target: this.mapId,
      layers: [
        component.tileLayer,
        component.layerVector
      ],
      view: new View({
        center: fromLonLat([ this.centerLongitude, this.centerLatitude ]),
        zoom: this.zoom
      })
    });
  }

  private subscribeToDestroyEvent(component: MapOptionsConsumer) {
    component.destroyedEvent.subscribe(options => {
      removeId(component.mapId);
    });
  }
}