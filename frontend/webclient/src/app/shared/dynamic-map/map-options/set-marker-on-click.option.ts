import { MapOption, MapOptionsConsumer } from "./map-options";
import { EventEmitter } from "@angular/core";
import {transform} from 'ol/proj';
import { fromLonLat } from 'ol/proj';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import { Coordinates } from "../../marker-map/coordinates.model";


export class SetMarkerOnClick extends MapOption {

  markerCreated: EventEmitter<Coordinates> = new EventEmitter();

  markerStyle: Style = new Style({
    image: new Icon({
      anchor: [0.5, 1],
      anchorXUnits: 'fraction',
      anchorYUnits: 'fraction',
      opacity: 1,
      src: '../../assets/images/map/map-marker.png',
      scale: 0.1
    }),
    zIndex: 5
  });

  component: MapOptionsConsumer;

  private marker: Feature;

  constructor(markerStyle?: Style) {
    super();
    if (markerStyle) {
      this.markerStyle = markerStyle;
    }
  }

  addOptionToComponent(component: MapOptionsConsumer) {
    this.component = component;
    this.validateMapInitialized(component);
    this.createClickListener(component);
    this.completeInitialization();
  }

  setMarker(coordinates: Coordinates) {
    this.validateMapInitialized(this.component);
    this.clearVectorSource(this.component);
    this.marker = new Feature({
      geometry: new Point(fromLonLat([coordinates.longitude, coordinates.latitude])),
      name: 'Marker'
    });
    this.marker.setStyle(this.markerStyle);
    this.component.vectorSource.addFeature(this.marker);
  }

  private createClickListener(component: MapOptionsConsumer) {
    component.map.on('click', (args) => {
      this.clearVectorSource(component);
      this.marker = new Feature({
        geometry: new Point(args.coordinate),
        name: 'Marker'
      });
      this.marker.setStyle(this.markerStyle);
      component.vectorSource.addFeature(this.marker);
      let lonlat = transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
      this.markerCreated.emit(new Coordinates(lonlat[1], lonlat[0]));
    });
  }

  private clearVectorSource(component: MapOptionsConsumer) {
    if (this.marker) {
      component.vectorSource.removeFeature(this.marker);
    }
  }

}
