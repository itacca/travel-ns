import { TestBed, inject } from '@angular/core/testing';

import { DocumentService } from './document.service';
import { SharedModule } from '../shared.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { environment } from 'src/environments/environment';
import { Http, RequestOptions } from '@angular/http';
import { givenHttpPostReturnsResponseContainingContent, 
         givenAuthGetHttpOptionsWithoutContentTypeReturnsHttpOptions, 
         givenHttpPostThrowsError, 
         givenHttpGetReturnsResponseContainingContent, 
         givenAuthGetHttpOptionsReturnsHttpOptions, 
         givenHttpGetThrowsError, 
         givenHttpPutReturnsResponseContainingContent } from 'src/app/test-shared/common-stubs.spec';
import { AuthService } from 'src/app/core/auth/auth.service';
import { expectServiceMethodToMapResponseToObject, 
         expectServiceMethodToThrowError } from 'src/app/test-shared/common-expects.spec';
import { UtilService } from 'src/app/core/util.service';
import { PageParams } from '../page.model';
import { Document } from './document.model';

describe('DocumentService', () => {

  let documentService: DocumentService;
  let http: Http;
  let auth: AuthService;
  let util: UtilService;
  let httpOptions = new RequestOptions();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        TestSharedModule
      ]
    });
  });

  beforeEach(() => {
    documentService = TestBed.get(DocumentService);
    http = TestBed.get(Http);
    auth = TestBed.get(AuthService);
    util = TestBed.get(UtilService);
  });

  it('should be created', inject([DocumentService], (service: DocumentService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', () => {
    expect(documentService.documentEndpoint).toEqual(`${environment.apiRoot}/document`);
    expect(documentService.documentsEndpoint).toEqual(`${environment.apiRoot}/documents`);
  });

  it('getImageUrl() should return correct image url', () => {
    let imageId = 1;

    expect(documentService.getImageUrl(imageId)).toEqual(`${documentService.documentEndpoint}/${imageId}`);
  });

  it('upload() should map json to object if http does not return error', () => {
    let expectedContent = {};
    let file = new File([new ArrayBuffer(2e+5)], 'test-file', { lastModified: null, type: 'image/jpeg' });
    let type = 'jpeg';
    let documentType = "SENIOR";
    let formData: FormData = new FormData();
    formData.append("file", file);

    givenHttpPostReturnsResponseContainingContent(http, expectedContent);
    givenAuthGetHttpOptionsWithoutContentTypeReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToMapResponseToObject(documentService.upload(file, type, documentType), expectedContent, 'upload()');
    expect(http.post).toHaveBeenCalledWith(`${documentService.documentsEndpoint}/?image_type=${type}&document_type=${documentType}`, formData, httpOptions);
  });

  it('upload() should throw error if http returns error', () => {
    let file = new File([new ArrayBuffer(2e+5)], 'test-file', { lastModified: null, type: 'image/jpeg' });
    let type = 'jpeg';
    let documentType = "SENIOR";
    let formData: FormData = new FormData();
    formData.append("file", file);

    givenHttpPostThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsWithoutContentTypeReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToThrowError(documentService.upload(file, type, documentType), 'test-error', 'upload()');
    expect(http.post).toHaveBeenCalledWith(`${documentService.documentsEndpoint}/?image_type=${type}&document_type=${documentType}`, formData, httpOptions);
  });

  it('findByActiveAndVerified() should map json to object if http does not return error', () => {
    let content = {};
    let active = true;
    let verified = false;
    let pageParams: PageParams = {page: 0, size: 8, sort: 'id,asc'};
    let pageParamString = '?testParamString=test&';
    let paramString = pageParamString + `active=${active}&verified=${verified}`;

    givenHttpGetReturnsResponseContainingContent(http, content);
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);
    spyOn(util, 'getPageableParamString').and.callFake(any => pageParamString);

    expectServiceMethodToMapResponseToObject(documentService.findByActiveAndVerified(pageParams, active, verified), content, 'findByActiveAndVerified()');
    expect(http.get).toHaveBeenCalledWith(`${documentService.documentsEndpoint}${paramString}`, httpOptions);
  });

  it('findByActiveAndVerified() should throw error if http returns error', () => {
    let active = true;
    let verified = false;
    let pageParamString = '?testParamString=test&';
    let paramString = pageParamString + `active=${active}&verified=${verified}`;
    let pageParams: PageParams = {page: 0, size: 8, sort: 'id,asc'};

    givenHttpGetThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);
    spyOn(util, 'getPageableParamString').and.callFake(any => pageParamString);

    expectServiceMethodToThrowError(documentService.findByActiveAndVerified(pageParams, active, verified), 'test-error', 'findByActiveAndVerified()');
    expect(http.get).toHaveBeenCalledWith(`${documentService.documentsEndpoint}${paramString}`, httpOptions);
  });

  it('verify() should call correct endpoint with correct params and body', () => {
    let document = new Document(1);
    let expectedBody = {
      id: document.id,
      verified: true
    };

    givenHttpPutReturnsResponseContainingContent(http, null)
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);

    documentService.verify(document).subscribe();

    expect(http.put).toHaveBeenCalledWith(`${documentService.documentEndpoint}/verify`, expectedBody, httpOptions);
  });
});
