import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ImageUploadData } from './image-upload-data.model';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

  @Output()
  loadSuccess: EventEmitter<ImageUploadData> = new EventEmitter();

  @Output()
  loadError: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onFileChange(event) {
    const fileTypes = ['jpg', 'jpeg', 'png', 'gif'];
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      let extension = file.name.split('.').pop().toLowerCase();
      let isSuccess = fileTypes.indexOf(extension) > -1;
      if (!isSuccess) {
        this.loadError.emit("Supported file types are jpg, png and gif.");
      } else {
        this.loadSuccess.emit(new ImageUploadData(file, extension));
      }
    }
  }



}
