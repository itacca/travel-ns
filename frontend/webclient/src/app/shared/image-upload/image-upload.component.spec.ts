import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageUploadComponent } from './image-upload.component';
import { SharedModule } from '../shared.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { By } from '@angular/platform-browser';
import { triggerFileChangedEvent } from 'src/app/test-shared/common-view-operations.spec';

describe('ImageUploadComponent', () => {
  let component: ImageUploadComponent;
  let fixture: ComponentFixture<ImageUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a button for adding image', () => {
    let button = fixture.debugElement.query(By.css('button'));

    expect(button).toBeTruthy();
    expect(button.nativeElement.innerHTML).toContain('Choose image');
  });

  it('should emit file changed event if file input changes', () => {
    let file: File = new File([new ArrayBuffer(2e+5)], 'test-file', { lastModified: null, type: 'image/jpeg' });
    let fileInput = fixture.debugElement.query(By.css('input'));
    
    component.loadSuccess.subscribe(imageUploadData => {
      expect(imageUploadData.file).toBe(file);
      expect(imageUploadData.type).toEqual('jpg');
    });

    component.loadError.error(error => {
      fail('should not throw error');
    })

    triggerFileChangedEvent(fileInput.nativeElement, file);
  });

  it('should emit error if image extesion is not supported', () => {
    let file: File = new File([new ArrayBuffer(2e+5)], 'test-file', { lastModified: null, type: 'image/bitmap' });
    let fileInput = fixture.debugElement.query(By.css('input'));

    component.loadSuccess.subscribe(imageUploadData => {
      fail('should throw error')
    });

    component.loadError.subscribe(error => {
      expect(error).toEqual("Supported file types are jpg, png and gif.");
    });

    triggerFileChangedEvent(fileInput.nativeElement, file);
  }); 
});
