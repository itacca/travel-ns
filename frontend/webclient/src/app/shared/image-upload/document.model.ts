import { Account } from "src/app/account/account.model";

export class Document {

  constructor(
    public id?: number,
    public imageType?: string,
    public imageBlob?: any,
    public active?: boolean,
    public verified?: boolean,
    public documentType?: 'STUDENT' | 'SENIOR',
    public account?: Account
  ) { }
}