export class ImageUploadData {

  constructor(
    public file?: any,
    public type?: string
  ) { }
}