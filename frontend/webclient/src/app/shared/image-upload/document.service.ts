import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Http } from '@angular/http';
import { AuthService } from 'src/app/core/auth/auth.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ImageMetaData } from './meta-data.model';
import { Page, PageParams } from '../page.model';
import { Document } from './document.model';
import { UtilService } from 'src/app/core/util.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  documentEndpoint: string = `${environment.apiRoot}/document`;

  documentsEndpoint: string = `${environment.apiRoot}/documents`;

  constructor(
    private http: Http,
    private auth: AuthService,
    private util: UtilService
  ) { }

  getImageUrl(imageId: number) {
    return `${this.documentEndpoint}/${imageId}`;
  }

  upload(file, imageType, documentType): Observable<ImageMetaData> {
    let formData: FormData = new FormData();
    formData.append("file", file);
    return this.http.post(`${this.documentsEndpoint}/?image_type=${imageType}&document_type=${documentType}`, 
      formData, this.auth.getHttpAuthOptionsWithoutContentType()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  findByActiveAndVerified(pageParams: PageParams, active: boolean, verified: boolean): Observable<Page<Document>> {
    return this.http.get(`${this.documentsEndpoint}${this.util.getPageableParamString(pageParams)}active=${active}&verified=${verified}`, 
      this.auth.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  verify(document: Document): Observable<any> {
    let body = {
      id: document.id,
      verified: true
    };
    return this.http.put(`${this.documentEndpoint}/verify`, body, this.auth.getHttpAuthOptions());
  } 
}
