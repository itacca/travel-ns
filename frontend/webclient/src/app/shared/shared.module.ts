import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageControlsComponent } from './page-controls/page-controls.component';
import { MaterialModule } from '../material/material.module';
import { MarkerMapComponent } from './marker-map/marker-map.component';
import { AngularOpenlayersModule } from "ngx-openlayers";
import { DynamicMapComponent } from './dynamic-map/dynamic-map.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AngularOpenlayersModule
  ],
  declarations: [
    PageControlsComponent,
    MarkerMapComponent,
    DynamicMapComponent,
    ImageUploadComponent
  ],
  providers: [],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PageControlsComponent,
    MarkerMapComponent,
    DynamicMapComponent,
    ImageUploadComponent
  ]
})
export class SharedModule { }
