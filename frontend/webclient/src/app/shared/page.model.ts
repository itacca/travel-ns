export interface Sort {

    sorted: boolean;

    unsorted: boolean;
}

export interface Pageable {
    
    sort?: Sort;

    offset?: number;

    pageSize?: number;

    pageNumber?: number;

    paged?: boolean;

    unpaged?: boolean;
}

export interface Page<T> {
    
    content?: T[];

    pageable?: Pageable;

    last?: boolean;

    totalElements?: number;

    totalPages?: number;

    size?: number;

    number?: number;

    sort?: Sort;

    first?: boolean;

    numberOfElements?: number;
}

export interface PageParams {
    
    size?: number;
    
    page?: number;

    sort?: string;
}

export class PageMock<T> implements Page<T> {

    content?: T[];

    last?: boolean;

    totalElements?: number;

    totalPages?: number;

    size?: number;

    number?: number;

    first?: boolean;

    numberOfElements?: number;

    constructor(content: T[]) {
        this.content = content;
        this.size = content.length;
        this.totalPages = 1;
        this.last = false;
        this.number = 0;
        this.numberOfElements = content.length;
        this.first = true;
    }
}
