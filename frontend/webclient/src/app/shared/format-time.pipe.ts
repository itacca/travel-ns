import { Pipe, PipeTransform } from '@angular/core';
import { Time } from '@angular/common';

@Pipe({
  name: 'formatTime'
})
export class FormatTime implements PipeTransform {

  transform(value: string): Date {
    const splited = value.split(':');
    const hours = +splited[0];
    const minutes = +splited[1];
    console.log(value);
    console.log(hours);
    console.log(minutes);
    const date = new Date();
    date.setHours(hours);
    date.setMinutes(minutes);
    return date;
  }
}
