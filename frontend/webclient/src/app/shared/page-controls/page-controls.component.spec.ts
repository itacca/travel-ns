import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageControlsComponent } from './page-controls.component';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Page, PageParams } from '../page.model';
import { By } from '@angular/platform-browser';
import { SharedModule } from '../shared.module';

describe('PageControlsComponent', () => {
  let component: PageControlsComponent;
  let fixture: ComponentFixture<PageControlsComponent>;
  let page: Page<any> = { 
    number: 5,
    pageable: {
      pageSize: 3
    },
    totalPages: 10
  };
  let pageParams: PageParams = {
    page: 5,
    size: 3,
    sort: 'asc'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageControlsComponent);
    component = fixture.componentInstance;
    component.page = page;
    component.pageParams = pageParams;
    fixture.detectChanges();
  });

  let expectRenderedDataToMatchComponentData = () => {
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.info')).nativeElement.innerHTML)
      .toContain(`${pageParams.page + 1 } out of ${page.totalPages }, showing ${pageParams.size } items per page`);
  };

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render page meta data', () => {
    expectRenderedDataToMatchComponentData();
  });

  it('should render updated page meta data after page number increases', () => {
    component.next();
    expectRenderedDataToMatchComponentData();
  });

  it('should render updated page meta data after page number decreases', () => {
    component.previous();
    expectRenderedDataToMatchComponentData();
  });

  it('should render updated page meta data after page size changes', () => {
    component.pageParams.size = 32;
    expectRenderedDataToMatchComponentData();
  });

  it('should render updated page meta data after page sort changes', () => {
    component.pageParams.sort = 'desc';
    expectRenderedDataToMatchComponentData();
  });
});
