import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageParams, Page } from '../page.model';

@Component({
  selector: 'app-page-controls',
  templateUrl: './page-controls.component.html',
  styleUrls: ['./page-controls.component.css']
})
export class PageControlsComponent implements OnInit {

  @Input()
  pageParams: PageParams;

  @Input()
  page: Page<any>;

  @Input()
  tinyView: boolean;

  _sortFields: {displayName: string, name: string}[] = [
    { displayName: 'ID', name: 'id' }
  ];

  @Output()
  pageParamsChanged: EventEmitter<PageParams> = new EventEmitter();

  sortField: string = 'id';

  sortOrder: string = 'asc'; 

  constructor() { }

  ngOnInit() {

  }

  @Input()
  set sortFields(sortFields: {displayName: string, name: string}[]) {
    this.sortField = sortFields[0].name;
    this._sortFields = sortFields;
  }

  get sortFields() {
    return this._sortFields;
  }

  previous() {
    let current = this.page.number - 1; 
    if (current < 0) {
      return;
    }
    this.pageParams.page = current;
    this.paramsChanged();
  }

  next() {
    let current = this.page.number + 1;
    if (current >= this.page.totalPages) {
      return;
    }
    this.pageParams.page = current;
    this.paramsChanged();
  }

  paramsChanged(sizeChanged?: boolean) {
    if (sizeChanged) {
      this.pageParams.page = 0;
    }
    this.pageParams.sort = `${this.sortField},${this.sortOrder}`;
    this.pageParamsChanged.emit(this.pageParams);
  }

}
