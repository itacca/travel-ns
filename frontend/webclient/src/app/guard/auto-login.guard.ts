import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../core/auth/auth.service';
import { UtilService } from '../core/util.service';

@Injectable({
  providedIn: 'root'
})
export class AutoLoginGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
    private util: UtilService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    if (this.authService.isLoggedIn()) {
      return true;
    }

    if (state.root.queryParamMap.has('access_token')) {
      let token = state.root.queryParamMap.get('access_token');
      this.authService.setToken(token);
      this.router.navigate([this.util.expandUrl(next.url)]);
      return true;
    }

    if (state.root.queryParamMap.keys.length > 0) {
      this.router.navigate([this.util.expandUrl(next.url)]);
    }

    if (this.authService.isLoggedInAndTokenNotLoaded()) {
      let redirectUri = `http://${window.location.host}/#${this.util.expandUrl(next.url)}?`;
      this.authService.login(redirectUri);
      return false;
    }

    return true;
  }

}
