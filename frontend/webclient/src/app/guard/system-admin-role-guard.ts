import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountService } from '../account/account.service';
import { AuthService } from '../core/auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Roles } from '../account/role.model';

@Injectable({
  providedIn: 'root'
})
export class SystemAdminRoleGuard implements CanActivate {

  constructor(
    private accountService: AccountService,
    private authService: AuthService,
    private toastrService: ToastrService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    if (!this.authService.isLoggedIn()) {
      this.toastPrivilegeError();
      this.router.navigate(['/home']);
      return false;
    }

    return Observable.create(observer => {
      this.accountService.hasAnyRole([Roles.systemAdministratorRole]).subscribe(isAdmin => {
        if (isAdmin) {
          observer.next(true);
        } else {
          this.toastPrivilegeError();
          this.router.navigate(['/home']);
          observer.next(false);
        }
      });
    });
  }

  toastPrivilegeError() {
    this.toastrService.error("Not enough privileges. Please log in with a system administrative account.", "Error");
  }
}
