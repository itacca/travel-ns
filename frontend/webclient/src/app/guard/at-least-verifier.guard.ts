import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Roles } from '../account/role.model';
import { AccountService } from '../account/account.service';
import { AuthService } from '../core/auth/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AtLeastVerifierGuard implements CanActivate {
  
  constructor(
    private accountService: AccountService,
    private authService: AuthService,
    private toastrService: ToastrService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    if (!this.authService.isLoggedIn()) {
      this.toastPrivilegeError();
      this.router.navigate(['/home']);
      return false;
    }

    return Observable.create(observer => {
      this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole, Roles.verifierRole])
        .subscribe(isAdmin => {
          if (isAdmin) {
            observer.next(true);
          } else {
            this.toastPrivilegeError();
            this.router.navigate(['/home']);
            observer.next(false);
          }
        });
    });
  }

  toastPrivilegeError() {
    this.toastrService.error("Not enough privileges. Please log in with an administrative account.", "Error");
  }
}
