import { TestBed, async, inject } from '@angular/core/testing';

import { AtLeastVerifierGuard } from './at-least-verifier.guard';
import { GuardModule } from './guard.module';
import { TestSharedModule } from '../test-shared/test-shared.module';

describe('AtLeastVerifierGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        GuardModule,
        TestSharedModule
      ]
    });
  });

  it('should ...', inject([AtLeastVerifierGuard], (guard: AtLeastVerifierGuard) => {
    expect(guard).toBeTruthy();
  }));
});
