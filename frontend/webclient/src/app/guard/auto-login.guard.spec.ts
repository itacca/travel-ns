import { TestBed, async, inject } from '@angular/core/testing';

import { AutoLoginGuard } from './auto-login.guard';
import { GuardModule } from './guard.module';
import { TestSharedModule } from '../test-shared/test-shared.module';

describe('AutoLoginGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        GuardModule,
        TestSharedModule
      ]
    });
  });

  it('should provide', inject([AutoLoginGuard], (guard: AutoLoginGuard) => {
    expect(guard).toBeTruthy();
  }));
});
