import { TestBed, inject } from '@angular/core/testing';
import { LoginGuard } from './login.guard';
import { GuardModule } from './guard.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, 
        GuardModule,
      ]
    });
  });

  it('should provide', inject([LoginGuard], (guard: LoginGuard) => {
    expect(guard).toBeTruthy();
  }));
});
