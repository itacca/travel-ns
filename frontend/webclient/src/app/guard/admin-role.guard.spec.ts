import { TestBed, async, inject } from '@angular/core/testing';

import { AdminRoleGuard } from './admin-role.guard';
import { TestSharedModule } from '../test-shared/test-shared.module';
import { Router } from '@angular/router';
import { AccountService } from '../account/account.service';
import { of, Observable } from 'rxjs';
import { AuthService } from '../core/auth/auth.service';
import { Roles } from '../account/role.model';

describe('AdminRoleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TestSharedModule
      ]
    });
  });

  it('should provide', inject([AdminRoleGuard], (guard: AdminRoleGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should navigate to home if user is not admin', inject([AdminRoleGuard, Router, AccountService, AuthService], 
    (guard: AdminRoleGuard, router: Router, accountService: AccountService, authService: AuthService) => {
    
    spyOn(router, 'navigate').and.callFake((rout) => null);
    spyOn(accountService, 'hasAnyRole').and.callFake((roles) => of(false));
    spyOn(authService, 'isLoggedIn').and.callFake(() => true);

    let observable: any = guard.canActivate(null, null);
    observable.subscribe(allowNavigation => expect(allowNavigation).toBeFalsy());
    
    expect(router.navigate).toHaveBeenCalledWith(['/home']);
    expect(accountService.hasAnyRole).toHaveBeenCalledWith([Roles.administratorRole, Roles.systemAdministratorRole]);
  }));

  it('should navigate to home if user is not logged in', inject([AdminRoleGuard, Router, AccountService, AuthService], 
    (guard: AdminRoleGuard, router: Router, accountService: AccountService, authService: AuthService) => {
    
    spyOn(router, 'navigate').and.callFake((rout) => null);
    spyOn(accountService, 'hasAnyRole').and.callFake((roles) => of(false));
    spyOn(authService, 'isLoggedIn').and.callFake(() => false);

    expect(guard.canActivate(null, null)).toBeFalsy();
    expect(router.navigate).toHaveBeenCalledWith(['/home']);
  }));

  it('should return true if user is admin', inject([AdminRoleGuard, Router, AccountService, AuthService], 
    (guard: AdminRoleGuard, router: Router, accountService: AccountService, authService: AuthService) => {
    
    spyOn(router, 'navigate').and.callFake((rout) => null);
    spyOn(accountService, 'hasAnyRole').and.callFake((roles) => of(true));
    spyOn(authService, 'isLoggedIn').and.callFake(() => true);

    let observable: any = guard.canActivate(null, null);
    observable.subscribe(allowNavigation => expect(allowNavigation).toBeTruthy());

    expect(accountService.hasAnyRole).toHaveBeenCalledWith([Roles.administratorRole, Roles.systemAdministratorRole]);
  }));
});
