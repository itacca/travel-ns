import { TestBed, async, inject } from '@angular/core/testing';

import { OnlyGuestGuard } from './only-guest.guard';
import { GuardModule } from './guard.module';
import { TestSharedModule } from '../test-shared/test-shared.module';

describe('OnlyGuestGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        GuardModule,
        TestSharedModule
      ]
    });
  });

  it('should provide', inject([OnlyGuestGuard], (guard: OnlyGuestGuard) => {
    expect(guard).toBeTruthy();
  }));
});
