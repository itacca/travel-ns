import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationsViewComponent } from './stations-view.component';
import { StationModule } from '../station.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Page } from 'src/app/shared/page.model';
import { Station } from '../station.model';
import { StationService } from '../station.service';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('StationsViewComponent', () => {
  let component: StationsViewComponent;
  let fixture: ComponentFixture<StationsViewComponent>;
  let stationService: StationService;
  let toastrService;
  let stationsPage: Page<Station> = {
    content: [
      new Station(0, 'Station1', [], 19.839120, 45.247803),
      new Station(1, 'Station1', [], 19.839120, 45.247803),
      new Station(2, 'Station1', [], 19.839120, 45.247803),
    ],
    totalPages: 1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StationModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    stationService = TestBed.get(StationService);
    toastrService = TestBed.get(ToastrService);
  });

  function createComponent() {
    fixture = TestBed.createComponent(StationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    spyOn(stationService, 'findAll').and.callFake(any => of(stationsPage));
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render "Stations" in h2 tag', () => {
    let h2Title = fixture.debugElement.query(By.css('h2'));
    expect(h2Title.nativeElement.innerHTML).toContain('Stations');
  });

  it('shoud render a stations list component with stations injected if stationsPage is loaded', () => {
    spyOn(stationService, 'findAll').and.callFake(any => of(stationsPage));
    createComponent()
    let stationsList = fixture.debugElement.query(By.css('app-station-list'));
    expect(stationsList).toBeTruthy();
    expect(stationsList.componentInstance.stations).toBe(stationsPage.content);
  });

  it('shoud not render a stations list component if stationsPage is not loaded', () => {
    spyOn(stationService, 'findAll').and.callFake(any => of(stationsPage));
    createComponent();
    component.stationsPage = null;
    fixture.detectChanges();
    let stationsList = fixture.debugElement.query(By.css('app-station-list'));
    expect(stationsList).toBeFalsy();
  });

  it('should toast error if stationsService findAll() throws error', () => {
    spyOn(stationService, 'findAll').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastrService, 'error').and.callFake(any => null);
    createComponent()
    expect(toastrService.error).toHaveBeenCalledWith('test-error', 'Error');
  });
});
