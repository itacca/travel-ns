import { Component, OnInit } from '@angular/core';
import { StationService } from '../station.service';
import { Page, PageParams } from 'src/app/shared/page.model';
import { Station } from '../station.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-stations-view',
  templateUrl: './stations-view.component.html',
  styleUrls: ['./stations-view.component.css']
})
export class StationsViewComponent implements OnInit {

  stationsPage: Page<Station>;

  pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'id,asc'
  };

  sortFields = [
    { displayName: "ID", name: "id" },
    { displayName: "NAME", name: "name" }
  ];

  constructor(
    private stationService: StationService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadStations(this.pageParams);
  }

  private loadStations(pageParams: PageParams) {
    this.stationService.findAll(pageParams).subscribe(response => {
      this.stationsPage = response;
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

  paramsChanged(pageParams: PageParams) {
    this.loadStations(pageParams);
  }

}
