import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { StationCardComponent } from './station-card.component';
import { StationModule } from '../station.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { AccountService } from 'src/app/account/account.service';
import { StationService } from '../station.service';
import { of, throwError } from 'rxjs';
import { Station } from '../station.model';
import { By } from '@angular/platform-browser';
import { Predicate, DebugElement } from '@angular/core';

describe('StationCardComponent', () => {
  let component: StationCardComponent;
  let fixture: ComponentFixture<StationCardComponent>;
  let station: Station = new Station(0, 'Rowland Station', [], 45.244573, 19.819510);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StationModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  function createComponent() {
    fixture = TestBed.createComponent(StationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  function expectElementToContain(predicate: Predicate<DebugElement>, content: string) {
    expect(fixture.debugElement.query(predicate).nativeElement.innerHTML)
      .toContain(content);
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should display all station data', inject([AccountService], (accountService: AccountService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(false));
    createComponent();
    component.station = station;
    fixture.detectChanges();
    
    expectElementToContain(By.css('.station-card-name'), station.name);
    expectElementToContain(By.css('.card-type'), 'Station');
    expect(fixture.debugElement.query(By.css('.map'))).toBeTruthy();
    expectElementToContain(By.css('.lng'), `Longitude: ${station.longitude}`);
    expectElementToContain(By.css('.lat'), `Latitude: ${station.latitude}`);
  }));

  it('should display admin options if user is admin', inject([AccountService], (accountService: AccountService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(true));

    createComponent();
    component.station = station;
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('.edit-station-btn'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('.delete-station-btn'))).toBeTruthy();
  }));

  it('should not display admin options if user is not admin', inject([AccountService, StationService], 
    (accountService: AccountService, stationService: StationService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(false));
    spyOn(stationService, 'delete').and.callFake(any => of(null));

    createComponent();
    component.station = station;
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('.edit-station-btn'))).toBeFalsy();
    expect(fixture.debugElement.query(By.css('.delete-station-btndelete-station-btn'))).toBeFalsy();
  }));

  it('deleteStation() should forward the call to the service and on success emit a deleted event', inject([AccountService, StationService],
    (accountService: AccountService, stationService: StationService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(true));
    spyOn(stationService, 'delete').and.callFake(any => of(null));
    
    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    spyOn(component, 'deleteStation').and.callThrough();
    component.station = station;
    fixture.detectChanges();

    let deleteBtn = fixture.debugElement.query(By.css('.delete-station-btn'));

    deleteBtn.nativeElement.click();
    
    expect(component.deleteStation).toHaveBeenCalled();
    expect(stationService.delete).toHaveBeenCalledWith(station);
    expect(component.deleted.emit).toHaveBeenCalled();
  }));

  it('deleteStation() should not emit a deleted event if when deletion fails', inject([AccountService, StationService],
    (accountService: AccountService, stationService: StationService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(true));
    spyOn(stationService, 'delete').and.callFake(any => throwError(new Error('failed deletion test')));

    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    component.station = station;
    fixture.detectChanges();

    expect(component.deleted.emit).toHaveBeenCalledTimes(0);
  }));

  it('should display station name', () => {
    createComponent();
    component.station = station;
    fixture.detectChanges();

    expectElementToContain(By.css('.station-card-name'), station.name);
  });

  it('should display latitude', () => {
    createComponent();
    component.station = station;
    fixture.detectChanges();

    expectElementToContain(By.css('.lat'), `${station.latitude}`);
  });

  it('should display longitude', () => {
    createComponent();
    component.station = station;
    fixture.detectChanges();

    expectElementToContain(By.css('.lng'), `${station.longitude}`);
  });

  it('should display edit station button', () => {
    createComponent();
    component.isAdmin = true;
    component.station = station;
    fixture.detectChanges();

    expectElementToContain(By.css('.edit-station-btn'), 'Edit')
  });

  it('should display delete station button', () => {
    createComponent();
    component.isAdmin = true;
    component.station = station;
    fixture.detectChanges();

    expectElementToContain(By.css('.delete-station-btn'), 'Delete')
  });
});
