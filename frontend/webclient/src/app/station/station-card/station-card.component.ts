import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Station } from '../station.model';
import { Coordinates } from 'src/app/shared/marker-map/coordinates.model';
import { Marker } from 'src/app/shared/marker-map/marker.model';
import { AccountService } from 'src/app/account/account.service';
import { Roles } from 'src/app/account/role.model';
import { StationService } from '../station.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-station-card',
  templateUrl: './station-card.component.html',
  styleUrls: ['./station-card.component.css']
})
export class StationCardComponent implements OnInit {

  _station: Station;

  coordinates: Coordinates;

  markers: Marker[];

  isAdmin: boolean;

  @Output()
  deleted: EventEmitter<any> = new EventEmitter();

  constructor(
    private accountService: AccountService,
    private stationService: StationService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.checkRoles();
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  @Input()
  set station(station: Station) {
    this._station = station;
    this.coordinates = new Coordinates(station.latitude, station.longitude);
    this.markers = [new Marker(this.coordinates)];
  }

  get station() {
    return this._station;
  }

  deleteStation() {
    this.stationService.delete(this.station).subscribe(
      success => {
        this.toastrService.success("Station deleted", "Success");
        this.deleted.emit();
      },
      error => this.toastrService.error(error.json().message, "Error")
    );
  }
}
