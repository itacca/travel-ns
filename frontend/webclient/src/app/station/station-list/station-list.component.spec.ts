import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationListComponent } from './station-list.component';
import { StationModule } from '../station.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Station } from '../station.model';
import { By } from '@angular/platform-browser';

describe('StationListComponent', () => {
  let component: StationListComponent;
  let fixture: ComponentFixture<StationListComponent>;

  let stations: Station[] = [
    new Station(0, 'Test station 0', [], 45.244573, 19.819510),
    new Station(1, 'Test station 1', [], 45.244573, 19.819510),
    new Station(2, 'Test station 2', [], 45.244573, 19.819510),
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StationModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a station card for each station in the stations list', () => {
    component.stations = stations;
    fixture.detectChanges();
    let stationCards = fixture.debugElement.queryAll(By.css('app-station-card'));
    expect(stationCards.length).toEqual(stations.length);
    for (let i = 0; i < stations.length; i++) {
      expect(stationCards[i].componentInstance.station).toEqual(stations[i]);
    }
  });

  it('removeStationFromList() should remove the given station from the list and the removed station should not be rendered.', () => {
    component.stations = stations;
    fixture.detectChanges();
    let stationCount = stations.length;
    let indexToRemove = 1;
    let stationCards = fixture.debugElement.queryAll(By.css('app-station-card'));
    let expectedRemovedStation = stationCards[indexToRemove];
    component.removeStationFromList(indexToRemove);
    fixture.detectChanges();
    stationCards = fixture.debugElement.queryAll(By.css('app-station-card'));
    expect(stationCards[indexToRemove]).not.toBe(expectedRemovedStation);
    expect(stationCards.length).toEqual(stationCount - 1);
  });
});
