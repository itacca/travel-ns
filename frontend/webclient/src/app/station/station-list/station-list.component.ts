import { Component, OnInit, Input } from '@angular/core';
import { Station } from '../station.model';

@Component({
  selector: 'app-station-list',
  templateUrl: './station-list.component.html',
  styleUrls: ['./station-list.component.css']
})
export class StationListComponent implements OnInit {

  @Input()
  stations: Station[];

  constructor() { }

  ngOnInit() {
  }

  removeStationFromList(index: number) {
    this.stations.splice(index, 1);
  }

}
