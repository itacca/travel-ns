import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { Station } from './station.model';
import { PageParams, Page } from '../shared/page.model';
import { UtilService } from '../core/util.service';
import { AuthService } from '../core/auth/auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StationService {

  stationEndpoint: string = `${environment.apiRoot}/station`;

  stationsEndpoint: string = `${environment.apiRoot}/stations`;

  constructor(
    private http: Http,
    private util: UtilService,
    private authService: AuthService
  ) { }

  delete(station: Station): Observable<any> {
    return this.http.delete(`${this.stationEndpoint}/${station.id}`, this.authService.getHttpAuthOptions());
  }

  findAll(pageParams: PageParams): Observable<Page<Station>> {
    return this.http.get(`${this.stationsEndpoint}${this.util.getPageableParamString(pageParams)}`, 
      this.authService.getHttpAuthOptions()).pipe(map(
      response => response.json(),
      error => Observable.throw(error)
    ));
  }

  findById(stationId: number): Observable<Station> {
    return this.http.get(`${this.stationEndpoint}/${stationId}`, 
      this.authService.getHttpAuthOptions()).pipe(map(
      response => response.json(),
      error => Observable.throw(error)
    ));
  }

  update(station: Station): Observable<any> {
    return this.http.put(this.stationEndpoint, station, this.authService.getHttpAuthOptions());
  }

  create(station: Station): Observable<any> {
    return this.http.post(this.stationsEndpoint, station, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }
  
}
