import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';
import { StationRoutingModule } from './station-routing.module';
import { StationCardComponent } from './station-card/station-card.component';
import { StationListComponent } from './station-list/station-list.component';
import { StationsViewComponent } from './stations-view/stations-view.component';
import { EditStationComponent } from './edit-station/edit-station.component';
import { CreateStationComponent } from './create-station/create-station.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    StationRoutingModule,
  ],
  exports: [
    StationRoutingModule
  ],
  declarations: [
    StationCardComponent,
    StationListComponent,
    StationsViewComponent,
    EditStationComponent,
    CreateStationComponent
  ]
})
export class StationModule { }
