import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { StationService } from '../station.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Station } from '../station.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';
import { SetMarkerOnClick } from 'src/app/shared/dynamic-map/map-options/set-marker-on-click.option';
import { MapOption, optionsInitialized } from 'src/app/shared/dynamic-map/map-options/map-options';
import { Coordinates } from 'src/app/shared/marker-map/coordinates.model';

@Component({
  selector: 'app-edit-station',
  templateUrl: './edit-station.component.html',
  styleUrls: ['./edit-station.component.css']
})
export class EditStationComponent implements OnInit, OnDestroy {

  routeSubscription: Subscription;

  station: Station;

  stationGroup: FormGroup;

  mapOptions: MapOption[];

  markerSetter: SetMarkerOnClick;

  constructor(
    private stationService: StationService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private util: UtilService
  ) { }

  ngOnInit() {
    this.initMapOptions();
    this.subscribeToRouteParams();
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  private initMapOptions() {
    let mapInitializer = new InitilizeMapWithId('unique', 45.249302, 19.824508, 14);
    this.markerSetter = new SetMarkerOnClick();

    this.markerSetter.markerCreated.subscribe(coordinates => {
      this.stationGroup.patchValue({
        latitudeCtrl: coordinates.latitude,
        longitudeCtrl: coordinates.longitude
      });
    });

    this.mapOptions = [
      mapInitializer,
      this.markerSetter
    ];
  }

  private initStationGroup() {
    this.stationGroup = this.formBuilder.group({
      nameCtrl: [this.station.name, Validators.required],
      latitudeCtrl: [this.station.latitude, Validators.required],
      longitudeCtrl: [this.station.longitude, Validators.required]
    });
  }

  private subscribeToRouteParams() {
    this.routeSubscription = this.activatedRoute.params.subscribe(params => {
      this.loadStation(params.stationId);
    });
  }

  private loadStation(stationId: number) {
    this.stationService.findById(stationId).subscribe(result => {
      this.station = result;
      this.initStationGroup();
      optionsInitialized(this.mapOptions).subscribe(() => {
        this.markerSetter.setMarker(new Coordinates(this.station.latitude, this.station.longitude));
      });
    }, error => {
      this.toastr.error(error.json().message, 'Error');
      this.router.navigate(['/stations']);
    });
  }

  saveStation() {
    let station: Station = this.util.formGroupToModel(this.station, [this.stationGroup], 'Ctrl');
    this.stationService.update(station).subscribe(result => {
      this.toastr.success("Station updated.", "Success");
      this.router.navigate(['/stations']);
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

}
