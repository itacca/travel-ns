import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStationComponent } from './edit-station.component';
import { StationModule } from '../station.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { StationService } from '../station.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { Station } from '../station.model';
import { UtilService } from 'src/app/core/util.service';
import { By } from '@angular/platform-browser';
import { expectElementToContain, expectElementToBeVisible } from 'src/app/test-shared/common-expects.spec';

describe('EditStationComponent', () => {
  let component: EditStationComponent;
  let fixture: ComponentFixture<EditStationComponent>;

  let stationService: StationService;
  let toastr: ToastrService;
  let activatedRoute: ActivatedRoute;
  let router: Router;
  let formBuilder: FormBuilder;
  let util: UtilService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StationModule,
        TestSharedModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ stationId: 0 })
          }
        },
        {
          provide: ToastrService,
          useValue: {
            error: () => null
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    stationService = TestBed.get(StationService);
    activatedRoute = TestBed.get(ActivatedRoute);
    formBuilder = TestBed.get(FormBuilder);
    toastr = TestBed.get(ToastrService);
    router = TestBed.get(Router);
    util = TestBed.get(UtilService);

    spyOn(toastr, 'error').and.callFake(() => null);
  });

  function createComponent() {
    fixture = TestBed.createComponent(EditStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should load station with stationId route param', () => {
    let params = { stationId: 0 };
    let expectedStation = new Station();
    activatedRoute.params = of(params);
    spyOn(activatedRoute.params, 'subscribe').and.callThrough();
    spyOn(stationService, 'findById').and.callFake(() => of(expectedStation));
    createComponent();

    expect(component.station).toBe(expectedStation);
    expect(stationService.findById).toHaveBeenCalledWith(params.stationId);
    expect(component.routeSubscription).toBeTruthy();
    expect(component.stationGroup.contains('nameCtrl')).toBeTruthy();
    expect(component.stationGroup.contains('latitudeCtrl')).toBeTruthy();
    expect(component.stationGroup.contains('longitudeCtrl')).toBeTruthy();
  });

  it('should toast error and navigate to station list if station with stationId param does not exist', () => {
    let params = { stationId: 0 };
    activatedRoute.params = of(params);
    spyOn(activatedRoute.params, 'subscribe').and.callThrough();
    spyOn(stationService, 'findById').and.callFake(() => throwError({ json: () => new Error('test-error') }));
    spyOn(router, 'navigate').and.callFake(() => null);
    createComponent();

    expect(component.station).toBeFalsy();
    expect(component.stationGroup).toBeFalsy();
    expect(component.routeSubscription).toBeTruthy();
    expect(stationService.findById).toHaveBeenCalledWith(params.stationId);
    expect(toastr.error).toHaveBeenCalledWith('test-error', 'Error');
    expect(router.navigate).toHaveBeenCalledWith(['/stations']);
  });

  it('should display station details in input fields', () => {
    let station = new Station(0, 'station1', [], 1, 2);
    spyOn(stationService, 'findById').and.callFake(() => of(station));
    createComponent();
    
    let name = fixture.debugElement.query(By.css('input[formControlName="nameCtrl"]'));
    let latitude = fixture.debugElement.query(By.css('input[formControlName="latitudeCtrl"]'));
    let longitude = fixture.debugElement.query(By.css('input[formControlName="longitudeCtrl"]'));

    expect(name.nativeElement.value).toContain(station.name);
    expect(latitude.nativeElement.value).toContain(station.latitude);
    expect(longitude.nativeElement.value).toContain(station.longitude);
  });

  it('should save ticket when save button is clicked', () => {
    let station = new Station(0, 'station1', [], 1, 2);
    spyOn(stationService, 'findById').and.callFake(() => of(station));
    createComponent();
    spyOn(component, 'saveStation').and.callFake(any => null);
    
    fixture.debugElement.query(By.css('.save-btn')).triggerEventHandler('click', {});
    expect(component.saveStation).toHaveBeenCalled();
  });

  it('saveStation() should toast error if service throws error', () => {
    let station = new Station(0, 'station1', [], 1, 2);
    spyOn(stationService, 'findById').and.callFake(() => of(station));
    spyOn(stationService, 'update').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    createComponent();
    
    component.saveStation();
    expect(toastr.error).toHaveBeenCalledWith('test-error', 'Error');
  });

  it('should display "Create station" in h2 tag', () => {
    expectElementToContain(fixture, By.css('h2.mat-h2'), 'Edit station');
  });

  it('should display name input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="nameCtrl"]'));
  });

  it('should display latitude input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="latitudeCtrl"]'));
  });

  it('should display longitude input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="longitudeCtrl"]'));
  });

  it('should display save button', () => {
    expectElementToContain(fixture, By.css('.save-btn'), 'Save');
  });
});
