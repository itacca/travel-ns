import { TestBed, inject } from '@angular/core/testing';

import { TestSharedModule } from '../test-shared/test-shared.module';
import { StationService } from './station.service';
import { StationModule } from './station.module';
import { Http, RequestOptions } from '@angular/http';
import { Station } from './station.model';
import { environment } from 'src/environments/environment';
import { UtilService } from '../core/util.service';
import { AuthService } from '../core/auth/auth.service';
import { givenHttpPostThrowsError, 
         givenAuthGetHttpOptionsReturnsHttpOptions, 
         givenHttpPostReturnsResponseContainingContent, 
         givenHttpPutReturnsResponseContainingContent, 
         givenHttpGetReturnsResponseContainingContent, 
         givenHttpGetThrowsError, 
         givenHttpDeleteReturnsResponseContainingContent, 
         givenHttpDeleteThrowsError } from '../test-shared/common-stubs.spec';
import { expectServiceMethodToThrowError, 
         expectServiceMethodToMapResponseToObject } from '../test-shared/common-expects.spec';

describe('StationService', () => {
  let httpOptions = new RequestOptions();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StationModule,
        TestSharedModule
      ]
    });
  });

  it('should be created', inject([StationService], (service: StationService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', inject([StationService], (stationService: StationService) => {
    expect(stationService.stationEndpoint).toEqual(`${environment.apiRoot}/station`);
    expect(stationService.stationsEndpoint).toEqual(`${environment.apiRoot}/stations`);
  }));

  it('delete() should use correct endpoints and params', 
    inject([StationService, Http, AuthService], (stationService: StationService, http: Http, authService: AuthService) => {
    let station = new Station(0);
    let expectedContent = {};

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpDeleteReturnsResponseContainingContent(http, expectedContent);

    stationService.delete(station).subscribe();

    expect(http.delete).toHaveBeenCalledWith(`${stationService.stationEndpoint}/${station.id}`, httpOptions);
  }));

  it('findAll() should map response to object if no errors are thrown', inject([Http, UtilService, AuthService, StationService], 
    (http: Http, utilService: UtilService, authService: AuthService, stationService: StationService) => {
    let expectedContent = { content: [] };
    let testParamString = '?test-params';

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetReturnsResponseContainingContent(http, expectedContent);
    spyOn(utilService, 'getPageableParamString').and.callFake(any => testParamString);

    expectServiceMethodToMapResponseToObject(stationService.findAll({}), expectedContent, 'findAll()');
    expect(http.get).toHaveBeenCalledWith(`${stationService.stationsEndpoint}${testParamString}`, httpOptions);
  }));

  it('findAll() should throw error if errors are thrown', inject([Http, UtilService, AuthService, StationService], 
    (http: Http, utilService: UtilService, authService: AuthService, stationService: StationService) => {
    let testParamString = '?test-params';

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetThrowsError(http, new Error('test-error'));
    spyOn(utilService, 'getPageableParamString').and.callFake(any => testParamString);

    expectServiceMethodToThrowError(stationService.findAll({}), 'test-error', 'findAll()');
    expect(http.get).toHaveBeenCalledWith(`${stationService.stationsEndpoint}${testParamString}`, httpOptions);
  }));

  it('findById() should map response to object if no errors are thrown', inject([Http, AuthService, StationService], 
    (http: Http, authService: AuthService, stationService: StationService) => {
    let expectedContent = new Station();
    let stationId = 0;

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetReturnsResponseContainingContent(http, expectedContent);
    
    expectServiceMethodToMapResponseToObject(stationService.findById(stationId), expectedContent, 'findById()');
    expect(http.get).toHaveBeenCalledWith(`${stationService.stationEndpoint}/${stationId}`, httpOptions);
  }));

  it('findById() should throw error if errors are thrown', inject([Http, AuthService, StationService], 
    (http: Http, authService: AuthService, stationService: StationService) => {
    let stationId = 0;

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetThrowsError(http, new Error('test-error'));
    
    expectServiceMethodToThrowError(stationService.findById(stationId), 'test-error', 'findById()');
    expect(http.get).toHaveBeenCalledWith(`${stationService.stationEndpoint}/${stationId}`, httpOptions);
  }));

  it('update() should use correct endpoint with correct params', inject([Http, AuthService, StationService], 
    (http: Http, authService: AuthService, stationService: StationService) => {
    let station = new Station();
  
    givenHttpPutReturnsResponseContainingContent(http, {});
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    stationService.update(station).subscribe();

    expect(http.put).toHaveBeenCalledWith(stationService.stationEndpoint, station, httpOptions);
  }));

  it('create() should map response to object if no error is thrown', inject([Http, AuthService, StationService], 
    (http: Http, authService: AuthService, stationService: StationService) => {
    let expectedContent = { id: 1 };
    let station = new Station();

    givenHttpPostReturnsResponseContainingContent(http, expectedContent);
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    expectServiceMethodToMapResponseToObject(stationService.create(station), expectedContent, 'create()');
    expect(http.post).toHaveBeenCalledWith(stationService.stationsEndpoint, new Station(), httpOptions);
  }));

  it('create() should throw error if http returns error', inject([Http, AuthService, StationService], 
    (http: Http, authService: AuthService, stationService: StationService) => {
    let expectedContent = { id: 1 };
    let station = new Station();

    givenHttpPostThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    expectServiceMethodToThrowError(stationService.create(station), 'test-error', 'create()');
    expect(http.post).toHaveBeenCalledWith(stationService.stationsEndpoint, station, httpOptions);
  }));
});
