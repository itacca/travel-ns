import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StationsViewComponent } from './stations-view/stations-view.component';
import { AutoLoginGuard } from '../guard/auto-login.guard';
import { EditStationComponent } from './edit-station/edit-station.component';
import { AdminRoleGuard } from '../guard/admin-role.guard';
import { CreateStationComponent } from './create-station/create-station.component';

const routes: Routes = [
  { path: 'stations', component: StationsViewComponent, canActivate: [AutoLoginGuard] },
  { path: 'edit-station/:stationId', component: EditStationComponent, canActivate: [AutoLoginGuard, AdminRoleGuard] },
  { path: 'create-station', component: CreateStationComponent, canActivate: [AutoLoginGuard, AdminRoleGuard] }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StationRoutingModule { }
