export class Station {

  constructor(
    public id?: number,
    public name?: string,
    public lineStations?: any[],
    public latitude?: number,
    public longitude?: number
  ) { }
}
