import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateStationComponent } from './create-station.component';
import { StationModule } from '../station.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { By } from '@angular/platform-browser';
import { UtilService } from 'src/app/core/util.service';
import { Station } from '../station.model';
import { Router } from '@angular/router';
import { StationService } from '../station.service';
import { of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { expectElementToContain, expectElementToBeVisible } from 'src/app/test-shared/common-expects.spec';

describe('CreateStationComponent', () => {
  let component: CreateStationComponent;
  let fixture: ComponentFixture<CreateStationComponent>;
  
  let util: UtilService;
  let router: Router;
  let stationService: StationService;
  let toastr: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StationModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    router = TestBed.get(Router);
    stationService = TestBed.get(StationService);
    toastr = TestBed.get(ToastrService);

    fixture = TestBed.createComponent(CreateStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Create station" in h2 tag', () => {
    let h2 = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(h2.innerHTML).toContain('Create station');
  });

  it('should render input fields for ticket information', () => {
    let name = fixture.debugElement.query(By.css('input[formControlName="nameCtrl"]'));
    let latitude = fixture.debugElement.query(By.css('input[formControlName="latitudeCtrl"]'));
    let longitude = fixture.debugElement.query(By.css('input[formControlName="longitudeCtrl"]'));

    expect(name).toBeTruthy();
    expect(latitude).toBeTruthy();
    expect(longitude).toBeTruthy();
  });

  it('saveStation() should call stationService.create() with correct stametion instance', () => {
    let expectedStation = new Station();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedStation);
    spyOn(stationService, 'create').and.callFake(any => of({ id: 1 }));
    spyOn(router, 'navigate').and.callFake(any => null);
    component.saveStation();
    expect(stationService.create).toHaveBeenCalledWith(expectedStation);
    expect(router.navigate).toHaveBeenCalledWith(['stations']);
  });

  it('saveStation() should toast error if stationService.create() throws error', () => {
    let expectedStation = new Station();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedStation);
    spyOn(stationService, 'create').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastr, 'error')
    component.saveStation();
    expect(stationService.create).toHaveBeenCalledWith(expectedStation);
    expect(toastr.error).toHaveBeenCalledWith('test-error', 'Error');
  });

  it('should display "Create station" in h2 tag', () => {
    expectElementToContain(fixture, By.css('h2.mat-h2'), 'Create station');
  });

  it('should display name input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="nameCtrl"]'));
  });

  it('should display latitude input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="latitudeCtrl"]'));
  });

  it('should display longitude input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="longitudeCtrl"]'));
  });

  it('should display save button', () => {
    expectElementToContain(fixture, By.css('button.save-btn'), 'Save');
  });
});
