import { Component, OnInit } from '@angular/core';
import { StationService } from '../station.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { Station } from '../station.model';
import { MapOption } from 'src/app/shared/dynamic-map/map-options/map-options';
import { SetMarkerOnClick } from 'src/app/shared/dynamic-map/map-options/set-marker-on-click.option';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';

@Component({
  selector: 'app-create-station',
  templateUrl: './create-station.component.html',
  styleUrls: ['./create-station.component.css']
})
export class CreateStationComponent implements OnInit {

  stationGroup: FormGroup;

  mapOptions: MapOption[];

  constructor(
    private stationService: StationService,
    private toastr: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder,
    private util: UtilService
  ) { }

  ngOnInit() {
    this.initStationGroup();
    this.initMapOptions();
  }

  private initMapOptions() {
    let mapInitializer = new InitilizeMapWithId('unique', 45.249302, 19.824508, 14);
    let markerSetter = new SetMarkerOnClick();

    markerSetter.markerCreated.subscribe(coordinates => {
      this.stationGroup.patchValue({
        latitudeCtrl: coordinates.latitude,
        longitudeCtrl: coordinates.longitude
      });
    });

    this.mapOptions = [
      mapInitializer,
      markerSetter
    ];
  }

  private initStationGroup() {
    this.stationGroup = this.formBuilder.group({
      nameCtrl: [null, Validators.required],
      latitudeCtrl: [null, Validators.required],
      longitudeCtrl: [null, Validators.required]
    });
  }

  saveStation() {
    let station: Station = this.util.formGroupToModel(new Station(), [this.stationGroup], 'Ctrl');
    this.stationService.create(station).subscribe(result => {
      this.toastr.success("Station created", "Success");
      this.router.navigate(['stations']);
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

}
