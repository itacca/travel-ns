import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { PriceListItemViewComponent } from './price-list-item-view.component';
import { PriceListModule } from '../../price-list/pricelist.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { AccountService } from 'src/app/account/account.service';
import { of, throwError } from 'rxjs';
import { PriceListItem } from '../price-list-item.model';
import { By } from '@angular/platform-browser';
import { Predicate, DebugElement } from '@angular/core';
import { Ticket } from 'src/app/ticket/ticket.model';
import { CityLine } from 'src/app/city-lines/city-line.model';

describe('PriceListItemViewComponent', () => {
  let component: PriceListItemViewComponent;
  let fixture: ComponentFixture<PriceListItemViewComponent>;
  let priceListItem: PriceListItem = new PriceListItem(0, 88, new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")), new Date());

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PriceListModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  function createComponent() {
    fixture = TestBed.createComponent(PriceListItemViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  function expectElementToContain(predicate: Predicate<DebugElement>, content: string) {
    expect(fixture.debugElement.query(predicate).nativeElement.innerHTML)
      .toContain(content);
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should display all price list item data', inject([AccountService], (accountService: AccountService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(true));
    createComponent();
    component.priceListItem = priceListItem;
    fixture.detectChanges();
    
    expectElementToContain(By.css('.price'),  `Price: ${priceListItem.price}`);
    expectElementToContain(By.css('.ticket-duration'), `Ticket duration: ${priceListItem.ticket.ticketDuration}`);
    expectElementToContain(By.css('.ticket-type'), `Ticket type: ${priceListItem.ticket.ticketType}`);
    expectElementToContain(By.css('.city-line-type'), `City line type: ${priceListItem.ticket.cityLine.cityLineType}`);
    expectElementToContain(By.css('.city-line-name'), `City line name: ${priceListItem.ticket.cityLine.name}`);
  }));
});
