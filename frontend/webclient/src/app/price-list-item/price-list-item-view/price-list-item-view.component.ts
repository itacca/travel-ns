import { Component, OnInit, Input } from '@angular/core';
import { PriceListService } from 'src/app/price-list/pricelist.service';
import { PriceListItem } from '../price-list-item.model';
import { ToastrService } from 'ngx-toastr';
import { Page, PageParams } from 'src/app/shared/page.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-price-list-item-view',
  templateUrl: './price-list-item-view.component.html',
  styleUrls: ['./price-list-item-view.component.css']
})
export class PriceListItemViewComponent implements OnInit {

  @Input()
  priceListItem: PriceListItem;

  pageParams: PageParams = {
    size: 8,
    page: 0,
    sort: 'asc'
  };

  constructor(
    private priceListService: PriceListService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  
  }  
}
