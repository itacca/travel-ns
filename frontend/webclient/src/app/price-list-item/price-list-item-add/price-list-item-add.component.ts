import { Component, OnInit } from '@angular/core';
import { PriceListService } from 'src/app/price-list/pricelist.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { PriceList } from 'src/app/price-list/pricelist.model';
import { Ticket } from 'src/app/ticket/ticket.model';
import { TicketService } from 'src/app/ticket/service/ticket.service';
import { PriceListItem } from 'src/app/price-list-item/price-list-item.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PageParams, Page } from 'src/app/shared/page.model';

@Component({
  selector: 'app-add-price-list-item',
  templateUrl: './price-list-item-add.component.html',
  styleUrls: ['./price-list-item-add.component.css']
})

export class PriceListItemAddComponent implements OnInit {

  priceListItemGroup: FormGroup;

  priceListHere: PriceList;

  selectedTicket: Ticket;

  tickets: Page<Ticket>;

  priceListItemGroups: Array<FormGroup> = [];

  pageParams: PageParams = {
    size: 200,
    page: 0,
    sort: 'asc'
  };

  constructor(
    private priceListService: PriceListService,
    private ticketService: TicketService,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService,
    private router: Router,
    public dialogRef: MatDialogRef<PriceListItemAddComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) {
    this.priceListHere = data.priceList;
    this.priceListItemGroups = data.priceListItemGroups;

  }

  ngOnInit() {
    this.initGroups();
    this.loadTicketsNotInPriceList();

  }

  initGroups() {
    this.priceListItemGroup = this.formBuilder.group({
      priceCtrl: ['', Validators.required],
    })
  }

  loadTicketsNotInPriceList() {
    this.ticketService.findTicketsThatDoNotHavePriceListItemInGivenPriceList(this.data.priceList.id, this.pageParams).subscribe(tickets => {
      this.tickets = tickets;
    }, error => this.toastr.error(error.message, 'Error'));
  }

  addPriceListItem() {
    let ticket: Ticket = this.selectedTicket;
    let priceListItem: PriceListItem = this.util.formGroupToModel(new PriceListItem(), [this.priceListItemGroup], 'Ctrl');
    priceListItem.ticket = ticket;
    priceListItem.priceList = this.data.priceList;
    this.priceListService.addPriceListItem(priceListItem).subscribe(
      success => {
        this.toastr.success("Price list item successfuly added", "Success");
        let newItem: PriceListItem = {};
        newItem.id = success.id;
        newItem.price = priceListItem.price;
        newItem.ticket = priceListItem.ticket;
        let priceListNew: PriceList = {};
        priceListNew.id = priceListItem.priceList.id;
        priceListNew.endDate = priceListItem.priceList.endDate;
        priceListNew.startDate = priceListItem.priceList.startDate;
        newItem.priceList = priceListNew;
        this.priceListHere.priceListItems.push(newItem);
        let group: FormGroup;
        group = this.formBuilder.group({
          priceCtrl: ['', Validators.required],
        });
        this.data.priceListItemGroups.push(group);
        this.dialogRef.close(priceListItem);

      },
      error =>{
        this.toastr.error("Unable to add price list item", 'Error');
        this.dialogRef.close(null);
      }
    )
  }
}
