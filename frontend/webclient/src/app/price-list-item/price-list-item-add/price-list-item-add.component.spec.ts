import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PriceListItemAddComponent } from './price-list-item-add.component';
import { PriceListModule } from '../../price-list/pricelist.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { PriceList } from '../../price-list/pricelist.model';
import { PriceListItem } from 'src/app/price-list-item/price-list-item.model';
import { Page } from 'src/app/shared/page.model';
import { Ticket } from 'src/app/ticket/ticket.model';
import { By } from '@angular/platform-browser';
import { FormGroup, FormControl } from '@angular/forms';
import { CityLine } from 'src/app/city-lines/city-line.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UtilService } from 'src/app/core/util.service';
import { Router } from '@angular/router';
import { PriceListService } from 'src/app/price-list/pricelist.service';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';


describe('PriceListItemAddComponent', () => {
    let component: PriceListItemAddComponent;
    let fixture: ComponentFixture<PriceListItemAddComponent>;

    let util: UtilService;
    let router: Router;
    let priceListService: PriceListService;
    let toastr: ToastrService;

    let priceListHere: PriceList = new PriceList(0, new Date(), new Date(), new Array<PriceListItem>());
    let isAdmin: boolean = true;
    let priceListItemGroups: Array<FormGroup> = [new FormGroup({
        priceCtrl: new FormControl()
    }), new FormGroup({
        priceCtrl: new FormControl()
    }), new FormGroup({
        priceCtrl: new FormControl()
    })];

    let data: {
        priceList: PriceList,
        priceListItemGroups: Array<FormGroup>
    } = {
        priceList: priceListHere,
        priceListItemGroups: priceListItemGroups
    };

    let priceListItemGroup = new FormGroup({
        priceCtrl: new FormControl()
    });

    let tickets: Page<Ticket> = {
        content: [
            new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")),
            new Ticket(2, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")),
            new Ticket(1, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS"))

        ],
        totalPages: 1
    };


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                PriceListModule,
                TestSharedModule
            ],
            providers: [{
                provide: MatDialogRef,
                useValue: { close: () => { } }
            }, {
                provide: MAT_DIALOG_DATA,
                useValue: data
            }]
        })
            .compileComponents();
    }));

    function createComponent() {
        fixture = TestBed.createComponent(PriceListItemAddComponent);
        util = TestBed.get(UtilService);
        router = TestBed.get(Router);
        priceListService = TestBed.get(PriceListService);
        toastr = TestBed.get(ToastrService);
        component = fixture.componentInstance;
        component.tickets = tickets;
        component.priceListItemGroup = priceListItemGroup;
        component.priceListHere = priceListHere;
        fixture.detectChanges();
    }

    it('should create', () => {
        createComponent();
        expect(component).toBeTruthy();
    });

    it('should render "Add price list item" in h1 tag', () => {
        createComponent();
        let h1 = fixture.debugElement.query(By.css('h1')).nativeElement;
        expect(h1.innerHTML).toContain('Add price list item');
    });

    it('should render input fields for price list item information', () => {
        createComponent();
        let price = fixture.debugElement.query(By.css('input[formControlName="priceCtrl"]'));
        expect(price).toBeTruthy();
    });

    it('addPriceListItem() should call priceListService.addPriceListItem() with correct price list item instance', () => {
        createComponent();
        let expectedPriceListItem = new PriceListItem();
        spyOn(util, 'formGroupToModel').and.callFake(any => expectedPriceListItem);
        spyOn(priceListService, 'addPriceListItem').and.callFake(any => of({ id: 1 }));
        component.addPriceListItem();
        expect(priceListService.addPriceListItem).toHaveBeenCalledWith(expectedPriceListItem);
    });
});
