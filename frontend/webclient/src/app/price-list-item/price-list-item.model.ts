import { Ticket } from "../ticket/ticket.model";
import { PriceList } from "../price-list/pricelist.model";

export class PriceListItem {
    constructor (
      public id?: number,
      public price?: number,
      public ticket?: Ticket,
      public dateActive?: Date,
      public priceList?: PriceList
    ){}
}