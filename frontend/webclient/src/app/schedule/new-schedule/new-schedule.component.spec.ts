import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewScheduleComponent } from './new-schedule.component';
import { UtilService } from '../../core/util.service';
import { Router } from '@angular/router';
import { Schedule } from '../schedule.model';
import { By } from '@angular/platform-browser';
import { ScheduleService } from '../service/schedule.service';
import { ToastrService } from 'ngx-toastr';
import { ScheduleModule } from '../schedule.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { of, throwError } from 'rxjs';

describe('NewScheduleComponent', () => {
  let component: NewScheduleComponent;
  let fixture: ComponentFixture<NewScheduleComponent>;

  let util: UtilService;
  let router: Router;
  let scheduleService: ScheduleService;
  let toastr: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ScheduleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    router = TestBed.get(Router);
    scheduleService = TestBed.get(ScheduleService);
    toastr = TestBed.get(ToastrService);

    fixture = TestBed.createComponent(NewScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
  it('should render input fields for schedule information', () => {
    let scheduleType = fixture.debugElement.query(By.css('.select-type-value'));
    let departureTime = fixture.debugElement.query(By.css('.select-new-time'));

    expect(scheduleType).toBeTruthy();
    expect(departureTime).toBeTruthy();
  });
  */

  it('saveSchedule() should call schedule.saveSchedule()', () => {
    let expectedSchedule = new Schedule();
    component.schedule = expectedSchedule;
    component.lineId = 1;
    spyOn(component, 'addNewItem').and.callFake(any => null);
    spyOn(scheduleService, 'saveSchedule').and.callFake(any => of({ id: 1 }));
    spyOn(scheduleService, 'saveScheduleItems').and.callFake(any => of({ id: 1 }));
    spyOn(router, 'navigate').and.callFake(any => null);
    component.saveSchedule();
    expect(scheduleService.saveSchedule).toHaveBeenCalledWith(expectedSchedule);
    expect(router.navigate).toHaveBeenCalledWith(['/schedule', 1]);
  });

  it('saveStation() should toast error if stationService.create() throws error', () => {
    let expectedSchedule = new Schedule();
    component.schedule = expectedSchedule;
    component.lineId = 1;

    spyOn(component, 'addNewItem').and.callFake(any => null);
    spyOn(scheduleService, 'saveSchedule').and.callFake(any => throwError({json: () => new Error('test-error')}));
    spyOn(scheduleService, 'saveScheduleItems').and.callFake(any => throwError({json: () => new Error('test-error')}));
    spyOn(router, 'navigate').and.callFake(any => null);

    spyOn(toastr, 'error');
    component.saveSchedule();
    expect(scheduleService.saveSchedule).toHaveBeenCalledWith(expectedSchedule);
    expect(toastr.error).toHaveBeenCalledWith('test-error');
  });
});
