import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Time } from '@angular/common';

import { AccountService } from '../../account/account.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ScheduleService } from '../service/schedule.service';
import { ToastrService } from 'ngx-toastr';
import { Roles } from '../../account/role.model';
import { Schedule } from '../schedule.model';
import { ScheduleItem } from '../schedule-item.model';

@Component({
  selector: 'app-new-schedule',
  templateUrl: './new-schedule.component.html',
  styleUrls: ['./new-schedule.component.css']
})
export class NewScheduleComponent implements OnInit, OnDestroy {

  private defaultYear = 2000;

  private defaultMonth = 0;

  private defaultDay = 1;

  schedule: Schedule;

  lineId: number;

  isAdmin: boolean;

  selectedType: string;

  newScheduleItems: ScheduleItem[];

  selectedVehicle: string;

  selectedTime: string;

  scheduleType: string;

  private routeParamSubscription: Subscription;

  constructor(
    private accountService: AccountService,
    private route: ActivatedRoute,
    private scheduleService: ScheduleService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  private loadSchedule() {
    this.scheduleService.getSchedule(this.lineId, this.scheduleType).subscribe(schedule => {
      this.schedule = schedule;
      this.formatSchedule();
      this.selectedType = schedule.scheduleType;
    }, error => this.toastr.error('Unable to load schedule', 'Error'));
  }

  set typeSelector(value: string) {
    if (value.toLocaleLowerCase() === 'weekend') {
      this.schedule.scheduleType = 'WEEKEND';
    } else {
      this.schedule.scheduleType = 'WORK_DAY';
    }
  }

  get typeSelector(): string {
    if (this.schedule.scheduleType.toLocaleLowerCase() === 'weekend') {
      return 'weekend';
    } else {
      return 'work_day';
    }
  }

  formatSchedule(): void {
    this.schedule.active = null;
    this.schedule.id = null;
    this.schedule.validFrom = null;
    this.schedule.validUntil = null;
  }

  ngOnInit() {
    this.checkRoles();
    this.routeParamSubscription = this.route.params.subscribe(params => {
      this.lineId = +params.line_id;
      this.scheduleType = params.schedule_type;
      this.newScheduleItems = [];
      this.loadSchedule();
    });
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  ngOnDestroy() {
    this.routeParamSubscription.unsubscribe();
  }

  addNewItem(): void {
    if (this.selectedTime === undefined || this.selectedTime === '') {
      this.toastr.error('Enter correct time', 'Error');
    } else {
      const splitedTime = this.selectedTime.split(':');
      const hours = +splitedTime[0];
      const minutes = +splitedTime[1];
      const newTime = new Date(this.defaultYear, this.defaultMonth, this.defaultDay, hours, minutes);
      const newItem = new ScheduleItem(null, newTime, null, new Schedule());
      this.newScheduleItems.push(newItem);
      console.log(newItem);
    }
  }

  saveSchedule(): void {
    this.schedule.scheduleItems = this.newScheduleItems;
    this.scheduleService.saveSchedule(this.schedule).subscribe(
      result => {
        this.newScheduleItems.forEach(element => {
          element.schedule.id = result.id;
        });
        this.scheduleService.saveScheduleItems(this.newScheduleItems).subscribe(
          success => {
            this.toastr.success("Schedule successfuly saved", "Success");
            this.router.navigate(['/schedule', this.lineId]);
          },
          error => {
            this.toastr.error(error.json().message);
          });
      },
      error => {
        this.toastr.error(error.json().message);
      });
  }
}
