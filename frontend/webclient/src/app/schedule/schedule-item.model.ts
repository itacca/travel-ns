import { Time } from '@angular/common';

import { Vehicle } from '../vehicle/vehicle.model';
import { Schedule } from './schedule.model';
import { Timestamp } from 'rxjs/internal/operators/timestamp';

export class ScheduleItem {

    constructor(
      public id?: number,
      public departureTime?: Date,
      public vehicle?: Vehicle,
      public schedule?: Schedule
    ) { }
  }
