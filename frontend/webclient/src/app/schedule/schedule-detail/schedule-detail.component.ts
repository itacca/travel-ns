import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

import { Schedule } from '../schedule.model';
import { ScheduleService } from '../service/schedule.service';
import { AccountService } from '../../account/account.service';
import { Roles } from '../../account/role.model';
import { CityLineService } from '../../city-lines/service/city-line.service';
import { CityLine } from '../../city-lines/city-line.model';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-schedule-detail',
  templateUrl: './schedule-detail.component.html',
  styleUrls: ['./schedule-detail.component.css']
})
export class ScheduleDetailComponent implements OnInit, OnDestroy {

  schedule: Schedule;

  cityLine: CityLine;

  lineId: number;

  isAdmin: boolean;

  typeField: string;

  private routeParamSubscription: Subscription;

  constructor(
    private accountService: AccountService,
    private cityLineService: CityLineService,
    private route: ActivatedRoute,
    private scheduleService: ScheduleService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  private loadSchedule() {
    if (!this.typeField) {
      this.typeField = '';
    }
    this.scheduleService.getSchedule(this.lineId, this.typeField.toUpperCase()).subscribe(schedule => {
      this.schedule = schedule;
      this.setActivity();
    }, error => {
      this.toastr.warning('Unable to load schedule', 'Error');
    });
  }

  setActivity(): void {
    if (this.schedule) {
      if (!this.schedule.active) {
        this.schedule.active = false;
      }
    }
  }

  ngOnInit() {
    this.checkRoles();
    this.routeParamSubscription = this.route.params.subscribe(params => {
      this.lineId = +params.line_id;
      this.loadSchedule();
      this.loadCityLine();
    });
  }

  loadCityLine() {
    this.cityLineService.findById(this.lineId).subscribe(result => {
      this.cityLine = result;
    },
     error => {
      this.toastr.error(error.message, 'Error');
    });
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  ngOnDestroy() {
    this.routeParamSubscription.unsubscribe();
  }

  addNewSchedule() {
    this.router.navigate(['/new-schedule', this.lineId, this.typeField]);
  }

  changeType() {
    if (this.typeField.toUpperCase() === 'WEEKEND') {
      this.typeField = 'WORK_DAY';
    } else {
      this.typeField = 'WEEKEND';
    }
    this.loadSchedule();
  }
}
