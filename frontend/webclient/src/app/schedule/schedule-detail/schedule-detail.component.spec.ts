import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleDetailComponent } from './schedule-detail.component';
import { Router } from '@angular/router';
import { ScheduleService } from '../service/schedule.service';
import { ToastrService } from 'ngx-toastr';
import { ScheduleModule } from '../schedule.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';

describe('ScheduleDetailComponent', () => {
  let component: ScheduleDetailComponent;
  let fixture: ComponentFixture<ScheduleDetailComponent>;
  
  let router: Router;
  let scheduleService: ScheduleService;
  let toastr: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ScheduleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    scheduleService = TestBed.get(ScheduleService);
    toastr = TestBed.get(ToastrService);

    fixture = TestBed.createComponent(ScheduleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
