import { CityLine } from '../city-lines/city-line.model';
import { ScheduleItem } from './schedule-item.model';

export class Schedule {

    constructor(
      public id?: number,
      public active?: boolean,
      public validFrom?: Date,
      public validUntil?: Date,
      public scheduleType?: 'WORK_DAY' | 'WEEKEND',
      public cityLine?: CityLine,
      public scheduleItems?: ScheduleItem[]
    ) { }
  }
