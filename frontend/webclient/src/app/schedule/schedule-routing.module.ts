import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ScheduleDetailComponent } from './schedule-detail/schedule-detail.component';
import { AutoLoginGuard } from '../guard/auto-login.guard';
import { NewScheduleComponent } from './new-schedule/new-schedule.component';

const routes: Routes = [
    { path: 'schedule/:line_id', component: ScheduleDetailComponent, canActivate: [AutoLoginGuard] },
    { path: 'new-schedule/:line_id/:schedule_type', component: NewScheduleComponent, canActivate: [AutoLoginGuard] }
  ];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ScheduleRoutingModule { }
