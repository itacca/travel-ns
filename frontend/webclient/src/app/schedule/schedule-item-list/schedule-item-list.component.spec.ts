import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleItemListComponent } from './schedule-item-list.component';
import { ScheduleItem } from '../schedule-item.model';
import { Schedule } from '../schedule.model';
import { ScheduleModule } from '../schedule.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { By } from '@angular/platform-browser';

describe('ScheduleItemListComponent', () => {
  let component: ScheduleItemListComponent;
  let fixture: ComponentFixture<ScheduleItemListComponent>;

  let schedule = new Schedule(1, true, null, null, "WEEKEND", null, null);
  let scheduleItems: ScheduleItem[] = [
    new ScheduleItem(1, new Date("2000-01-01 10:00:00"), null, schedule),
    new ScheduleItem(2, new Date("2000-01-01 10:00:00"), null, schedule),
    new ScheduleItem(3, new Date("2000-01-01 10:00:00"), null, schedule),
    new ScheduleItem(4, new Date("2000-01-01 10:00:00"), null, schedule),
    new ScheduleItem(5, new Date("2000-01-01 10:00:00"), null, schedule),
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ScheduleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
