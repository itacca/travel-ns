import { Component, OnInit, Input } from '@angular/core';

import { ScheduleItem } from '../schedule-item.model';
import { AccountService } from '../../account/account.service';
import { ActivatedRoute } from '@angular/router';
import { ScheduleService } from '../service/schedule.service';
import { ToastrService } from 'ngx-toastr';
import { Schedule } from '../schedule.model';

@Component({
  selector: 'app-schedule-item-list',
  templateUrl: './schedule-item-list.component.html',
  styleUrls: ['./schedule-item-list.component.css']
})
export class ScheduleItemListComponent implements OnInit {

  scheduleIdFiled: number;

  @Input()
  typeField: string;

  @Input()
  isAdmin: boolean;

  private defaultYear = 2000;

  private defaultMonth = 0;

  private defaultDay = 1;

  selectedTime: string;

  scheduleItems: ScheduleItem[];

  constructor(
    private accountService: AccountService,
    private route: ActivatedRoute,
    private scheduleService: ScheduleService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {

  }

  get scheduleId() {
    return this.scheduleIdFiled;
  }

  @Input()
  set scheduleId(value: number) {
    this.scheduleIdFiled = value;
    this.loadScheduleItems();
  }

  loadScheduleItems(): void {
    this.scheduleService.getScheduleItems(this.scheduleId).subscribe(
      scheduleItems => {
        this.scheduleItems = scheduleItems;
        this.sortItems();
      },
      error => this.toastr.error(error.message, 'Error'));
  }

  sortItems(): void {
    this.scheduleItems.sort((item1, item2) => this.compareTwoDates(item1.departureTime, item2.departureTime));
  }

  compareTwoDates(date1: Date, date2: Date): number {
    const convertedDate1 = new Date(date1.toString());
    const convertedDate2 = new Date(date2.toString());
    if (convertedDate1.getHours() < convertedDate2.getHours()) {
      return -1;
    } else if (convertedDate1.getHours() > convertedDate2.getHours()) {
      return 1;
    } else {
      if (convertedDate1.getMinutes() < convertedDate2.getMinutes()) {
        return -1;
      } else if (convertedDate1.getMinutes() > convertedDate2.getMinutes()) {
        return 1;
      }
    }
    return 0;
  }

  deleteItem(scheduleItemId: number) {
    this.scheduleService.deleteScheduleItem(scheduleItemId).subscribe(
      success => {
        this.toastr.success('Item deleted', 'Success');
        this.loadScheduleItems();
      },
      error => this.toastr.error('Unable to delete item', 'Error')
    );
  }

  addNewItem() {
    if (this.selectedTime === undefined || this.selectedTime === '') {
      this.toastr.error('Enter correct time', 'Error');
    } else {
      const splitedTime = this.selectedTime.split(':');
      const hours = +splitedTime[0];
      const minutes = +splitedTime[1];
      const newTime = new Date(this.defaultYear, this.defaultMonth, this.defaultDay, hours, minutes);
      const parentSchedule = new Schedule();
      parentSchedule.id = this.scheduleId;
      const newItem = new ScheduleItem(null, newTime, null, parentSchedule);
      this.saveScheduleItem(newItem);
    }
  }

  saveScheduleItem(item: ScheduleItem): void {
    let requestList = [];
    requestList.push(item);
    this.scheduleService.saveScheduleItems(requestList).subscribe(
      success => {
        this.toastr.success("Item added successfuly", "Success");
        this.loadScheduleItems();
      },
      error => this.toastr.error("Unable to add item", "Error")
    );
  }
}
