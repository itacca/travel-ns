import { TestBed, inject } from '@angular/core/testing';

import { ScheduleService } from './schedule.service';
import { RequestOptions, Http } from '@angular/http';
import { PageParams } from '../../shared/page.model';
import { Schedule } from '../schedule.model';
import { CityLine } from '../../city-lines/city-line.model';
import { ScheduleItem } from '../schedule-item.model';
import { ScheduleModule } from '../schedule.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../core/auth/auth.service';
import { throwError, of } from 'rxjs';

describe('ScheduleService', () => {

  let testRequestOptions = new RequestOptions();
  let pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'asc'
  };
  let cityLine = new CityLine(5, "Linija 5", "BUS");
  let schedule: Schedule = new Schedule(1, true, null, null, "WEEKEND", cityLine, null);
  let schedules: Schedule[] = [
    schedule,
    new Schedule(2, true, null, null, "WORK_DAY", cityLine, null),
    new Schedule(3, true, null, null, "WEEKEND", cityLine, null),
    new Schedule(4, true, null, null, "WORK_DAY", cityLine, null),
    new Schedule(5, true, null, null, "WEEKEND", cityLine, null)
  ];
  let scheduleItems: ScheduleItem[] = [
    new ScheduleItem(1, new Date("2000-01-01 10:00:00"), null, schedule),
    new ScheduleItem(1, new Date("2000-01-01 11:00:00"), null, schedule),
    new ScheduleItem(1, new Date("2000-01-01 12:00:00"), null, schedule),
    new ScheduleItem(1, new Date("2000-01-01 13:00:00"), null, schedule),
    new ScheduleItem(1, new Date("2000-01-01 14:00:00"), null, schedule)
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ScheduleModule,
        TestSharedModule
      ]
    });
  });

  it('should be created', inject([ScheduleService], (service: ScheduleService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', inject([ScheduleService], (service: ScheduleService) => {
    expect(service.scheduleEndpoint).toEqual(`${environment.apiRoot}/schedule`);
    expect(service.schedulesEndpoint).toEqual(`${environment.apiRoot}/schedules`);
    expect(service.scheduleItemEndpoint).toEqual(`${environment.apiRoot}/schedule-item`);
    expect(service.scheduleItemsEndpoint).toEqual(`${environment.apiRoot}/schedule-items`);
  }));

  /*
  it('getSchedule() should throw error if http returns error', inject([ScheduleService, Http, AuthService],
    (scheduleService: ScheduleService, http: Http, authService: AuthService) => {
    spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    spyOn(http, 'get').and.callFake(any => throwError(new Error('test-error')));
    let lineId = 0;
    let type = "WORK_DAY";
    scheduleService.getSchedule(lineId, type).subscribe(result => {
      expect(result).toBeUndefined('getSchedule() should have thrown error');
    }, error => {
      expect(error.message).toEqual('test-error');
    });
    expect(http.get).toHaveBeenCalledWith(
      `${scheduleService.scheduleEndpoint}?city_line_id=${lineId}&schedule_type=${type}`);
  }));

  it('getSchedule() should return schedule', inject([ScheduleService, Http, AuthService],
    (scheduleService: ScheduleService, http: Http, authService: AuthService) => {
    spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    let expectedSchedule = new Schedule();
    spyOn(http, 'get').and.callFake(any => of({ json: () => expectedSchedule }));
    let lineId = 0;
    let type = "WORK_DAY";
    scheduleService.getSchedule(lineId, type).subscribe(result => {
      expect(result).toBe(expectedSchedule);
    }, error => {
      expect(error).toBeUndefined('getSchedule() should have returned schedule');
    });
    expect(http.get).toHaveBeenCalledWith(
      `${scheduleService.scheduleEndpoint}?city_line_id=${lineId}&schedule_type=${type}`);
  }));
  */
});
