import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { AuthService } from '../../core/auth/auth.service';
import { UtilService } from '../../core/util.service';
import { Schedule } from '../schedule.model';
import { ScheduleItem } from '../schedule-item.model';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  scheduleEndpoint: string = `${environment.apiRoot}/schedule`;
  schedulesEndpoint: string = `${environment.apiRoot}/schedules`;
  scheduleItemsEndpoint: string = `${environment.apiRoot}/schedule-items`;
  scheduleItemEndpoint: string = `${environment.apiRoot}/schedule-item`;

  lineId: number;

  constructor(
    private http: Http,
    private authService: AuthService,
    private util: UtilService
  ) { }

  getSchedule(id: number | string, type: string): Observable<Schedule> {
    this.lineId = +id;
    let givenType = type;
    if (type.toUpperCase() !== 'WORK_DAY' && type.toUpperCase() !== 'WEEKEND') {
      givenType = 'WORK_DAY';
    }
    return this.http.get(`${this.scheduleEndpoint}?city_line_id=${this.lineId}&schedule_type=${givenType}`,
     this.authService.getHttpAuthOptionsWithoutToken())
    .pipe(map(
      response => response.json(),
      error => Observable.throw(error)
    ));
  }

  saveSchedule(schedule: Schedule): Observable<any> {
    return this.http.post(`${this.schedulesEndpoint}`, schedule,
    this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  saveScheduleItems(scheduleItems: ScheduleItem[]): Observable<any> {
    return this.http.post(`${this.scheduleItemsEndpoint}`, scheduleItems,
    this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  saveScheduleItem(item: ScheduleItem):  Observable<any> {
    return this.http.post(this.scheduleItemsEndpoint, item, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  getScheduleItems(scheduleId: number): Observable<ScheduleItem[]> {
    return this.http.get(`${this.scheduleItemsEndpoint}?schedule_id=${scheduleId}`, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  deleteScheduleItem(scheduleItemId: number) {
    return this.http.delete(`${this.scheduleItemEndpoint}/${scheduleItemId}`, this.authService.getHttpAuthOptions());
  }
}
