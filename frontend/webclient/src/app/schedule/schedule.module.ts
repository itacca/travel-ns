import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleDetailComponent } from './schedule-detail/schedule-detail.component';
import { ScheduleItemListComponent } from './schedule-item-list/schedule-item-list.component';
import { ConvertToSpacesPipe } from '../shared/convert-to-spaces.pipe';
import { FormatTime } from '../shared/format-time.pipe';
import { NewScheduleComponent } from './new-schedule/new-schedule.component';
import { SharedModule } from '../shared/shared.module';
import { ScheduleRoutingModule } from './schedule-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ScheduleRoutingModule
  ],
  declarations: [
    ScheduleDetailComponent,
    ScheduleItemListComponent,
    ConvertToSpacesPipe,
    FormatTime,
    NewScheduleComponent
  ],
  exports: [
    ScheduleRoutingModule
  ]
})
export class ScheduleModule { }
