import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatFormFieldModule, 
         MatInputModule, 
         MatDatepickerModule,
         MatNativeDateModule,
         MatButtonModule,
         MatToolbarModule,
         MatMenuModule,
         MatSelectModule,
         MatCardModule,
         MatRadioModule } from '@angular/material';

const matModules = [
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatButtonModule,
  MatToolbarModule,
  MatMenuModule,
  MatSelectModule,
  MatCardModule,
  DragDropModule,
  MatRadioModule
];

@NgModule({
  imports: [
    ...matModules
  ],
  exports: [
    ...matModules
  ]
})
export class MaterialModule { }
