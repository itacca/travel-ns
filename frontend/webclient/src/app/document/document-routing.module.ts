import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadDocumentComponent } from './upload-document/upload-document.component';
import { LoginGuard } from '../guard/login.guard';
import { VerifyDocumentComponent } from './verify-document/verify-document.component';
import { AutoLoginGuard } from '../guard/auto-login.guard';
import { AtLeastVerifierGuard } from '../guard/at-least-verifier.guard';

const routes: Routes = [
  { path: 'upload-document', component: UploadDocumentComponent, canActivate: [LoginGuard] },
  { path: 'verify-document', component: VerifyDocumentComponent, canActivate: [AutoLoginGuard, AtLeastVerifierGuard] }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DocumentRoutingModule { }
