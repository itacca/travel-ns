import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/account/account.service';
import { ToastrService } from 'ngx-toastr';
import { Page, PageParams } from 'src/app/shared/page.model';
import { DocumentService } from 'src/app/shared/image-upload/document.service';
import { Document } from 'src/app/shared/image-upload/document.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-verify-document',
  templateUrl: './verify-document.component.html',
  styleUrls: ['./verify-document.component.css']
})
export class VerifyDocumentComponent implements OnInit {

  unverifiedDocumentsPage: Page<Document>;

  pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'id,asc'
  };

  images: any = {};

  constructor(
    private toastr: ToastrService,
    private documentService: DocumentService,
    private domSanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.loadUnverifiedDocumentAccounts(this.pageParams);
  }

  private loadUnverifiedDocumentAccounts(pageParams: PageParams) {
    this.documentService.findByActiveAndVerified(pageParams, true, false).subscribe(result => {
      this.unverifiedDocumentsPage = result;
      result.content.forEach(imageDocument => {
        let blob = new Blob([imageDocument.imageBlob], { type: `image/${imageDocument.imageType}` });
        let reader = new FileReader();
        reader.onload = event => {
          this.images[imageDocument.id] = this.domSanitizer
            .bypassSecurityTrustResourceUrl(`data:image/${imageDocument.imageType};base64,${reader.result}`);
        }
        reader.readAsText(blob);
      });
    }, error => {
      this.toastr.error(error.json().message);
    });
  }

  veirfy(document: Document, index: number) {
    this.documentService.verify(document).subscribe(result => {
      this.unverifiedDocumentsPage.content.splice(index, 1);
      this.toastr.success("Document verified", "Success");
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }
}
