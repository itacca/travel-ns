import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VdDocumentAccountInfoCardComponent } from './vd-document-account-info-card.component';
import { DocumentModule } from '../../document.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Document } from 'src/app/shared/image-upload/document.model';
import { Account } from 'src/app/account/account.model';
import { expectElementToContain } from 'src/app/test-shared/common-expects.spec';
import { By } from '@angular/platform-browser';
import { DatePipe, CommonModule } from '@angular/common';

describe('VdDocumentAccountInfoCardComponent', () => {
  let component: VdDocumentAccountInfoCardComponent;
  let fixture: ComponentFixture<VdDocumentAccountInfoCardComponent>;

  let document: Document = new Document(1, 'jpg', '', true, false, 'SENIOR', 
    new Account(1, 'test@gmail.com', '', 'Test first name', 'Test last name', new Date(), true, false, [], []));

  let datePipe: DatePipe;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        DocumentModule,
        TestSharedModule
      ],
      providers: [
        DatePipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    datePipe = TestBed.get(DatePipe);

    fixture = TestBed.createComponent(VdDocumentAccountInfoCardComponent);
    component = fixture.componentInstance;
    component.document = document;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display document type', () => {
    expectElementToContain(fixture, By.css('div.account-details'), `Document type: ${document.documentType}`);
  });

  it('should display account first name', () => {
    expectElementToContain(fixture, By.css('div.account-details'), `First name: ${document.account.firstName}`);
  });

  it('should display account last name', () => {
    expectElementToContain(fixture, By.css('div.account-details'), `Last name: ${document.account.lastName}`);
  });

  it('should display account date of birth', () => {
    expectElementToContain(fixture, By.css('div.account-details'), `Date of birth: ${datePipe.transform(document.account.dateOfBirth)}`);
  });
});
