import { Component, OnInit, Input } from '@angular/core';
import { Document } from 'src/app/shared/image-upload/document.model';

@Component({
  selector: 'app-vd-document-account-info-card',
  templateUrl: './vd-document-account-info-card.component.html',
  styleUrls: ['./vd-document-account-info-card.component.css']
})
export class VdDocumentAccountInfoCardComponent implements OnInit {

  @Input()
  document: Document;

  constructor() { }

  ngOnInit() {
  }

}
