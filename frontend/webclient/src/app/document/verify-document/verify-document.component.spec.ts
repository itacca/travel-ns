import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyDocumentComponent } from './verify-document.component';
import { DocumentModule } from '../document.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { DomSanitizer, By } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { DocumentService } from 'src/app/shared/image-upload/document.service';
import { of } from 'rxjs';
import { Document } from 'src/app/shared/image-upload/document.model';
import { Page, PageMock } from 'src/app/shared/page.model';
import { Account } from 'src/app/account/account.model';
import { expectElementToContain, expectElementToBeVisible, expectModelsToBeBoundToElements, expectElementsToAppearNTimes } from 'src/app/test-shared/common-expects.spec';

describe('VerifyDocumentComponent', () => {
  let component: VerifyDocumentComponent;
  let fixture: ComponentFixture<VerifyDocumentComponent>;

  let toastr: ToastrService;
  let documentService: DocumentService;
  let domSanitizer: DomSanitizer;

  let documentsPage: Page<Document> = new PageMock([
    new Document(1, 'jpg', '', true, false, 'STUDENT', new Account(1)),
    new Document(2, 'jpg', '', true, false, 'STUDENT', new Account(2)),
    new Document(3, 'jpg', '', true, false, 'STUDENT', new Account(3)),
    new Document(4, 'jpg', '', true, false, 'STUDENT', new Account(4)),
    new Document(5, 'jpg', '', true, false, 'STUDENT', new Account(5)),
  ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        DocumentModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    toastr = TestBed.get(ToastrService);
    documentService = TestBed.get(DocumentService);
    domSanitizer = TestBed.get(DomSanitizer);

    spyOn(documentService, 'findByActiveAndVerified').and.callFake(any => of(documentsPage))
    spyOn(toastr, 'success').and.callFake(any => null);
    spyOn(toastr, 'error').and.callFake(any => null);

    fixture = TestBed.createComponent(VerifyDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('veirfy() should call service method with correct params', () => {
    let document = new Document();

    spyOn(documentService, 'verify').and.callFake(any => of({}));

    component.veirfy(document, 0);

    expect(toastr.success).toHaveBeenCalledWith("Document verified", "Success");
    expect(documentService.verify).toHaveBeenCalledWith(document);
  });

  it('should render "Verify documents" in h2 tag', () => {
    expectElementToContain(fixture, By.css('h2.mat-h2'), 'Verify documents');
  });

  it('should render documents container if unverifiedDocumentsPage is available', () => {
    expectElementToBeVisible(fixture, By.css('div.documents'));
  });

  it('should render page controls if unverifiedDocumentsPage is available', () => {
    expectElementToBeVisible(fixture, By.css('app-page-controls'));
  });

  it('should render a document card for each document in unverifiedDocumentsPage', () => {
    expectModelsToBeBoundToElements(fixture, documentsPage.content, By.css('app-vd-document-account-info-card'), 'document');
  });

  it('should render verify button for each document', () => {
    expectElementsToAppearNTimes(fixture, By.css('div.document > button'), documentsPage.content.length);
  });
});
