import { NgModule } from '@angular/core';
import { UploadDocumentComponent } from './upload-document/upload-document.component';
import { DocumentRoutingModule } from './document-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';
import { VerifyDocumentComponent } from './verify-document/verify-document.component';
import { VdDocumentAccountInfoCardComponent } from './verify-document/vd-document-account-info-card/vd-document-account-info-card.component';

@NgModule({
  imports: [
    DocumentRoutingModule,
    SharedModule,
    MaterialModule
  ],
  declarations: [
    UploadDocumentComponent,
    VerifyDocumentComponent,
    VdDocumentAccountInfoCardComponent
  ],
  exports: [
    DocumentRoutingModule
  ]
})
export class DocumentModule { }
