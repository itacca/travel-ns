import { Component, OnInit } from '@angular/core';
import { DocumentService } from 'src/app/shared/image-upload/document.service';
import { ToastrService } from 'ngx-toastr';
import { ImageUploadData } from 'src/app/shared/image-upload/image-upload-data.model';

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.css']
})
export class UploadDocumentComponent implements OnInit {

  documentType: 'SENIOR' | 'STUDENT' = null;

  imageUploadData: ImageUploadData = null;

  imageDataUrl: string | ArrayBuffer;

  constructor(
    private documentService: DocumentService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  imageLoaded(imageUploadData: ImageUploadData) {
    this.imageUploadData = imageUploadData;
    
    let reader = new FileReader();
    reader.readAsDataURL(imageUploadData.file);
    reader.onload = event => {
      this.imageDataUrl = reader.result;
    };
  }

  imageLoadError(error: string) {
    this.toastr.error(error, 'Error');
  }

  uploadDocument() {
    this.documentService.upload(this.imageUploadData.file, this.imageUploadData.type, this.documentType).subscribe(result => {
      this.toastr.success('Document uploaded', 'Success');
    }, error => {
      this.toastr.error(error.json().error);
    });
    this.imageDataUrl = null;
    this.imageUploadData = null;
    this.documentType = null;
  }
}
