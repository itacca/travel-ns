import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadDocumentComponent } from './upload-document.component';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { DocumentModule } from '../document.module';
import { DocumentService } from 'src/app/shared/image-upload/document.service';
import { ToastrService } from 'ngx-toastr';
import { ImageUploadData } from 'src/app/shared/image-upload/image-upload-data.model';
import { of } from 'rxjs';
import { expectElementToContain, expectElementToBeVisible } from 'src/app/test-shared/common-expects.spec';
import { By } from '@angular/platform-browser';

describe('UploadDocumentComponent', () => {
  let component: UploadDocumentComponent;
  let fixture: ComponentFixture<UploadDocumentComponent>;

  let documentService: DocumentService;
  let toastr: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        DocumentModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    documentService = TestBed.get(DocumentService);
    toastr = TestBed.get(ToastrService);

    fixture = TestBed.createComponent(UploadDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('imageLoaded() should populate imageUploadData field', () => {
    let file = new File([new ArrayBuffer(2e+5)], 'test-file', { lastModified: null, type: 'image/jpeg' });
    let imageData = new ImageUploadData(file, 'jpg');
    component.imageLoaded(imageData);
    expect(component.imageUploadData).toBe(imageData);
  });

  it('imageLoadError() should toast error', () => {
    spyOn(toastr, 'error').and.callFake(any => {})
    
    component.imageLoadError('test-error');
  
    expect(toastr.error).toHaveBeenCalledWith('test-error', 'Error');
  });

  it('uploadDocument() should call service method with correct params', () => {
    let file = new File([new ArrayBuffer(2e+5)], 'test-file', { lastModified: null, type: 'image/jpeg' });
    let imageData = new ImageUploadData(file, 'jpg');

    component.imageUploadData = imageData;
    component.documentType = 'SENIOR';

    spyOn(documentService, 'upload').and.callFake(any => of(null));

    component.uploadDocument();

    expect(documentService.upload).toHaveBeenCalledWith(imageData.file, imageData.type, 'SENIOR');
  });

  it('should render radio button for student id', () => {
    expectElementToContain(fixture, By.css('mat-radio-button[value="STUDENT"]'), 'Student ID');
  });

  it('should render radio button for senior id', () => {
    expectElementToContain(fixture, By.css('mat-radio-button[value="SENIOR"]'), 'Senior ID');
  });

  it('should render image if imageDataUrl is available', () => {
    component.imageDataUrl = 'example.com';
    fixture.detectChanges();
    expectElementToBeVisible(fixture, By.css('div.image-preview > img'));
  });

  it('should render image upload input', () => {
    expectElementToBeVisible(fixture, By.css('app-image-upload'));
  });

  it('should render button for uploading', () => {
    expectElementToContain(fixture, By.css('div.image-upload > button'), 'Upload document');
  });
});
