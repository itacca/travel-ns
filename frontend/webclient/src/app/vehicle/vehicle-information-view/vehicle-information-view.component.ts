import { Component, OnInit } from '@angular/core';
import { VehicleService } from '../vehicle.service';
import { Page, PageParams } from 'src/app/shared/page.model';
import { Vehicle } from '../vehicle.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vehicles-view',
  templateUrl: './vehicle-information-view.component.html',
  styleUrls: ['./vehicle-information-view.component.css']
})
export class VehicleInformationViewComponent implements OnInit {

  vehiclesPage: Page<Vehicle>;

  pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'id,asc'
  };

  sortFields: {displayName: string, name: string}[] = [{displayName: "ID", name: "id"}, {displayName: "PLATE NUMBER", name: "plateNumber"}, {displayName: "TYPE", name: "vehicleType"}, {displayName: "CITY LINE", name: "cityLine.name"}];

  constructor(
    private vehicleService: VehicleService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadVehicles(this.pageParams);
  }

  private loadVehicles(pageParams: PageParams) {
    this.vehicleService.findAll(pageParams).subscribe(response => {
      this.vehiclesPage = response;
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

  paramsChanged(pageParams: PageParams) {
    this.loadVehicles(pageParams);
  }

}
