import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleInformationViewComponent } from './vehicle-information-view.component';
import { VehicleModule } from '../vehicle.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Page } from 'src/app/shared/page.model';
import { Vehicle } from '../vehicle.model';
import { VehicleService } from '../vehicle.service';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import { CityLine } from 'src/app/city-lines/city-line.model';
import { Station } from 'src/app/station/station.model';

describe('VehicleInformationViewComponent', () => {
  let component: VehicleInformationViewComponent;
  let fixture: ComponentFixture<VehicleInformationViewComponent>;
  let vehicleService: VehicleService;
  let toastrService;
  let vehiclesPage: Page<Vehicle> = {
    content: [
    new Vehicle(1, 'testPlateNumber1', 'BUS', new CityLine(1, "Linija1", "BUS"), new Station()),
    new Vehicle(2, 'testPlateNumber2', 'BUS', new CityLine(2, "Linija1", "BUS"), new Station()),
    new Vehicle(3, 'testPlateNumber3', 'BUS', new CityLine(3, "Linija1", "BUS"), new Station())    
    ],
    totalPages: 1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        VehicleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    vehicleService = TestBed.get(VehicleService);
    toastrService = TestBed.get(ToastrService);
  });

  function createComponent() {
    fixture = TestBed.createComponent( VehicleInformationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    spyOn(vehicleService, 'findAll').and.callFake(any => of(vehiclesPage));
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render "Vehicles" in h2 tag', () => {
    let h2Title = fixture.debugElement.query(By.css('h2'));
    expect(h2Title.nativeElement.innerHTML).toContain('Vehicles');
  });

  it('shoud render a vehicles list component with vehicles injected if vehiclesPage is loaded', () => {
    spyOn(vehicleService, 'findAll').and.callFake(any => of(vehiclesPage));
    createComponent()
    let vehiclesList = fixture.debugElement.query(By.css('app-vehicle-list'));
    expect(vehiclesList).toBeTruthy();
    expect(vehiclesList.componentInstance.vehicles).toBe(vehiclesPage.content);
  });

  it('shoud not render a vehicles list component if vehiclesPage is not loaded', () => {
    spyOn(vehicleService, 'findAll').and.callFake(any => of(vehiclesPage));
    createComponent();
    component.vehiclesPage = null;
    fixture.detectChanges();
    let vehiclesList = fixture.debugElement.query(By.css('app-vehicle-list'));
    expect(vehiclesList).toBeFalsy();
  });

  it('should toast error if vehicleService findAll() throws error', () => {
    spyOn(vehicleService, 'findAll').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastrService, 'error').and.callFake(any => null);
    createComponent()
    expect(toastrService.error).toHaveBeenCalledWith('test-error', 'Error');
  });
});
