import { Component, OnInit } from '@angular/core';
import { VehicleService } from '../vehicle.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { Vehicle } from '../vehicle.model';
import { CityLine } from 'src/app/city-lines/city-line.model';
import { CityLineService } from 'src/app/city-lines/service/city-line.service';
import { Station } from 'src/app/station/station.model';
import { Page, PageParams } from 'src/app/shared/page.model';

@Component({
    selector: 'app-create-vehicle',
    templateUrl: './create-vehicle.component.html',
    styleUrls: ['./create-vehicle.component.css']
})
export class CreateVehicleComponent implements OnInit {

    vehicleGroup: FormGroup;

    selectedType: "BUS" | "TRAM";

    selectedCityLine: CityLine;

    cityLines: Page<CityLine>;

    pageParams: PageParams = {
        page: 0,
        size: 200,
        sort: 'id,asc'
      };

    constructor(
        private vehicleService: VehicleService,
        private cityLineService: CityLineService,
        private toastr: ToastrService,
        private router: Router,
        private formBuilder: FormBuilder,
        private util: UtilService
    ) { }

    ngOnInit() {
        this.initVehicleGroup();
    }

    private initVehicleGroup() {
        this.vehicleGroup = this.formBuilder.group({
            plateNumberCtrl: [null, Validators.required]
        });
    }

    loadCityLinesByType(pageParams: PageParams, param: string) {
        this.cityLineService.getCityLinesByType(pageParams, param).subscribe(response => {
            this.cityLines = response;
          }, error => {
            this.toastr.error(error.json().message, 'Error');
          });
    }

    saveVehicle() {
        let vehicle: Vehicle = this.util.formGroupToModel(new Vehicle(), [this.vehicleGroup], 'Ctrl');
        vehicle.cityLine = this.selectedCityLine;
        vehicle.vehicleType = this.selectedType;
        this.vehicleService.create(vehicle).subscribe(result => {
            this.toastr.success("Vehicle is added.", 'Success')
            this.router.navigate(['view-vehicles']);
        }, error => {
            this.toastr.error("Unable to add vehicle", 'Error');
        });
    }

    paramsChanged(pageParams: PageParams) {
        this.loadCityLinesByType(pageParams, this.selectedType);
      }

}
