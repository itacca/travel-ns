import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateVehicleComponent } from './create-vehicle.component';
import { VehicleModule } from '../vehicle.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { By } from '@angular/platform-browser';
import { UtilService } from 'src/app/core/util.service';
import { Vehicle } from '../vehicle.model';
import { Router } from '@angular/router';
import { VehicleService } from '../vehicle.service';
import { of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { expectElementToContain, expectElementToBeVisible } from 'src/app/test-shared/common-expects.spec';

describe('CreateVehicleComponent', () => {
  let component: CreateVehicleComponent;
  let fixture: ComponentFixture<CreateVehicleComponent>;
  
  let util: UtilService;
  let router: Router;
  let vehicleService: VehicleService;
  let toastr: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        VehicleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    router = TestBed.get(Router);
    vehicleService = TestBed.get(VehicleService);
    toastr = TestBed.get(ToastrService);

    fixture = TestBed.createComponent(CreateVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Create vehicle" in h2 tag', () => {
    let h2 = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(h2.innerHTML).toContain('Create vehicle');
  });

  it('should render input fields for vehicle information', () => {
    let name = fixture.debugElement.query(By.css('input[formControlName="plateNumberCtrl"]'));

    expect(name).toBeTruthy();
  });

  it('saveVehicle() should call vehicleService.create() with correct stametion instance', () => {
    let expectedVehicle = new Vehicle();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedVehicle);
    spyOn(vehicleService, 'create').and.callFake(any => of({ id: 1 }));
    spyOn(router, 'navigate').and.callFake(any => null);
    component.saveVehicle();
    expect(vehicleService.create).toHaveBeenCalledWith(expectedVehicle);
    expect(router.navigate).toHaveBeenCalledWith(['view-vehicles']);
  });

  it('saveVehicle() should toast error if vehicleService.create() throws error', () => {
    let expectedVehicle = new Vehicle();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedVehicle);
    spyOn(vehicleService, 'create').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastr, 'error')
    component.saveVehicle();
    expect(vehicleService.create).toHaveBeenCalledWith(expectedVehicle);
    expect(toastr.error).toHaveBeenCalledWith('Unable to add vehicle', 'Error');
  });

  it('should display "Create vehicle" in h2 tag', () => {
    expectElementToContain(fixture, By.css('h2.mat-h2'), 'Create vehicle');
  });

  it('should display save button', () => {
    expectElementToContain(fixture, By.css('button.save-btn'), 'Save');
  });
});
