import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleMapViewComponent } from './vehicle-map-view/vehicle-map-view.component';
import { AutoLoginGuard } from '../guard/auto-login.guard';
import { VehicleInformationViewComponent } from './vehicle-information-view/vehicle-information-view.component';
import { AdminRoleGuard } from '../guard/admin-role.guard';
import { CreateVehicleComponent } from './create-vehicle/create-vehicle.component';
import { LoginGuard } from '../guard/login.guard';

const routes: Routes = [
  { path: 'track-vehicle', component: VehicleMapViewComponent, canActivate: [LoginGuard] },
  { path: 'view-vehicles', component: VehicleInformationViewComponent, canActivate: [AutoLoginGuard, AdminRoleGuard] },
  { path: 'create-vehicle', component: CreateVehicleComponent, canActivate: [AutoLoginGuard, AdminRoleGuard] }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class VehicleRoutingModule { }
