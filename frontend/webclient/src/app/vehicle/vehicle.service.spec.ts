import { TestBed, inject } from '@angular/core/testing';

import { VehicleService } from './vehicle.service';
import { TestSharedModule } from '../test-shared/test-shared.module';
import { VehicleModule } from './vehicle.module';
import { Http, RequestOptions } from '@angular/http';
import { environment } from 'src/environments/environment';
import { UtilService } from '../core/util.service';
import { AuthService } from '../core/auth/auth.service';
import { givenHttpGetReturnsResponseContainingContent, givenAuthGetHttpOptionsReturnsHttpOptions, givenHttpGetThrowsError, givenHttpDeleteReturnsResponseContainingContent, givenHttpPutReturnsResponseContainingContent, givenHttpPostReturnsResponseContainingContent, givenHttpPostThrowsError } from '../test-shared/common-stubs.spec';
import { expectServiceMethodToMapResponseToObject, expectServiceMethodToThrowError } from '../test-shared/common-expects.spec';
import { Vehicle } from './vehicle.model';
import { CityLine } from '../city-lines/city-line.model';

describe('VehicleService', () => {
  let testRequestOptions = new RequestOptions();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        VehicleModule,
        TestSharedModule
      ],
    });
  });

  beforeEach(() => {
    let utilService: UtilService = TestBed.get(UtilService);
    let authService: AuthService = TestBed.get(AuthService);
    spyOn(utilService, 'getPageableParamString').and.callFake(any => '');
    spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
  });

  it('should be created', inject([VehicleService], (service: VehicleService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', inject([VehicleService], (service: VehicleService) => {
    expect(service.vehicleEndpoint).toEqual(`${environment.apiRoot}/vehicle`);
    expect(service.vehiclesEndpoint).toEqual(`${environment.apiRoot}/vehicles`);
  }));

  it('findAll() should map http response from json to object',
    inject([Http, VehicleService], (http: Http, vehicleService: VehicleService) => {
      let testObject = { number: 42 };

      givenHttpGetReturnsResponseContainingContent(http, testObject);

      expectServiceMethodToMapResponseToObject(vehicleService.findAll(null), testObject, 'findAll()');
      expect(http.get).toHaveBeenCalledWith(vehicleService.vehiclesEndpoint, testRequestOptions);
    }));

  it('findAll() should throw error if http returns error',
    inject([Http, VehicleService], (http: Http, vehicleService: VehicleService) => {

      givenHttpGetThrowsError(http, new Error('test-error'));

      expectServiceMethodToThrowError(vehicleService.findAll(null), 'test-error', 'findAll()');
      expect(http.get).toHaveBeenCalledWith(vehicleService.vehiclesEndpoint, testRequestOptions);
    }));

  it('delete() should use correct endpoints and params',
    inject([VehicleService, Http, AuthService], (vehicleService: VehicleService, http: Http, authService: AuthService) => {
      let vehicle = new Vehicle(1, 'testPlateNumber1', 'BUS', new CityLine(1, "Linija1", "BUS"), new Vehicle());
      let expectedContent = {};
      givenHttpDeleteReturnsResponseContainingContent(http, expectedContent);

      vehicleService.delete(vehicle).subscribe();

      expect(http.delete).toHaveBeenCalledWith(`${vehicleService.vehicleEndpoint}/${vehicle.id}`, testRequestOptions);
    }));

    it('update() should use correct endpoint with correct params', inject([Http, AuthService, VehicleService], 
      (http: Http, authService: AuthService, vehicleService: VehicleService) => {
      let vehicle = new Vehicle();
    
      givenHttpPutReturnsResponseContainingContent(http, {});
  
      vehicleService.update(vehicle).subscribe();
  
      expect(http.put).toHaveBeenCalledWith(vehicleService.vehicleEndpoint, vehicle, testRequestOptions);
    }));
  
    it('create() should map response to object if no error is thrown', inject([Http, AuthService, VehicleService], 
      (http: Http, authService: AuthService, vehicleService: VehicleService) => {
      let expectedContent = { id: 1 };
      let vehicle = new Vehicle();
  
      givenHttpPostReturnsResponseContainingContent(http, expectedContent);
  
      expectServiceMethodToMapResponseToObject(vehicleService.create(vehicle), expectedContent, 'create()');
      expect(http.post).toHaveBeenCalledWith(vehicleService.vehiclesEndpoint, new Vehicle(), testRequestOptions);
    }));
  
    it('create() should throw error if http returns error', inject([Http, AuthService, VehicleService], 
      (http: Http, authService: AuthService, vehicleService: VehicleService) => {
      let expectedContent = { id: 1 };
      let vehicle = new Vehicle();
  
      givenHttpPostThrowsError(http, new Error('test-error'));
  
      expectServiceMethodToThrowError(vehicleService.create(vehicle), 'test-error', 'create()');
      expect(http.post).toHaveBeenCalledWith(vehicleService.vehiclesEndpoint, vehicle, testRequestOptions);
    }));
    
});
