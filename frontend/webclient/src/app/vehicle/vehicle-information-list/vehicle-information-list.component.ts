import { Component, OnInit, Input } from '@angular/core';
import { Vehicle } from '../vehicle.model';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-information-list.component.html',
  styleUrls: ['./vehicle-information-list.component.css']
})
export class VehicleInformationListComponent implements OnInit {

  @Input()
  vehicles: Vehicle[];

  constructor() { }

  ngOnInit() {
  }

  removeVehicleFromList(index: number) {
    this.vehicles.splice(index, 1);
  }

}
