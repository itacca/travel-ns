import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleInformationListComponent } from './vehicle-information-list.component';
import { VehicleModule } from '../vehicle.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Vehicle } from '../vehicle.model';
import { By } from '@angular/platform-browser';
import { CityLine } from 'src/app/city-lines/city-line.model';
import { Station } from 'src/app/station/station.model';

describe('VehicleInformationListComponent', () => {
  let component: VehicleInformationListComponent;
  let fixture: ComponentFixture<VehicleInformationListComponent>;

  let vehicles: Vehicle[] = [
    new Vehicle(1, 'testPlateNumber1', 'BUS', new CityLine(1, "Linija1", "BUS"), new Station()),
    new Vehicle(2, 'testPlateNumber2', 'BUS', new CityLine(2, "Linija1", "BUS"), new Station()),
    new Vehicle(3, 'testPlateNumber3', 'BUS', new CityLine(3, "Linija1", "BUS"), new Station())
    
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        VehicleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleInformationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a vehicle card for each vehicle in the vehicles list', () => {
    component.vehicles = vehicles;
    fixture.detectChanges();
    let vehicleCards = fixture.debugElement.queryAll(By.css('app-vehicle-card'));
    expect(vehicleCards.length).toEqual(vehicles.length);
    for (let i = 0; i < vehicles.length; i++) {
      expect(vehicleCards[i].componentInstance.vehicle).toEqual(vehicles[i]);
    }
  });

  it('removeVehicleFromList() should remove the given vehicle from the list and the removed vehicle should not be rendered.', () => {
    component.vehicles = vehicles;
    fixture.detectChanges();
    let adminCount = vehicles.length;
    let indexToRemove = 1;
    let vehicleCards = fixture.debugElement.queryAll(By.css('app-vehicle-card'));
    let expectedRemovedVehicle = vehicleCards[indexToRemove];
    component.removeVehicleFromList(indexToRemove);
    fixture.detectChanges();
    vehicleCards = fixture.debugElement.queryAll(By.css('app-vehicle-card'));
    expect(vehicleCards[indexToRemove]).not.toBe(expectedRemovedVehicle);
    expect(vehicleCards.length).toEqual(adminCount - 1);
  });
});
