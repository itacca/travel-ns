import { Station } from "../station/station.model";

export class Vehicle {

    constructor(
        public id?: number,
        public plateNumber?: string,
        public vehicleType?: 'BUS' | 'TRAM',
        public cityLine?: any,
        public station?: Station
    ) { }
}
