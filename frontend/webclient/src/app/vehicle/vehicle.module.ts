import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { VehicleMapViewComponent } from './vehicle-map-view/vehicle-map-view.component';
import { VehicleRoutingModule } from './vehicle-routing.module';
import { MaterialModule } from '../material/material.module';
import { VehicleInformationCardComponent } from './vehicle-information-card/vehicle-information-card.component';
import { VehicleInformationListComponent } from './vehicle-information-list/vehicle-information-list.component';
import { VehicleInformationViewComponent } from './vehicle-information-view/vehicle-information-view.component';
import { CreateVehicleComponent } from './create-vehicle/create-vehicle.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    VehicleRoutingModule
  ],
  declarations: [
    VehicleMapViewComponent,
    VehicleInformationCardComponent,
    VehicleInformationListComponent,
    VehicleInformationViewComponent,
    CreateVehicleComponent
  ],
  exports: [
    VehicleRoutingModule
  ]
})
export class VehicleModule { }
