import { Component, OnInit } from '@angular/core';
import { VehicleService } from '../vehicle.service';
import { Vehicle } from '../vehicle.model';
import { ToastrService } from 'ngx-toastr';
import { MapOption, optionsInitialized } from 'src/app/shared/dynamic-map/map-options/map-options';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';
import { DrawMarkersAndNotifyWhenClicked } from 'src/app/shared/dynamic-map/map-options/draw-markers-and-notify-when-clicked.option';
import { NamedPoint } from 'src/app/shared/dynamic-map/map-options/named-point.model';
import { Page, PageParams } from 'src/app/shared/page.model';

@Component({
  selector: 'app-vehicle-map-view',
  templateUrl: './vehicle-map-view.component.html',
  styleUrls: ['./vehicle-map-view.component.css']
})
export class VehicleMapViewComponent implements OnInit {

  selectedVehicle: Vehicle;

  vehiclesPage: Page<Vehicle>;

  pageParams: PageParams = {
    page: 0,
    size: 50,
    sort: 'id,asc'
  };

  mapOptions: MapOption[];

  markerDrawer: DrawMarkersAndNotifyWhenClicked

  constructor(
    private vehicleService: VehicleService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadVehicles(this.pageParams);
    this.initMapOptions();
  }

  private initMapOptions() {
    this.markerDrawer = new DrawMarkersAndNotifyWhenClicked([]);
    this.mapOptions = [
      new InitilizeMapWithId('unique', 45.249302, 19.824508, 14),
      this.markerDrawer
    ];
  }

  private loadVehicles(pageParams: PageParams) {
    this.vehicleService.findAll(pageParams).subscribe(result => {
      this.vehiclesPage = result;
    }, error => this.toastr.error(error.json().message, 'Error'));
  }

  selectedVehicleChanged(event) {
    let vehicle: Vehicle = event.value;
    optionsInitialized(this.mapOptions).subscribe(() => {
      this.markerDrawer.updatePoints([vehicle.station as NamedPoint]);
    });
  }

  pageParamsChanged(pageParams: PageParams) {
    this.loadVehicles(pageParams);
  }

}
