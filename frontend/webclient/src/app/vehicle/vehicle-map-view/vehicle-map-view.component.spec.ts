import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { VehicleMapViewComponent } from './vehicle-map-view.component';
import { VehicleModule } from '../vehicle.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { VehicleService } from '../vehicle.service';
import { Vehicle } from '../vehicle.model';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { PageMock } from 'src/app/shared/page.model';
import { expectElementToContain } from 'src/app/test-shared/common-expects.spec';

describe('VehicleMapViewComponent', () => {
  let component: VehicleMapViewComponent;
  let fixture: ComponentFixture<VehicleMapViewComponent>;
  let vehiclesPage = new PageMock([
    new Vehicle(null, 'BT111', null, null, { latitude: 45.247803, longitude: 19.839120 }),
    new Vehicle(null, 'BT111', null, null, { latitude: 45.247803, longitude: 19.839120 }),
    new Vehicle(null, 'BT111', null, null, { latitude: 45.247803, longitude: 19.839120 })
  ]);
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        VehicleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    let vehicleService: VehicleService = TestBed.get(VehicleService);
  });

  function createComponent() {
    fixture = TestBed.createComponent(VehicleMapViewComponent);
    component = fixture.componentInstance;
    component.selectedVehicle = vehiclesPage.content[0];
    fixture.detectChanges();
  }

  it('should create', inject([VehicleService], (vehicleService: VehicleService) => {
    spyOn(vehicleService, 'findAll').and.callFake(any => of(vehiclesPage));
    createComponent()
    expect(component).toBeTruthy();
  }));

  it('should render vehicle selection if vehicles are loaded', 
    inject([VehicleService], (vehicleService: VehicleService) => {
    spyOn(vehicleService, 'findAll').and.callFake((any) => of(vehiclesPage));
    createComponent();
    expect(fixture.debugElement.queryAll(By.css('mat-select[placeholder="Select vehicle"]')).length).toEqual(1);
  }));

  it('should not render vehicle selection if vehicles are not loaded', inject([VehicleService], (vehicleService: VehicleService) => {
    spyOn(vehicleService, 'findAll').and.callFake((any) => of(null));
    createComponent();
    expect(fixture.debugElement.queryAll(By.css('mat-select[placeholder="Select vehicle"]')).length).toEqual(0);
  }));

  it('should render map if vehicle is selected', inject([VehicleService], (vehicleService: VehicleService) => {
    spyOn(vehicleService, 'findAll').and.callFake((any) => of(vehiclesPage));
    createComponent();
    component.selectedVehicle = component.vehiclesPage.content[0];
    fixture.detectChanges();
    expect(fixture.debugElement.queryAll(By.css('app-dynamic-map')).length).toEqual(1);
  }));

  it('should render map if vehicle is not selected', inject([VehicleService], (vehicleService: VehicleService) => {
    spyOn(vehicleService, 'findAll').and.callFake((any) => of(vehiclesPage));
    createComponent();
    component.selectedVehicle = null;
    fixture.detectChanges();
    expect(fixture.debugElement.queryAll(By.css('app-dynamic-map')).length).toEqual(1);
  }));

  it('loadVehicles() shoud toast error if vehicleService throws error', inject([VehicleService, ToastrService], 
    (vehicleService: VehicleService, toastr: ToastrService) => {
    spyOn(vehicleService, 'findAll').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastr, 'error').and.callFake(any => null);
    createComponent();
    expect(vehicleService.findAll).toHaveBeenCalled();
    expect(toastr.error).toHaveBeenCalledWith('test-error', 'Error');
  }));

  it('should render "Vehicle data" in h2 tag if vehicle is selected', inject([VehicleService, ToastrService], 
    (vehicleService: VehicleService, toastr: ToastrService) => {
    spyOn(vehicleService, 'findAll').and.callFake((any) => of(vehiclesPage));
    createComponent();
    component.selectedVehicle = component.vehiclesPage.content[0];
    fixture.detectChanges();

    expectElementToContain(fixture, By.css('div.vehicle-data > h2.mat-h2'), 'Vehicle data');
  }));

  it('should render latitude if vehicle is selected', inject([VehicleService, ToastrService], 
    (vehicleService: VehicleService, toastr: ToastrService) => {
    spyOn(vehicleService, 'findAll').and.callFake((any) => of(vehiclesPage));
    createComponent();
    component.selectedVehicle = component.vehiclesPage.content[0];
    fixture.detectChanges();

    expectElementToContain(fixture, By.css('span.lat'), `Latitude: ${component.vehiclesPage.content[0].station.latitude}`);
  }));

  it('should render longitude if vehicle is selected', inject([VehicleService, ToastrService], 
    (vehicleService: VehicleService, toastr: ToastrService) => {
    spyOn(vehicleService, 'findAll').and.callFake((any) => of(vehiclesPage));
    createComponent();
    component.selectedVehicle = component.vehiclesPage.content[0];
    fixture.detectChanges();

    expectElementToContain(fixture, By.css('span.lng'), `Longitude: ${component.vehiclesPage.content[0].station.longitude}`);
  }));
});
