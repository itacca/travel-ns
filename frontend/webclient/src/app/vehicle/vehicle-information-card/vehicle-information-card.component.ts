import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Vehicle } from '../vehicle.model';
import { VehicleService } from '../vehicle.service';
import { ToastrService } from 'ngx-toastr';
import { UtilService } from 'src/app/core/util.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-vehicle-card',
  templateUrl: './vehicle-information-card.component.html',
  styleUrls: ['./vehicle-information-card.component.css']
})
export class VehicleInformationCardComponent implements OnInit {

  _vehicle: Vehicle;

  vehicleGroup: FormGroup;

  @Output()
  deleted: EventEmitter<any> = new EventEmitter();

  constructor(
    private vehicleService: VehicleService,
    private toastrService: ToastrService,
    private formBuilder: FormBuilder,
    private util: UtilService
  ) { }

  ngOnInit() {
    this.initVehicleGroup();
  }

  @Input()
  set vehicle(vehicle: Vehicle) {
    this._vehicle = vehicle;
  }

  get vehicle() {
    return this._vehicle;
  }

  private initVehicleGroup() {
    this.vehicleGroup = this.formBuilder.group({
      plateNumberCtrl: [this.vehicle.plateNumber, Validators.required]
    });
  }

  deleteVehicle() {
    this.vehicleService.delete(this.vehicle).subscribe(
      success => {
        this.toastrService.success("Vehicle deleted", "Success");
        this.deleted.emit();
      },
      error => this.toastrService.error(error.json().message, "Error")
    );
  }

  updateVehicle() {
    let vehicle: Vehicle = this.util.formGroupToModel(this.vehicle, [this.vehicleGroup], 'Ctrl');
    this.vehicleService.update(vehicle).subscribe(result => {
      this.toastrService.success("Vehicle updated.", "Success");
    }, error => {
      this.toastrService.error("Unable to update vehicle", 'Error');
    });
  }
}
