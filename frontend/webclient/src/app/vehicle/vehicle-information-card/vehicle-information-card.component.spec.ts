import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { VehicleInformationCardComponent } from './vehicle-information-card.component';
import { VehicleModule } from '../vehicle.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { VehicleService } from '../vehicle.service';
import { of, throwError } from 'rxjs';
import { Vehicle } from '../vehicle.model';
import { By } from '@angular/platform-browser';
import { Predicate, DebugElement } from '@angular/core';
import { CityLine } from 'src/app/city-lines/city-line.model';
import { Station } from 'src/app/station/station.model';
import { FormGroup, FormControl } from '@angular/forms';

describe('VehicleInformationCardComponent', () => {
  let component: VehicleInformationCardComponent;
  let fixture: ComponentFixture<VehicleInformationCardComponent>;
  let vehicle: Vehicle = new Vehicle(1, 'testPlateNumber', 'BUS', new CityLine(1, "Linija1", "BUS"), new Station());

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        VehicleModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  function createComponent() {
    fixture = TestBed.createComponent(VehicleInformationCardComponent);
    component = fixture.componentInstance;
    component._vehicle= vehicle;
    
    fixture.detectChanges();
  }

  function expectElementToContain(predicate: Predicate<DebugElement>, content: string) {
    expect(fixture.debugElement.query(predicate).nativeElement.innerHTML)
      .toContain(content);
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should display all vehicle data', inject([VehicleService], (vehicleService: VehicleService) => {
    createComponent();
    component.vehicle = vehicle;
    fixture.detectChanges();
    
    expectElementToContain(By.css('.vehicleType'), vehicle.vehicleType);
    expectElementToContain(By.css('.cityLine'), vehicle.cityLine.name);
  }));

  it('deleteVehicle() should forward the call to the service and on success emit a deleted event', inject([VehicleService],
    (vehicleService: VehicleService) => {
    spyOn(vehicleService, 'delete').and.callFake(any => of(null));
    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    spyOn(component, 'deleteVehicle').and.callThrough();
    component.vehicle = vehicle;
    fixture.detectChanges();

    let deleteBtn = fixture.debugElement.query(By.css('.delete-vehicle-btn'));

    deleteBtn.nativeElement.click();
    
    expect(component.deleteVehicle).toHaveBeenCalled();
    expect(vehicleService.delete).toHaveBeenCalledWith(vehicle);
    expect(component.deleted.emit).toHaveBeenCalled();
  }));

  it('deleteVehicle() should not emit a deleted event if deletion fails', inject([VehicleService],
    (vehicleService: VehicleService) => {
    spyOn(vehicleService, 'delete').and.callFake(any => throwError(new Error('failed deletion test')));

    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    component.vehicle = vehicle;
    fixture.detectChanges();

    expect(component.deleted.emit).toHaveBeenCalledTimes(0);
  }));
});
