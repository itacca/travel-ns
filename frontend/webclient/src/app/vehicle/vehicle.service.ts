import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'src/environments/environment';
import { PageParams, Page } from '../shared/page.model';
import { Vehicle } from './vehicle.model';
import { AuthService } from '../core/auth/auth.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UtilService } from '../core/util.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  vehicleEndpoint: string = `${environment.apiRoot}/vehicle`;

  vehiclesEndpoint: string = `${environment.apiRoot}/vehicles`;

  constructor(
    private http: Http,
    private auth: AuthService,
    private util: UtilService
  ) { }
  
  findAll(pageParams: PageParams): Observable<Page<Vehicle>> {
    return this.http.get(`${this.vehiclesEndpoint}${this.util.getPageableParamString(pageParams)}`, 
      this.auth.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  delete(vehicle: Vehicle): Observable<any> {
    return this.http.delete(`${this.vehicleEndpoint}/${vehicle.id}`, this.auth.getHttpAuthOptions());
  }

  create(vehicle: Vehicle): Observable<any> {
    return this.http.post(this.vehiclesEndpoint, vehicle, this.auth.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  update(vehicle: Vehicle): Observable<any> {
    return this.http.put(this.vehicleEndpoint, vehicle, this.auth.getHttpAuthOptions());
  }

}
