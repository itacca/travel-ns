import { AuthService } from "../core/auth/auth.service";
import { RequestOptions, Http } from "@angular/http";
import { of, throwError } from "rxjs";

function givenHttpMethodReturnsResponseContainingContent(http: Http, method: 'get' | 'post' | 'put' | 'delete', content: any) {
  spyOn(http, method).and.callFake(any => of({ json: () => content }));
}

function givenHttpMethodThrowsError(http: Http, method: 'get' | 'post' | 'put' | 'delete', error: Error) {
  spyOn(http, method).and.callFake(any => throwError(error));
}

export function givenAuthGetHttpOptionsReturnsHttpOptions(auth: AuthService, httpOptions: RequestOptions) {
  spyOn(auth, 'getHttpAuthOptions').and.callFake(any => httpOptions);
}

export function givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions(auth: AuthService, httpOptions: RequestOptions) {
  spyOn(auth, 'getHttpAuthOptionsWithoutToken').and.callFake(any => httpOptions);
}

export function givenAuthGetHttpOptionsWithoutContentTypeReturnsHttpOptions(auth: AuthService, httpOptions: RequestOptions) {
  spyOn(auth, 'getHttpAuthOptionsWithoutContentType').and.callFake(any => httpOptions);
}

export function givenHttpPostReturnsResponseContainingContent(http: Http, content: any) {
  givenHttpMethodReturnsResponseContainingContent(http, 'post', content);
}

export function givenHttpPutReturnsResponseContainingContent(http: Http, content: any) {
  givenHttpMethodReturnsResponseContainingContent(http, 'put', content);
}

export function givenHttpGetReturnsResponseContainingContent(http: Http, content: any) {
  givenHttpMethodReturnsResponseContainingContent(http, 'get', content);
}

export function givenHttpDeleteReturnsResponseContainingContent(http: Http, content: any) {
  givenHttpMethodReturnsResponseContainingContent(http, 'delete', content);
}

export function givenHttpPostThrowsError(http: Http, error: Error) {
  givenHttpMethodThrowsError(http, 'post', error);
}

export function givenHttpPutThrowsError(http: Http, error: Error) {
  givenHttpMethodThrowsError(http, 'put', error);
}

export function givenHttpGetThrowsError(http: Http, error: Error) {
  givenHttpMethodThrowsError(http, 'get', error);
}

export function givenHttpDeleteThrowsError(http: Http, error: Error) {
  givenHttpMethodThrowsError(http, 'delete', error);
}