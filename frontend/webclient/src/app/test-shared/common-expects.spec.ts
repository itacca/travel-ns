import { Observable } from "rxjs";
import { FormGroup } from "@angular/forms";
import { Predicate, DebugElement } from "@angular/core";
import { ComponentFixture } from "@angular/core/testing";

export function expectServiceMethodToMapResponseToObject(observable: Observable<any>, expectedObject, methodName: string) {
  observable.subscribe(result => {
    expect(result).toBe(expectedObject);
  }, error => {
    fail(`${methodName} should have mapped response to object`);
  });
}

export function expectServiceMethodToThrowError(observable: Observable<any>, expectedErrorMessage: string, methodName: string) {
  observable.subscribe(result => {
    fail(`${methodName} should have thrown error`);
  }, error => {
    expect(error.message).toEqual(expectedErrorMessage);
  });
}

export function expectFormGroupToContainControls(formGroup: FormGroup, controls: string[]) {
  controls.forEach(control => {
    expect(formGroup.get(control)).toBeTruthy();
  });
}

export function expectElementToContain(fixture: ComponentFixture<any>, predicate: Predicate<DebugElement>, content: string) {
  let element = fixture.debugElement.query(predicate);
  if (!element) {
    fail('Element not found.');
  }
  expect(element.nativeElement.innerHTML).toContain(content);
}

export function expectElementToBeVisible(fixture: ComponentFixture<any>, predicate: Predicate<DebugElement>) {
  expect(fixture.debugElement.query(predicate)).toBeTruthy();
}

export function expectModelsToBeBoundToElements(fixture: ComponentFixture<any>, models: any[], 
    predicate: Predicate<DebugElement>, fieldName: string) {
  let elements = fixture.debugElement.queryAll(predicate);
  if (elements.length !== models.length) {
    fail(`There should be an element for each model. Number of elements: ${elements.length}, number of models: ${models.length}.`);
  }
  elements.forEach((element, index) => {
    expect(element.componentInstance[fieldName]).toBe(models[index]);
  });
}

export function expectElementsToAppearNTimes(fixture: ComponentFixture<any>, predicate: Predicate<DebugElement>, count: number) {
  let elements = fixture.debugElement.queryAll(predicate);
  expect(elements.length).toEqual(count);
}
