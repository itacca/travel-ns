
export function sendInput(inputElement, text: string, fixture) {
  inputElement.value = text;
  inputElement.dispatchEvent(new Event('input'));
  fixture.detectChanges();
  return fixture.whenStable();
}

export function triggerFileChangedEvent(input: HTMLInputElement, mockFile: File) {
  let fileList = { files: [mockFile] };
  input.dispatchEvent(new CustomEvent('change',  {
    detail: {
      target: {
        files: fileList
      }
    }
  }));
}
