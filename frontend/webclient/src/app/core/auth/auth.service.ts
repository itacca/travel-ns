import {ActivatedRoute} from '@angular/router';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';
import { RequestOptions, Headers } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private storageKey = `67gZ,VakaHt"/mZG=u)+'2&Nbr?dWBP*]]Q$?GA-.fkWqsW=sv`;

  private loginPath = environment.authServerLogin;

  private customLoginPath = environment.authServerLoginWithUri;

  private logoutPath = environment.authServerLogout;

  private token: string = null;

  tokenAvailable: EventEmitter<any> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
  ) { }

  login(redirectUri?: string) {
    if (!this.token) {
      if (redirectUri) {
        window.location.replace(this.customLoginPath + encodeURIComponent(redirectUri));
      } else {
        window.location.replace(this.loginPath);
      }
    }
  }
  
  logout() {
    this.token = null;
    localStorage.removeItem(this.storageKey);
    window.location.replace(this.logoutPath);
  }

  isLoggedIn(): boolean {
    return this.token && this.token.length && this.token !== '' && localStorage.getItem(this.storageKey) === 'loggedin';
  }

  setToken(token: string) {
    this.token = token;
    localStorage.setItem(this.storageKey, 'loggedin');
    this.tokenAvailable.emit();
  }

  isLoggedInAndTokenNotLoaded() {
    return this.token === null && localStorage.getItem(this.storageKey) === 'loggedin';
  }

  getToken() {
    return this.token;
  }

  getHttpAuthOptions() {
    let headers: Headers = new Headers({ 
      'Authorization': `Bearer ${this.getToken()}`,
      'Content-Type': 'application/json; charset=utf-8'
    });
    return new RequestOptions({ headers: headers });
  }

  getHttpAuthOptionsWithoutContentType() {
    let headers: Headers = new Headers({ 
      'Authorization': `Bearer ${this.getToken()}`
    });
    return new RequestOptions({ headers: headers });
  }

  getHttpAuthOptionsWithoutToken() {
    let headers: Headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8'
    });
    return new RequestOptions({ headers: headers });
  }

}
