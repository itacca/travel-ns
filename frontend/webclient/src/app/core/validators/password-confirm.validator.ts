import { AbstractControl } from "@angular/forms";

export function passwordConfirmValidator(passwordControl: AbstractControl) {
  return (passwordConfirmCtrl: AbstractControl) => {
    if (passwordControl.touched && passwordControl.value !== passwordConfirmCtrl.value) {
      return {"error": 'Passwords must match'};
    }
    return null;
  }
}