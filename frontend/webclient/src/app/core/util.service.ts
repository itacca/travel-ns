import { Injectable } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { environment } from "../../environments/environment";
import { PageParams } from '../shared/page.model';
import { UrlSegment } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  formGroupToModel(model: any, formGroups: FormGroup[], controlSufix: string) {
    let mergedGroup = this.mergeGroups(formGroups, controlSufix);

    for (let key of Object.keys(model)) {
      let controlName = key + controlSufix;
      if (mergedGroup[controlName] || mergedGroup[controlName] === 0) {
        model[key] = mergedGroup[controlName];
      } else {
        if (!environment.production) {
          console.info(`Merged group doesn't provide a value for field '${key}'. This field will be unchanged.`);
        }
      }
    }

    return model;
  }

  mergeGroups(formGroups: FormGroup[], controlSufix: string) {
    let mergedGroup = {};

    for (let group of formGroups) {
      for (let key of Object.keys(group.value)) {
        if (key.search(controlSufix) !== -1) {
          if (mergedGroup[key]) {
            throw "Cannot merge groups. Multiple groups have form controls with the same name.";
          }
          mergedGroup[key] = group.value[key];
        }
      }
    }
    
    return mergedGroup;
  }

  getPageableParamString(params: PageParams) {
    let result = "?";
    for (let key of Object.keys(params)) {
      if (params[key]) {
        result += `${key}=${params[key]}&`;
      }
    }
    return result;
  }

  expandUrl(url: UrlSegment[]): string {
    let result =  '';
    for (let i = 0; i < url.length; i++) {
      result += `/${url[i].path}`;
    }
    return result;
  }
  
}
