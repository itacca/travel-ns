import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationComponent } from './navigation.component';
import { CoreModule } from '../core.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { AuthService } from '../auth/auth.service';
import { AccountService } from 'src/app/account/account.service';
import { expectElementToContain, expectElementToBeVisible } from 'src/app/test-shared/common-expects.spec';
import { By } from '@angular/platform-browser';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  let auth: AuthService;
  let accountService: AccountService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    auth = TestBed.get(AuthService);
    accountService = TestBed.get(AccountService);
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('logOut() should call logout method of auth service', () => {
    spyOn(auth, 'logout').and.callFake(any => {});
    
    component.logOut();

    expect(auth.logout).toHaveBeenCalled();
  });

  it('logIn() should call login method of auth service', () => {
    spyOn(auth, 'login').and.callFake(any => {});

    component.logIn();

    expect(auth.login).toHaveBeenCalledWith(`${window.location.href}?`);
  });

  it('should display "Travel-NS" in span', () => {
    expectElementToContain(fixture, By.css('span.nav-title'), 'Travel-NS');
  });

  it('should display home button', () => {
    expectElementToContain(fixture, By.css('.home-btn'), 'Home');
  });

  it('should display account button', () => {
    expectElementToContain(fixture, By.css('.account-btn'), 'Account');
  });

  it('should display ticket button', () => {
    expectElementToContain(fixture, By.css('.ticket-btn'), 'Ticket');
  });

  it('should display city line button', () => {
    expectElementToContain(fixture, By.css('.city-line-btn'), 'City lines');
  });

  it('should display station button', () => {
    expectElementToContain(fixture, By.css('.station-btn'), 'Stations');
  });
});
