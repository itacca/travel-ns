import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AccountService } from 'src/app/account/account.service';
import { Roles } from 'src/app/account/role.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  isAdmin: boolean = false;
  isVerifier: boolean = false;
  isSystemAdmin: boolean = false;

  constructor(
    public authService: AuthService,
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.checkRoles();
    this.checkIfSystemAdmin();
  }

  private checkRoles() {
    this.authService.tokenAvailable.subscribe(available => {
      this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole]).subscribe(isAdmin => {
        this.isAdmin = isAdmin;
      });
      this.accountService.hasAnyRole([Roles.verifierRole]).subscribe(isVerifier => {
        this.isVerifier = isVerifier;
      });
    });
  }

  private checkIfSystemAdmin() {
    this.authService.tokenAvailable.subscribe(available => {
      this.accountService.hasAnyRole([Roles.systemAdministratorRole]).subscribe(hasAdminRole => {
        this.isSystemAdmin = hasAdminRole;
      });
    });
  }

  logOut() {
    this.authService.logout();
  }

  logIn() {
    this.authService.login(`${window.location.href}?`);
  }
}
