import { TestBed, inject } from '@angular/core/testing';

import { UtilService } from './util.service';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';

describe('UtilService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      providers: [UtilService]
    });
  });

  it('should be created', inject([UtilService], (service: UtilService) => {
    expect(service).toBeTruthy();
  }));

  it('getPageableParamString should return param string for given parameters', inject([UtilService], (utilService: UtilService) => {
    let expectedString = '?size=3&page=1&sort=asc&';
    expect(utilService.getPageableParamString({ size: 3, page: 1, sort: 'asc' })).toEqual(expectedString);
  }));

  it('mergeGroups should return object with all available values', 
    inject([UtilService, FormBuilder], (utilService: UtilService, formBuilder: FormBuilder) => {

    let group1 = formBuilder.group({
      field1Ctrl: ['field1value'],
      field2Ctrl: ['field2value']
    });

    let group2 = formBuilder.group({
      field3Ctrl: ['field3value'],
      field4Ctrl: ['field4value']
    });

    let merged = utilService.mergeGroups([group1, group2], 'Ctrl');
    expect(merged['field1Ctrl']).toEqual('field1value');
    expect(merged['field2Ctrl']).toEqual('field2value');
    expect(merged['field3Ctrl']).toEqual('field3value');
    expect(merged['field4Ctrl']).toEqual('field4value');
  }));

  it('formGroup to model should return model with filled in values', 
    inject([UtilService, FormBuilder], (utilService: UtilService, formBuilder: FormBuilder) => {

    spyOn(utilService, 'mergeGroups').and.callThrough();

    let group1 = formBuilder.group({
      field1Ctrl: ['field1value'],
      field2Ctrl: ['field2value']
    });
    let model = {
      field1: null,
      field2: null
    }

    model = utilService.formGroupToModel(model, [group1], 'Ctrl');
    
    expect(utilService.mergeGroups).toHaveBeenCalledWith([group1], 'Ctrl');
    expect(model.field1).toEqual('field1value');
    expect(model.field2).toEqual('field2value');
  }));
});
