import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountViewComponent } from './account-view/account-view.component';
import { LoginGuard } from '../guard/login.guard';
import { SystemAdminRoleGuard } from '../guard/system-admin-role-guard';
import { AccountRegistrationComponent } from './account-registration/account-registration.component';
import { TravelAdminRegistrationComponent } from './travel-admin-registration/travel-admin-registration.component';
import { OnlyGuestGuard } from '../guard/only-guest.guard';
import { TravelAdminViewComponent } from './travel-admin-view/travel-admin-view.component';
import { AutoLoginGuard } from '../guard/auto-login.guard';

const routes: Routes = [
  { path: 'account', component: AccountViewComponent, canActivate: [LoginGuard] },
  { path: 'registration', component: AccountRegistrationComponent, canActivate: [OnlyGuestGuard] },
  { path: 'admin-registration', component: TravelAdminRegistrationComponent, canActivate: [AutoLoginGuard, SystemAdminRoleGuard] },
  { path: 'admin-view', component: TravelAdminViewComponent, canActivate: [AutoLoginGuard, SystemAdminRoleGuard] },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccountRoutingModule { }
