import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Account } from '../account.model';
import { AccountService } from 'src/app/account/account.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-card',
  templateUrl: './travel-admin-card.component.html',
  styleUrls: ['./travel-admin-card.component.css']
})
export class TravelAdminCardComponent implements OnInit {

  _admin: Account;

  @Output()
  deleted: EventEmitter<any> = new EventEmitter();

  constructor(
    private accountService: AccountService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
  }

  @Input()
  set admin(admin: Account) {
    this._admin = admin;
  }

  get admin() {
    return this._admin;
  }

  deleteAdmin() {
    this.accountService.delete(this.admin).subscribe(
      success => {
        this.toastrService.success("Admin deleted", "Success");
        this.deleted.emit();
      },
      error => this.toastrService.error(error.json().message, "Error")
    );
  }
}
