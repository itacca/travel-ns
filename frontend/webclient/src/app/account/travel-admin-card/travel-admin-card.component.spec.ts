import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { TravelAdminCardComponent } from './travel-admin-card.component';
import { AccountModule } from '../account.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { AccountService } from 'src/app/account/account.service';
import { of, throwError } from 'rxjs';
import { Account } from '../account.model';
import { By } from '@angular/platform-browser';
import { Predicate, DebugElement } from '@angular/core';

describe('TravelAdminCardComponent', () => {
  let component: TravelAdminCardComponent;
  let fixture: ComponentFixture<TravelAdminCardComponent>;
  let admin: Account = new Account(1, 'test1@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], []);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AccountModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  function createComponent() {
    fixture = TestBed.createComponent(TravelAdminCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  function expectElementToContain(predicate: Predicate<DebugElement>, content: string) {
    expect(fixture.debugElement.query(predicate).nativeElement.innerHTML)
      .toContain(content);
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should display all admin data', inject([AccountService], (accountService: AccountService) => {
    createComponent();
    component.admin = admin;
    fixture.detectChanges();
    
    expectElementToContain(By.css('.firstName'), admin.firstName);
    expectElementToContain(By.css('.lastName'), admin.lastName);
    expectElementToContain(By.css('.email'), admin.email);
  }));

  it('deleteAdmin() should forward the call to the service and on success emit a deleted event', inject([AccountService],
    (accountService: AccountService) => {
    spyOn(accountService, 'delete').and.callFake(any => of(null));
    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    spyOn(component, 'deleteAdmin').and.callThrough();
    component.admin = admin;
    fixture.detectChanges();

    let deleteBtn = fixture.debugElement.query(By.css('.delete-admin-btn'));

    deleteBtn.nativeElement.click();
    
    expect(component.deleteAdmin).toHaveBeenCalled();
    expect(accountService.delete).toHaveBeenCalledWith(admin);
    expect(component.deleted.emit).toHaveBeenCalled();
  }));

  it('deleteAdmin() should not emit a deleted event if deletion fails', inject([AccountService],
    (accountService: AccountService) => {
    spyOn(accountService, 'delete').and.callFake(any => throwError(new Error('failed deletion test')));

    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    component.admin = admin;
    fixture.detectChanges();

    expect(component.deleted.emit).toHaveBeenCalledTimes(0);
  }));
});
