import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelAdminRegistrationComponent } from './travel-admin-registration.component';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { AccountModule } from '../account.module';
import { By } from '@angular/platform-browser';
import { Observable, of, throwError } from 'rxjs';
import { Account } from '../account.model';
import { AccountService } from '../account.service';
import { UtilService } from 'src/app/core/util.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

describe('TravelAdminRegistrationComponent', () => {
  let component: TravelAdminRegistrationComponent;
  let fixture: ComponentFixture<TravelAdminRegistrationComponent>;
  
  let util: UtilService;
  let router: Router;
  let accountService: AccountService;
  let toastr: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AccountModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    router = TestBed.get(Router);
    accountService = TestBed.get(AccountService);
    toastr = TestBed.get(ToastrService);

    fixture = TestBed.createComponent(TravelAdminRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Travel admin registration" in h2 tag', () => {
    let h2 = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(h2.innerHTML).toContain('Travel admin registration');
  });

  it('should render input fields for admin information', () => {
    let name = fixture.debugElement.query(By.css('input[formControlName="firstNameCtrl"]'));
    let lastName = fixture.debugElement.query(By.css('input[formControlName="lastNameCtrl"]'));
    let email = fixture.debugElement.query(By.css('input[formControlName="emailCtrl"]'));
    let dateOfBirth = fixture.debugElement.query(By.css('input[formControlName="dateOfBirthCtrl"]'));

    expect(name).toBeTruthy();
    expect(lastName).toBeTruthy();
    expect(email).toBeTruthy();
    expect(dateOfBirth).toBeTruthy();
  });

  it('registerAdmin() should call accountService.registerAdmin() with correct stametion instance', () => {
    let expectedAdmin = new Account();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedAdmin);
    spyOn(accountService, 'registerAdmin').and.callFake(any => of({ id: 1 }));
    spyOn(router, 'navigate').and.callFake(any => null);
    component.registerAdmin();
    expect(accountService.registerAdmin).toHaveBeenCalledWith(expectedAdmin);
  });

  it('registerAdmin() should toast error if accountService.registerAdmin() throws error', () => {
    let expectedAdmin = new Account();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedAdmin);
    spyOn(accountService, 'registerAdmin').and.callFake(any => throwError({ json: () => new Error('Unable to register travel admin') }));
    spyOn(toastr, 'error')
    component.registerAdmin();
    expect(accountService.registerAdmin).toHaveBeenCalledWith(expectedAdmin);
    expect(toastr.error).toHaveBeenCalledWith('Unable to register travel admin', 'Error');
  });
});
