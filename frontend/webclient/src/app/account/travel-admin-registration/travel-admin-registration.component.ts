import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { Account } from '../account.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-registration',
  templateUrl: './travel-admin-registration.component.html',
  styleUrls: ['./travel-admin-registration.component.css']
})
export class TravelAdminRegistrationComponent implements OnInit {

  accountGroup: FormGroup;

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initAccountGroup();
  }

  private initAccountGroup() {
    this.accountGroup = this.formBuilder.group({
      emailCtrl: ['', Validators.compose([Validators.required, Validators.email])],
      firstNameCtrl: ['', Validators.required],
      lastNameCtrl: ['', Validators.required],
      dateOfBirthCtrl: [null, Validators.required]
    });
  }

  registerAdmin() {
    let account: Account = this.util.formGroupToModel(new Account(), [this.accountGroup], 'Ctrl');
    this.accountService.registerAdmin(account).subscribe(
      success => {
        this.toastr.success("Travel admin successfuly registered", "Success");
        this.router.navigate(['/admin-view']);
      },
      error => this.toastr.error("Unable to register travel admin", "Error")
    );
  }
}
