import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {  TravelAdminViewComponent } from './travel-admin-view.component';
import { AccountModule } from '../account.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Page } from 'src/app/shared/page.model';
import { Account } from '../account.model';
import { AccountService } from '../account.service';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe(' TravelAdminViewComponent', () => {
  let component:  TravelAdminViewComponent;
  let fixture: ComponentFixture< TravelAdminViewComponent>;
  let accountService: AccountService;
  let toastrService;
  let adminsPage: Page<Account> = {
    content: [
        new Account(1, 'test1@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], []),
        new Account(2, 'test2@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], []),
        new Account(3, 'test3@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], [])    
    ],
    totalPages: 1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AccountModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    accountService = TestBed.get(AccountService);
    toastrService = TestBed.get(ToastrService);
  });

  function createComponent() {
    fixture = TestBed.createComponent( TravelAdminViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    spyOn(accountService, 'findTravelAdmins').and.callFake(any => of(adminsPage));
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render "Travel admins" in h2 tag', () => {
    let h2Title = fixture.debugElement.query(By.css('h2'));
    expect(h2Title.nativeElement.innerHTML).toContain('Travel admins');
  });

  it('shoud render a admins list component with admins injected if adminsPage is loaded', () => {
    spyOn(accountService, 'findTravelAdmins').and.callFake(any => of(adminsPage));
    createComponent()
    let adminsList = fixture.debugElement.query(By.css('app-admin-list'));
    expect(adminsList).toBeTruthy();
    expect(adminsList.componentInstance.admins).toBe(adminsPage.content);
  });

  it('shoud not render a admins list component if adminsPage is not loaded', () => {
    spyOn(accountService, 'findTravelAdmins').and.callFake(any => of(adminsPage));
    createComponent();
    component.adminsPage = null;
    fixture.detectChanges();
    let adminsList = fixture.debugElement.query(By.css('app-admin-list'));
    expect(adminsList).toBeFalsy();
  });

  it('should toast error if accountService findTravelAdmins() throws error', () => {
    spyOn(accountService, 'findTravelAdmins').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastrService, 'error').and.callFake(any => null);
    createComponent()
    expect(toastrService.error).toHaveBeenCalledWith('test-error', 'Error');
  });
});
