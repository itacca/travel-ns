import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Page, PageParams } from 'src/app/shared/page.model';
import { Account } from '../account.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admins-view',
  templateUrl: './travel-admin-view.component.html',
  styleUrls: ['./travel-admin-view.component.css']
})
export class TravelAdminViewComponent implements OnInit {

  adminsPage: Page<Account>;

  pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'id,asc'
  };

  sortFields: {displayName: string, name: string}[] = [{displayName: "ID", name: "id"}, {displayName: "FIRST NAME", name: "firstName"}, {displayName: "LAST NAME", name: "lastName"}];


  constructor(
    private accountService: AccountService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadAdmins(this.pageParams);
  }

  private loadAdmins(pageParams: PageParams) {
    this.accountService.findTravelAdmins(pageParams).subscribe(response => {
      this.adminsPage = response;
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

  paramsChanged(pageParams: PageParams) {
    this.loadAdmins(pageParams);
  }

}
