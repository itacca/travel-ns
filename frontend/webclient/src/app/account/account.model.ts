import { Role } from "./role.model";
import { BoughtTicket } from "../ticket/bought-ticket.model";

export class Account {

  constructor(
    public id?: number,
    public email?: string,
    public password?: string,
    public firstName?: string, 
    public lastName?: string,
    public dateOfBirth?: Date,
    public validated?: boolean, 
    public idValidated?: boolean,
    public roles?: Role[],
    public boughtTickets?: BoughtTicket[]
  ) { }
}
