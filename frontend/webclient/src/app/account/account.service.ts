import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Http } from '@angular/http';
import { AuthService } from '../core/auth/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Account } from './account.model';
import { Role } from './role.model';
import { UtilService } from '../core/util.service';
import { PageParams, Page } from '../shared/page.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  accountEndpoint: string = `${environment.apiRoot}/account`;

  accountsEndpoint: string = `${environment.apiRoot}/accounts`;

  constructor(
    private http: Http,
    private authService: AuthService,
    private util: UtilService
  ) { }

  getLoggedAccount(): Observable<Account> {
    return this.http.get(`${this.accountEndpoint}/me`, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json() as Account,
        error => Observable.throw(error)
      ));
  }

  updateLoggedAccount(account: Account): Observable<any> {
    return this.http.put(`${this.accountEndpoint}`, account, this.authService.getHttpAuthOptions());
  }

  registerAccount(account: Account): Observable<Account> {
    return this.http.post(`${this.accountsEndpoint}`, account, 
      this.authService.getHttpAuthOptionsWithoutToken()).pipe(map(
        response => response.json() as Account,
        error => Observable.throw(error)
      ));
  }

  registerAdmin(account: Account): Observable<Account> {
    return this.http.post(`${this.accountsEndpoint}/travel-admins`, account, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  getUnverifiedDocumentAccounts(pageParams: PageParams): Observable<Page<Account>> {
    return this.http.get(`${this.accountsEndpoint}/document-not-verified${this.util.getPageableParamString(pageParams)}`,
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  hasAnyRole(roles: Role[]): Observable<boolean> {
    return Observable.create(observer => {
      this.getLoggedAccount().subscribe(account => {
        for (let role of account.roles) {
          if (roles.find(testRole => role.id === testRole.id) !== undefined) {
            observer.next(true);
            return;
          }
        }
        observer.next(false);
      }, error => observer.next(false));
    });
  }

  hasRoles(roles: Role[]): Observable<boolean> {
    return Observable.create(observer => {
      this.getLoggedAccount().subscribe(account => {
        for (let role of account.roles) {
          if (roles.find(testRole => role.id === testRole.id) === undefined) {
            observer.next(false);
            return;
          }
        }
        observer.next(true);
      }, error => observer.next(false));
    });
  }

  findTravelAdmins(pageParams: PageParams): Observable<Page<Account>> {
    return this.http.get(`${this.accountsEndpoint}?role_id=2&${this.util.getPageableParamString(pageParams)}`, 
      this.authService.getHttpAuthOptions()).pipe(map(
      response => response.json(),
      error => Observable.throw(error)
    ));
  }

  delete(admin: Account): Observable<any> {
    return this.http.delete(`${this.accountEndpoint}/travel-admin/${admin.id}`, this.authService.getHttpAuthOptions());
  }
}
