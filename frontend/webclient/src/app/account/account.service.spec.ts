import { TestBed, inject, async } from '@angular/core/testing';

import { AccountService } from './account.service';
import { AccountModule } from './account.module';
import { TestSharedModule } from '../test-shared/test-shared.module';
import { Account } from './account.model';
import { Roles } from './role.model';
import { of } from 'rxjs';
import { givenHttpPutReturnsResponseContainingContent, 
         givenAuthGetHttpOptionsReturnsHttpOptions, 
         givenHttpGetReturnsResponseContainingContent, 
         givenHttpGetThrowsError,
         givenHttpPostReturnsResponseContainingContent, 
         givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions, 
         givenHttpPostThrowsError, 
         givenHttpDeleteReturnsResponseContainingContent} from '../test-shared/common-stubs.spec';
import { Http, RequestOptions } from '@angular/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../core/auth/auth.service';
import { expectServiceMethodToMapResponseToObject, 
         expectServiceMethodToThrowError } from '../test-shared/common-expects.spec';
import { UtilService } from '../core/util.service';

describe('AccountService', () => {

  let accountService: AccountService;
  let http: Http;
  let auth: AuthService;

  let httpOptions = new RequestOptions();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AccountModule,
        TestSharedModule
      ]
    });
  });

  beforeEach(() => {
    accountService = TestBed.get(AccountService);
    http = TestBed.get(Http);
    auth = TestBed.get(AuthService);
  });

  it('should be created', inject([AccountService], (service: AccountService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', () => {
    expect(accountService.accountEndpoint).toEqual(`${environment.apiRoot}/account`);
    expect(accountService.accountsEndpoint).toEqual(`${environment.apiRoot}/accounts`);
  });

  it('hasAnyRole should return true if account has tested role', async(inject([AccountService], (accountService: AccountService) => {
    let account = new Account();
    account.roles = [Roles.administratorRole, Roles.passengerRole];
    spyOn(accountService, 'getLoggedAccount').and.callFake(() => of(account));
    accountService.hasAnyRole([Roles.passengerRole]).subscribe(hasAnyRole => expect(hasAnyRole).toBeTruthy());
  })));

  it('hasAnyRole should return false if account does not have tested role', 
    async(inject([AccountService], (accountService: AccountService) => {
    
    let account = new Account();
    account.roles = [Roles.administratorRole, Roles.passengerRole];
    spyOn(accountService, 'getLoggedAccount').and.callFake(() => of(account));
    accountService.hasAnyRole([Roles.systemAdministratorRole])
      .subscribe(hasAnyRole => expect(hasAnyRole).toBeFalsy());
  })));

  it('hasRoles should return true if account has tested roles', 
    async(inject([AccountService], (accountService: AccountService) => {
    
    let account = new Account();
    account.roles = [Roles.administratorRole, Roles.passengerRole];
    spyOn(accountService, 'getLoggedAccount').and.callFake(() => of(account));
    accountService.hasRoles([Roles.administratorRole, Roles.passengerRole])
      .subscribe(hasRoles => expect(hasRoles).toBeTruthy());
  })));

  it('hasRoles should return false if account does not have any of the tested roles', 
    async(inject([AccountService], (accountService: AccountService) => {
    
    let account = new Account();
    account.roles = [Roles.administratorRole, Roles.passengerRole];
    spyOn(accountService, 'getLoggedAccount').and.callFake(() => of(account));
    accountService.hasRoles([Roles.administratorRole])
      .subscribe(hasRoles => expect(hasRoles).toBeFalsy());
    accountService.hasRoles([Roles.passengerRole])
      .subscribe(hasRoles => expect(hasRoles).toBeFalsy());
  })));

  it('updateLoggedAccount() should use correct endpoint and correcy params', () => {
    let content = {};
    let account = new Account();
    
    givenHttpPutReturnsResponseContainingContent(http, content);
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);

    accountService.updateLoggedAccount(account).subscribe();

    expect(http.put).toHaveBeenCalledWith(accountService.accountEndpoint, account, httpOptions);
  });

  it('getLoggedAccount() should map json to object if http does not return error', () => {
    let response = {};

    givenHttpGetReturnsResponseContainingContent(http, response);
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToMapResponseToObject(accountService.getLoggedAccount(), response, 'getLoggedAccount()');
    expect(http.get).toHaveBeenCalledWith(`${accountService.accountEndpoint}/me`, httpOptions);
  });

  it('getLoggedAccount() should throw error if http returns error', () => {
    givenHttpGetThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToThrowError(accountService.getLoggedAccount(), 'test-error', 'getLoggedAccount()');
    expect(http.get).toHaveBeenCalledWith(`${accountService.accountEndpoint}/me`, httpOptions);
  });

  it('registerAccount() should map json to object if http does not return error', () => {
    let content = {};
    let account = new Account();

    givenHttpPostReturnsResponseContainingContent(http, content);
    givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToMapResponseToObject(accountService.registerAccount(account), content, 'registerAccount()');
    expect(http.post).toHaveBeenCalledWith(`${accountService.accountsEndpoint}`, account, httpOptions);
  });

  it('registerAccount() should throw error if http returns error', () => {
    let content = {};
    let account = new Account();

    givenHttpPostThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToThrowError(accountService.registerAccount(account), 'test-error', 'registerAccount()');
    expect(http.post).toHaveBeenCalledWith(`${accountService.accountsEndpoint}`, account, httpOptions);
  });
  
  it('delete() should use correct endpoints and params', 
    inject([AccountService, Http, AuthService], (accountService: AccountService, http: Http, authService: AuthService) => {
    let account = new Account(0);
    let expectedContent = {};

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpDeleteReturnsResponseContainingContent(http, expectedContent);

    accountService.delete(account).subscribe();

    expect(http.delete).toHaveBeenCalledWith(`${accountService.accountEndpoint}/travel-admin/${account.id}`, httpOptions);
  }));

  it('findTravelAdmins() should map response to object if no errors are thrown', inject([Http, UtilService, AuthService, AccountService], 
    (http: Http, utilService: UtilService, authService: AuthService, accountService: AccountService) => {
    let expectedContent = { content: [] };
    let testParamString = '?test-params';

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetReturnsResponseContainingContent(http, expectedContent);
    spyOn(utilService, 'getPageableParamString').and.callFake(any => testParamString);

    expectServiceMethodToMapResponseToObject(accountService.findTravelAdmins({}), expectedContent, 'findTravelAdmins()');
    expect(http.get).toHaveBeenCalledWith(`${accountService.accountsEndpoint}?role_id=2&${testParamString}`, httpOptions);
  }));

  it('findAll() should throw error if errors are thrown', inject([Http, UtilService, AuthService, AccountService], 
    (http: Http, utilService: UtilService, authService: AuthService, accountService: AccountService) => {
    let testParamString = '?test-params';

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetThrowsError(http, new Error('test-error'));
    spyOn(utilService, 'getPageableParamString').and.callFake(any => testParamString);

    expectServiceMethodToThrowError(accountService.findTravelAdmins({}), 'test-error', 'findTravelAdmins()');
    expect(http.get).toHaveBeenCalledWith(`${accountService.accountsEndpoint}?role_id=2&${testParamString}`, httpOptions);
  }));
});
