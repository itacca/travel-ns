import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AccountViewComponent } from './account-view/account-view.component';
import { AccountRoutingModule } from './account-routing.module';
import { MaterialModule } from '../material/material.module';
import { AccountRegistrationComponent } from './account-registration/account-registration.component';
import { TravelAdminRegistrationComponent } from './travel-admin-registration/travel-admin-registration.component';
import { TravelAdminViewComponent } from './travel-admin-view/travel-admin-view.component';
import { TravelAdminCardComponent } from './travel-admin-card/travel-admin-card.component';
import { TravelAdminListComponent } from './travel-admin-list/travel-admin-list.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    AccountRoutingModule,
  ],
  declarations: [
    AccountViewComponent,
    AccountRegistrationComponent,
    TravelAdminRegistrationComponent,
    TravelAdminViewComponent,
    TravelAdminCardComponent,
    TravelAdminListComponent

  ],
  exports: [
    AccountRoutingModule
  ]
})
export class AccountModule { }
