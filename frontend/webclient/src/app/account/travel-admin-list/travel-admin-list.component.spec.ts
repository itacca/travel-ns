import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelAdminListComponent } from './travel-admin-list.component';
import { AccountModule } from '../account.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Account } from '../account.model';
import { By } from '@angular/platform-browser';

describe('TravelAdminListComponent', () => {
  let component: TravelAdminListComponent;
  let fixture: ComponentFixture<TravelAdminListComponent>;

  let admins: Account[] = [
    new Account(1, 'test1@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], []),
    new Account(2, 'test2@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], []),
    new Account(3, 'test3@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], [])
    
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AccountModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelAdminListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a admin card for each admin in the admins list', () => {
    component.admins = admins;
    fixture.detectChanges();
    let adminCards = fixture.debugElement.queryAll(By.css('app-admin-card'));
    expect(adminCards.length).toEqual(admins.length);
    for (let i = 0; i < admins.length; i++) {
      expect(adminCards[i].componentInstance.admin).toEqual(admins[i]);
    }
  });

  it('removeAdminFromList() should remove the given admin from the list and the removed admin should not be rendered.', () => {
    component.admins = admins;
    fixture.detectChanges();
    let adminCount = admins.length;
    let indexToRemove = 1;
    let adminCards = fixture.debugElement.queryAll(By.css('app-admin-card'));
    let expectedRemovedAdmin = adminCards[indexToRemove];
    component.removeAdminFromList(indexToRemove);
    fixture.detectChanges();
    adminCards = fixture.debugElement.queryAll(By.css('app-admin-card'));
    expect(adminCards[indexToRemove]).not.toBe(expectedRemovedAdmin);
    expect(adminCards.length).toEqual(adminCount - 1);
  });
});
