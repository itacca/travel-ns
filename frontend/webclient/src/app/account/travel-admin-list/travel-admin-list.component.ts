import { Component, OnInit, Input } from '@angular/core';
import { Account } from '../account.model';

@Component({
  selector: 'app-admin-list',
  templateUrl: './travel-admin-list.component.html',
  styleUrls: ['./travel-admin-list.component.css']
})
export class TravelAdminListComponent implements OnInit {

  @Input()
  admins: Account[];

  constructor() { }

  ngOnInit() {
  }

  removeAdminFromList(index: number) {
    this.admins.splice(index, 1);
  }

}
