import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { passwordConfirmValidator } from 'src/app/core/validators/password-confirm.validator';
import { UtilService } from 'src/app/core/util.service';
import { Account } from '../account.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-registration',
  templateUrl: './account-registration.component.html',
  styleUrls: ['./account-registration.component.css']
})
export class AccountRegistrationComponent implements OnInit {

  accountGroup: FormGroup;

  maxDate = new Date();

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initAccountGroup();
  }

  private initAccountGroup() {
    this.accountGroup = this.formBuilder.group({
      emailCtrl: ['', Validators.compose([Validators.required, Validators.email])],
      firstNameCtrl: ['', Validators.required],
      lastNameCtrl: ['', Validators.required],
      dateOfBirthCtrl: [null, Validators.required]
    });
    let passwordCtrl = this.formBuilder.control('', Validators.compose([Validators.required, Validators.minLength(8)]));
    let passwordConfirmCtrl = this.formBuilder.control('', passwordConfirmValidator(passwordCtrl));
    this.accountGroup.addControl('passwordCtrl', passwordCtrl);
    this.accountGroup.addControl('passwordConfirmCtrl', passwordConfirmCtrl);
  }

  registerAccount() {
    let account: Account = this.util.formGroupToModel(new Account(), [this.accountGroup], 'Ctrl');
    this.accountService.registerAccount(account).subscribe(
      success => {
        this.toastr.success("Account successfuly registered", "Success");
        this.router.navigate(['/account']);
      },
      error => this.toastr.error(error.json().message, "Error")
    );
  }
}
