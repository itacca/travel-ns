import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { AccountRegistrationComponent } from './account-registration.component';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { AccountModule } from '../account.module';
import { AccountService } from '../account.service';
import { FormBuilder } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Account } from '../account.model';
import { of } from 'rxjs';
import { expectElementToBeVisible } from 'src/app/test-shared/common-expects.spec';
import { By } from '@angular/platform-browser';

describe('AccountRegistrationComponent', () => {
  let component: AccountRegistrationComponent;
  let fixture: ComponentFixture<AccountRegistrationComponent>;
  
  let accountService: AccountService;
  let formBuilder: FormBuilder;
  let util: UtilService;
  let toastr: ToastrService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AccountModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    accountService = TestBed.get(AccountService);
    formBuilder = TestBed.get(FormBuilder);
    util = TestBed.get(UtilService);
    toastr = TestBed.get(ToastrService);
    router = TestBed.get(Router);

    fixture = TestBed.createComponent(AccountRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('registerAccount() should call service with correct params', () => {
    let account = new Account();

    spyOn(util, 'formGroupToModel').and.callFake(() => account);
    spyOn(accountService, 'registerAccount').and.callFake(() => of({}));
    spyOn(toastr, 'success').and.callFake(any => null);
    spyOn(router, 'navigate').and.callFake(any => {});

    component.registerAccount();

    expect(accountService.registerAccount).toHaveBeenCalledWith(account);
    expect(toastr.success).toHaveBeenCalledWith("Account successfuly registered", "Success");
    expect(router.navigate).toHaveBeenCalledWith(['/account']);
  });

  it('should render input for first name', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="firstNameCtrl"]'));
  });

  it('should render input for last name', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="lastNameCtrl"]'));
  });

  it('should render input for email', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="emailCtrl"]'));
  });

  it('should render input for date of birth', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="dateOfBirthCtrl"]'));
  });

  it('should render input for password', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="passwordCtrl"]'));
  });

  it('should render input for password confirm', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="passwordConfirmCtrl"]'));
  });

  it('should display register button', () => {
    expectElementToBeVisible(fixture, By.css('button'));
  });
});
