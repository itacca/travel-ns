export class Role {
  
  constructor(
    public id: number,
    public label: string
  ) { }
}

export const Roles = {
  systemAdministratorRole: new Role(1, 'ROLE_SYSTEM_ADMINISTRATOR'),
  administratorRole: new Role(2, 'ROLE_ADMINISTRATOR'),
  passengerRole: new Role(3, 'ROLE_PASSENGER'),
  verifierRole: new Role(4, 'ROLE_VERIFIER')
}

