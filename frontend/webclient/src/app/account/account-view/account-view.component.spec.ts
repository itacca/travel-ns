import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { AccountViewComponent } from './account-view.component';
import { AccountModule } from '../account.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { AccountService } from '../account.service';
import { of } from 'rxjs';
import { Account } from '../account.model';
import { By } from '@angular/platform-browser';
import { expectElementToContain, expectElementToBeVisible } from 'src/app/test-shared/common-expects.spec';

describe('AccountViewComponent', () => {
  let component: AccountViewComponent;
  let fixture: ComponentFixture<AccountViewComponent>;

  let account = new Account(15, 'test@admin.com', 'password', 'first name', 'last name', new Date(), true, true, [], []);

  let accountService: AccountService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AccountModule,
        TestSharedModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    accountService = TestBed.get(AccountService);

    spyOn(accountService, 'getLoggedAccount').and.callFake(() => of(account));

    fixture = TestBed.createComponent(AccountViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get current user from service', () => {
    expect(accountService.getLoggedAccount).toHaveBeenCalled();
  });

  it('should display first name and last name in h2 tag', () => {
    expectElementToContain(fixture, By.css('h2.mat-h2'), `${account.firstName} ${account.lastName}`);
  });

  it('should display first name input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="firstNameCtrl"]'));
  });

  it('should display last name input', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="lastNameCtrl"]'));
  });

  it('should display disabled email input', () => {
    expectElementToBeVisible(fixture, By.css('input[placeholder="Email"][disabled]'));
  });

  it('should display date of brith', () => {
    expectElementToBeVisible(fixture, By.css('input[formControlName="dateOfBirthCtrl"]'));
  });

  it('should display update password button', () => {
    expectElementToBeVisible(fixture, By.css('button[color="warn"]'));
  });
});
