import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Account } from '../account.model';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { passwordConfirmValidator } from 'src/app/core/validators/password-confirm.validator';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.css']
})
export class AccountViewComponent implements OnInit {

  account: Account;

  accountGroup: FormGroup;

  updatePassword: boolean;

  passwordCtrl: FormControl;

  passwordConfirmCtrl: FormControl;

  maxDate = new Date();

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadAccount();
  }

  private loadAccount() {
    this.accountService.getLoggedAccount().subscribe(account => {
      this.account = account;
      this.initAccountGroup();
    });
  }

  private initAccountGroup() {
    this.accountGroup = this.formBuilder.group({
      emailCtrl: [this.account.email, Validators.compose([Validators.required, Validators.email])],
      firstNameCtrl: [this.account.firstName, Validators.required],
      lastNameCtrl: [this.account.lastName, Validators.required],
      dateOfBirthCtrl: [this.account.dateOfBirth, Validators.required]
    });
    this.passwordCtrl = this.formBuilder.control(this.account.password, Validators.compose([Validators.minLength(8)]));
    this.passwordConfirmCtrl = this.formBuilder.control('', passwordConfirmValidator(this.passwordCtrl));
    this.accountGroup.addControl('passwordCtrl', this.passwordCtrl);
    this.accountGroup.addControl('passwordConfirmCtrl', this.passwordConfirmCtrl);
  }

  saveProfile() {
    let account: Account = this.util.formGroupToModel(new Account(), [this.accountGroup], 'Ctrl');
    this.accountService.updateLoggedAccount(account).subscribe(
      success => this.toastr.success('Profile updated', 'Success'),
      error => this.toastr.error(error.json().message, 'Error')
    );
  }

  applyPasswordValidation() {
    this.passwordCtrl.setValidators(Validators.minLength(8));
    this.passwordConfirmCtrl.setValidators(passwordConfirmValidator(this.passwordCtrl));
  }
}
