import { NgModule } from '@angular/core';

import { CityLinesRoutingModule } from './city-lines-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CityLineListComponent } from './city-line-list/city-line-list.component';
import { CreateCityLineComponent } from './create-city-line/create-city-line.component';
import { MaterialModule } from '../material/material.module';
import { EditCityLineComponent } from './edit-city-line/edit-city-line.component';
import { CityLineDetailsComponent } from './city-line-details/city-line-details.component';
import { MatListModule } from '@angular/material/list';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    CityLinesRoutingModule,
    MatListModule,
    ScrollDispatchModule
  ],
  declarations: [
    CityLineListComponent,
    CreateCityLineComponent,
    EditCityLineComponent,
    CityLineDetailsComponent
  ],
  exports: [
    CityLinesRoutingModule
  ]
})
export class CityLinesModule {}
