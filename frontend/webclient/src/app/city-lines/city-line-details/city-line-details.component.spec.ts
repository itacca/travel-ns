import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { CityLineDetailsComponent } from './city-line-details.component';
import { CityLinesModule } from '../city-lines.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { UtilService } from 'src/app/core/util.service';
import { ToastrService } from 'ngx-toastr';
import { CityLineService } from '../service/city-line.service';
import { LineStationService } from '../service/line-station.service';
import { StationService } from 'src/app/station/station.service';
import { Page, PageMock } from 'src/app/shared/page.model';
import { Station } from 'src/app/station/station.model';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { DrawRoute } from 'src/app/shared/dynamic-map/map-options/draw-route.option';
import { DrawMarkersAndNotifyWhenClicked } from 'src/app/shared/dynamic-map/map-options/draw-markers-and-notify-when-clicked.option';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';
import { LineStation } from '../line-station.model';
import { CityLine } from '../city-line.model';
import { expectFormGroupToContainControls } from 'src/app/test-shared/common-expects.spec';
import { By } from '@angular/platform-browser';

fdescribe('CityLineDetailsComponent', () => {
  let component: CityLineDetailsComponent;
  let fixture: ComponentFixture<CityLineDetailsComponent>;

  let util: UtilService;
  let toastr: ToastrService;
  let cityLineService: CityLineService;
  let lineStationService: LineStationService;
  let stationService: StationService;

  let stationsPage: Page<Station> = new PageMock([
    new Station(1, 'Station1', [], 1, 2),
    new Station(2, 'Station2', [], 2, 2),
    new Station(3, 'Station3', [], 3, 2),
    new Station(4, 'Station4', [], 4, 2),
  ]);

  let lineStations: LineStation[] = [
    new LineStation(1, null, stationsPage.content[0], 0),
    new LineStation(2, null, stationsPage.content[1], 1),
  ];

  let cityLine = new CityLine();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CityLinesModule,
        TestSharedModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ cityLineId: 0 })
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    toastr = TestBed.get(ToastrService);
    cityLineService = TestBed.get(CityLineService);
    lineStationService = TestBed.get(LineStationService);
    stationService = TestBed.get(StationService);
  });

  function createComponent() {
    fixture = TestBed.createComponent(CityLineDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }


  function givenLineStationServiceReturnsLineStations() {
    spyOn(lineStationService, 'findByCityLineId').and.callFake(any => of(lineStations));
  }

  function givenCityLineServiceReturnsCityLine() {
    spyOn(cityLineService, 'findById').and.callFake(any => of(cityLine));
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should initialize dynamic map with correct options', () => {
    givenLineStationServiceReturnsLineStations();
    createComponent();

    expect(component.mapRouteDrawer).toEqual(jasmine.any(DrawRoute));
    expect(component.mapStationDrawer).toEqual(jasmine.any(DrawMarkersAndNotifyWhenClicked));
    expect(component.mapOptions[0]).toEqual(jasmine.any(InitilizeMapWithId));
    expect(component.mapOptions[1]).toEqual(component.mapRouteDrawer);
    expect(component.mapOptions[2]).toEqual(component.mapStationDrawer);
  });

  it('should not render stations list if stations on line list is empty', () => {
    createComponent();

    let stationsList = fixture.debugElement.query(By.css('div.stations'));

    expect(stationsList).toBeFalsy();
  });

  it('should render stations list if stations on line list is not empty', () => {
    givenCityLineServiceReturnsCityLine();
    givenLineStationServiceReturnsLineStations();
    createComponent();
    component.lineStations = [stationsPage.content[0], stationsPage.content[1], stationsPage.content[2]]
      .map((station, index) => new LineStation(null, cityLine, station, index));
    fixture.detectChanges();

    let stations = fixture.debugElement.query(By.css('cdk-virtual-scroll-viewport.stations'));

    expect(stations).toBeTruthy();
  });

  it('should render map if options are present', () => {
      givenCityLineServiceReturnsCityLine();
    givenLineStationServiceReturnsLineStations();
    createComponent();
    let map = fixture.debugElement.query(By.css('div.map'));
    expect(map).toBeTruthy();
  });

  it('should not render map if options are not present', () => {
    createComponent();
    component.mapOptions = null;
    fixture.detectChanges();
    let map = fixture.debugElement.query(By.css('div.map'));
    expect(map).toBeFalsy();
  });
});
