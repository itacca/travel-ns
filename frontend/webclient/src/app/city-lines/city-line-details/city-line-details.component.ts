import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CityLineService } from '../service/city-line.service';
import { LineStationService } from '../service/line-station.service';
import { StationService } from 'src/app/station/station.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CityLine } from '../city-line.model';
import { DrawRoute } from 'src/app/shared/dynamic-map/map-options/draw-route.option';
import { DrawMarkersAndNotifyWhenClicked } from 'src/app/shared/dynamic-map/map-options/draw-markers-and-notify-when-clicked.option';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';
import { Station } from 'src/app/station/station.model';
import { MapOption, optionsInitialized } from 'src/app/shared/dynamic-map/map-options/map-options';
import { PageParams, Page } from 'src/app/shared/page.model';
import { LineStation } from '../line-station.model';
import { NamedPoint } from 'src/app/shared/dynamic-map/map-options/named-point.model';

@Component({
  selector: 'app-city-line-details',
  templateUrl: './city-line-details.component.html',
  styleUrls: ['./city-line-details.component.css']
})
export class CityLineDetailsComponent implements OnInit, OnDestroy {

  routerSubscription: Subscription;

  cityLine: CityLine;

  lineStations: LineStation[] = [];

  stationsPage: Page<Station>;

  pageParams: PageParams = {
    page: 0,
    size: 100,
    sort: 'id,asc'
  };  

  mapOptions: MapOption[] = [];

  mapRouteDrawer: DrawRoute;

  mapStationDrawer: DrawMarkersAndNotifyWhenClicked;

  mapInitializerSubscription: Subscription;

  stationsOnLine: Station[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private cityLineService: CityLineService,
    private lineStationService: LineStationService
  ) { }

  ngOnInit() {
    this.initMapOptions();
    this.subscribeToRouterParams();
  }

  ngOnDestroy() {
    if (this.mapInitializerSubscription) {
      this.mapInitializerSubscription.unsubscribe();
    }
  }
  
  private initMapOptions() {
    let mapInitializer = new InitilizeMapWithId('unique', 45.249302, 19.824508, 14);
    this.mapRouteDrawer = new DrawRoute([]);
    this.mapStationDrawer = new DrawMarkersAndNotifyWhenClicked([]);

    this.mapOptions = [
      mapInitializer,
      this.mapRouteDrawer,
      this.mapStationDrawer
    ];
  }

  private subscribeToRouterParams() {
    this.routerSubscription = this.activatedRoute.params.subscribe(params => {
      this.cityLineService.findById(params.cityLineId).subscribe(result => {
        this.cityLine = result;
      }, error => {
        this.toastr.error(error.json().message, 'Error');
      });

      this.lineStationService.findByCityLineId(params.cityLineId).subscribe(lineStations => {
        this.lineStations = lineStations;
        this.updateRouteOnMap();
      }, error => {
        this.toastr.error(error.json().message, 'Error');
      });
    });
  }

  updateRouteOnMap() {
    optionsInitialized(this.mapOptions).subscribe(() => {
      this.mapRouteDrawer.updateRoute(this.lineStations.map(lineStation => lineStation.station));
    });
  } 
}
