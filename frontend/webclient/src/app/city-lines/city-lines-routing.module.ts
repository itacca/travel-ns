import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CityLineListComponent } from './city-line-list/city-line-list.component';
import { AutoLoginGuard } from '../guard/auto-login.guard';
import { ScheduleDetailComponent } from '../schedule/schedule-detail/schedule-detail.component';
import { NewScheduleComponent } from '../schedule/new-schedule/new-schedule.component';
import { CreateCityLineComponent } from './create-city-line/create-city-line.component';
import { AdminRoleGuard } from '../guard/admin-role.guard';
import { EditCityLineComponent } from './edit-city-line/edit-city-line.component';
import { CityLineDetailsComponent } from './city-line-details/city-line-details.component';

const routes: Routes = [
  { path: 'city-lines', component: CityLineListComponent, canActivate: [AutoLoginGuard]},
  { path: 'schedule/:line_id', component: ScheduleDetailComponent, canActivate: [AutoLoginGuard]},

  { path: 'new-schedule/:line_id', component: NewScheduleComponent, canActivate: [AutoLoginGuard] },
  { path: 'create-city-line', component: CreateCityLineComponent, canActivate: [AutoLoginGuard, AdminRoleGuard] },
  { path: 'edit-city-line/:cityLineId', component: EditCityLineComponent, canActivate: [AutoLoginGuard, AdminRoleGuard] },
  { path: 'city-line-details/:cityLineId', component: CityLineDetailsComponent, canActivate: [AutoLoginGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityLinesRoutingModule { }
