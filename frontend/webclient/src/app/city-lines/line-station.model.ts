import { CityLine } from "./city-line.model";
import { Station } from "../station/station.model";

export class LineStation {

  constructor(
    public id?: number,
    public cityLine?: CityLine,
    public station?: Station,
    public relativePosition?: number
  ) {}
}