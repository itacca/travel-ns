import { TestBed, inject } from '@angular/core/testing';

import { CityLineService } from './city-line.service';
import { CityLinesModule } from '../city-lines.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Http, RequestOptions } from '@angular/http';
import { AuthService } from 'src/app/core/auth/auth.service';
import { UtilService } from 'src/app/core/util.service';
import { CityLine } from '../city-line.model';
import { environment } from 'src/environments/environment';
import { givenAuthGetHttpOptionsReturnsHttpOptions, 
         givenHttpPostReturnsResponseContainingContent, 
         givenHttpPostThrowsError,
         givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions,
         givenHttpGetReturnsResponseContainingContent,
         givenHttpGetThrowsError,
         givenHttpPutReturnsResponseContainingContent} from 'src/app/test-shared/common-stubs.spec';
import { expectServiceMethodToMapResponseToObject, 
         expectServiceMethodToThrowError } from 'src/app/test-shared/common-expects.spec';
import { PageParams } from '../../shared/page.model';
import { of, throwError } from 'rxjs';

describe('CityLineService', () => {
  
  let cityLineService: CityLineService;
  let http: Http;
  let auth: AuthService;
  let util: UtilService;
  let httpOptions = new RequestOptions();

  let param: string = "BUS";
  let testRequestOptions = new RequestOptions();
  let pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'asc'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CityLinesModule,
        TestSharedModule
      ]
    });
  });

  beforeEach(() => {
    cityLineService = TestBed.get(CityLineService);
    http = TestBed.get(Http);
    auth = TestBed.get(AuthService);
    util = TestBed.get(UtilService);
  });

  it('should be created', inject([CityLineService], (service: CityLineService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', () => {
    expect(cityLineService.cityLineEndpoint).toEqual(`${environment.apiRoot}/city-line`);
    expect(cityLineService.cityLinesEndpoint).toEqual(`${environment.apiRoot}/city-lines`);
  });

  it('createCityLine() should map response from json to object if http does not return error', () => {
    let content = {};
    let cityLine = new CityLine();

    givenHttpPostReturnsResponseContainingContent(http, content);
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);
  
    expectServiceMethodToMapResponseToObject(cityLineService.createCityLine(cityLine), content, 'createCityLine()');
    expect(http.post).toHaveBeenCalledWith(cityLineService.cityLinesEndpoint, cityLine, httpOptions);
  });

  it('createCityLine() should throw error if http returns error', () => {
    let cityLine = new CityLine();

    givenHttpPostThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToThrowError(cityLineService.createCityLine(cityLine), 'test-error', 'createCityLine()');
    expect(http.post).toHaveBeenCalledWith(cityLineService.cityLinesEndpoint, cityLine, httpOptions);
  });

  it('findById() should map response from json to object if http does not return error', () => {
    let cityLineId = 0;
    let cityLine = new CityLine();

    givenHttpGetReturnsResponseContainingContent(http, cityLine);
    givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions(auth, httpOptions);
  
    expectServiceMethodToMapResponseToObject(cityLineService.findById(cityLineId), cityLine, 'findById()');
    expect(http.get).toHaveBeenCalledWith(`${cityLineService.cityLineEndpoint}/${cityLineId}`, httpOptions);
  });

  it('findById() should throw error if http returns error', () => {
    let cityLineId = 0;
    let cityLine = new CityLine();

    givenHttpGetThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions(auth, httpOptions);
  
    expectServiceMethodToThrowError(cityLineService.findById(cityLineId), 'test-error', 'findById()');
    expect(http.get).toHaveBeenCalledWith(`${cityLineService.cityLineEndpoint}/${cityLineId}`, httpOptions);
  });

  it('update() should call http put with correct params', () => {
    let cityLine = new CityLine();

    givenHttpPutReturnsResponseContainingContent(http, cityLine);
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);

    cityLineService.update(cityLine).subscribe();

    expect(http.put).toHaveBeenCalledWith(cityLineService.cityLineEndpoint, cityLine, httpOptions);
  });

  it('findAll() should map the json response to object', () => {
      spyOn(auth, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
      let expectedResult = { content: [new CityLine()] };
      spyOn(http, 'get').and.callFake(any => of({ json: () => expectedResult }));
      cityLineService.findAll(pageParams).subscribe(result => {
        expect(result.content).toBe(expectedResult.content);
      }, error => {
        expect(error).toBe(undefined, 'findAll() should have mapped the json response to object');
      });
      expect(http.get).toHaveBeenCalledWith(`${cityLineService.cityLinesEndpoint}${util.getPageableParamString(pageParams)}`,
       testRequestOptions);
  });

  it('findAll() should throw error if http returns error', () => {
    spyOn(auth, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    spyOn(http, 'get').and.callFake(any => throwError(new Error('test-error')));
    cityLineService.findAll(pageParams).subscribe(result => {
      expect(result).toBe(undefined, 'findAll() should have thrown error when http returns error');
    }, result => {
      expect(result.message).toEqual('test-error');
    });
    expect(http.get).toHaveBeenCalledWith(`${cityLineService.cityLinesEndpoint}${util.getPageableParamString(pageParams)}`,
       testRequestOptions);
  });

  it('getCityLinesByType() should map the json response to object', () => {
    spyOn(auth, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    let expectedResult = { content: [new CityLine()] };
    spyOn(http, 'get').and.callFake(any => of({ json: () => expectedResult }));
    cityLineService.getCityLinesByType(pageParams, param).subscribe(result => {
      expect(result.content).toBe(expectedResult.content);
    }, error => {
      expect(error).toBe(undefined, 'getCityLinesByType() should have mapped the json response to object');
    });
    expect(http.get).toHaveBeenCalledWith(`${cityLineService.cityLinesEndpoint}?type=${param}&${util.getPageableParamString(pageParams)}`,
      testRequestOptions);
    });

  it('getCityLinesByType() should throw error if http returns error', () => {
    spyOn(auth, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    spyOn(http, 'get').and.callFake(any => throwError(new Error('test-error')));
    cityLineService.getCityLinesByType(pageParams, param).subscribe(result => {
      expect(result).toBe(undefined, 'getCityLinesByType() should have thrown error when http returns error');
    }, result => {
      expect(result.message).toEqual('test-error');
    });
    expect(http.get).toHaveBeenCalledWith(`${cityLineService.cityLinesEndpoint}?type=${param}&${util.getPageableParamString(pageParams)}`,
      testRequestOptions);
    });
});
