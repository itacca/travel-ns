import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Http } from '@angular/http';
import { CityLine } from '../city-line.model';
import { Observable, pipe } from 'rxjs';
import { AuthService } from 'src/app/core/auth/auth.service';
import { LineStation } from '../line-station.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LineStationService {

  lineStationEndpoint: string = `${environment.apiRoot}/line-station`;

  lineStationsEndpoint: string = `${environment.apiRoot}/line-stations`;

  constructor(
    private http: Http,
    private auth: AuthService
  ) { }

  createAll(lineStations: LineStation[]): Observable<any> {
    return this.http.post(this.lineStationsEndpoint, lineStations, this.auth.getHttpAuthOptions());
  }

  createOrUpdateForCityLine(lineStations: LineStation[]): Observable<any> {
    return this.http.put(this.lineStationsEndpoint, lineStations, this.auth.getHttpAuthOptions());
  }

  findByCityLineId(cityLineId: number): Observable<LineStation[]> {
    return this.http.get(`${this.lineStationsEndpoint}?city_line_id=${cityLineId}`, this.auth.getHttpAuthOptionsWithoutToken())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }
}
