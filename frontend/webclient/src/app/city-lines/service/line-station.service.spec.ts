import { TestBed, inject } from '@angular/core/testing';

import { LineStationService } from './line-station.service';
import { CityLinesModule } from '../city-lines.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Http, RequestOptions } from '@angular/http';
import { AuthService } from 'src/app/core/auth/auth.service';
import { environment } from 'src/environments/environment';
import { givenHttpPutReturnsResponseContainingContent, 
         givenAuthGetHttpOptionsReturnsHttpOptions, 
         givenHttpGetReturnsResponseContainingContent,
         givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions,
         givenHttpGetThrowsError} from 'src/app/test-shared/common-stubs.spec';
import { CityLine } from '../city-line.model';
import { expectServiceMethodToMapResponseToObject, 
         expectServiceMethodToThrowError } from 'src/app/test-shared/common-expects.spec';

describe('LineStationService', () => {

  let lineStationService: LineStationService;
  let http: Http;
  let auth: AuthService;
  let httpOptions = new RequestOptions();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CityLinesModule,
        TestSharedModule
      ]
    });
  });

  beforeEach(() => {
    http = TestBed.get(Http);
    auth = TestBed.get(AuthService);
    lineStationService = TestBed.get(LineStationService);
  });

  it('should be created', inject([LineStationService], (service: LineStationService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', () => {
    expect(lineStationService.lineStationEndpoint).toEqual(`${environment.apiRoot}/line-station`);
    expect(lineStationService.lineStationsEndpoint).toEqual(`${environment.apiRoot}/line-stations`);
  });

  it('createOrUpdateForCityLine() should call http put with correct parameters', () => {
    let lineStations = [];

    givenHttpPutReturnsResponseContainingContent(http, {});
    givenAuthGetHttpOptionsReturnsHttpOptions(auth, httpOptions);
    
    lineStationService.createOrUpdateForCityLine(lineStations).subscribe();

    expect(http.put).toHaveBeenCalledWith(lineStationService.lineStationsEndpoint, lineStations, httpOptions);
  });

  it('findByCityLineId() should map response json to ojbect if http does not return error', () => {
    let cityLineId = 0;
    let cityLine = new CityLine();

    givenHttpGetReturnsResponseContainingContent(http, cityLine);
    givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToMapResponseToObject(lineStationService.findByCityLineId(cityLineId), cityLine, 'findByCityLineId');
    expect(http.get).toHaveBeenCalledWith(`${lineStationService.lineStationsEndpoint}?city_line_id=${cityLineId}`, httpOptions);
  });

  it('findByCityLineId() should throw error if http returns error', () => {
    let cityLineId = 0;

    givenHttpGetThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsWithoutTokenReturnsHttpOptions(auth, httpOptions);

    expectServiceMethodToThrowError(lineStationService.findByCityLineId(cityLineId), 'test-error', 'findByCityLineId()');
    expect(http.get).toHaveBeenCalledWith(`${lineStationService.lineStationsEndpoint}?city_line_id=${cityLineId}`, httpOptions);
  });
});
