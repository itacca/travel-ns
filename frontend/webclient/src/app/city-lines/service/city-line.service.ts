import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CityLine } from '../city-line.model';
import { AuthService } from '../../core/auth/auth.service';
import { UtilService } from '../../core/util.service';
import { environment } from 'src/environments/environment';
import { PageParams, Page } from '../../shared/page.model';

@Injectable({
    providedIn: 'root'
})
export class CityLineService {

  private BUS_TYPE: string = 'BUS';
  private TRAM_TYPE: string = 'TRAM';

  cityLinesEndpoint: string = `${environment.apiRoot}/city-lines`;

  cityLineEndpoint: string = `${environment.apiRoot}/city-line`;

  constructor(
    private http: Http,
    private authService: AuthService,
    private util: UtilService
  ) {}

  getCityLines(pageParams: PageParams): Observable<CityLine[]> {
    return  this.http.get(this.cityLinesEndpoint,
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  getCityLinesByType(pageParams: PageParams, param: string): Observable<Page<CityLine>> {
    param = this.validateParam(param);
    return this.http.get(`${this.cityLinesEndpoint}?type=${param}&${this.util.getPageableParamString(pageParams)}`,
    this.authService.getHttpAuthOptions()).pipe(map(
      response => response.json(),
      error => Observable.throw(error)
    ));
  }

  validateParam(param: string) {
    if (param != null) {
      if (param === this.BUS_TYPE.toLocaleLowerCase() || param === this.TRAM_TYPE.toLocaleLowerCase()) {
        return param;
      }
    }
    return this.BUS_TYPE;
  }

  createCityLine(cityLine: CityLine): Observable<any> {
    return this.http.post(this.cityLinesEndpoint, cityLine, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  findById(id: number): Observable<CityLine> {
    return this.http.get(`${this.cityLineEndpoint}/${id}`, this.authService.getHttpAuthOptionsWithoutToken())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  update(cityLine: CityLine): Observable<any> {
    return this.http.put(this.cityLineEndpoint, cityLine, this.authService.getHttpAuthOptions());
  }

  findAll(pageParams: PageParams): Observable<Page<CityLine>> {
    return this.http.get(`${this.cityLinesEndpoint}${this.util.getPageableParamString(pageParams)}`,
      this.authService.getHttpAuthOptions()).pipe(map(
      response => response.json(),
      error => Observable.throw(error)
    ));
  }
}
