import { Component, OnInit, ChangeDetectorRef, OnDestroy, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { ToastrService } from 'ngx-toastr';
import { CityLineService } from 'src/app/city-lines/service/city-line.service';
import { LineStationService } from '../service/line-station.service';
import { MapOption, optionsInitialized } from 'src/app/shared/dynamic-map/map-options/map-options';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';
import { Station } from 'src/app/station/station.model';
import { DrawRoute } from 'src/app/shared/dynamic-map/map-options/draw-route.option';
import { DrawMarkersAndNotifyWhenClicked } from 'src/app/shared/dynamic-map/map-options/draw-markers-and-notify-when-clicked.option';
import { PageParams, Page } from 'src/app/shared/page.model';
import { StationService } from 'src/app/station/station.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CityLine } from '../city-line.model';
import { LineStation } from '../line-station.model';
import { Subscription, Subject, forkJoin } from 'rxjs';
import { NamedPoint } from 'src/app/shared/dynamic-map/map-options/named-point.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-city-line',
  templateUrl: './create-city-line.component.html',
  styleUrls: ['./create-city-line.component.css']
})
export class CreateCityLineComponent implements OnInit, OnDestroy {

  mapOptions: MapOption[];

  mapRouteDrawer: DrawRoute;

  mapStationDrawer: DrawMarkersAndNotifyWhenClicked;

  mapInitializer: InitilizeMapWithId;

  mapInitializerSubscription: Subscription;

  cityLineGroup: FormGroup;

  pageParams: PageParams = {
    page: 0,
    size: 100,
    sort: 'id,asc'
  };

  stationsPage: Page<Station>;

  stationsOnLine: Station[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService,
    private cityLineService: CityLineService,
    private lineStationService: LineStationService,
    private stationService: StationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initMapOptions();
    this.initCityLineGroup();
  }

  ngOnDestroy() {
    if (this.mapInitializerSubscription) {
      this.mapInitializerSubscription.unsubscribe();
    }
  }

  private initCityLineGroup() {
    this.cityLineGroup = this.formBuilder.group({
      nameCtrl: [null, Validators.required],
      cityLineTypeCtrl: [null, Validators.required]
    });
  }

  private initMapOptions() {
    let mapInitializer = new InitilizeMapWithId('unique', 45.249302, 19.824508, 14);
    this.mapRouteDrawer = new DrawRoute([]);
    this.mapStationDrawer = new DrawMarkersAndNotifyWhenClicked([]);
    this.mapStationDrawer.markerClicked.subscribe(index => {
      this.stationsOnLine.push(this.stationsPage.content[index]);
      this.mapRouteDrawer.updateRoute(this.stationsOnLine)
    });

    this.mapOptions = [
      mapInitializer,
      this.mapRouteDrawer,
      this.mapStationDrawer
    ];

    optionsInitialized(this.mapOptions).subscribe(() => {
      this.loadStations(this.pageParams);
    });
  }

  private loadStations(pageParams: PageParams) {
    this.stationService.findAll(pageParams).subscribe(result => {
      this.stationsPage = result;
      this.mapStationDrawer.updatePoints(this.stationsPage.content as NamedPoint[]);
    });
  }

  removeStationFromLine(index: number) {
    this.stationsOnLine.splice(index, 1);
    this.mapRouteDrawer.updateRoute(this.stationsOnLine);
  } 

  stationDropped(event: CdkDragDrop<Station[]>) {
    moveItemInArray(this.stationsOnLine, event.previousIndex, event.currentIndex);
    this.mapRouteDrawer.updateRoute(this.stationsOnLine);
  }

  saveCityLine() {
    if (!this.cityLineGroup.valid) {
      return;
    }
    let cityLine: CityLine = this.util.formGroupToModel(new CityLine(), [this.cityLineGroup], 'Ctrl');
    this.cityLineService.createCityLine(cityLine).subscribe(result => {
      cityLine.id = result.id;
      let lineStations: LineStation[] = this.stationsOnLine.map((station, index) => new LineStation(null, cityLine, station, index));
      this.lineStationService.createAll(lineStations).subscribe(result => {
        this.toastr.success("City line has been created.", "Success");
        this.router.navigate(['/city-lines']);
      }, error => {
        this.toastr.error(error.json().message);
      });
    }, error => {
      this.toastr.error(error.json().message);
    });
  }
}
