import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { CreateCityLineComponent } from './create-city-line.component';
import { CityLinesModule } from '../city-lines.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { UtilService } from 'src/app/core/util.service';
import { ToastrService } from 'ngx-toastr';
import { CityLineService } from '../service/city-line.service';
import { LineStationService } from '../service/line-station.service';
import { StationService } from 'src/app/station/station.service';
import { of } from 'rxjs';
import { Station } from 'src/app/station/station.model';
import { Page, PageMock } from 'src/app/shared/page.model';
import { DrawRoute } from 'src/app/shared/dynamic-map/map-options/draw-route.option';
import { DrawMarkersAndNotifyWhenClicked } from 'src/app/shared/dynamic-map/map-options/draw-markers-and-notify-when-clicked.option';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';
import { expectFormGroupToContainControls } from 'src/app/test-shared/common-expects.spec';
import { By } from '@angular/platform-browser';
import { CityLine } from '../city-line.model';

describe('CreateCityLineComponent', () => {
  let component: CreateCityLineComponent;
  let fixture: ComponentFixture<CreateCityLineComponent>;

  let util: UtilService;
  let toastr: ToastrService;
  let cityLineService: CityLineService;
  let lineStationService: LineStationService;
  let stationService: StationService;

  let stationsPage: Page<Station> = new PageMock([
      new Station(1, 'Station1', [], 1, 2),
      new Station(2, 'Station2', [], 2, 2),
      new Station(3, 'Station3', [], 3, 2),
      new Station(4, 'Station4', [], 4, 2),
  ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CityLinesModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    toastr = TestBed.get(ToastrService);
    cityLineService = TestBed.get(CityLineService);
    lineStationService = TestBed.get(LineStationService);
    stationService = TestBed.get(StationService);
  });

  function createComponent() {
    fixture = TestBed.createComponent(CreateCityLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  function givenStationServiceReturnsStations() {
    spyOn(stationService, 'findAll').and.callFake(any => of(stationsPage));
  }

  it('should create', () => {
    givenStationServiceReturnsStations();
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should load stations', fakeAsync(() => {
    givenStationServiceReturnsStations();
    createComponent();
    fixture.whenStable().then(() => {
      expect(stationService.findAll).toHaveBeenCalledWith(component.pageParams);
      expect(component.stationsPage).toBe(stationsPage);
    });
  }));

  it('should initialize dynamic map with correct options', () => {
    givenStationServiceReturnsStations();
    createComponent();

    expect(component.mapRouteDrawer).toEqual(jasmine.any(DrawRoute));
    expect(component.mapStationDrawer).toEqual(jasmine.any(DrawMarkersAndNotifyWhenClicked));
    expect(component.mapOptions[0]).toEqual(jasmine.any(InitilizeMapWithId));
    expect(component.mapOptions[1]).toEqual(component.mapRouteDrawer);
    expect(component.mapOptions[2]).toEqual(component.mapStationDrawer);
  });

  it('should initialize city line form group', () => {
    givenStationServiceReturnsStations();
    createComponent();
    
    expectFormGroupToContainControls(component.cityLineGroup, ['nameCtrl', 'cityLineTypeCtrl']);
  });

  it('removeStationFromLine() should remove station from line and update map', () => {
    givenStationServiceReturnsStations();
    createComponent();
    spyOn(component.mapRouteDrawer, 'updateRoute').and.callFake(any => null);
    component.stationsOnLine = [stationsPage.content[0], stationsPage.content[1], stationsPage.content[2]];

    component.removeStationFromLine(1);

    expect(component.stationsOnLine[0]).toBe(stationsPage.content[0]);
    expect(component.stationsOnLine[1]).toBe(stationsPage.content[2]);
    expect(component.mapRouteDrawer.updateRoute).toHaveBeenCalledWith(component.stationsOnLine);
  });

  it('saveCityLine() should save city line with line stations', () => {
    let cityLine = new CityLine();
    
    givenStationServiceReturnsStations();
    createComponent();

    spyOnProperty(component.cityLineGroup, 'valid', 'get').and.callFake(any => true);
    spyOn(util, 'formGroupToModel').and.callFake(any => cityLine);
    spyOn(cityLineService, 'createCityLine').and.callFake(any => of({ id: 1 }));
    spyOn(lineStationService, 'createAll').and.callFake(any => of({}));
    spyOn(toastr, 'success').and.callFake(any => null);

    component.saveCityLine();
    
    expect(util.formGroupToModel).toHaveBeenCalledWith(jasmine.any(CityLine), [component.cityLineGroup], 'Ctrl');
    expect(cityLineService.createCityLine).toHaveBeenCalledWith(cityLine);
    expect(lineStationService.createAll).toHaveBeenCalled();
    expect(toastr.success).toHaveBeenCalledWith("City line has been created.", "Success");
  });

  it('should render name input', () => {
    givenStationServiceReturnsStations();
    createComponent();

    let nameInput = fixture.debugElement.query(By.css('input[formControlName="nameCtrl"]'));
    
    expect(nameInput).toBeTruthy();
  });

  it('should render city line type select', () => {
    givenStationServiceReturnsStations();
    createComponent();

    let typeSelect = fixture.debugElement.query(By.css('mat-select[formControlName="cityLineTypeCtrl"]'));
    
    expect(typeSelect).toBeTruthy();
  });

  it('should not render stations list if stations on line list is empty', () => {
    givenStationServiceReturnsStations();
    createComponent();

    let stationsList = fixture.debugElement.query(By.css('div.stations'));

    expect(stationsList).toBeFalsy();
  });

  it('should render stations list if stations on line list is empty', () => {
    givenStationServiceReturnsStations();
    createComponent();
    component.stationsOnLine = [stationsPage.content[0], stationsPage.content[1], stationsPage.content[2]];
    fixture.detectChanges();

    let stations = fixture.debugElement.query(By.css('div.stations'));

    expect(stations).toBeTruthy();
  });

  it('should render a station card for each station in stations on line list', () => {
    givenStationServiceReturnsStations();
    createComponent();
    component.stationsOnLine = [stationsPage.content[0], stationsPage.content[1], stationsPage.content[2]];
    fixture.detectChanges();

    let stationNames = fixture.debugElement.queryAll(By.css('div.station > div.name'));

    for (let i = 0; i < component.stationsOnLine.length; i++) {
      expect(stationNames[i].nativeElement.innerHTML).toContain(component.stationsOnLine[i].name);
    }
  });

  it('should render map if options are present', () => {
    givenStationServiceReturnsStations();
    createComponent();
    let map = fixture.debugElement.query(By.css('div.map'));
    expect(map).toBeTruthy();
  });

  it('should not render map if options are not present', () => {
    givenStationServiceReturnsStations();
    createComponent();
    component.mapOptions = null;
    fixture.detectChanges();
    let map = fixture.debugElement.query(By.css('div.map'));
    expect(map).toBeFalsy();
  });
});
