export class CityLine {

    constructor(
      public id?: number,
      public name?: string,
      public cityLineType?: 'BUS' | 'TRAM'
    ) { }
  }

