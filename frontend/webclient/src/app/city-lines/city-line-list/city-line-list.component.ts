import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { CityLine } from '../city-line.model';
import { CityLineService } from '../service/city-line.service';
import { PageParams, Page } from '../../shared/page.model';
import { Router } from '@angular/router';

@Component({
    selector: 'app-city-line-list',
    templateUrl: './city-line-list.component.html',
    styleUrls: ['./city-line-list.component.css']
})
export class CityLineListComponent implements OnInit {

    pageTitle: String = 'City lines';
    cityLines: CityLine[];
    filteredLines: CityLine[];
    listFilterField = '';
    typeFilterField = 'All';
    errorMessage = '';
    cityLinesPage: Page<CityLine>;

    pageParams: PageParams = {
      page: 0,
      size: 8,
      sort: 'asc'
    };

    sortFields = [
      { name: 'id', displayName: 'ID' },
      { name: 'name', displayName: 'Name' },
      { name: 'cityLineType', displayName: 'Type' }
    ];

    constructor(
        private cityLineService: CityLineService,
        private toastr: ToastrService,
        private router: Router
    ) {}


    get typeFilter(): string {
        return this.typeFilterField;
    }

    set typeFilter(value: string) {
        if (this.typeFilterField !== value) {
            this.typeFilterField = value;
            this.filterCityLines();
        }
    }

    get listFilter(): string {
        return this.listFilterField;
    }

    set listFilter(value: string) {
        if (this.listFilterField !== value) {
            this.listFilterField = value;
            this.filteredLines = this.performFilter();
        }
    }

    filterCityLines() {
        if (this.typeFilterField.toLocaleLowerCase() === 'all') {
            this.loadAllLines(this.pageParams);
        } else {
            this.loadCityLinesByType();
        }
        this.listFilterField = '';
    }

    performFilter(): CityLine[] {
        if (!this.listFilterField) {
            return this.cityLines;
        } else {
            return this.cityLines.filter((cityLine: CityLine) =>
            cityLine.name.toLocaleLowerCase().indexOf(this.listFilterField.toLocaleLowerCase()) !== -1);
        }
    }

    loadCityLinesByType() {
        this.cityLineService.getCityLinesByType(this.pageParams, this.typeFilterField.toLocaleLowerCase()).subscribe(
            cityLines => {
                this.cityLinesPage = cityLines;
                this.cityLines = this.cityLinesPage.content;
                this.filteredLines = this.cityLines;
            },
            error => this.toastr.error(error.json().message, 'Error'));
    }

    loadAllLines(pageParams: PageParams) {
        this.cityLineService.findAll(pageParams).subscribe(
            cityLines => {
                this.cityLinesPage = cityLines;
                this.cityLines = this.cityLinesPage.content;
                this.filteredLines = this.cityLines;
            },
            error => this.toastr.error(error.json().message, 'Error'));
    }

    showCityLine(id : number) {
        this.router.navigate(['city-line-details', id])
    }

    ngOnInit(): void {
        this.loadAllLines(this.pageParams);
    }

    paramsChanged(pageParams: PageParams) {
        if (this.typeFilter.toLocaleLowerCase() === 'bus' || this.typeFilter.toLocaleLowerCase() === 'tram') {
            this.loadCityLinesByType();
        } else {
            this.loadAllLines(pageParams);
        }
      }
}
