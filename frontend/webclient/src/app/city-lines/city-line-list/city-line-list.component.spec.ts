import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityLinesModule } from '../city-lines.module';
import { CityLineListComponent } from './city-line-list.component';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { CityLineService } from '../service/city-line.service';
import { Page } from '../../shared/page.model';
import { CityLine } from '../city-line.model';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('CityLineListComponent', () => {

  let component: CityLineListComponent;
  let fixture: ComponentFixture<CityLineListComponent>;
  let cityLineService: CityLineService;
  let toastrService;
  let cityLine1 = new CityLine(1, "Linija 1", "BUS");
  let cityLine2 = new CityLine(2, "Linija 2", "BUS");
  let cityLinesPage: Page<CityLine> = {
    content: [
      cityLine1,
      cityLine2,
      new CityLine(3, "Linija 3", "BUS"),
      new CityLine(4, "Linija 4", "TRAM"),
      new CityLine(5, "Linija 5", "BUS")
    ],
    totalPages: 1
  };
  let cityLinesBusPage: Page<CityLine> = {
    content: [
      cityLine1,
      cityLine2
    ],
    totalPages: 1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CityLinesModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    cityLineService = TestBed.get(CityLineService);
    toastrService = TestBed.get(ToastrService);
  });

  function createComponent() {
    fixture = TestBed.createComponent(CityLineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    spyOn(cityLineService, 'findAll').and.callFake(any => of(cityLinesPage));
    spyOn(cityLineService, 'getCityLinesByType').and.callFake(any => of(cityLinesBusPage));
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render "City lines" in h2 tag', () => {
    let h2Title = fixture.debugElement.query(By.css('.card-header'));
    expect(h2Title.nativeElement.innerHTML).toContain('City lines');
  });

  it('shoud render a city line table if cityLinesPage is loaded', () => {
    spyOn(cityLineService, 'findAll').and.callFake(any => of(cityLinesPage));
    spyOn(cityLineService, 'getCityLinesByType').and.callFake(any => of(cityLinesBusPage));
    createComponent();
    let cityLinesTable = fixture.debugElement.query(By.css('.table'));
    expect(cityLinesTable).toBeTruthy();
  });

  /*
  it('shoud not render a city line table if cityLinesPage is not loaded', () => {
    spyOn(cityLineService, 'findAll').and.callFake(any => of(cityLinesPage));
    spyOn(cityLineService, 'getCityLinesByType').and.callFake(any => of(cityLinesBusPage));
    createComponent();
    component.cityLinesPage = null;
    fixture.detectChanges();
    let cityLinesTable = fixture.debugElement.query(By.css('.table-responsive'));
    expect(cityLinesTable).toBeFalsy();
  });
  */

  it('should toast error if cityLineService findAll() throws error', () => {
    spyOn(cityLineService, 'findAll').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(cityLineService, 'getCityLinesByType').and.callFake(any => of(cityLinesBusPage));
    spyOn(toastrService, 'error').and.callFake(any => null);
    createComponent();
    expect(toastrService.error).toHaveBeenCalledWith('test-error', 'Error');
  });
});
