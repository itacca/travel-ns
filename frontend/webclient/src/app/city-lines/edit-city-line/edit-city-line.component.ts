import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { ToastrService } from 'ngx-toastr';
import { CityLineService } from '../service/city-line.service';
import { LineStationService } from '../service/line-station.service';
import { StationService } from 'src/app/station/station.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CityLine } from '../city-line.model';
import { DrawRoute } from 'src/app/shared/dynamic-map/map-options/draw-route.option';
import { DrawMarkersAndNotifyWhenClicked } from 'src/app/shared/dynamic-map/map-options/draw-markers-and-notify-when-clicked.option';
import { InitilizeMapWithId } from 'src/app/shared/dynamic-map/map-options/initialize-map-with-id.option';
import { Station } from 'src/app/station/station.model';
import { MapOption, optionsInitialized } from 'src/app/shared/dynamic-map/map-options/map-options';
import { PageParams, Page } from 'src/app/shared/page.model';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { LineStation } from '../line-station.model';
import { NamedPoint } from 'src/app/shared/dynamic-map/map-options/named-point.model';

@Component({
  selector: 'app-edit-city-line',
  templateUrl: './edit-city-line.component.html',
  styleUrls: ['./edit-city-line.component.css']
})
export class EditCityLineComponent implements OnInit, OnDestroy {

  routerSubscription: Subscription;

  cityLine: CityLine;

  lineStations: LineStation[] = [];

  stationsPage: Page<Station>;

  pageParams: PageParams = {
    page: 0,
    size: 100,
    sort: 'id,asc'
  };  

  cityLineGroup: FormGroup;

  mapOptions: MapOption[] = [];

  mapRouteDrawer: DrawRoute;

  mapStationDrawer: DrawMarkersAndNotifyWhenClicked;

  mapInitializerSubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService,
    private cityLineService: CityLineService,
    private lineStationService: LineStationService,
    private stationService: StationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initMapOptions();
    this.subscribeToRouterParams();
    this.loadStations(this.pageParams);
  }

  ngOnDestroy() {
    if (this.mapInitializerSubscription) {
      this.mapInitializerSubscription.unsubscribe();
    }
  }

  private initCityLineGroup() {
    this.cityLineGroup = this.formBuilder.group({
      nameCtrl: [this.cityLine.name, Validators.required],
      cityLineTypeCtrl: [this.cityLine.cityLineType, Validators.required]
    });
  }
  
  private initMapOptions() {
    let mapInitializer = new InitilizeMapWithId('unique', 45.249302, 19.824508, 14);
    this.mapRouteDrawer = new DrawRoute([]);
    this.mapStationDrawer = new DrawMarkersAndNotifyWhenClicked([]);
    this.mapStationDrawer.markerClicked.subscribe(index => {
      this.lineStations.push(new LineStation(null, this.cityLine, this.stationsPage.content[index], this.lineStations.length));
      this.updateRouteOnMap();
    });

    this.mapOptions = [
      mapInitializer,
      this.mapRouteDrawer,
      this.mapStationDrawer
    ];
  }

  private subscribeToRouterParams() {
    this.routerSubscription = this.activatedRoute.params.subscribe(params => {
      this.cityLineService.findById(params.cityLineId).subscribe(result => {
        this.cityLine = result;
        this.initCityLineGroup();
      }, error => {
        this.toastr.error(error.json().message, 'Error');
      });

      this.lineStationService.findByCityLineId(params.cityLineId).subscribe(lineStations => {
        this.lineStations = lineStations;
        this.updateRouteOnMap();
      }, error => {
        this.toastr.error(error.json().message, 'Error');
      });
    });
  }

  private loadStations(pageParams: PageParams) {
    this.stationService.findAll(pageParams).subscribe(result => {
      this.stationsPage = result;
      
      optionsInitialized(this.mapOptions).subscribe(() => {
        this.mapStationDrawer.updatePoints(this.stationsPage.content as NamedPoint[]);
      });
    });
  }

  removeStationFromLine(index: number) {
    this.lineStations.splice(index, 1);
    this.updateRouteOnMap();
  } 

  stationDropped(event: CdkDragDrop<Station[]>) {
    moveItemInArray(this.lineStations, event.previousIndex, event.currentIndex);
    this.updateRouteOnMap();
  }

  updateRouteOnMap() {
    optionsInitialized(this.mapOptions).subscribe(() => {
      this.mapRouteDrawer.updateRoute(this.lineStations.map(lineStation => lineStation.station));
    });
  } 

  saveCityLine() {
    if (!this.cityLineGroup.valid) {
      return;
    }
    let cityLine: CityLine = this.util.formGroupToModel(this.cityLine, [this.cityLineGroup], 'Ctrl');
    this.cityLineService.update(cityLine).subscribe(result => {
      this.lineStations.forEach((lineStation, index) => lineStation.relativePosition = index);
      this.lineStationService.createOrUpdateForCityLine(this.lineStations).subscribe(result => {
        this.toastr.success("City line has been updated.", "Success");
        this.router.navigate(['/city-lines']);
      }, error => {
        this.toastr.error(error.json().message, 'Error');
      });
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

}
