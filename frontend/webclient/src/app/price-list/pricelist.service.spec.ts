import { TestBed, inject } from '@angular/core/testing';

import { PriceListService } from './pricelist.service';
import { PriceListModule } from './pricelist.module';
import { TestSharedModule } from '../../app/test-shared/test-shared.module';
import { Http, RequestOptions } from '@angular/http';
import { AuthService } from 'src/app/core/auth/auth.service';
import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PriceList } from './pricelist.model';
import { UtilService } from '../core/util.service';
import { PriceListItem } from '../price-list-item/price-list-item.model';
import { givenAuthGetHttpOptionsReturnsHttpOptions, givenHttpGetReturnsResponseContainingContent, givenHttpGetThrowsError, givenHttpDeleteReturnsResponseContainingContent, givenHttpPostReturnsResponseContainingContent, givenHttpPostThrowsError, givenHttpPutReturnsResponseContainingContent } from '../test-shared/common-stubs.spec';
import { expectServiceMethodToMapResponseToObject, expectServiceMethodToThrowError } from '../test-shared/common-expects.spec';

describe('PriceListService', () => {
  let httpOptions = new RequestOptions();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        PriceListModule,
        TestSharedModule
      ]
    });
  });

  beforeEach(() => {
    let utilService: UtilService = TestBed.get(UtilService);
    let authService: AuthService = TestBed.get(AuthService);
  });

  it('should be created', inject([PriceListService], (service: PriceListService) => {
    expect(service).toBeTruthy();
  }));
  
  it('should have correct endpoints', inject([PriceListService], (service: PriceListService) => {
    expect(service.priceListEndpoint).toEqual(`${environment.apiRoot}/price-list`);
    expect(service.priceListsEndpoint).toEqual(`${environment.apiRoot}/price-lists`);
    expect(service.priceListItemEndpoint).toEqual(`${environment.apiRoot}/price-list-item`);
    expect(service.priceListItemsEndpoint).toEqual(`${environment.apiRoot}/price-list-items`);
  }));

  it('getPriceLists() should map the json response to object',  inject([Http, UtilService, AuthService, PriceListService], 
    (http: Http, utilService: UtilService, authService: AuthService, priceListService: PriceListService) => {
    let expectedContent = { content: [] };
    let testParamString = '?';

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetReturnsResponseContainingContent(http, expectedContent);
    
    expectServiceMethodToMapResponseToObject(priceListService.getPriceLists({}), expectedContent, 'getPriceLists()');
    expect(http.get).toHaveBeenCalledWith(`${priceListService.priceListsEndpoint}${testParamString}`, httpOptions);
  }));
  
  it('getPriceLists() should throw error if http returns error', inject([Http, UtilService, AuthService, PriceListService], 
    (http: Http, utilService: UtilService, authService: AuthService, priceListService: PriceListService) => {
    let testParamString = '?';

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpGetThrowsError(http, new Error('test-error'));
  
    expectServiceMethodToThrowError(priceListService.getPriceLists({}), 'test-error', 'getPriceLists()');
    expect(http.get).toHaveBeenCalledWith(`${priceListService.priceListsEndpoint}${testParamString}`, httpOptions);
  }));

  
  it('getByPriceListAndTicketId() should map the json response to object', inject([Http, UtilService, AuthService, PriceListService], 
    (http: Http, utilService: UtilService, authService: AuthService, priceListService: PriceListService) => {
      let testParamString = '?';
      let expectedContent = { content: [] };

      givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
      givenHttpGetReturnsResponseContainingContent(http, expectedContent);
      let ticketId = 0;
      let priceListId = 0;
      expectServiceMethodToMapResponseToObject(priceListService.getByPriceListAndTicketId(priceListId, ticketId,{}), expectedContent, 'getByPriceListAndTicketId()');
      expect(http.get).toHaveBeenCalledWith(`${priceListService.priceListItemsEndpoint}?price_list_id=${priceListId}&ticket_id=${ticketId}&${testParamString}`, httpOptions);
  }));
  
  it('getByPriceListAndTicketId() should throw error if http returns error', inject([Http, UtilService, AuthService, PriceListService], 
    (http: Http, utilService: UtilService, authService: AuthService, priceListService: PriceListService) => {
      let expectedContent = { content: [] };
      let testParamString = '?';
  
      givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
      givenHttpGetThrowsError(http, new Error('test-error'));
      let ticketId = 0;
      let priceListId = 0;
      expectServiceMethodToThrowError(priceListService.getByPriceListAndTicketId(priceListId, ticketId,{}), 'test-error', 'getByPriceListIdAndTicketId()');
      expect(http.get).toHaveBeenCalledWith(`${priceListService.priceListItemsEndpoint}?price_list_id=${priceListId}&ticket_id=${ticketId}&${testParamString}`, httpOptions);
  }));

  it('getByPriceListId() should map the json response to object', inject([Http, UtilService, AuthService, PriceListService], 
    (http: Http, utilService: UtilService, authService: AuthService, priceListService: PriceListService) => {
      let testParamString = '?';
      let expectedContent = { content: [] };

      givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
      givenHttpGetReturnsResponseContainingContent(http, expectedContent);
      let priceListId = 0;
      expectServiceMethodToMapResponseToObject(priceListService.getByPriceListId(priceListId,{}), expectedContent, 'getByPriceListAndId()');
      expect(http.get).toHaveBeenCalledWith(`${priceListService.priceListItemsEndpoint}?price_list_id=${priceListId}&${testParamString}`, httpOptions);
  }));
  
  it('getByPriceListId() should throw error if http returns error', inject([Http, UtilService, AuthService, PriceListService], 
    (http: Http, utilService: UtilService, authService: AuthService, priceListService: PriceListService) => {
      let expectedContent = { content: [] };
      let testParamString = '?';
  
      givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
      givenHttpGetThrowsError(http, new Error('test-error'));
  
      let priceListId = 0;
      expectServiceMethodToThrowError(priceListService.getByPriceListId(priceListId,{}), 'test-error', 'getByPriceListId()');
      expect(http.get).toHaveBeenCalledWith(`${priceListService.priceListItemsEndpoint}?price_list_id=${priceListId}&${testParamString}`, httpOptions);
  }));

  
  it('deletePriceList() should use correct endpoints and params', 
    inject([PriceListService, Http, AuthService], (priceListService: PriceListService, http: Http, authService: AuthService) => {
    let id = 0;
    let expectedContent = {};

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpDeleteReturnsResponseContainingContent(http, expectedContent);

    priceListService.deletePriceList(id).subscribe();

    expect(http.delete).toHaveBeenCalledWith(`${priceListService.priceListEndpoint}/${id}`, httpOptions);
  }));

  it('deletePriceListItem() should use correct endpoints and params', 
    inject([PriceListService, Http, AuthService], (priceListService: PriceListService, http: Http, authService: AuthService) => {
    let id = 0;
    let expectedContent = {};

    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);
    givenHttpDeleteReturnsResponseContainingContent(http, expectedContent);

    priceListService.deletePriceListItem(id).subscribe();

    expect(http.delete).toHaveBeenCalledWith(`${priceListService.priceListItemEndpoint}/${id}`, httpOptions);
  }));

  it('addPriceList() should map response to object if no error is thrown', inject([Http, AuthService, PriceListService], 
    (http: Http, authService: AuthService, priceListService: PriceListService) => {
    let expectedContent = { id: 1 };
    let priceList = new PriceList();

    givenHttpPostReturnsResponseContainingContent(http, expectedContent);
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    expectServiceMethodToMapResponseToObject(priceListService.addPriceList(priceList), expectedContent, 'addPriceList()');
    expect(http.post).toHaveBeenCalledWith(priceListService.priceListsEndpoint, priceList, httpOptions);
  }));

  it('addPriceList() should throw error if http returns error', inject([Http, AuthService, PriceListService], 
    (http: Http, authService: AuthService, priceListService: PriceListService) => {
    let expectedContent = { id: 1 };
    let priceList = new PriceList();

    givenHttpPostThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    expectServiceMethodToThrowError(priceListService.addPriceList(priceList), 'test-error', 'addPriceList()');
    expect(http.post).toHaveBeenCalledWith(priceListService.priceListsEndpoint, priceList, httpOptions);
  }));

  it('addPriceListItem() should map response to object if no error is thrown', inject([Http, AuthService, PriceListService], 
    (http: Http, authService: AuthService, priceListService: PriceListService) => {
    let expectedContent = { id: 1 };
    let priceListItem = new PriceListItem();

    givenHttpPostReturnsResponseContainingContent(http, expectedContent);
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    expectServiceMethodToMapResponseToObject(priceListService.addPriceListItem(priceListItem), expectedContent, 'addPriceListItem()');
    expect(http.post).toHaveBeenCalledWith(priceListService.priceListItemsEndpoint, priceListItem, httpOptions);
  }));

  it('addPriceListItem() should throw error if http returns error', inject([Http, AuthService, PriceListService], 
    (http: Http, authService: AuthService, priceListService: PriceListService) => {
    let expectedContent = { id: 1 };
    let priceListItem = new PriceListItem();

    givenHttpPostThrowsError(http, new Error('test-error'));
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    expectServiceMethodToThrowError(priceListService.addPriceListItem(priceListItem), 'test-error', 'addPriceListItem()');
    expect(http.post).toHaveBeenCalledWith(priceListService.priceListItemsEndpoint, priceListItem, httpOptions);
  }));

  it('updatePriceList() should use correct endpoint with correct params', inject([Http, AuthService, PriceListService], 
    (http: Http, authService: AuthService, priceListService: PriceListService) => {
    let priceList = new PriceList();
  
    givenHttpPutReturnsResponseContainingContent(http, {});
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    priceListService.updatePriceList(priceList).subscribe();

    expect(http.put).toHaveBeenCalledWith(priceListService.priceListEndpoint, priceList, httpOptions);
  }));

  it('updatePriceListItem() should use correct endpoint with correct params', inject([Http, AuthService, PriceListService], 
    (http: Http, authService: AuthService, priceListService: PriceListService) => {
    let priceListItem = new PriceListItem();
  
    givenHttpPutReturnsResponseContainingContent(http, {});
    givenAuthGetHttpOptionsReturnsHttpOptions(authService, httpOptions);

    priceListService.updatePriceListItem(priceListItem).subscribe();

    expect(http.put).toHaveBeenCalledWith(priceListService.priceListItemEndpoint, priceListItem, httpOptions);
  }));
});