import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceListAddComponent } from './price-list-add.component';
import { PriceListModule } from '../pricelist.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { By } from '@angular/platform-browser';
import { UtilService } from 'src/app/core/util.service';
import { PriceList } from '../pricelist.model';
import { Router } from '@angular/router';
import { PriceListService } from '../pricelist.service';
import { of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

describe('PriceListAddComponent', () => {
  let component: PriceListAddComponent;
  let fixture: ComponentFixture<PriceListAddComponent>;
  
  let util: UtilService;
  let router: Router;
  let priceListService: PriceListService;
  let toastr: ToastrService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PriceListModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    router = TestBed.get(Router);
    priceListService = TestBed.get(PriceListService);
    toastr = TestBed.get(ToastrService);

    fixture = TestBed.createComponent(PriceListAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Add new price list" in h2 tag', () => {
    let h2 = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(h2.innerHTML).toContain('Add new price list');
  });

  it('should render input fields for price list information', () => {
    let startDate = fixture.debugElement.query(By.css('input[formControlName="startDateCtrl"]'));
    let endDate = fixture.debugElement.query(By.css('input[formControlName="endDateCtrl"]'));

    expect(startDate).toBeTruthy();
    expect(endDate).toBeTruthy();
  });

  it('addPriceList() should call priceListService.addPriceList() with correct price list instance', () => {
    let expectedPriceList = new PriceList();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedPriceList);
    spyOn(priceListService, 'addPriceList').and.callFake(any => of({ id: 1 }));
    spyOn(router, 'navigate').and.callFake(any => null);
    component.addPriceList();
    expect(priceListService.addPriceList).toHaveBeenCalledWith(expectedPriceList);
    expect(router.navigate).toHaveBeenCalledWith(['/pricelist']);
  });

  it('addPriceList() should toast error if priceListService.addPriceList() throws error', () => {
    let expectedPriceList = new PriceList();
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedPriceList);
    spyOn(priceListService, 'addPriceList').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastr, 'error')
    component.addPriceList();
    expect(priceListService.addPriceList).toHaveBeenCalledWith(expectedPriceList);
    expect(toastr.error).toHaveBeenCalledWith('Unable to add price list.', 'Error');
  });
});
