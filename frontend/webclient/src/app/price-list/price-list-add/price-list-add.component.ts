import { Component, OnInit } from '@angular/core';
import { PriceListService } from '../pricelist.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { PriceList } from '../pricelist.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { PriceListItem } from 'src/app/price-list-item/price-list-item.model';
import { AccountService } from 'src/app/account/account.service';
import { Roles } from 'src/app/account/role.model';

@Component({
  selector: 'app-add-price-list',
  templateUrl: './price-list-add.component.html',
  styleUrls: ['./price-list-add.component.css']
})
export class PriceListAddComponent implements OnInit {

  priceListGroup: FormGroup;

  isAdmin: boolean;

  constructor(
    private priceListService: PriceListService,
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initAccountGroup();
    this.checkRoles();
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  private initAccountGroup() {
    this.priceListGroup = this.formBuilder.group({
      startDateCtrl: [null, Validators.required],
      endDateCtrl: [null, Validators.required]
    });
  }

  addPriceList() {
      let priceList: PriceList = this.util.formGroupToModel(new PriceList(), [this.priceListGroup], 'Ctrl');
      let items: PriceListItem[] = [];
      priceList.priceListItems = items;
      this.priceListService.addPriceList(priceList).subscribe(
        success => {
            this.toastr.success("Price list successfuly added", "Success");
            this.router.navigate(['/pricelist']);
          },
          error => this.toastr.error("Unable to add price list.", "Error")
      )
  }
}
