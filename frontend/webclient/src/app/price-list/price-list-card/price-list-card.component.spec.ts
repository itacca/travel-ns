import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PriceListCardComponent } from './price-list-card.component';
import { PriceListModule } from '../pricelist.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { PriceList } from '../pricelist.model';
import { PriceListItem } from 'src/app/price-list-item/price-list-item.model';
import { Page } from 'src/app/shared/page.model';
import { Ticket } from 'src/app/ticket/ticket.model';
import { By } from '@angular/platform-browser';
import { FormGroup, FormControl } from '@angular/forms';
import { CityLine } from 'src/app/city-lines/city-line.model';


describe('PriceListCardComponent', () => {
  let component: PriceListCardComponent;
  let fixture: ComponentFixture<PriceListCardComponent>;
  let priceList: PriceList = new PriceList(0, new Date(), new Date());
  let isAdmin: boolean = true;
  let priceListUpdateGroup = new FormGroup({
    endDateCtrl: new FormControl()
 });
 let priceListItemGroups: Array<FormGroup> = [new FormGroup({
  priceCtrl: new FormControl()
}), new FormGroup({
  priceCtrl: new FormControl()
}), new FormGroup({
  priceCtrl: new FormControl()
})];

  let priceListItems: Page<PriceListItem> = {
    content: [
        new PriceListItem(0, 55, new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")), new Date()),
        new PriceListItem(0, 77, new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")), new Date()),
        new PriceListItem(0, 88, new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")), new Date())
        
      ],
      totalPages: 1

  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PriceListModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  function createComponent() {
    fixture = TestBed.createComponent(PriceListCardComponent);
    component = fixture.componentInstance;
    component.priceList = priceList;
    component.priceListItems = priceListItems;
    component.isAdmin = isAdmin;
    component.priceListUpdateGroup = priceListUpdateGroup;
    component.priceListItemGroups = priceListItemGroups;
    fixture.detectChanges();
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render a price list item card for each price list item in the items list', () => {
    createComponent();
    fixture.detectChanges();
    let priceListItemCards = fixture.debugElement.queryAll(By.css('app-price-list-item-view'));
    expect(priceListItemCards.length).toEqual(priceListItems.content.length);
  });

});
