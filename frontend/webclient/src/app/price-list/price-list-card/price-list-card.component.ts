import { Component, OnInit, Input, Inject } from '@angular/core';
import { PriceListService } from '../pricelist.service';
import { PageParams, Page } from 'src/app/shared/page.model';
import { PriceListItem } from 'src/app/price-list-item/price-list-item.model';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UtilService } from 'src/app/core/util.service';
import { PriceList } from '../pricelist.model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { PriceListItemAddComponent } from 'src/app/price-list-item/price-list-item-add/price-list-item-add.component';
import { AccountService } from 'src/app/account/account.service';
import { Roles } from 'src/app/account/role.model';

@Component({
  selector: 'app-price-list-card',
  templateUrl: './price-list-card.component.html',
  styleUrls: ['./price-list-card.component.css']
})
export class PriceListCardComponent implements OnInit {

  @Input()
  priceList: PriceList;

  @Input()
  index: number;

  priceListItems: Page<PriceListItem>;

  priceListUpdateGroup: FormGroup;

  isAdmin: boolean;

  priceListItemGroups: Array<FormGroup> = [];

  pageParams: PageParams = {
    size: 8,
    page: 0,
    sort: 'asc',  
  };

  sortFields: {displayName: string, name: string}[] = [{displayName: "PRICE", name: "price"}, {displayName: "ID", name: "id"}, 
  {displayName: "TICKET TYPE", name: "ticket.ticketType"}, {displayName: "TICKET DURATION", name: "ticket.ticketDuration"}, {displayName: "CITY LINE TYPE", name: "ticket.cityLine.cityLineType"}, {displayName: "NAME", name: "ticket.cityLine.name"}];

  constructor(
    private toastr: ToastrService,
    private accountService: AccountService,
    private priceListService: PriceListService,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private router: Router,
    public dialog: MatDialog
  ) {
  }
  ngOnInit() {
    this.checkRoles();
    this.initPriceListGroup();
    this.loadPriceListItems(this.pageParams);
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  private initPriceListGroup() {
    this.priceListUpdateGroup = this.formBuilder.group({
      endDateCtrl: [this.priceList.endDate, Validators.required],
    })
  }

  private initGroups() {
    
    for (let index = 0; index < this.priceListItems.content.length; index++) {
      console.log(this.priceListItems.content[index].price)
      let group: FormGroup;
      group = this.formBuilder.group({
        priceCtrl: ['', Validators.required],
      });
      this.priceListItemGroups.push(group);
    }
  }

  loadPriceListItems(params: PageParams) {
    this.priceListService.getByPriceListId(this.priceList.id, params).subscribe(priceListItems => {
      this.priceList.priceListItems = priceListItems.content;
      this.priceListItems = priceListItems;
      this.initGroups();
    }, error => this.toastr.error(error.message, 'Error'));
  }

  updatePriceList() {
    let priceList: PriceList = this.util.formGroupToModel(new PriceList(), [this.priceListUpdateGroup], 'Ctrl');
    priceList.id = this.priceList.id;
    priceList.startDate = this.priceList.startDate;
    this.priceListService.updatePriceList(priceList).subscribe(
      success => {
        this.toastr.success("Price list successfuly updated", "Success");
        this.router.navigate(['/pricelist']);
      },
      error => this.toastr.error("Unable to update price list.", "Error")
    )
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PriceListItemAddComponent, {
      width: '600px',
      data: {
        priceList: this.priceList,
        priceListItemGroups: this.priceListItemGroups
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  deletePriceListItem(id: number, i: number) {
    this.priceListItems.content.splice(i, 1);
    this.priceListService.deletePriceListItem(id).subscribe(
      success => {
        this.toastr.success("Price list item successfuly deleted.", "Success");
        this.router.navigate(['/pricelist']);
      },
      error => this.toastr.error("Unable to delete price list item.", "Error")
    )
  }

  updatePriceListItem(priceListItem: PriceListItem, i: number) {
    let priceListItemNew: PriceListItem = this.util.formGroupToModel(new PriceListItem(), [this.priceListItemGroups[i]], 'Ctrl');
    priceListItemNew.ticket = priceListItem.ticket;
    priceListItemNew.id = priceListItem.id;
    priceListItemNew.priceList = priceListItem.priceList;
    this.priceListService.updatePriceListItem(priceListItemNew).subscribe(
      success => {
        this.toastr.success("Price list item price successfuly updated", "Success");
        priceListItem.price = priceListItemNew.price;
        priceListItem.id = success.id;
        this.priceListItems.content[i] = priceListItem;
        //this.initGroups();
        this.router.navigate(['/pricelist']);
      },
      error => this.toastr.error("Unable to update price list item", "Error")
    )
  }

  paramsChanged(pageParams: PageParams) {
    this.loadPriceListItems(pageParams);
  }



}