import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceListHistoryComponent } from './price-list-history.component';
import { PriceListModule } from '../pricelist.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { By } from '@angular/platform-browser';
import { UtilService } from 'src/app/core/util.service';
import { PriceList } from '../pricelist.model';
import { Router } from '@angular/router';
import { PriceListService } from '../pricelist.service';
import { of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Page } from 'src/app/shared/page.model';
import { Ticket } from 'src/app/ticket/ticket.model';
import { PriceListItem } from 'src/app/price-list-item/price-list-item.model';
import { TicketService } from 'src/app/ticket/service/ticket.service';
import { CityLine } from 'src/app/city-lines/city-line.model';

describe('PriceListHistoryComponent', () => {
  let component: PriceListHistoryComponent;
  let fixture: ComponentFixture<PriceListHistoryComponent>;
  
  let util: UtilService;
  let router: Router;
  let priceListService: PriceListService;
  let ticketService: TicketService;
  let toastr: ToastrService;
  var priceListServiceSpy1;
  var priceListServiceSpy2;
  var ticketServiceSpy;

  let priceLists: Page<PriceList> = {
    content: [
      new PriceList(0, new Date(), new Date()),
      new PriceList(1, new Date(), new Date()),
      new PriceList(2,  new Date(), new Date()),
    ],
    totalPages: 1
  };

  let tickets: Page<Ticket> = {
    content: [
      new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")),
      new Ticket(2, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")),
      new Ticket(1, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS"))
      
    ],
    totalPages: 1
  };

  let priceListItems: Page<PriceListItem> = {
    content: [
        new PriceListItem(0, 55, new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")), new Date()),
        new PriceListItem(0, 77, new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")), new Date()),
        new PriceListItem(0, 88, new Ticket(0, "DAILY", "REGULAR", new CityLine(0, "Linija 1", "BUS")), new Date())
        
      ],
      totalPages: 1

  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PriceListModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));


  function createComponent() {
    fixture = TestBed.createComponent(PriceListHistoryComponent);
    component = fixture.componentInstance;
    component.priceLists = priceLists;
    component.tickets = tickets;
    component.priceListItems = priceListItems;
    fixture.detectChanges();
  }

  it('should create', () => {
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render "Price list history" in h2 tag', () => {
    createComponent();
    let h2 = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(h2.innerHTML).toContain('Price list history');
  });

  it('should render a price list item for each price list item in the items list', () => {
    createComponent();
    fixture.detectChanges();
    let priceListItemCards = fixture.debugElement.queryAll(By.css('.item'));
    expect(priceListItemCards.length).toEqual(priceListItems.content.length);
  });

});
