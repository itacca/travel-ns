import { Component, OnInit } from '@angular/core';
import { PriceListService } from '../pricelist.service';
import { PriceList } from '../pricelist.model';
import { ToastrService } from 'ngx-toastr';
import { Page, PageParams } from 'src/app/shared/page.model';
import { Router } from '@angular/router';
import { Ticket } from 'src/app/ticket/ticket.model';
import { TicketService } from 'src/app/ticket/service/ticket.service';
import { PriceListItem } from 'src/app/price-list-item/price-list-item.model';
import { AccountService } from 'src/app/account/account.service';
import { Roles } from 'src/app/account/role.model';

@Component({
  selector: 'price-list-history',
  templateUrl: './price-list-history.component.html',
  styleUrls: ['./price-list-history.component.css']
})
export class PriceListHistoryComponent implements OnInit {

  priceLists: Page<PriceList>;

  selectedPriceList: PriceList;

  tickets: Page<Ticket>;

  selectedTicket: Ticket;

  isAdmin: boolean;

  priceListItems: Page<PriceListItem>;
 
  pageParams: PageParams = {
    size: 200,
    page: 0,
    sort: 'asc'
  };

  sortFields: {displayName: string, name: string}[] = [{displayName: "PRICE", name: "price"}, {displayName: "DATE ACTIVE", name: "dateActive"}];


  constructor(
    private priceListService: PriceListService,
    private accountService: AccountService,
    private ticketService: TicketService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadPriceLists(this.pageParams);
    this.checkRoles();
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  loadPriceLists(params: PageParams) {
    this.priceListService.getPriceLists(params).subscribe(priceLists => {
      this.priceLists = priceLists;
    }, error => this.toastr.error(error.message, 'Error'));
  }

 loadTickets(id: number) {
    this.ticketService.getTicketsWithDefinedPriceForGivenPriceList(id, this.pageParams).subscribe(tickets => {
        this.tickets = tickets;
      }, error => this.toastr.error(error.message, 'Error'));
  }

 loadPriceListItems(priceListId: number, ticketId: number, pageParams: PageParams) {
    this.priceListService.getByPriceListAndTicketId(priceListId, ticketId, pageParams).subscribe(priceListItems => {
        this.priceListItems = priceListItems;
      }, error => this.toastr.error(error.message, 'Error'));
  }

  paramsChanged(pageParams: PageParams) {
    this.loadPriceListItems(this.selectedPriceList.id, this.selectedTicket.id, pageParams);
  }

  
}
