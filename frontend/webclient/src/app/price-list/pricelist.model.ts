import { PriceListItem } from "../price-list-item/price-list-item.model";
export class PriceList {
    constructor (
      public id?: number,
      public startDate?: Date,
      public endDate?: Date,
      public priceListItems?: PriceListItem[]
    ){}
}