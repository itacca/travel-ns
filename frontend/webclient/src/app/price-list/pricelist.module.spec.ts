import { PriceListModule } from './pricelist.module';

describe('PriceListModule', () => {
  let priceListModule: PriceListModule;

  beforeEach(() => {
    priceListModule = new PriceListModule();
  });

  it('should create an instance', () => {
    expect(priceListModule).toBeTruthy();
  });
});