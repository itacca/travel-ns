import { Component, OnInit } from '@angular/core';
import { PriceListService } from '../pricelist.service';
import { PriceList } from '../pricelist.model';
import { ToastrService } from 'ngx-toastr';
import { Page, PageParams } from 'src/app/shared/page.model';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/account/account.service';
import { Roles } from 'src/app/account/role.model';

@Component({
  selector: 'app-pricelist-view',
  templateUrl: './pricelist-view.component.html',
  styleUrls: ['./pricelist-view.component.css']
})
export class PriceListViewComponent implements OnInit {

  priceLists: Page<PriceList>;

  isAdmin: boolean;

  pageParams: PageParams = {
    size: 8,
    page: 0,
    sort: 'asc'
  };

  sortFields: {displayName: string, name: string}[] = [{displayName: "ID", name: "id"}, {displayName: "START DATE", name: "startDate"}, {displayName: "END DATE", name: "endDate"}];


  constructor(
    private priceListService: PriceListService,
    private accountService: AccountService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadPriceLists(this.pageParams);
    this.checkRoles();
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  private loadPriceLists(params: PageParams) {
    this.priceListService.getPriceLists(params).subscribe(priceLists => {
      this.priceLists = priceLists;
    }, error => this.toastr.error(error.message, 'Error'));
  }

  deletePriceList(id: number, i:number) {
    this.priceLists.content.splice(i, 1);
    this.priceListService.deletePriceList(id).subscribe(
      success => {
        this.toastr.success("Price list successfuly deleted", "Success");
        this.router.navigate(['/pricelist']);
      },
      error => this.toastr.error("Unable to delete price list.", "Error")
    )
  }

  paramsChanged(pageParams: PageParams) {
    this.loadPriceLists(pageParams);
  }
}
