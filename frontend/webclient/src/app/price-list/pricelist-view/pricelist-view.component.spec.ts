import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceListViewComponent } from './pricelist-view.component';
import { PriceListModule } from '../pricelist.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Page } from 'src/app/shared/page.model';
import { PriceList } from '../pricelist.model';
import { PriceListService } from '../pricelist.service';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('PriceListViewComponent', () => {
  let component:  PriceListViewComponent;
  let fixture: ComponentFixture<PriceListViewComponent>;
  let priceListService: PriceListService;
  let toastrService;
  let priceLists: Page<PriceList> = {
    content: [
      new PriceList(0, new Date(), new Date()),
      new PriceList(1, new Date(), new Date()),
      new PriceList(2,  new Date(), new Date()),
    ],
    totalPages: 1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PriceListModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    priceListService = TestBed.get(PriceListService);
    toastrService = TestBed.get(ToastrService);
  });

  function createComponent() {
    fixture = TestBed.createComponent(PriceListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    spyOn(priceListService, 'getPriceLists').and.callFake(any => of(priceLists));
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render "Price lists" in h2 tag', () => {
    let h2Title = fixture.debugElement.query(By.css('h2'));
    expect(h2Title.nativeElement.innerHTML).toContain('Price lists');
  });


  it('should render a price list card for each price list in the price list list', () => {
    spyOn(priceListService, 'getPriceLists').and.callFake(any => of(priceLists));
    createComponent();
    let priceListCards = fixture.debugElement.queryAll(By.css('app-price-list-card'));
    expect(priceListCards.length).toEqual(priceLists.content.length);
  });
});
