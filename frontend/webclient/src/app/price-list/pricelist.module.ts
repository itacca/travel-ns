import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PriceListViewComponent } from './pricelist-view/pricelist-view.component';
import { PriceListCardComponent } from './price-list-card/price-list-card.component';
import { PriceListAddComponent } from './price-list-add/price-list-add.component';
import { PriceListItemViewComponent } from '../price-list-item/price-list-item-view/price-list-item-view.component';
import { PriceListItemAddComponent } from '../price-list-item/price-list-item-add/price-list-item-add.component';
import { PriceListRoutingModule } from './pricelist-routing.module';
import { MaterialModule } from '../material/material.module';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PriceListHistoryComponent } from './price-list-history/price-list-history.component';


@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    MatCardModule,
    MatExpansionModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    PriceListRoutingModule,
    MatDialogModule
  ],
  declarations: [
    PriceListViewComponent,
    PriceListCardComponent,
    PriceListAddComponent,
    PriceListItemViewComponent,
    PriceListItemAddComponent,
    PriceListHistoryComponent
  ],
  exports: [
    PriceListRoutingModule,
    
  ],
  entryComponents: [
    PriceListItemAddComponent
  ]
})
export class PriceListModule { }
