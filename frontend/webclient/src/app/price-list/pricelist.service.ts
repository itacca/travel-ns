import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from '../core/auth/auth.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PriceList } from './pricelist.model';
import { PriceListItem } from '../price-list-item/price-list-item.model'
import { UtilService } from '../core/util.service';

import { Pageable, Page, PageParams } from '../shared/page.model';

@Injectable({
  providedIn: 'root'
})
export class PriceListService {

  priceListEndpoint: string = `${environment.apiRoot}/price-list`;

  priceListsEndpoint: string = `${environment.apiRoot}/price-lists`;

  priceListItemsEndpoint: string = `${environment.apiRoot}/price-list-items`;

  priceListItemEndpoint: string = `${environment.apiRoot}/price-list-item`;

  constructor(
    private http: Http,
    private authService: AuthService,
    private util: UtilService
  ) { }
  
  getPriceLists(pageParams: PageParams): Observable<Page<PriceList>> {
    return this.http.get(`${this.priceListsEndpoint}${this.util.getPageableParamString(pageParams)}`, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  addPriceList(priceList: PriceList): Observable<PriceList> {
    return this.http.post(`${this.priceListsEndpoint}`, priceList, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json() as PriceList,
        error => Observable.throw(error)
      ));
  }

  addPriceListItem(priceListItem: PriceListItem): Observable<PriceListItem> {
    return this.http.post(`${this.priceListItemsEndpoint}`, priceListItem, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json() as PriceListItem,
        error => Observable.throw(error)
      ));
  }

  updatePriceListItem(priceListItem: PriceListItem): Observable<PriceListItem> {
    return this.http.put(`${this.priceListItemEndpoint}`, priceListItem, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json() as PriceListItem,
        error => Observable.throw(error)
      ));
  }

  updatePriceList(priceList: PriceList): Observable<PriceList> {
    return this.http.put(`${this.priceListEndpoint}`, priceList, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json() as PriceList,
        error => Observable.throw(error)
      ));
  }

  deletePriceList(id: number): Observable<any> {
    return this.http.delete(`${this.priceListEndpoint}/${id}`, 
      this.authService.getHttpAuthOptions());
  }

  getByPriceListId(id: number, pageParams: PageParams): Observable<Page<PriceListItem>> {
    return this.http.get(`${this.priceListItemsEndpoint}?price_list_id=${id}&${this.util.getPageableParamString(pageParams)}`,
    this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }
  
  deletePriceListItem(id: number): Observable<any> {
    return this.http.delete(`${this.priceListItemEndpoint}/${id}`, 
      this.authService.getHttpAuthOptions());
  }

  getByPriceListAndTicketId(priceListId: number, ticketId:number, pageParams: PageParams): Observable<Page<PriceListItem>> {
    return this.http.get(`${this.priceListItemsEndpoint}?price_list_id=${priceListId}&ticket_id=${ticketId}&${this.util.getPageableParamString(pageParams)}`,
    this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }  
}
