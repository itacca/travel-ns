import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PriceListViewComponent } from './pricelist-view/pricelist-view.component';
import { PriceListAddComponent } from './price-list-add/price-list-add.component';
import { AutoLoginGuard } from '../guard/auto-login.guard';
import { LoginGuard } from '../guard/login.guard';
import { OnlyGuestGuard } from '../guard/only-guest.guard';
import { AdminRoleGuard } from '../guard/admin-role.guard'
import { PriceListHistoryComponent } from './price-list-history/price-list-history.component';

const routes: Routes = [
  { path: 'pricelist', component: PriceListViewComponent, canActivate: [LoginGuard] },
  { path: 'add-price-list', component: PriceListAddComponent, canActivate: [AutoLoginGuard, AdminRoleGuard] },
  { path: 'price-list-history', component: PriceListHistoryComponent, canActivate: [AutoLoginGuard, AdminRoleGuard]}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PriceListRoutingModule { }
