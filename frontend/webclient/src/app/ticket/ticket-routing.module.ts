import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchasedTicketsViewComponent } from './purchased-tickets-view/purchased-tickets-view.component';
import { LoginGuard } from '../guard/login.guard';
import { TicketValidationComponent } from './ticket-validation/ticket-validation.component';
import { AdminRoleGuard } from '../guard/admin-role.guard';
import { TicketTypesViewComponent } from './ticket-types-view/ticket-types-view.component';
import { EditTicketComponent } from './edit-ticket/edit-ticket.component';
import { CreateTicketTypeComponent } from './create-ticket-type/create-ticket-type.component';

const routes: Routes = [
  { path: 'purchased-tickets', component: PurchasedTicketsViewComponent, canActivate: [LoginGuard] },
  { path: 'validate-ticket/:id', component: TicketValidationComponent, canActivate: [AdminRoleGuard] },
  { path: 'tickets', component: TicketTypesViewComponent, canActivate: [AdminRoleGuard] },
  { path: 'edit-ticket/:ticketId', component: EditTicketComponent, canActivate: [AdminRoleGuard]},
  { path: 'create-ticket', component: CreateTicketTypeComponent, canActivate: [AdminRoleGuard]}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TicketRoutingModule { }
