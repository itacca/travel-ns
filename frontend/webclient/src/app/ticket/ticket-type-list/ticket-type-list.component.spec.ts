import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketTypeListComponent } from './ticket-type-list.component';
import { Ticket } from '../ticket.model';
import { CityLine } from '../../city-lines/city-line.model';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { By } from '@angular/platform-browser';

describe('TicketTypeListComponent', () => {
  let component: TicketTypeListComponent;
  let fixture: ComponentFixture<TicketTypeListComponent>;
  let cityLine = new CityLine(5, "Linija 5", "BUS");
  let tickets: Ticket[] = [
    new Ticket(1, "DAILY", "STUDENT", cityLine),
    new Ticket(2, "ANNUAL", "STUDENT", cityLine),
    new Ticket(3, "SINGLE_RIDE", "REGULAR", cityLine),
    new Ticket(4, "DAILY", "REGULAR", cityLine),
    new Ticket(5, "MONTHLY", "REGULAR", cityLine)
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a ticket card for each ticket in list', () => {
    component.tickets = tickets;
    fixture.detectChanges();
    let ticketCards = fixture.debugElement.queryAll(By.css('app-ticket-card'));
    expect(ticketCards.length).toEqual(tickets.length);
    for (let i = 0; i < tickets.length; i++) {
      expect(ticketCards[i].componentInstance.ticket).toEqual(tickets[i]);
    }
  });

  it('removeTicketFromList() should remove the given ticket from the list and the removed ticket should not be rendered.', () => {
    component.tickets = tickets;
    fixture.detectChanges();
    let ticketsCount = tickets.length;
    let indexToRemove = 1;
    let ticketCards = fixture.debugElement.queryAll(By.css('app-ticket-card'));
    let expectedRemovedTicket = ticketCards[indexToRemove];
    component.removeTicketFromList(indexToRemove);
    fixture.detectChanges();
    ticketCards = fixture.debugElement.queryAll(By.css('app-ticket-card'));
    expect(ticketCards[indexToRemove]).not.toBe(expectedRemovedTicket);
    expect(ticketCards.length).toEqual(ticketsCount - 1);
  });
});
