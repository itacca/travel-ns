import { Component, OnInit, Input } from '@angular/core';
import { Ticket } from '../ticket.model';

@Component({
  selector: 'app-ticket-type-list',
  templateUrl: './ticket-type-list.component.html',
  styleUrls: ['./ticket-type-list.component.css']
})
export class TicketTypeListComponent implements OnInit {

  @Input()
  tickets: Ticket[];

  constructor() { }

  ngOnInit() {
  }

  removeTicketFromList(index: number) {
    this.tickets.splice(index, 1);
  }
}
