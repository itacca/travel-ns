import { TestBed, inject } from '@angular/core/testing';

import { TicketService } from './ticket.service';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { Http, RequestOptions } from '@angular/http';
import { AuthService } from 'src/app/core/auth/auth.service';
import { of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Ticket } from '../ticket.model';
import { Page, PageMock, PageParams } from '../../shared/page.model';
import { CityLine } from '../../city-lines/city-line.model';
import { UtilService } from '../../core/util.service';

describe('TicketService', () => {
  let testRequestOptions = new RequestOptions();
  let pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'asc'
  };
  let cityLine = new CityLine(5, "Linija 5", "BUS");
  let ticketPage: Page<Ticket> = new PageMock([
    new Ticket(1, "DAILY", "STUDENT", cityLine, null),
    new Ticket(2, "MONTHLY", "REGULAR", cityLine, null),
    new Ticket(3, "DAILY", "STUDENT", cityLine, null),
    new Ticket(4, "ANNUAL", "REGULAR", cityLine, null),
    new Ticket(5, "DAILY", "STUDENT", cityLine, null)
]);


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    });
  });

  it('should be created', inject([TicketService], (service: TicketService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', inject([TicketService], (service: TicketService) => {
    expect(service.ticketEndpoint).toEqual(`${environment.apiRoot}/ticket`);
    expect(service.ticketsEndpoint).toEqual(`${environment.apiRoot}/tickets`);
  }));

  it('findById() should throw error if http returns error', inject([TicketService, Http, AuthService], 
    (ticketService: TicketService, http: Http, authService: AuthService) => {
    spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    spyOn(http, 'get').and.callFake(any => throwError(new Error('test-error')));
    let ticketId = 0;
    ticketService.findById(ticketId).subscribe(result => {
      expect(result).toBeUndefined('findById() should have thrown error');
    }, error => {
      expect(error.message).toEqual('test-error');
    });
    expect(http.get).toHaveBeenCalledWith(`${ticketService.ticketEndpoint}/${ticketId}`, testRequestOptions);
  }));

  it('findById() should return ticket http returns returns ticket', inject([TicketService, Http, AuthService], 
    (ticketService: TicketService, http: Http, authService: AuthService) => {
    spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    let expectedTicket = new Ticket();
    spyOn(http, 'get').and.callFake(any => of({ json: () => expectedTicket }));
    let ticketId = 0;
    ticketService.findById(ticketId).subscribe(result => {
      expect(result).toBe(expectedTicket);
    }, error => {
      expect(error).toBeUndefined('findById() should have returned ticket');
    });
    expect(http.get).toHaveBeenCalledWith(`${ticketService.ticketEndpoint}/${ticketId}`, testRequestOptions);
  }));
  /*
  it('findAll() should map the json response to object', inject([TicketService, Http, UtilService, AuthService],
    (ticketService: TicketService, http: Http, authService: AuthService, utilService: UtilService) => {
      spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
      let expectedResult = { content: [new Ticket()] };
      spyOn(http, 'get').and.callFake(any => of({ json: () => expectedResult }));
      ticketService.findAll(pageParams).subscribe(result => {
        expect(result.content).toBe(expectedResult.content);
      }, error => {
        expect(error).toBe(undefined, 'findAll() should have mapped the json response to object');
      });
      expect(http.get).toHaveBeenCalledWith(`${ticketService.ticketsEndpoint}${utilService.getPageableParamString(pageParams)}`,
       testRequestOptions);
  }));
  */

  it('findAll() should throw error if http returns error', inject([TicketService, Http, UtilService, AuthService],
    (ticketService: TicketService, http: Http, utilService: UtilService, authService: AuthService) => {
    spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
    spyOn(http, 'get').and.callFake(any => throwError(new Error('test-error')));
    ticketService.findAll(pageParams).subscribe(result => {
      expect(result).toBe(undefined, 'findAll() should have thrown error when http returns error');
    }, result => {
      expect(result.message).toEqual('test-error');
    });
    expect(http.get).toHaveBeenCalledWith(`${ticketService.ticketsEndpoint}${utilService.getPageableParamString(pageParams)}`,
     testRequestOptions);
  }));
});
