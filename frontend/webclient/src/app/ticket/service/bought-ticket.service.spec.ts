import { TestBed, inject } from '@angular/core/testing';

import { BoughtTicketService } from './bought-ticket.service';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { TicketModule } from '../ticket.module';
import { Http, RequestOptions } from '@angular/http';
import { throwError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UtilService } from 'src/app/core/util.service';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Ticket } from '../ticket.model';

describe('BoughtTicketService', () => {
  let testRequestOptions = new RequestOptions();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    });
  });

  beforeEach(() => {
    let utilService: UtilService = TestBed.get(UtilService);
    let authService: AuthService = TestBed.get(AuthService);
    spyOn(utilService, 'getPageableParamString').and.callFake(any => '');
    spyOn(authService, 'getHttpAuthOptions').and.callFake(any => testRequestOptions);
  });

  it('should be created', inject([BoughtTicketService], (service: BoughtTicketService) => {
    expect(service).toBeTruthy();
  }));

  it('should have correct endpoints', inject([BoughtTicketService], (boughtTicketService: BoughtTicketService) => {
    expect(boughtTicketService.boughtTicketEndpoint).toEqual(`${environment.apiRoot}/bought-ticket`);
    expect(boughtTicketService.boughtTicketsEndpoint).toEqual(`${environment.apiRoot}/bought-tickets`);
  }));

  it('getLoggedUsersBoughtTickets() should map the json response to object', inject([BoughtTicketService, Http], 
    (boughtTicketService: BoughtTicketService, http: Http) => {
    let expectedResult = { content: [new Ticket()] };
    spyOn(http, 'get').and.callFake(any => of({ json: () => expectedResult }));
    boughtTicketService.getLoggedUsersBoughtTickets({}).subscribe(result => {
      expect(result.content).toBe(expectedResult.content);
    }, error => {
      expect(error).toBe(undefined, 'getLoggedUsersBoughtTickets() should have mapped the json response to object');
    });
    expect(http.get).toHaveBeenCalledWith(boughtTicketService.boughtTicketsEndpoint, testRequestOptions);
  }));
  
  it('getLoggedUsersBoughtTickets() should throw error if http returns error', inject([BoughtTicketService, Http], 
    (boughtTicketService: BoughtTicketService, http: Http) => {
    spyOn(http, 'get').and.callFake(any => throwError(new Error('test-error')));
    boughtTicketService.getLoggedUsersBoughtTickets({}).subscribe(result => {
      expect(result).toBe(undefined, 'getLoggedUsersBoughtTickets() should have thrown error when http returns error');
    }, result => {
      expect(result.message).toEqual('test-error');
    });
    expect(http.get).toHaveBeenCalledWith(boughtTicketService.boughtTicketsEndpoint, testRequestOptions);
  }));

  it('validateTicket() should return true when ticket is valid', inject([BoughtTicketService, Http, AuthService], 
    (boughtTicketService: BoughtTicketService, http: Http, authService: AuthService) => {
    let expectedResult = { isActive: true };
    spyOn(http, 'get').and.callFake(any => of({ json: () => expectedResult }));
    let ticketId = 0;
    boughtTicketService.validateBoughtTicket(ticketId).subscribe(result => {
      expect(result).toEqual(true);
    }, error => {
      expect(error).toBeUndefined('validateTicket() should have returned true');
    });
    expect(http.get).toHaveBeenCalledWith(`${boughtTicketService.boughtTicketEndpoint}/validate/${ticketId}`, testRequestOptions);
  }));

  it('validateTicket() should throw error if http returns error', inject([BoughtTicketService, Http, AuthService], 
    (boughtTicketService: BoughtTicketService, http: Http, authService: AuthService) => {
    spyOn(http, 'get').and.callFake(any => throwError(new Error('test-error')));
    let ticketId = 0;
    boughtTicketService.validateBoughtTicket(ticketId).subscribe(result => {
      expect(result).toBeUndefined('validateTicket() should have thrown error');
    }, error => {
      expect(error.message).toEqual('test-error');
    });
    expect(http.get).toHaveBeenCalledWith(`${boughtTicketService.boughtTicketEndpoint}/validate/${ticketId}`, testRequestOptions);
  }));
});
