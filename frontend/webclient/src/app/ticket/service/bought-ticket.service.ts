import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PageParams, Page } from 'src/app/shared/page.model';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { AuthService } from 'src/app/core/auth/auth.service';
import { UtilService } from 'src/app/core/util.service';
import { map } from 'rxjs/operators';
import { BoughtTicket } from '../bought-ticket.model';

@Injectable({
  providedIn: 'root'
})
export class BoughtTicketService {

  boughtTicketEndpoint: string = `${environment.apiRoot}/bought-ticket`;

  boughtTicketsEndpoint: string = `${environment.apiRoot}/bought-tickets`;

  constructor(
    private http: Http,
    private authService: AuthService,
    private util: UtilService
  ) { }

  getLoggedUsersBoughtTickets(pageParams: PageParams): Observable<Page<BoughtTicket>> {
    return this.http.get(`${this.boughtTicketsEndpoint}${this.util.getPageableParamString(pageParams)}`, 
      this.authService.getHttpAuthOptions()).pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  validateBoughtTicket(id: number): Observable<boolean> {
    return this.http.get(`${this.boughtTicketEndpoint}/validate/${id}`, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json().isActive,
        error => Observable.throw(error)
      ));
  }

  findById(id: number): Observable<BoughtTicket> {
    return this.http.get(`${this.boughtTicketEndpoint}/${id}`, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }
}
