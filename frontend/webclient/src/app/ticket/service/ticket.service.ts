import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthService } from '../../core/auth/auth.service';
import { Observable } from 'rxjs';
import { Ticket } from '../ticket.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { UtilService } from '../../core/util.service';
import { PageParams, Page } from 'src/app/shared/page.model';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  ticketEndpoint: string = `${environment.apiRoot}/ticket`;

  ticketsEndpoint: string = `${environment.apiRoot}/tickets`;

  constructor(
    private http: Http,
    private authService: AuthService,
    private util: UtilService
  ) { }

  findById(id: number): Observable<Ticket> {
    return this.http.get(`${this.ticketEndpoint}/${id}`, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  findAll(pageParams: PageParams): Observable<Page<Ticket>> {
    return this.http.get(`${this.ticketsEndpoint}${this.util.getPageableParamString(pageParams)}`,
      this.authService.getHttpAuthOptions()).pipe(map(
      response => response.json(),
      error => Observable.throw(error)
    ));
  }

  update(ticket: Ticket): Observable<any> {
    return this.http.put(this.ticketEndpoint, ticket, this.authService.getHttpAuthOptions());
  }

  create(ticket: Ticket): Observable<any> {
    return this.http.post(this.ticketsEndpoint, ticket, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  findTicketsThatDoNotHavePriceListItemInGivenPriceList(id: number, pageParams: PageParams): Observable<Page<Ticket>> {
    return this.http.get(`${this.ticketsEndpoint}/not-in-price-list?price_list_id=${id}`, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }

  delete(ticket: Ticket): Observable<any> {
    return this.http.delete(`${this.ticketEndpoint}/${ticket.id}`, this.authService.getHttpAuthOptions());
  }

  getTicketsWithDefinedPriceForGivenPriceList(id: number, pageParams: PageParams): Observable<Page<Ticket>> {
    return this.http.get(`${this.ticketsEndpoint}/in-price-list?price_list_id=${id}`, this.authService.getHttpAuthOptions())
      .pipe(map(
        response => response.json(),
        error => Observable.throw(error)
      ));
  }
}
