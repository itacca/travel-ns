import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PurchasedTicketsViewComponent } from './purchased-tickets-view/purchased-tickets-view.component';
import { TicketRoutingModule } from './ticket-routing.module';
import { MaterialModule } from '../material/material.module';
import { TicketCardComponent } from './ticket-card/ticket-card.component';
import { QRCodeModule } from 'angularx-qrcode';
import { TicketValidationComponent } from './ticket-validation/ticket-validation.component';
import { TicketTypeListComponent } from './ticket-type-list/ticket-type-list.component';
import { TicketTypesViewComponent } from './ticket-types-view/ticket-types-view.component';
import { EditTicketComponent } from './edit-ticket/edit-ticket.component';
import { PurchasedTicketCardComponent } from './purchased-ticket-card/purchased-ticket-card.component';
import { CreateTicketTypeComponent } from './create-ticket-type/create-ticket-type.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    TicketRoutingModule,
    QRCodeModule
  ],
  declarations: [
    PurchasedTicketsViewComponent,
    PurchasedTicketCardComponent,
    TicketCardComponent,
    TicketValidationComponent,
    TicketTypeListComponent,
    TicketTypesViewComponent,
    EditTicketComponent,
    CreateTicketTypeComponent
  ],
  exports: [
    TicketRoutingModule
  ]
})
export class TicketModule { }
