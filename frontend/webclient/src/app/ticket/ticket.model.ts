import { PriceListItem } from "../price-list-item/price-list-item.model";
import { CityLine } from "../city-lines/city-line.model";

export class Ticket {

    constructor(
        public id?: number,
        public ticketDuration?: "DAILY" | "MONTHLY" | "ANNUAL" | "SINGLE_RIDE",
        public ticketType?: "STUDENT" | "REGULAR" | "SENIOR",
        public cityLine?: CityLine,
        public priceListItems?: PriceListItem[],
    ) { }
}
