import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasedTicketsViewComponent } from './purchased-tickets-view.component';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Ticket } from '../ticket.model';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Page } from 'src/app/shared/page.model';
import { BoughtTicketService } from '../service/bought-ticket.service';
import { BoughtTicket } from '../bought-ticket.model';
import { Account } from 'src/app/account/account.model';
import { expectElementToContain } from 'src/app/test-shared/common-expects.spec';

describe('PurchasedTicketsViewComponent', () => {
  let component: PurchasedTicketsViewComponent;
  let fixture: ComponentFixture<PurchasedTicketsViewComponent>;
  let boughtTicketService;
  let bouhgTicketServiceSpy;
  let tickets: Page<BoughtTicket> = { 
    content: [
      new BoughtTicket(0, new Date(), new Account(), new Ticket(0, "DAILY", "STUDENT")),
      new BoughtTicket(1, new Date(), new Account(), new Ticket(1, "DAILY", "STUDENT")),
      new BoughtTicket(2, new Date(), new Account(), new Ticket(2, "DAILY", "STUDENT")),
    ],
    pageable: { pageNumber: 0, pageSize: 8 },
    number: 0
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    boughtTicketService = TestBed.get(BoughtTicketService);
    bouhgTicketServiceSpy = spyOn(boughtTicketService, 'getLoggedUsersBoughtTickets').and.callFake(() => of(tickets));
    fixture = TestBed.createComponent(PurchasedTicketsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call service method to retrieve bought tickets', () => {
    expect(boughtTicketService.getLoggedUsersBoughtTickets).toHaveBeenCalled();
  });

  it('should set tickets list to bought tickets returned by the service', () => {
    expect(component.tickets).toBe(tickets);
  });

  it('should render a card for each ticket', () => {
    let views = fixture.debugElement.queryAll(By.css('app-purchased-ticket-card'));
    expect(views.length).toEqual(tickets.content.length);
    for (let i = 0; i < views.length; i++) {
      expect(views[i].componentInstance.ticket).toBe(tickets.content[i].ticket);
    }
  });

  it('should display "Purchased tickets" in h2 tag', () => {
    expectElementToContain(fixture, By.css('h2.mat-h2'), 'Purchased tickets');
  });
});
