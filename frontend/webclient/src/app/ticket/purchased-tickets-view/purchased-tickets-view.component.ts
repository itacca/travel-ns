import { Component, OnInit } from '@angular/core';
import { TicketService } from '../service/ticket.service';
import { Ticket } from '../ticket.model';
import { ToastrService } from 'ngx-toastr';
import { Page, PageParams } from 'src/app/shared/page.model';
import { BoughtTicketService } from '../service/bought-ticket.service';
import { BoughtTicket } from '../bought-ticket.model';

@Component({
  selector: 'app-purchased-tickets-view',
  templateUrl: './purchased-tickets-view.component.html',
  styleUrls: ['./purchased-tickets-view.component.css']
})
export class PurchasedTicketsViewComponent implements OnInit {

  tickets: Page<BoughtTicket>;

  pageParams: PageParams = {
    size: 8,
    page: 0,
    sort: 'asc'
  };

  sortFields = [
    { name: 'id', displayName: 'ID' },
    { name: 'boughtDate', displayName: 'Date of purchase' },
    { name: 'ticket.ticketType', displayName: 'Type' },
    { name: 'ticket.ticketDuration', displayName: 'Duration' },
    { name: 'soldPrice', displayName: 'Price' }
  ];

  constructor(
    private boughtTicketService: BoughtTicketService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadBoughtTickets(this.pageParams);
  }

  private loadBoughtTickets(params: PageParams) {
    this.boughtTicketService.getLoggedUsersBoughtTickets(params).subscribe(tickets => {
      this.tickets = tickets;
    }, error => this.toastr.error(error.json().message, 'Error'));
  }

  paramsChanged(pageParams: PageParams) {
    this.loadBoughtTickets(pageParams);
  }

}
