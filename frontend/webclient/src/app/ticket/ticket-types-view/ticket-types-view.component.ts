import { Component, OnInit } from '@angular/core';
import { Page, PageParams } from '../../shared/page.model';
import { Ticket } from '../ticket.model';
import { TicketService } from '../service/ticket.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ticket-types-view',
  templateUrl: './ticket-types-view.component.html',
  styleUrls: ['./ticket-types-view.component.css']
})
export class TicketTypesViewComponent implements OnInit {

  ticketsPage: Page<Ticket>;

  pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'asc'
  };

  sortFields = [
    { name: 'id', displayName: 'ID' },
    { name: 'ticketDuration', displayName: 'Duration' },
    { name: 'ticketType', displayName: 'Type' },
    { name: 'cityLine.name', displayName: 'Line Name' }
  ];

  constructor(
    private ticketService: TicketService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadTickets(this.pageParams);
  }

  private loadTickets(pageParams: PageParams) {
    this.ticketService.findAll(pageParams).subscribe(response => {
      this.ticketsPage = response;
      console.log('Tickets page: ' );
      console.log(this.ticketsPage);
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

  paramsChanged(pageParams: PageParams) {
    this.loadTickets(pageParams);
  }
}
