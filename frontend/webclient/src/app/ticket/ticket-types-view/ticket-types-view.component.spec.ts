import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketTypesViewComponent } from './ticket-types-view.component';
import { TicketService } from '../service/ticket.service';
import { Page } from '../../shared/page.model';
import { Ticket } from '../ticket.model';
import { CityLine } from '../../city-lines/city-line.model';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('TicketTypesViewComponent', () => {
  let component: TicketTypesViewComponent;
  let fixture: ComponentFixture<TicketTypesViewComponent>;

  let ticketService: TicketService;
  let toastrService;
  let cityLine = new CityLine(5, "Linija 5", "BUS");
  let ticketsPage: Page<Ticket> = {
    content: [
      new Ticket(1, "DAILY", "STUDENT", cityLine),
      new Ticket(2, "MONTHLY", "SENIOR", cityLine),
      new Ticket(3, "ANNUAL", "REGULAR", cityLine),
      new Ticket(4, "SINGLE_RIDE", "STUDENT", cityLine),
      new Ticket(5, "DAILY", "STUDENT", cityLine),
    ],
    totalPages: 1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    ticketService = TestBed.get(TicketService);
    toastrService = TestBed.get(ToastrService);
  });

  function createComponent() {
    fixture = TestBed.createComponent(TicketTypesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    spyOn(ticketService, 'findAll').and.callFake(any => of(ticketsPage));
    createComponent();
    expect(component).toBeTruthy();
  });

  it('should render "Tickets" in h2 tag', () => {
    let h2Title = fixture.debugElement.query(By.css('h2'));
    expect(h2Title.nativeElement.innerHTML).toContain('Tickets');
  });

  it('shoud render a tickets list component with tickets injected if ticketsPage is loaded', () => {
    spyOn(ticketService, 'findAll').and.callFake(any => of(ticketsPage));
    createComponent();
    let ticketsList = fixture.debugElement.query(By.css('app-ticket-type-list'));
    expect(ticketsList).toBeTruthy();
    expect(ticketsList.componentInstance.tickets).toBe(ticketsPage.content);
  });

  it('shoud not render a tickets list component if ticketsPage is not loaded', () => {
    spyOn(ticketService, 'findAll').and.callFake(any => of(ticketsPage));
    createComponent();
    component.ticketsPage = null;
    fixture.detectChanges();
    let ticketsList = fixture.debugElement.query(By.css('app-ticket-list'));
    expect(ticketsList).toBeFalsy();
  });

  it('should toast error if ticketService findAll() throws error', () => {
    spyOn(ticketService, 'findAll').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastrService, 'error').and.callFake(any => null);
    createComponent();
    expect(toastrService.error).toHaveBeenCalledWith('test-error', 'Error');
  });
});
