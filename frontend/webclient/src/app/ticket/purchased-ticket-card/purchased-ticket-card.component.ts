import { Component, OnInit, Input } from '@angular/core';
import { Ticket } from '../ticket.model';
import { environment } from 'src/environments/environment';
import { BoughtTicket } from '../bought-ticket.model';

@Component({
  selector: 'app-purchased-ticket-card',
  templateUrl: './purchased-ticket-card.component.html',
  styleUrls: ['./purchased-ticket-card.component.css']
})
export class PurchasedTicketCardComponent implements OnInit {

  @Input()
  ticket: Ticket;

  @Input()
  boughtTicket: BoughtTicket;

  constructor() { }

  ngOnInit() {
  }

  getTicketVerificationUrl() {
    return `${environment.ticketValidationUrl}/${this.ticket.id}`;
  }

  getPrice() {
    if (!this.ticket.priceListItems) {
      return 'No price listed';
    }
    return this.ticket.priceListItems[this.ticket.priceListItems.length - 1].price;
  }

}
