import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasedTicketCardComponent } from './purchased-ticket-card.component';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Ticket } from '../ticket.model';
import { By } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { BoughtTicket } from '../bought-ticket.model';
import { Account } from 'src/app/account/account.model';

describe('PurchasedTicketCardComponent', () => {
  let component: PurchasedTicketCardComponent;
  let fixture: ComponentFixture<PurchasedTicketCardComponent>;
  let ticket = new Ticket(0, "DAILY", "REGULAR");
  let boughTicket = new BoughtTicket(0, new Date(), new Account(), ticket, 12.99);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasedTicketCardComponent);
    component = fixture.componentInstance;
    component.ticket = ticket;
    component.boughtTicket = boughTicket;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display qr code for ticket validation', () => {
    let qrElement = fixture.debugElement.nativeElement.querySelector('qrcode');
    expect(qrElement).toBeTruthy();
  });

  it('should display ticket duration', () => {
    let durationEl = fixture.debugElement.query(By.css('.ticket-duration'));
    expect(durationEl.nativeElement.innerHTML).toContain(ticket.ticketDuration);
  });

  it('should display ticket type', () => {
    let typeEl = fixture.debugElement.query(By.css('.ticket-type'));
    expect(typeEl.nativeElement.innerHTML).toContain(ticket.ticketType);
  });
  
  it('should display "No price listed" when ticket price is not defined', () => {
    let priceEl = fixture.debugElement.query(By.css('.ticket-price'));
    component.boughtTicket.soldPrice = null;
    fixture.detectChanges();
    expect(priceEl.nativeElement.innerHTML).toContain("No price listed");
  });

  it('should return verification url when getTicketVerificationUrl is called', () => {
    let expectedUri = `${environment.ticketValidationUrl}/${ticket.id}`;
    expect(component.getTicketVerificationUrl()).toEqual(expectedUri);
  });
});
