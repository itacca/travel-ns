import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTicketComponent } from './edit-ticket.component';
import { UtilService } from '../../core/util.service';
import { Router } from '@angular/router';
import { TicketService } from '../service/ticket.service';
import { CityLineService } from '../../city-lines/service/city-line.service';
import { ToastrService } from 'ngx-toastr';
import { Page, PageMock } from '../../shared/page.model';
import { CityLine } from '../../city-lines/city-line.model';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import { Ticket } from '../ticket.model';

describe('EditTicketComponent', () => {
  let component: EditTicketComponent;
  let fixture: ComponentFixture<EditTicketComponent>;

  let util: UtilService;
  let router: Router;
  let ticketService: TicketService;
  let cityLineService: CityLineService;
  let toastr: ToastrService;
  let cityLine = new CityLine(5, "Linija 5", "BUS");
  let ticket: Ticket = new Ticket(1, "DAILY", "STUDENT", cityLine);


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    router = TestBed.get(Router);
    ticketService = TestBed.get(TicketService);
    cityLineService = TestBed.get(CityLineService);
    toastr = TestBed.get(ToastrService);
    spyOn(ticketService, 'findById').and.callFake(any => of(ticket));

    fixture = TestBed.createComponent(EditTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Edit ticket" in h2 tag', () => {
    let h2 = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(h2.innerHTML).toContain('Edit ticket');
  });

  it('should render input fields for ticket information', () => {
    let ticketType = fixture.debugElement.query(By.css('mat-select[formControlName="ticketTypeCtrl"]'));
    let ticketDuration = fixture.debugElement.query(By.css('mat-select[formControlName="ticketDurationCtrl"]'));

    expect(ticketType).toBeTruthy();
    expect(ticketDuration).toBeTruthy();
  });

  it('saveTicket() should call ticketService.update()', () => {
    let expectedTicket = new Ticket();
    spyOnProperty(component.ticketGroup, 'valid', 'get').and.callFake(any => true);
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedTicket);
    spyOn(ticketService, 'update').and.callFake(any => of({ id: 1 }));
    component.saveTicket();
    expect(ticketService.update).toHaveBeenCalledWith(expectedTicket);
  });

  it('saveTickets() should toast error if ticketService.update() throws error', () => {
    let expectedTicket = new Ticket();
    spyOnProperty(component.ticketGroup, 'valid', 'get').and.callFake(any => true);
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedTicket);
    spyOn(ticketService, 'update').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastr, 'error');
    component.saveTicket();
    expect(ticketService.update).toHaveBeenCalledWith(expectedTicket);
    expect(toastr.error).toHaveBeenCalledWith('test-error', 'Error');
  });

});
