import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ticket } from '../ticket.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UtilService } from '../../core/util.service';
import { ToastrService } from 'ngx-toastr';
import { TicketService } from '../service/ticket.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-ticket',
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.css']
})
export class EditTicketComponent implements OnInit {

  routerSubscription: Subscription;

  ticket: Ticket;

  ticketGroup: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private util: UtilService,
    private toastr: ToastrService,
    private ticketService: TicketService,
  ) { }

  ngOnInit() {
    this.subscribeToRouterParams();
  }

  private initTicketGroup() {
    this.ticketGroup = this.formBuilder.group({
      idCtrl: [this.ticket.id, Validators.required],
      ticketDurationCtrl: [this.ticket.ticketDuration, Validators.required],
      ticketTypeCtrl: [this.ticket.ticketType, Validators.required],
      cityLineCtrl: [this.ticket.cityLine, Validators.required],
      priceListItemsCtrl: [this.ticket.priceListItems]
    });
  }

  private subscribeToRouterParams() {
    this.routerSubscription = this.activatedRoute.params.subscribe(params => {
      this.ticketService.findById(params.ticketId).subscribe(result => {
        this.ticket = result;
        this.ticket.priceListItems = [];
        this.initTicketGroup();
      }, error => {
        this.toastr.error(error.json().message, 'Error');
      });
    });
  }

  saveTicket() {
    if (!this.ticketGroup.valid) {
      return;
    }
    const ticket: Ticket = this.util.formGroupToModel(new Ticket(), [this.ticketGroup], 'Ctrl');
    this.ticketService.update(ticket).subscribe(result => {
        this.toastr.success("Ticket has been updated.", "Success");
      }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }


}
