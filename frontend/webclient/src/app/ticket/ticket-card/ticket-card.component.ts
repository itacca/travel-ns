import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Ticket } from '../ticket.model';
import { AccountService } from '../../account/account.service';
import { TicketService } from '../service/ticket.service';
import { ToastrService } from 'ngx-toastr';
import { Roles } from '../../account/role.model';

@Component({
  selector: 'app-ticket-card',
  templateUrl: './ticket-card.component.html',
  styleUrls: ['./ticket-card.component.css']
})
export class TicketCardComponent implements OnInit {

  _ticket: Ticket;

  isAdmin: boolean;

  @Output()
  deleted: EventEmitter<any> = new EventEmitter();

  constructor(
    private accountService: AccountService,
    private ticketService: TicketService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    console.log(this.ticket);
    this.checkRoles();
  }

  private checkRoles() {
    this.accountService.hasAnyRole([Roles.administratorRole, Roles.systemAdministratorRole])
      .subscribe(hasAnyAdminRole => {
        this.isAdmin = hasAnyAdminRole;
      });
  }

  get ticket() {
    return this._ticket;
  }

  @Input()
  set ticket(value: Ticket) {
    this._ticket = value;
  }

  deleteTicket() {
    this.ticketService.delete(this.ticket).subscribe(
      success => {
        this.toastrService.success("Ticket deleted", "Success");
        this.deleted.emit();
      },
      error => this.toastrService.error(error.json().message, "Error")
    );
  }
}
