import { async, ComponentFixture, TestBed, inject, tick } from '@angular/core/testing';

import { TicketCardComponent } from './ticket-card.component';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { Ticket } from '../ticket.model';
import { By } from '@angular/platform-browser';
import { AccountService } from '../../account/account.service';
import { TicketService } from '../service/ticket.service';
import { of, throwError } from 'rxjs';

describe('TicketCardComponent', () => {
  let component: TicketCardComponent;
  let fixture: ComponentFixture<TicketCardComponent>;
  let ticket = new Ticket(0, "DAILY", "REGULAR");

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketCardComponent);
    component = fixture.componentInstance;
    component.ticket = ticket;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display ticket duration', () => {
    let durationEl = fixture.debugElement.query(By.css('.ticket-duration'));
    expect(durationEl.nativeElement.innerHTML).toContain(ticket.ticketDuration);
  });

  it('should display ticket type', () => {
    let typeEl = fixture.debugElement.query(By.css('.ticket-type'));
    expect(typeEl.nativeElement.innerHTML).toContain(ticket.ticketType);
  });

  function createComponent() {
    fixture = TestBed.createComponent(TicketCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }


  it('deleteTicket() should forward the call to the service and on success emit a deleted event', inject([AccountService,
    TicketService],
    (accountService: AccountService, ticketService: TicketService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(true));
    spyOn(ticketService, 'delete').and.callFake(any => of(null));

    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    spyOn(component, 'deleteTicket').and.callThrough();
    component.ticket = ticket;
    fixture.detectChanges();

    let deleteBtn = fixture.debugElement.query(By.css('.delete-ticket-btn'));

    deleteBtn.nativeElement.click();

    expect(component.deleteTicket).toHaveBeenCalled();
    expect(ticketService.delete).toHaveBeenCalledWith(ticket);
    expect(component.deleted.emit).toHaveBeenCalled();
  }));

  it('deleteTicket() should not emit a deleted event if when deletion fails', inject([AccountService, TicketService],
    (accountService: AccountService, ticketService: TicketService) => {
    spyOn(accountService, 'hasAnyRole').and.callFake(any => of(true));
    spyOn(ticketService, 'delete').and.callFake(any => throwError(new Error('failed deletion test')));

    createComponent();
    spyOn(component.deleted, 'emit').and.callFake(() => null);
    component.ticket = ticket;
    fixture.detectChanges();

    expect(component.deleted.emit).toHaveBeenCalledTimes(0);
  }));
});
