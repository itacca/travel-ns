import { Ticket } from "./ticket.model";
import { Account } from "src/app/account/account.model";

export class BoughtTicket {
  
  constructor(
    public id?: number,
    public boughtDate?: Date,
    public account?: Account,
    public ticket?: Ticket,
    public soldPrice?: number
  ) { }
}
