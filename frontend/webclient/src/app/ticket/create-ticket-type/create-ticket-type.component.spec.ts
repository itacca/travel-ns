import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTicketTypeComponent } from './create-ticket-type.component';
import { UtilService } from '../../core/util.service';
import { Router } from '@angular/router';
import { TicketService } from '../service/ticket.service';
import { ToastrService } from 'ngx-toastr';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from '../../test-shared/test-shared.module';
import { By } from '@angular/platform-browser';
import { Ticket } from '../ticket.model';
import { of, throwError } from 'rxjs';
import { Page, PageMock } from 'src/app/shared/page.model';
import { CityLine } from '../../city-lines/city-line.model';
import { CityLineService } from '../../city-lines/service/city-line.service';

describe('CreateTicketTypeComponent', () => {
  let component: CreateTicketTypeComponent;
  let fixture: ComponentFixture<CreateTicketTypeComponent>;

  let util: UtilService;
  let router: Router;
  let ticketService: TicketService;
  let cityLineService: CityLineService;
  let toastr: ToastrService;
  let cityLinesPage: Page<CityLine> = new PageMock([
    new CityLine(1, "Linija 1", "BUS"),
    new CityLine(2, "Linija 2", "TRAM"),
    new CityLine(3, "Linija 3", "BUS"),
    new CityLine(4, "Linija 4", "TRAM"),
    new CityLine(5, "Linija 5", "BUS")
]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    util = TestBed.get(UtilService);
    router = TestBed.get(Router);
    ticketService = TestBed.get(TicketService);
    cityLineService = TestBed.get(CityLineService);
    toastr = TestBed.get(ToastrService);
    spyOn(cityLineService, 'findAll').and.callFake(any => of(cityLinesPage));

    fixture = TestBed.createComponent(CreateTicketTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "Create new ticket type" in h2 tag', () => {
    let h2 = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(h2.innerHTML).toContain('Create new ticket type');
  });

  it('should render input fields for ticket information', () => {
    let ticketType = fixture.debugElement.query(By.css('mat-select[formControlName="ticketTypeCtrl"]'));
    let ticketDuration = fixture.debugElement.query(By.css('mat-select[formControlName="ticketDurationCtrl"]'));
    let cityLine = fixture.debugElement.query(By.css('mat-select[formControlName="cityLineCtrl"]'));

    expect(ticketType).toBeTruthy();
    expect(ticketDuration).toBeTruthy();
    expect(cityLine).toBeTruthy();
  });

  it('saveTicket() should call ticketService.create()', () => {
    let expectedTicket = new Ticket();
    spyOnProperty(component.ticketGroup, 'valid', 'get').and.callFake(any => true);
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedTicket);
    spyOn(ticketService, 'create').and.callFake(any => of({ id: 1 }));
    spyOn(router, 'navigate').and.callFake(any => null);
    component.saveTicket();
    expect(ticketService.create).toHaveBeenCalledWith(expectedTicket);
    expect(router.navigate).toHaveBeenCalledWith(['/tickets']);
  });

  it('saveTickets() should toast error if ticketService.create() throws error', () => {
    let expectedTicket = new Ticket();
    spyOnProperty(component.ticketGroup, 'valid', 'get').and.callFake(any => true);
    spyOn(util, 'formGroupToModel').and.callFake(any => expectedTicket);
    spyOn(ticketService, 'create').and.callFake(any => throwError({ json: () => new Error('test-error') }));
    spyOn(toastr, 'error');
    component.saveTicket();
    expect(ticketService.create).toHaveBeenCalledWith(expectedTicket);
    expect(toastr.error).toHaveBeenCalledWith('test-error', 'Error');
  });
});
