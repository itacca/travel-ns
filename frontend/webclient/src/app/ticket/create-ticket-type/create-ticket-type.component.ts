import { Component, OnInit } from '@angular/core';
import { Page, PageParams } from '../../shared/page.model';
import { Ticket } from '../ticket.model';
import { TicketService } from '../service/ticket.service';
import { CityLineService } from '../../city-lines/service/city-line.service';
import { ToastrService } from 'ngx-toastr';
import { CityLine } from '../../city-lines/city-line.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilService } from '../../core/util.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-ticket-type',
  templateUrl: './create-ticket-type.component.html',
  styleUrls: ['./create-ticket-type.component.css']
})
export class CreateTicketTypeComponent implements OnInit {

  cityLinesPage: Page<CityLine>;

  cityLines: CityLine[] = [];

  ticket: Ticket;

  ticketGroup: FormGroup;

  lineChosen: boolean = false;

  pageParams: PageParams = {
    page: 0,
    size: 8,
    sort: 'asc'
  };

  sortFields = [
    { name: 'id', displayName: 'ID' },
    { name: 'name', displayName: 'Line Name' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private util: UtilService,
    private ticketService: TicketService,
    private cityLineService: CityLineService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loadCityLines(this.pageParams);
    this.initTicketGroup();
  }

  private loadCityLines(pageParams: PageParams) {
    this.cityLineService.findAll(pageParams).subscribe(response => {
      this.cityLinesPage = response;
      this.cityLines = response.content;
    }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }

  paramsChanged(pageParams: PageParams) {
    this.loadCityLines(pageParams);
  }

  private initTicketGroup() {
    this.ticketGroup = this.formBuilder.group({
      ticketDurationCtrl: [null, Validators.required],
      ticketTypeCtrl: [null, Validators.required],
      cityLineCtrl: [null, Validators.required],
      priceListItemsCtrl: [null]
    });
  }

  saveTicket() {
    if (!this.ticketGroup.valid) {
      return;
    }
    const ticket: Ticket = this.util.formGroupToModel(new Ticket(), [this.ticketGroup], 'Ctrl');
    this.ticketService.create(ticket).subscribe(result => {
        this.toastr.success("Ticket created.", "Success");
        this.router.navigate(['/tickets']);
      }, error => {
      this.toastr.error(error.json().message, 'Error');
    });
  }
}
