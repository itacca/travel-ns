import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';

import { TicketValidationComponent } from './ticket-validation.component';
import { ActivatedRoute } from '@angular/router';
import { TicketModule } from '../ticket.module';
import { TestSharedModule } from 'src/app/test-shared/test-shared.module';
import { Ticket } from '../ticket.model';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { BoughtTicket } from '../bought-ticket.model';
import { Account } from 'src/app/account/account.model';
import { BoughtTicketService } from '../service/bought-ticket.service';
import { expectElementToContain } from 'src/app/test-shared/common-expects.spec';

describe('TicketValidationComponent', () => {
  let component: TicketValidationComponent;
  let fixture: ComponentFixture<TicketValidationComponent>;
  let boughtTicketService: BoughtTicketService;
  let ticketId = 15;
  let ticket = new BoughtTicket(0, new Date(), new Account(), new Ticket(ticketId, "DAILY", "REGULAR"));
  

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TicketModule,
        TestSharedModule
      ],
      providers: [
        { 
          provide: ActivatedRoute, 
          useValue: { 
            params: of({ id: ticketId }) 
          } 
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    boughtTicketService = TestBed.get(BoughtTicketService);
  });

  function initComponent() {
    fixture = TestBed.createComponent(TicketValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  it('should create', () => {
    spyOn(boughtTicketService, 'findById').and.callFake((ticketId) => of(ticket));
    spyOn(boughtTicketService, 'validateBoughtTicket').and.callFake((ticketId) => of(true));
    initComponent();
    expect(component).toBeTruthy();
  });

  it('should load ticket with route param id', () => {
    spyOn(boughtTicketService, 'findById').and.callFake((ticketId) => of(ticket));
    spyOn(boughtTicketService, 'validateBoughtTicket').and.callFake((ticketId) => of(true));
    initComponent()
    expect(boughtTicketService.findById).toHaveBeenCalledWith(ticketId);
    expect(component.ticket).toBe(ticket.ticket);
  });

  it('should display "Ticket is valid" if ticket is valid', () => {
    spyOn(boughtTicketService, 'findById').and.callFake((ticketId) => of(ticket));
    spyOn(boughtTicketService, 'validateBoughtTicket').and.callFake((ticketId) => of(true));
    initComponent();
    let ticketValidityH2 = fixture.debugElement.query(By.css('.ticket-validity'));
    expect(boughtTicketService.findById).toHaveBeenCalledWith(ticketId);
    expect(boughtTicketService.validateBoughtTicket).toHaveBeenCalledWith(ticketId);
    expect(component.isValid).toBeTruthy();
    expect(ticketValidityH2.nativeElement.innerHTML).toContain("Ticket is valid");
  });

  it('should display "Ticket is invalid" if ticket is invalid', () => {
    spyOn(boughtTicketService, 'findById').and.callFake((ticketId) => of(ticket));
    spyOn(boughtTicketService, 'validateBoughtTicket').and.callFake((ticketId) => of(false));
    initComponent();
    let ticketValidityH2 = fixture.debugElement.query(By.css('.ticket-validity'));
    expect(boughtTicketService.findById).toHaveBeenCalledWith(ticketId);
    expect(boughtTicketService.validateBoughtTicket).toHaveBeenCalledWith(ticketId);
    expect(component.isValid).toBeFalsy();
    expect(ticketValidityH2.nativeElement.innerHTML).toContain("Ticket is invalid");
  });

  it('should display "Ticket validation" in h2 tag', () => {
    expectElementToContain(fixture, By.css('h2.mat-h2'), 'Ticket validation');
  });
});
