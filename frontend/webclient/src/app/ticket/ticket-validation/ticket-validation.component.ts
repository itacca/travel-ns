import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ticket } from '../ticket.model';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { BoughtTicketService } from '../service/bought-ticket.service';
import { BoughtTicket } from '../bought-ticket.model';

@Component({
  selector: 'app-ticket-validation',
  templateUrl: './ticket-validation.component.html',
  styleUrls: ['./ticket-validation.component.css']
})
export class TicketValidationComponent implements OnInit, OnDestroy {
  
  ticket: Ticket;

  boughtTicket: BoughtTicket;

  id: number;

  isValid: boolean = null;

  private routeParamSubscription: Subscription;

  constructor(
    private boughtTicketService: BoughtTicketService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.routeParamSubscription = this.route.params.subscribe(params => {
      this.isValid = null;
      this.id = params.id;
      this.loadTicket();
      this.validateTicket();
    });
  }

  ngOnDestroy() {
    this.routeParamSubscription.unsubscribe();
  }

  private loadTicket() {
    this.boughtTicketService.findById(this.id).subscribe(ticket => {
      this.ticket = ticket.ticket;
      this.boughtTicket = ticket;
    }, error => this.toastr.error("Unable to load ticket", "Error"));
  }

  private validateTicket() {
    this.boughtTicketService.validateBoughtTicket(this.id).subscribe(isValid => {
      this.isValid = isValid;
    }, error => this.toastr.error("Unable to check ticket validity. Service might be unavailable.", "Error"));
  }

}
