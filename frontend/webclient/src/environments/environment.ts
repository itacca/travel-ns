// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const backendIp = 'http://localhost';
const authRedirect = encodeURIComponent(`${backendIp}:4200/#/login?`);
const logoutRedirect = encodeURIComponent(`${backendIp}:4200/#/home?`);

export const environment = {
  production: false,
  redirectUri: authRedirect,
  authServerRoot: `${backendIp}:9190/oauth/authorize`,
  authServerLoginWithUri: `${backendIp}:9190/oauth/authorize?response_type=token&client_id=web-client&redirect_uri=`,
  authServerLogin: `${backendIp}:9190/oauth/authorize?response_type=token&client_id=web-client&redirect_uri=${authRedirect}`,
  authServerLogout: `${backendIp}:9190/logout?redirect_uri=${logoutRedirect}logout%3Dtrue`,
  apiRoot: `${backendIp}:9190/api/v1`,
  ticketValidationPath: 'validate-ticket',
  ticketValidationUrl: `${backendIp}:4200/validate-ticket`
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
